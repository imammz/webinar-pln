# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: pembelajaran_digital2
# Generation Time: 2016-01-03 14:29:52 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table wb_bank_soal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_bank_soal`;

CREATE TABLE `wb_bank_soal` (
  `id_bank_soal` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelajaran` varchar(50) DEFAULT NULL,
  `jenis_soal` varchar(20) DEFAULT NULL,
  `pertanyaan` text,
  `a` varchar(300) DEFAULT NULL,
  `b` varchar(300) DEFAULT NULL,
  `c` varchar(300) DEFAULT NULL,
  `d` varchar(300) DEFAULT NULL,
  `e` varchar(300) DEFAULT NULL,
  `jawaban_pg` varchar(2) DEFAULT NULL,
  `jawaban_essay` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bank_soal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_bank_soal` WRITE;
/*!40000 ALTER TABLE `wb_bank_soal` DISABLE KEYS */;

INSERT INTO `wb_bank_soal` (`id_bank_soal`, `id_pembelajaran`, `jenis_soal`, `pertanyaan`, `a`, `b`, `c`, `d`, `e`, `jawaban_pg`, `jawaban_essay`, `created_date`, `created_by`, `updated_date`, `updated_by`)
VALUES
	(1,'PB001','pg','Menurut deret tribolistrik, jika emas (Au) digosok dengan kain wol, maka emas akan ?     ','bermuatan positif','bermuatan negatif','netral','mungkin positif, mungkin negatif','tidak ada jawaban yang benar1','a','','2015-09-27 01:37:15',NULL,'2015-12-11 18:44:17',18),
	(3,'PB001','essay','Sebuah benda akan bermuatan positif bila?        ','','','','','','','1','2015-09-27 20:02:35',5,'2015-12-22 00:00:13',18),
	(4,'PB001','essay','Jika di dalam suatu benda terdapat keseimbangan antara jumlah proton dengan jumlah elektron, maka benda tersebut? ','','','','','','','netral,,,,,,,, ','2015-09-27 20:38:39',NULL,'2015-10-06 22:57:16',5),
	(5,'PB001','pg','Sebuah benda akan bermuatan negatif bila ?','kelebihan elektron','kekurangan elektron','kekurangan proton','jumlah proton sama dengan jumlah elektron','semua pilihan salah','a','','2015-10-06 22:56:28',5,'0000-00-00 00:00:00',NULL),
	(6,'PB001','essay','Jika dua muatan listrik sejenis didekatkan akan tolak-menolak dan bila tidak sejenis didekatkan akan tarik-menarik. Pernyataan tersebut sesuai dengan','','','','','','','Hukum ,,,,','2015-10-06 22:57:43',5,'0000-00-00 00:00:00',NULL),
	(7,'PB001','pg','Pada hukum Coulomb besar gaya tarik atau gaya tolak antara dua muatan berbanding terbalik dengan? ','besar muatan masing-masing','kuadrat muatan masing-masing','jarak antara dua muatan','kuadrat jarak antara dua muatan','kuadrat jarak antara dua muatan','a','','2015-10-06 22:59:07',5,'2015-12-21 23:24:24',18),
	(8,'PB001','pg','Satuan sistem internasional muatan listrik adala?','coulomb','ampere','farad','mikrocoulomb','watt','a','','2015-10-06 22:59:58',5,'0000-00-00 00:00:00',NULL),
	(9,'PB001','essay','Gelas dan mika akan mendapat muatan negatif bila digosok dengan bulu kelinci. Apakah muatan negatif tersebut mempunyai intensitas yang sama? Jelaskan!','','','','','','','?','2015-10-06 23:00:36',5,'0000-00-00 00:00:00',NULL),
	(10,'PB001','essay','Dua buah muatan listrik yang tidak sejenis didekatkan akan tarikmenarik dan apabila semakin didekatkan gaya tarik akan semakin besar, mengapa demikian?','','','','','','','?','2015-10-06 23:00:52',5,'0000-00-00 00:00:00',NULL),
	(11,'PB001','essay','Muatan +Q1 dan muatan -Q2 pada jarak R tarik menarik dengan gaya sebesar F. Jika jarak kedua muatan dibuat menjadi 3/4 R berapa besar gaya tarik menarik kedua muatan tersebut?','','','','','','','?','2015-10-06 23:01:05',5,'0000-00-00 00:00:00',NULL),
	(12,'PB001','pg','Berkas cahaya monokromatik 600 nm digunakan untuk menyinari suatu kisi difraksi secara tegak lurus. Jika jarak terang ke terang yang berdekatan adalah 1 cm dan jarak antar kisi adalah 45 ?m maka layar terletak dari kisi sejauh....','30 cm','40 cm','50 cm','60 cm','90 cm','a','','2015-10-21 22:09:02',1,'0000-00-00 00:00:00',NULL),
	(13,'PB001','pg','Cahaya sebuah sumber dengan panjang gelombang sebesar 590 nm melalui dua celah sempit hingga terjadi pola-pola pada layar yang berjarak 1 m. Jika jarak antara garis gelap pertama dengan garis terang pertama adalah 2,95 mm, maka lebar celah adalah....','0,1 mm','0,2 mm','0,3 mm','0,4mm','0,5 mm','a','','2015-10-21 22:09:55',1,'0000-00-00 00:00:00',NULL),
	(14,'PB001','essay','Percobaan lenturan cahaya pada kisi-kisi dapat diketahui bahwa penyimpangan cahaya biru lebih kecil dari cahaya kuning. Sedangkan penyimpangan kuning lebih kecil dari pada penyimpangan cahaya merah. Urutan cahaya dari panjang gelombang yang besar ke yang kecil adalah … ','','','','','','','aaaa','2015-10-21 22:10:48',1,'2015-12-12 14:40:34',18),
	(15,'PB001','essay','Pada percobaan Young, dua celah berjarak 1 mm diletak kan pada jarak 1 meter dari sebuah layar. Bila jarak terdekat antara pola interferensi garis terang pertama dan garis terang kesebelas adalah 4 mm, maka panjang gelombang cahaya yang menyinari adalah…   ','','','','','','','qs','2015-10-21 22:11:25',1,'2015-12-12 14:38:35',18),
	(16,'PB001','essay','Seberkas cahaya dengan panjang gelombang 5000 Å jatuh tegak lurus pada pada suatu kisi yang terdiri dari 5000 garis tiap cm. Sudut bias orde kedua yang terjadi adalah....','','','','','','','xx','2015-10-21 22:12:16',1,'0000-00-00 00:00:00',NULL),
	(17,'PB001','essay','Berkas cahaya monokromatik 600 nm digunakan untuk menyinari suatu kisi difraksi secara tegak lurus. Jika jarak terang ke terang yang berdekatan adalah 1 cm dan jarak antar kisi adalah 45 ?m maka layar terletak dari kisi sejauh....','','','','','','','yy','2015-10-21 22:12:30',1,'0000-00-00 00:00:00',NULL),
	(18,'PB001','essay',' Dua celah yang berjarak 1 mm disinari cahaya merah dengan panjang gelombang 6,5 x 10?7 m. Sebuah layar diletakkan 1 m dari celah untuk mengamati pola-pola yang terjadi. Jarak antara gelap pertama dengan terang ketiga adalah....','','','','','','','xx','2015-10-21 22:12:41',1,'0000-00-00 00:00:00',NULL),
	(19,'PB001','pg','aku adlah','1','2','3','4','5','a','','2015-12-21 23:15:20',18,'0000-00-00 00:00:00',NULL),
	(20,'PB001','essay','aku','','','','','','','kamu','2015-12-21 23:30:47',18,'0000-00-00 00:00:00',NULL),
	(21,'PB001','essay','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>satu</p>\r\n</body>\r\n</html>','','','','','','','<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>dua</p>\r\n</body>\r\n</html>','2015-12-21 23:46:18',18,'0000-00-00 00:00:00',NULL),
	(22,'PB003','pg','Arus listrik 500 mA, waktu 1 jam. Tentukan besar muatan listriknya ....','1800 Coulomb','2000 Coulomb','500 Coulumb','3000 Coulumb','1200 Coulumb','a','','2016-01-03 21:23:30',1,'0000-00-00 00:00:00',NULL),
	(23,'PB001','pg','Dua buah benda bermuatan listrik tidak sejenis, tarik-menarik dengan gaya sebesar F. Jika jarak kedua muatan dijauhkan menjadi 4 kali semula, maka gaya tarik-menarik antara kedua muatan menjadi...F','16','1/16','14','1/14','1','b','','2016-01-03 21:26:48',18,'0000-00-00 00:00:00',NULL);

/*!40000 ALTER TABLE `wb_bank_soal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_class`;

CREATE TABLE `wb_class` (
  `id_class` varchar(50) NOT NULL,
  `id_pembelajaran` varchar(50) NOT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_class`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_class` WRITE;
/*!40000 ALTER TABLE `wb_class` DISABLE KEYS */;

INSERT INTO `wb_class` (`id_class`, `id_pembelajaran`, `lokasi`, `keterangan`, `start_date`, `status`, `end_date`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('CLS151220111855','PB001','bsd','test','2015-12-23',NULL,'2015-12-31',19,'2015-12-20 17:29:53',19,'2015-12-20 17:29:53'),
	('CLS160103150934','PB002','Pd. Cabe','Telekinesis','2016-02-01',NULL,'2016-02-06',1,'2016-01-03 21:09:34',NULL,'0000-00-00 00:00:00'),
	('CLS160103152043','PB003','PD. Cabe','Membahas seputar Tegangan listrik','2016-01-03',NULL,'2016-01-10',1,'2016-01-03 21:20:43',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_document`;

CREATE TABLE `wb_document` (
  `id_document` varchar(50) NOT NULL,
  `id_pembelajaran` varchar(50) DEFAULT NULL,
  `document` varchar(150) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_document`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_document` WRITE;
/*!40000 ALTER TABLE `wb_document` DISABLE KEYS */;

INSERT INTO `wb_document` (`id_document`, `id_pembelajaran`, `document`, `file_name`, `type`, `path`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('FL151220233149','PB002','sd','tugas 1 PPP (1).xlsx','file','./front_assets/file/cafceda7883e838ea6953e9c01ffdedd.xlsx',18,'2015-12-20 23:31:49',NULL,'0000-00-00 00:00:00'),
	('FL151220235012','PB001','s','e8c1020281c447ef52691116169c4fef.pdf','materi','./front_assets/materi/4b9a3082390283d8e04520cd689c94d0.pdf',18,'2015-12-20 23:50:12',18,'2015-12-21 22:29:14'),
	('FL151221215917','PB001','qw','e8c1020281c447ef52691116169c4fef.pdf','file','./front_assets/file/024592452229867d3449e0d338f6280d.pdf',18,'2015-12-21 21:59:17',NULL,'0000-00-00 00:00:00'),
	('FL151221221035','PB002','asS','0813dad61be61c27052e8cf15bae7276.pdf','video','./front_assets/file/f9c7d1510c6c1b96279fc9e2a8b9c26e.pdf',18,'2015-12-21 22:10:35',18,'2015-12-21 22:22:08'),
	('MTR150927161413','PB002','MATERI 2','1.2 Dasar-Dasar Pemrograman.pdf','materi','./front_assets/file/277fa6e7208d819245e0b26616ccdf53.pdf',5,'2015-09-27 21:14:13',18,'2015-12-20 23:49:23'),
	('MTR151002193527','PB001','asdadasdwwww','Transkip2.jpg','materi','./front_assets/file/eb3a7fb8f2b06e35bc1167a71432b29f.jpg',1,'2015-10-03 00:35:27',NULL,'0000-00-00 00:00:00'),
	('MTR151212162109','PB001','sada','tj_no.102.doc','materi','./front_assets/file/ae8fae3569496cee7bc8e69051d6f5ea.doc',18,'2015-12-12 16:21:09',NULL,'0000-00-00 00:00:00'),
	('VD150927161826','PB001','VIDEO 1','#1 - Android Studio Tutorial   Php Mysql Login and Register.mp4','video','./front_assets/file/5ba2debe1c42b8bf416cf4b461c8bfcb.mp4',5,'2015-09-27 21:18:31',1,'2015-09-29 23:24:24'),
	('VD150927161931','PB001','VIDEO 2222','#2 - Android Studio Tutorial   Php Mysql Login and Register.mp4','video','./front_assets/file/8ab36c8c1f01a031cbb7edb4f5b7d0de.mp4',5,'2015-09-27 21:19:35',18,'2015-12-12 20:42:56');

/*!40000 ALTER TABLE `wb_document` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_document_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_document_class`;

CREATE TABLE `wb_document_class` (
  `id_document_class` int(11) NOT NULL AUTO_INCREMENT,
  `id_document` varchar(50) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_document_class`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_document_class` WRITE;
/*!40000 ALTER TABLE `wb_document_class` DISABLE KEYS */;

INSERT INTO `wb_document_class` (`id_document_class`, `id_document`, `id_class`, `id_periode`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,'MTR150927161413','CLS151220111855','PRD151220113553',19,'2015-12-20 17:39:05',NULL,'0000-00-00 00:00:00'),
	(2,'MTR151002192817','CLS151220111855','PRD151220113553',19,'2015-12-20 17:39:06',NULL,'0000-00-00 00:00:00'),
	(3,'MTR151002192817','CLS151220111855','PRD15122011181',19,'2015-12-20 17:39:11',NULL,'0000-00-00 00:00:00'),
	(4,'MTR151002193527','CLS151220111855','PRD15122011181',19,'2015-12-20 17:39:13',NULL,'0000-00-00 00:00:00'),
	(5,'FL151221221035','CLS151220111855','PRD15122011181',19,'2015-12-26 21:59:41',NULL,'0000-00-00 00:00:00'),
	(6,'VD150927161931','CLS151220111855','PRD151223003640',19,'2016-01-03 21:28:24',5,'2016-01-03 21:28:24'),
	(7,'FL151221215917','CLS151220111855','PRD15122011181',19,'2015-12-26 22:00:07',NULL,'0000-00-00 00:00:00'),
	(8,'FL151220233149','CLS151220111855','PRD151223003640',19,'2015-12-26 22:00:20',NULL,'0000-00-00 00:00:00'),
	(9,'MTR150927161413','CLS151220111855','PRD151223003640',19,'2015-12-26 22:01:54',NULL,'0000-00-00 00:00:00'),
	(10,'MTR150927161413','CLS160103150934','PRD16010315091',5,'2016-01-03 21:11:52',NULL,'0000-00-00 00:00:00'),
	(11,'MTR151212162109','CLS160103150934','PRD16010315091',5,'2016-01-03 21:11:55',NULL,'0000-00-00 00:00:00'),
	(12,'MTR150927161413','CLS160103150934','PRD16010315091',5,'2016-01-03 21:12:03',NULL,'0000-00-00 00:00:00'),
	(13,'MTR150927161413','CLS160103150934','PRD16010315091',5,'2016-01-03 21:12:03',NULL,'0000-00-00 00:00:00'),
	(14,'MTR151212162109','CLS160103150934','PRD16010315091',5,'2016-01-03 21:12:05',NULL,'0000-00-00 00:00:00'),
	(15,'FL151220235012','CLS160103150934','PRD16010315091',5,'2016-01-03 21:12:34',NULL,'0000-00-00 00:00:00'),
	(16,'FL151220235012','CLS160103150934','PRD16010315091',5,'2016-01-03 21:12:37',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_document_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_jadwal_ujian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_jadwal_ujian`;

CREATE TABLE `wb_jadwal_ujian` (
  `id_jadwal_ujian` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ujian` varchar(150) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `durasi` varchar(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_jadwal_ujian`),
  KEY `fk_sch_prd` (`id_periode`),
  CONSTRAINT `fk_sch_prd` FOREIGN KEY (`id_periode`) REFERENCES `wb_periode` (`id_periode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_jadwal_ujian` WRITE;
/*!40000 ALTER TABLE `wb_jadwal_ujian` DISABLE KEYS */;

INSERT INTO `wb_jadwal_ujian` (`id_jadwal_ujian`, `nama_ujian`, `id_periode`, `start_date`, `end_date`, `durasi`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(2,'ujian 2','PRD15122011181','2015-12-29',NULL,'1',18,'2015-12-27 09:34:16',18,'2015-12-29 05:06:51'),
	(3,'Telekinesis EXAM','PRD16010315091','2016-02-02',NULL,'90',5,'2016-01-03 21:16:23',NULL,NULL),
	(4,'TL EXAM 2','PRD16010315202','2016-01-10',NULL,'-20',5,'2016-01-03 21:21:57',18,'2016-01-03 21:29:33'),
	(5,'TL EXAM 1','PRD16010315201','2016-01-06',NULL,'60',5,'2016-01-03 21:23:30',NULL,NULL);

/*!40000 ALTER TABLE `wb_jadwal_ujian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_jawaban
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_jawaban`;

CREATE TABLE `wb_jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `id_jadwal_ujian` int(11) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `jawaban_pg` varchar(50) DEFAULT NULL,
  `jawaban_essay` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_jawaban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_jawaban` WRITE;
/*!40000 ALTER TABLE `wb_jawaban` DISABLE KEYS */;

INSERT INTO `wb_jawaban` (`id_jawaban`, `id_jadwal_ujian`, `id_bank_soal`, `jawaban_pg`, `jawaban_essay`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(10,2,3,'','xx',18,'2015-12-29 05:42:31',NULL,NULL),
	(11,2,4,'','tassdafqw',18,'2015-12-29 05:43:06',NULL,NULL),
	(12,2,1,'a','',18,'2015-12-29 06:13:41',NULL,NULL),
	(13,2,5,'a','',18,'2015-12-29 06:19:31',NULL,NULL),
	(17,2,1,'e','',2,'2015-12-29 06:44:51',NULL,NULL),
	(18,2,5,'a','',2,'2015-12-29 06:44:55',NULL,NULL),
	(19,2,3,'','yty',2,'2015-12-29 06:45:07',NULL,NULL),
	(20,2,4,'','guygyu',2,'2015-12-29 06:45:14',NULL,NULL);

/*!40000 ALTER TABLE `wb_jawaban` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_level`;

CREATE TABLE `wb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_level` WRITE;
/*!40000 ALTER TABLE `wb_level` DISABLE KEYS */;

INSERT INTO `wb_level` (`id_level`, `level_name`, `created_by`, `created_date`, `updated_by`, `updated_date`, `isactive`)
VALUES
	(1,'Admin',NULL,'2015-09-09 00:19:58',NULL,NULL,1),
	(2,'Peserta',NULL,'2015-09-09 00:20:08',1,'2015-09-26',1),
	(6,'Instruktur',1,'2015-09-15 00:14:01',NULL,NULL,1),
	(7,'Pusdiklat',1,'2015-12-11 15:08:33',NULL,NULL,1),
	(8,'Udiklat',1,'2015-12-11 15:08:52',NULL,NULL,1);

/*!40000 ALTER TABLE `wb_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_login_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_login_log`;

CREATE TABLE `wb_login_log` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `id_sesion` varchar(90) DEFAULT NULL,
  `id_pegawai` varchar(50) DEFAULT NULL,
  `ip_address` varchar(90) DEFAULT NULL,
  `login_start` datetime DEFAULT NULL,
  `login_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wb_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_menu`;

CREATE TABLE `wb_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `menu_name` varchar(90) DEFAULT NULL,
  `menu_url` varchar(100) DEFAULT NULL,
  `menu_icon` varchar(100) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_menu` WRITE;
/*!40000 ALTER TABLE `wb_menu` DISABLE KEYS */;

INSERT INTO `wb_menu` (`id_menu`, `id_parent`, `menu_name`, `menu_url`, `menu_icon`, `menu_order`, `isactive`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,0,'Home','home','xE871',1,1,'1',NULL,'1','2015-10-17 22:35:07'),
	(2,0,'Pembelajaran DIgital','class_m','xE1BD',2,1,'1',NULL,'1','2015-12-11 15:26:52'),
	(3,0,'Materi','document','xE24D',6,1,'1',NULL,'1','2015-12-20 11:41:33'),
	(4,3,'video','document/video','file-movie-o',1,1,'1',NULL,'1','2015-09-14 01:18:59'),
	(5,3,'File','document/file','file-pdf-o',2,1,'1',NULL,NULL,NULL),
	(6,3,'Materi','document/materi','file-word-o',3,1,'1',NULL,NULL,NULL),
	(7,0,'Nilai','nilai','xE53E',10,1,'1',NULL,'1','2015-10-17 22:41:50'),
	(8,0,'Report','report','xE85C',20,1,'1',NULL,'1','2015-10-17 22:39:34'),
	(9,0,'Administration','a','xE8C0',30,1,'1',NULL,'1','2015-10-17 23:36:16'),
	(10,9,'Menu','menu','bars',1,1,'1',NULL,NULL,NULL),
	(11,9,'level','level','bookmark-o',2,1,'1',NULL,NULL,NULL),
	(12,9,'User','user','users',3,1,NULL,NULL,NULL,NULL),
	(13,9,'Reset Password','user/reset','edit',4,1,NULL,NULL,NULL,NULL),
	(15,14,'Instruktur','instruktur','user-plus',1,1,NULL,NULL,NULL,NULL),
	(16,14,'Peserta','peserta','user-md',2,1,NULL,NULL,NULL,NULL),
	(17,0,'Kelas (Peserta)','class_p','xE8CB',5,1,'1','2015-09-16 20:31:32','1','2015-10-17 22:48:24'),
	(18,0,'Kode Pembelajaran','pembelajaran','xE0B9',9,1,'1','2015-09-26 15:12:24','1','2015-12-20 11:39:54'),
	(19,0,'Jadwal Ujian','exam_schd','xE8D2',7,1,'1','2015-09-28 20:00:25','1','2015-10-17 22:44:39'),
	(20,0,'Ujian','exam','xE32A',8,1,'1','2015-09-30 00:13:10','1','2015-10-17 22:46:42'),
	(21,0,'Master Peserta','participant','xE87B',4,0,'1','2015-09-30 22:15:13','1','2015-12-18 06:06:47'),
	(22,0,'Master Pegawai','employee','xE87C',3,1,'1','2015-10-01 00:37:05','1','2015-10-17 22:42:52'),
	(23,0,'Profile','profile','xE87C',40,1,'1','2015-10-17 23:36:49','1','2015-10-17 23:37:25'),
	(24,0,'Master Soal','soal','xE53E',6,1,'1','2015-12-21 22:45:43','1','2015-12-21 22:49:15'),
	(25,0,'Instruktur','class_i','xE1BD',3,1,'1','2015-12-23 19:01:28','1','2015-12-23 19:02:11');

/*!40000 ALTER TABLE `wb_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_menu_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_menu_role`;

CREATE TABLE `wb_menu_role` (
  `id_menu_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu_role`),
  KEY `FK_wb_menu_role` (`id_menu`),
  KEY `FK_wb_menu_level` (`id_level`),
  CONSTRAINT `FK_wb_menu_level` FOREIGN KEY (`id_level`) REFERENCES `wb_level` (`id_level`),
  CONSTRAINT `FK_wb_menu_role` FOREIGN KEY (`id_menu`) REFERENCES `wb_menu` (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_menu_role` WRITE;
/*!40000 ALTER TABLE `wb_menu_role` DISABLE KEYS */;

INSERT INTO `wb_menu_role` (`id_menu_role`, `id_level`, `id_menu`, `isactive`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,1,1,1,'1',NULL,NULL,NULL),
	(2,1,2,1,'1',NULL,NULL,NULL),
	(3,1,3,1,'1',NULL,NULL,NULL),
	(4,1,4,1,'1',NULL,NULL,NULL),
	(5,1,5,1,'1',NULL,NULL,NULL),
	(6,1,6,1,'1',NULL,NULL,NULL),
	(7,1,7,1,'1',NULL,NULL,NULL),
	(8,1,8,1,'1',NULL,NULL,NULL),
	(9,1,9,1,'1',NULL,NULL,NULL),
	(10,1,10,1,NULL,NULL,NULL,NULL),
	(11,1,11,1,NULL,NULL,NULL,NULL),
	(12,1,12,1,NULL,NULL,NULL,NULL),
	(13,1,13,1,NULL,NULL,NULL,NULL),
	(15,1,15,1,NULL,NULL,NULL,NULL),
	(16,1,16,1,NULL,NULL,NULL,NULL),
	(21,2,1,1,'1',NULL,NULL,NULL),
	(22,2,2,0,'1',NULL,NULL,NULL),
	(23,2,3,0,'1',NULL,NULL,NULL),
	(24,2,4,0,'1',NULL,NULL,NULL),
	(25,2,5,0,'1',NULL,NULL,NULL),
	(26,2,6,0,'1',NULL,NULL,NULL),
	(27,2,7,0,'1',NULL,NULL,NULL),
	(28,2,8,0,'1',NULL,NULL,NULL),
	(29,2,9,0,'1',NULL,NULL,NULL),
	(30,2,10,0,NULL,NULL,NULL,NULL),
	(31,2,11,0,NULL,NULL,NULL,NULL),
	(32,2,12,0,NULL,NULL,NULL,NULL),
	(33,2,13,0,NULL,NULL,NULL,NULL),
	(35,2,15,0,NULL,NULL,NULL,NULL),
	(36,2,16,0,NULL,NULL,NULL,NULL),
	(40,6,1,1,'1','2015-09-15 00:17:05',NULL,NULL),
	(41,6,2,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(42,6,3,1,'1','2015-09-15 00:17:05',NULL,NULL),
	(43,6,4,1,'1','2015-09-15 00:17:05',NULL,NULL),
	(44,6,5,1,'1','2015-09-15 00:17:05',NULL,NULL),
	(45,6,6,1,'1','2015-09-15 00:17:05',NULL,NULL),
	(46,6,7,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(47,6,8,1,'1','2015-09-15 00:17:05',NULL,NULL),
	(48,6,9,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(49,6,10,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(50,6,11,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(51,6,12,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(52,6,13,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(54,6,15,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(55,6,16,0,'1','2015-09-15 00:17:05',NULL,NULL),
	(56,1,17,1,'1','2015-09-16 20:31:32',NULL,NULL),
	(57,2,17,1,'1','2015-09-16 20:31:32',NULL,NULL),
	(58,6,17,0,'1','2015-09-16 20:31:32',NULL,NULL),
	(59,1,18,1,'1','2015-09-26 15:12:24',NULL,NULL),
	(60,2,18,0,'1','2015-09-26 15:12:24',NULL,NULL),
	(61,6,18,1,'1','2015-09-26 15:12:24',NULL,NULL),
	(62,1,19,1,'1','2015-09-28 20:00:25',NULL,NULL),
	(63,2,19,1,'1','2015-09-28 20:00:25',NULL,NULL),
	(64,6,19,1,'1','2015-09-28 20:00:25',NULL,NULL),
	(65,1,20,1,'1','2015-09-30 00:13:10',NULL,NULL),
	(66,2,20,1,'1','2015-09-30 00:13:10',NULL,NULL),
	(67,6,20,0,'1','2015-09-30 00:13:10',NULL,NULL),
	(68,1,21,1,'1','2015-09-30 22:15:13',NULL,NULL),
	(69,2,21,0,'1','2015-09-30 22:15:13',NULL,NULL),
	(70,6,21,0,'1','2015-09-30 22:15:13',NULL,NULL),
	(71,1,22,1,'1','2015-10-01 00:37:05',NULL,NULL),
	(72,2,22,0,'1','2015-10-01 00:37:05',NULL,NULL),
	(73,6,22,0,'1','2015-10-01 00:37:06',NULL,NULL),
	(74,1,23,1,'1','2015-10-17 23:36:49',NULL,NULL),
	(75,2,23,1,'1','2015-10-17 23:36:49',NULL,NULL),
	(76,6,23,0,'1','2015-10-17 23:36:49',NULL,NULL),
	(77,7,1,1,'1','2015-12-11 15:08:33',NULL,NULL),
	(78,7,2,1,'1','2015-12-11 15:08:33',NULL,NULL),
	(79,7,3,1,'1','2015-12-11 15:08:33',NULL,NULL),
	(80,7,4,1,'1','2015-12-11 15:08:33',NULL,NULL),
	(81,7,5,1,'1','2015-12-11 15:08:33',NULL,NULL),
	(82,7,6,1,'1','2015-12-11 15:08:33',NULL,NULL),
	(83,7,7,1,'1','2015-12-11 15:08:33',NULL,NULL),
	(84,7,8,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(85,7,9,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(86,7,10,0,'1','2015-12-11 15:08:34',NULL,NULL),
	(87,7,11,0,'1','2015-12-11 15:08:34',NULL,NULL),
	(88,7,12,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(89,7,13,0,'1','2015-12-11 15:08:34',NULL,NULL),
	(90,7,15,0,'1','2015-12-11 15:08:34',NULL,NULL),
	(91,7,16,0,'1','2015-12-11 15:08:34',NULL,NULL),
	(92,7,17,0,'1','2015-12-11 15:08:34',NULL,NULL),
	(93,7,18,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(94,7,19,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(95,7,20,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(96,7,21,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(97,7,22,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(98,7,23,1,'1','2015-12-11 15:08:34',NULL,NULL),
	(99,8,1,1,'1','2015-12-11 15:08:52',NULL,NULL),
	(100,8,2,1,'1','2015-12-11 15:08:52',NULL,NULL),
	(101,8,3,0,'1','2015-12-11 15:08:52',NULL,NULL),
	(102,8,4,0,'1','2015-12-11 15:08:52',NULL,NULL),
	(103,8,5,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(104,8,6,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(105,8,7,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(106,8,8,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(107,8,9,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(108,8,10,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(109,8,11,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(110,8,12,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(111,8,13,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(112,8,15,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(113,8,16,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(114,8,17,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(115,8,18,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(116,8,19,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(117,8,20,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(118,8,21,1,'1','2015-12-11 15:08:53',NULL,NULL),
	(119,8,22,0,'1','2015-12-11 15:08:53',NULL,NULL),
	(120,8,23,1,'1','2015-12-11 15:08:53',NULL,NULL),
	(121,1,24,1,'1','2015-12-21 22:45:43',NULL,NULL),
	(122,2,24,0,'1','2015-12-21 22:45:43',NULL,NULL),
	(123,6,24,0,'1','2015-12-21 22:45:43',NULL,NULL),
	(124,7,24,1,'1','2015-12-21 22:45:43',NULL,NULL),
	(125,8,24,0,'1','2015-12-21 22:45:43',NULL,NULL),
	(126,1,25,1,'1','2015-12-23 19:01:28',NULL,NULL),
	(127,2,25,0,'1','2015-12-23 19:01:28',NULL,NULL),
	(128,6,25,1,'1','2015-12-23 19:01:28',NULL,NULL),
	(129,7,25,0,'1','2015-12-23 19:01:28',NULL,NULL),
	(130,8,25,0,'1','2015-12-23 19:01:28',NULL,NULL);

/*!40000 ALTER TABLE `wb_menu_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_pegawai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_pegawai`;

CREATE TABLE `wb_pegawai` (
  `nip` varchar(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pend_terakhir` varchar(50) NOT NULL,
  `jurusan_pend` varchar(40) NOT NULL,
  `grade` varchar(15) NOT NULL,
  `level` varchar(3) NOT NULL,
  `jabatan` varchar(40) NOT NULL,
  `ket_jabatan` text NOT NULL,
  `orgunit` varchar(30) NOT NULL,
  `nama_atasanlgsg` varchar(50) NOT NULL,
  `nama_manajersdm` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL,
  `jk_pegawai` enum('L','P') NOT NULL,
  `agama` varchar(15) NOT NULL,
  `tmp_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp_kntr` text NOT NULL,
  `no_hp` text NOT NULL,
  `kd_udiklat` varchar(2) NOT NULL,
  `kd_unit` varchar(2) NOT NULL,
  `kd_sunit` varchar(2) NOT NULL,
  `kd_ssunit` varchar(2) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_pegawai` WRITE;
/*!40000 ALTER TABLE `wb_pegawai` DISABLE KEYS */;

INSERT INTO `wb_pegawai` (`nip`, `nama`, `alamat`, `pend_terakhir`, `jurusan_pend`, `grade`, `level`, `jabatan`, `ket_jabatan`, `orgunit`, `nama_atasanlgsg`, `nama_manajersdm`, `status`, `jk_pegawai`, `agama`, `tmp_lahir`, `tgl_lahir`, `email`, `no_telp_kntr`, `no_hp`, `kd_udiklat`, `kd_unit`, `kd_sunit`, `kd_ssunit`, `photo`)
VALUES
	('001','Kusnadi','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Kusnadi@yahoo.com','','0856742001','','','','','./front_assets/profile/587b74e132aeddd7937e646b85a840f1.JPG'),
	('002','Dudung','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Dudung@yahoo.com','','0856742002','','','','','./front_assets/profile/18f5305f8560dfe5a9259ac823eaaab0.JPG'),
	('003','imam','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','imam@yahoo.com','','0856742003','','','','','./front_assets/profile/avatar_03.PNG'),
	('004','Intan','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Intan@yahoo.com','','0856742004','','','','',NULL),
	('005','Nani','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Nani@yahoo.com','','0856742005','','','','','./front_assets/profile/avatar_05.PNG'),
	('006','Emmy','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Emmy@yahoo.com','','0856742006','','','','','./front_assets/profile/avatar_06.PNG'),
	('007','Joni','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Joni@yahoo.com','','0856742007','','','','','./front_assets/profile/avatar_07.PNG'),
	('008','Mamad','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Mamad@yahoo.com','','0856742008','','','','',NULL),
	('009','yusuf','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','yusuf@yahoo.com','','0856742009','','','','','./front_assets/profile/5adf1f83568256154af85a166ab0f336.jpg'),
	('010','Asep','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Asep@yahoo.com','','0856742010','','','','',NULL),
	('011','Fitri','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Fitri@yahoo.com','','0856742011','','','','',NULL),
	('012','Dadang','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Dadang@yahoo.com','','0856742012','','','','',NULL),
	('013','Hardi','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Hardi@yahoo.com','','0856742013','','','','',NULL),
	('014','Soni','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Soni@yahoo.com','','0856742014','','','','',NULL),
	('015','Jamal','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Jamal@yahoo.com','','0856742015','','','','',NULL),
	('016','Pusdiklat','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','','','','','','','','L','','','0000-00-00','pusdiklat@pln.com','','0856742015','','','','','./front_assets/profile/661c5afb9b2e4b0cdee1aa1dcc8389e9.png'),
	('017','Udiklat','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','','','','','','','','L','','','0000-00-00','udiklat@pln.com','','0856742015','','','','','./front_assets/profile/8bb83e6d3d69ecc36c6b167ed5149951.jpg');

/*!40000 ALTER TABLE `wb_pegawai` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_pegawai_bak
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_pegawai_bak`;

CREATE TABLE `wb_pegawai_bak` (
  `nip` varchar(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pend_terakhir` enum('SMA/SMK','D3','S1','S2','S3') NOT NULL,
  `jurusan_pend` varchar(40) NOT NULL,
  `grade` varchar(15) NOT NULL,
  `level` varchar(3) NOT NULL,
  `jabatan` varchar(40) NOT NULL,
  `ket_jabatan` text NOT NULL,
  `orgunit` varchar(30) NOT NULL,
  `nama_atasanlgsg` varchar(50) NOT NULL,
  `nama_manajersdm` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL,
  `jk_pegawai` enum('L','P') NOT NULL,
  `agama` varchar(15) NOT NULL,
  `tmp_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp_kntr` text NOT NULL,
  `no_hp` text NOT NULL,
  `kd_udiklat` varchar(2) NOT NULL,
  `kd_unit` varchar(2) NOT NULL,
  `kd_sunit` varchar(2) NOT NULL,
  `kd_ssunit` varchar(2) NOT NULL,
  `photo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_pegawai_bak` WRITE;
/*!40000 ALTER TABLE `wb_pegawai_bak` DISABLE KEYS */;

INSERT INTO `wb_pegawai_bak` (`nip`, `nama`, `alamat`, `pend_terakhir`, `jurusan_pend`, `grade`, `level`, `jabatan`, `ket_jabatan`, `orgunit`, `nama_atasanlgsg`, `nama_manajersdm`, `status`, `jk_pegawai`, `agama`, `tmp_lahir`, `tgl_lahir`, `email`, `no_telp_kntr`, `no_hp`, `kd_udiklat`, `kd_unit`, `kd_sunit`, `kd_ssunit`, `photo`)
VALUES
	('001','Kusnadi','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Kusnadi@yahoo.com','','0856742001','','','','','./front_assets/profile/587b74e132aeddd7937e646b85a840f1.JPG'),
	('002','Dudung','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Dudung@yahoo.com','','0856742002','','','','','./front_assets/profile/18f5305f8560dfe5a9259ac823eaaab0.JPG'),
	('003','imam','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','imam@yahoo.com','','0856742003','','','','','./front_assets/profile/avatar_03.PNG'),
	('004','Intan','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Intan@yahoo.com','','0856742004','','','','','./front_assets/profile/avatar_04.PNG'),
	('005','Nani','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Nani@yahoo.com','','0856742005','','','','','./front_assets/profile/avatar_05.PNG'),
	('006','Emmy','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Emmy@yahoo.com','','0856742006','','','','','./front_assets/profile/avatar_06.PNG'),
	('007','Joni','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Joni@yahoo.com','','0856742007','','','','','./front_assets/profile/avatar_07.PNG'),
	('008','Mamad','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Mamad@yahoo.com','','0856742008','','','','',NULL),
	('009','yusuf','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','yusuf@yahoo.com','','0856742009','','','','','./front_assets/profile/5adf1f83568256154af85a166ab0f336.jpg'),
	('010','Asep','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Asep@yahoo.com','','0856742010','','','','',NULL),
	('011','Fitri','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Fitri@yahoo.com','','0856742011','','','','',NULL),
	('012','Dadang','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Dadang@yahoo.com','','0856742012','','','','',NULL),
	('013','Hardi','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Hardi@yahoo.com','','0856742013','','','','',NULL),
	('014','Soni','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Soni@yahoo.com','','0856742014','','','','',NULL),
	('015','Jamal','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','Sta','','','','','','','L','','','0000-00-00','Jamal@yahoo.com','','0856742015','','','','',NULL),
	('016','Pusdiklat','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','','','','','','','','L','','','0000-00-00','pusdiklat@pln.com','','0856742015','','','','','./front_assets/profile/661c5afb9b2e4b0cdee1aa1dcc8389e9.png'),
	('017','Udiklat','Jl. Pulau Seribu NO. 12 A, BSD','SMA/SMK','','','','','','','','','','L','','','0000-00-00','udiklat@pln.com','','0856742015','','','','',NULL);

/*!40000 ALTER TABLE `wb_pegawai_bak` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_pegawai_x
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_pegawai_x`;

CREATE TABLE `wb_pegawai_x` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `sertifikasi` varchar(200) DEFAULT NULL,
  `jabatan` varchar(200) DEFAULT NULL,
  `skill` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_pegawai_x` WRITE;
/*!40000 ALTER TABLE `wb_pegawai_x` DISABLE KEYS */;

INSERT INTO `wb_pegawai_x` (`id_pegawai`, `nip`, `nama`, `alamat`, `telp`, `email`, `tgl_lahir`, `photo`, `sertifikasi`, `jabatan`, `skill`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,'001','Kusnadi','Jl. Pulau Seribu NO. 12 A, BSD','0856742001','Kusnadi@yahoo.com',NULL,'./front_assets/profile/587b74e132aeddd7937e646b85a840f1.JPG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(2,'002','Dudung','Jl. Pulau Seribu NO. 12 A, BSD','0856742002','Dudung@yahoo.com',NULL,'./front_assets/profile/18f5305f8560dfe5a9259ac823eaaab0.JPG',NULL,'Staff','Elektronika',NULL,'0000-00-00 00:00:00',NULL,NULL),
	(3,'003','imam','Jl. Pulau Seribu NO. 12 A, BSD','0856742003','imam@yahoo.com',NULL,'./front_assets/profile/avatar_03.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(4,'004','Intan','Jl. Pulau Seribu NO. 12 A, BSD','0856742004','Intan@yahoo.com',NULL,'./front_assets/profile/avatar_04.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(5,'005','Nani','Jl. Pulau Seribu NO. 12 A, BSD','0856742005','Nani@yahoo.com',NULL,'./front_assets/profile/avatar_05.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(6,'006','Emmy','Jl. Pulau Seribu NO. 12 A, BSD','0856742006','Emmy@yahoo.com',NULL,'./front_assets/profile/avatar_06.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(7,'007','Joni','Jl. Pulau Seribu NO. 12 A, BSD','0856742007','Joni@yahoo.com',NULL,'./front_assets/profile/avatar_07.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(8,'008','Mamad','Jl. Pulau Seribu NO. 12 A, BSD','0856742008','Mamad@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(9,'009','yusuf','Jl. Pulau Seribu NO. 12 A, BSD','0856742009','yusuf@yahoo.com',NULL,'./front_assets/profile/5adf1f83568256154af85a166ab0f336.jpg',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(10,'010','Asep','Jl. Pulau Seribu NO. 12 A, BSD','0856742010','Asep@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(11,'011','Fitri','Jl. Pulau Seribu NO. 12 A, BSD','0856742011','Fitri@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(12,'012','Dadang','Jl. Pulau Seribu NO. 12 A, BSD','0856742012','Dadang@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(13,'013','Hardi','Jl. Pulau Seribu NO. 12 A, BSD','0856742013','Hardi@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(14,'014','Soni','Jl. Pulau Seribu NO. 12 A, BSD','0856742014','Soni@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(15,'015','Jamal','Jl. Pulau Seribu NO. 12 A, BSD','0856742015','Jamal@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(16,'016','Pusdiklat','Jl. Pulau Seribu NO. 12 A, BSD','0856742015','pusdiklat@pln.com',NULL,'./front_assets/profile/661c5afb9b2e4b0cdee1aa1dcc8389e9.png',NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(17,'017','Udiklat','Jl. Pulau Seribu NO. 12 A, BSD','0856742015','udiklat@pln.com',NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `wb_pegawai_x` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_pembelajaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_pembelajaran`;

CREATE TABLE `wb_pembelajaran` (
  `id_pembelajaran` varchar(50) NOT NULL,
  `nama_pembelajaran` varchar(150) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_pembelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_pembelajaran` WRITE;
/*!40000 ALTER TABLE `wb_pembelajaran` DISABLE KEYS */;

INSERT INTO `wb_pembelajaran` (`id_pembelajaran`, `nama_pembelajaran`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('PB001','Listrik Statis',NULL,'2015-12-11 10:12:55',NULL,'0000-00-00 00:00:00'),
	('PB002','Listrik Dinamis',NULL,'2015-12-11 10:13:10',NULL,'0000-00-00 00:00:00'),
	('PB003','Tegangan Listrik',1,'2016-01-03 21:18:10',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_pembelajaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_periode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_periode`;

CREATE TABLE `wb_periode` (
  `id_periode` varchar(50) NOT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `instruktur1` int(11) DEFAULT NULL,
  `instruktur2` int(11) DEFAULT NULL,
  `id_schedule` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_periode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_periode` WRITE;
/*!40000 ALTER TABLE `wb_periode` DISABLE KEYS */;

INSERT INTO `wb_periode` (`id_periode`, `periode`, `start_date`, `end_date`, `id_class`, `instruktur1`, `instruktur2`, `id_schedule`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('PRD15122011181','Periode x','2015-12-23','2015-12-22','CLS151220111855',5,5,NULL,19,'2015-12-20 17:18:55',19,'2015-12-30 07:41:45'),
	('PRD151223003640','periode','2015-12-15','2015-12-10','CLS151220111855',5,10,NULL,19,'2015-12-23 06:36:40',NULL,'0000-00-00 00:00:00'),
	('PRD151230014302','Periode IV','2016-01-07','2016-01-09','CLS151220111855',5,9,NULL,19,'2015-12-30 07:43:02',19,'2015-12-30 07:43:29'),
	('PRD16010315091','Periode 1','2016-02-01','2016-02-02','CLS160103150934',5,9,NULL,1,'2016-01-03 21:09:34',NULL,'0000-00-00 00:00:00'),
	('PRD16010315092','Periode 2','2016-02-03','2016-02-04','CLS160103150934',9,5,NULL,1,'2016-01-03 21:09:34',NULL,'0000-00-00 00:00:00'),
	('PRD16010315093','Periode 3','2016-02-05','2016-02-06','CLS160103150934',10,5,NULL,1,'2016-01-03 21:09:34',NULL,'0000-00-00 00:00:00'),
	('PRD16010315201','Periode 1','2016-01-03','2016-01-07','CLS160103152043',5,9,NULL,1,'2016-01-03 21:20:43',NULL,'0000-00-00 00:00:00'),
	('PRD16010315202','Periode 2','2016-01-07','2016-01-10','CLS160103152043',9,5,NULL,1,'2016-01-03 21:20:43',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_periode` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_peserta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_peserta`;

CREATE TABLE `wb_peserta` (
  `id_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_peserta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_peserta` WRITE;
/*!40000 ALTER TABLE `wb_peserta` DISABLE KEYS */;

INSERT INTO `wb_peserta` (`id_peserta`, `id_user`, `id_class`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(2,2,'CLS151220111855','19','2015-12-26 21:40:18',NULL,'0000-00-00 00:00:00'),
	(3,6,'CLS151220111855','19','2015-12-26 22:00:47',NULL,'0000-00-00 00:00:00'),
	(4,7,'CLS151220111855','19','2015-12-26 22:00:49',NULL,'0000-00-00 00:00:00'),
	(5,1,'CLS160103150934','5','2016-01-03 21:13:35',NULL,'0000-00-00 00:00:00'),
	(6,14,'CLS160103150934','5','2016-01-03 21:13:36',NULL,'0000-00-00 00:00:00'),
	(7,16,'CLS160103150934','5','2016-01-03 21:13:38',NULL,'0000-00-00 00:00:00'),
	(14,2,'CLS160103150934','5','2016-01-03 21:13:49',NULL,'0000-00-00 00:00:00'),
	(34,9,'CLS160103150934','5','2016-01-03 21:14:04',NULL,'0000-00-00 00:00:00'),
	(47,1,'CLS151220111855','5','2016-01-03 21:27:38',NULL,'0000-00-00 00:00:00'),
	(48,13,'CLS151220111855','5','2016-01-03 21:27:42',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_peserta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_quiz
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_quiz`;

CREATE TABLE `wb_quiz` (
  `id_quiz` varchar(50) NOT NULL,
  `quiz` varchar(100) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_quiz`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wb_soal_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_soal_class`;

CREATE TABLE `wb_soal_class` (
  `id_soal_class` int(11) NOT NULL AUTO_INCREMENT,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_soal_class`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_soal_class` WRITE;
/*!40000 ALTER TABLE `wb_soal_class` DISABLE KEYS */;

INSERT INTO `wb_soal_class` (`id_soal_class`, `id_class`, `id_periode`, `id_bank_soal`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,'CLS151220111855','PRD151220113553',1,18,'2015-12-21 22:42:09',NULL,'0000-00-00 00:00:00'),
	(6,'CLS151220111855','PRD15122011181',1,19,'2015-12-26 14:27:11',NULL,'0000-00-00 00:00:00'),
	(7,'CLS151220111855','PRD15122011181',5,19,'2015-12-26 14:27:12',NULL,'0000-00-00 00:00:00'),
	(8,'CLS151220111855','PRD151223003640',1,19,'2015-12-26 14:27:29',NULL,'0000-00-00 00:00:00'),
	(9,'CLS151220111855','PRD151223003640',5,19,'2015-12-26 14:27:31',NULL,'0000-00-00 00:00:00'),
	(10,'CLS151220111855','PRD15122011181',3,19,'2015-12-26 14:27:48',NULL,'0000-00-00 00:00:00'),
	(11,'CLS151220111855','PRD15122011181',4,19,'2015-12-26 14:27:49',NULL,'0000-00-00 00:00:00'),
	(12,'CLS160103152043','PRD16010315201',1,1,'2016-01-03 21:24:25',NULL,'0000-00-00 00:00:00'),
	(13,'CLS160103152043','PRD16010315201',5,1,'2016-01-03 21:24:30',NULL,'0000-00-00 00:00:00'),
	(14,'CLS160103152043','PRD16010315201',22,1,'2016-01-03 21:24:59',NULL,'0000-00-00 00:00:00'),
	(15,'CLS160103152043','PRD16010315201',7,1,'2016-01-03 21:25:52',NULL,'0000-00-00 00:00:00'),
	(16,'CLS160103152043','PRD16010315201',8,1,'2016-01-03 21:25:54',NULL,'0000-00-00 00:00:00'),
	(17,'CLS160103152043','PRD16010315201',12,1,'2016-01-03 21:25:58',NULL,'0000-00-00 00:00:00'),
	(18,'CLS160103152043','PRD16010315201',13,1,'2016-01-03 21:25:59',NULL,'0000-00-00 00:00:00'),
	(19,'CLS160103152043','PRD16010315201',23,1,'2016-01-03 21:28:42',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_soal_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_test`;

CREATE TABLE `wb_test` (
  `nama` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wb_time_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_time_history`;

CREATE TABLE `wb_time_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_ujian` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wb_udiklat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_udiklat`;

CREATE TABLE `wb_udiklat` (
  `kd_udiklat` varchar(2) NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `alamat_udiklat` text NOT NULL,
  `notelp_udiklat` text NOT NULL,
  PRIMARY KEY (`kd_udiklat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_udiklat` WRITE;
/*!40000 ALTER TABLE `wb_udiklat` DISABLE KEYS */;

INSERT INTO `wb_udiklat` (`kd_udiklat`, `nama_udiklat`, `alamat_udiklat`, `notelp_udiklat`)
VALUES
	('AA','Udiklat Jakarta','Jl. Raya S.Parman, Slipi, Jakarta Barat','(021)8976542'),
	('AB','Udiklat Bogor','Jl. Raya Puncak KM.72, Bogor 16750','(0251)8254545'),
	('AC','Udiklat Pandaan','Jl.Raya Surabaya-Malang KM.50, Pandaan Pasuruan 67156','(0343)631688'),
	('AD','Udiklat Suralaya','Jl. Raya Kompl. PLTU Suralaya No. 7 Merak, 42439, Jawa Barat','(0254)571218'),
	('AE','Udiklat Semarang','Jl. Raya Kedung Mundu, Salak Utama, Semarang Timur 5001','(024)6718316'),
	('AF','Udiklat Tuntungan','jl. Lapangan Golf Tuntungan No. 35 Pancarbatu - MEDAN','(061)8360855'),
	('AG','Udiklat Makassar','Jl. Malino 377- PO BOX 1182 Makassar','(0411)865390'),
	('AH','Udiklat Padang','Jl. Padang Bukit Tinggi KM. 37 Lubuk Agung - Sumatera Barat','(0751)98768'),
	('AI','Udiklat Palembang','Jl. Bendungan No. 22 Sekip Palembang ','(0711)322342'),
	('AJ','Udiklat Banjarbaru','Jl. A.Yani KM.32 Loktabat, Banjarbaru Kalimantan Selatan','(0511)4777357'),
	('AK','Assessment Center','JL. Harsono RM No.59, Ragunan-PasarMinggu, Jakarta, 12550','(021)7811390'),
	('AL','Unit Sertifikasi','JL. Harsono RM No.59, Ragunan-PasarMinggu, Jakarta, 12550','(021)7811292'),
	('AM','Museum Listrik Energi Baru','JL. Raya Taman Mini','(021)8413451');

/*!40000 ALTER TABLE `wb_udiklat` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_ujian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_ujian`;

CREATE TABLE `wb_ujian` (
  `id_ujian` varchar(50) NOT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `id_soal` varchar(50) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sesion` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_ujian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wb_ujian_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_ujian_history`;

CREATE TABLE `wb_ujian_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_jadwal_ujian` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table wb_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_users`;

CREATE TABLE `wb_users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `passwords` varchar(120) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `islogin` tinyint(1) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_sesion` varchar(100) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_user`,`nip`),
  KEY `FK_wb_level` (`id_level`),
  CONSTRAINT `FK_wb_level` FOREIGN KEY (`id_level`) REFERENCES `wb_level` (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `wb_users` WRITE;
/*!40000 ALTER TABLE `wb_users` DISABLE KEYS */;

INSERT INTO `wb_users` (`id_user`, `nip`, `username`, `passwords`, `isactive`, `islogin`, `id_level`, `id_sesion`, `last_login`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,'001','admin','1dc485b4e573826275e518765ddb6671',1,1,1,'047f58b32effc7831c918e22d3659ac2','2016-01-03 21:05:35',NULL,'0000-00-00 00:00:00',1,'2015-10-17 21:37:45'),
	(2,'002','dudung','1dc485b4e573826275e518765ddb6671',1,0,2,'','2016-01-03 21:06:06',NULL,'0000-00-00 00:00:00',2,'2015-10-17 21:37:12'),
	(5,'003','imam','1dc485b4e573826275e518765ddb6671',1,1,6,'517c804edae44fbcb6c5c53478c60775','2016-01-03 21:06:07',1,'2015-09-14 23:01:47',NULL,'0000-00-00 00:00:00'),
	(6,'004','sopo','1dc485b4e573826275e518765ddb6671',1,0,2,'','2016-01-03 20:59:58',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00'),
	(7,'006','emmy','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-07 00:09:53',1,'2015-10-07 00:09:53',NULL,'0000-00-00 00:00:00'),
	(8,'007','joni','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-07 00:10:14',1,'2015-10-07 00:10:14',NULL,'0000-00-00 00:00:00'),
	(9,'012','dadang','1dc485b4e573826275e518765ddb6671',1,NULL,6,NULL,'2015-10-21 21:07:20',1,'2015-10-21 21:07:20',NULL,'0000-00-00 00:00:00'),
	(10,'013','hardi','1dc485b4e573826275e518765ddb6671',1,0,6,'','2015-12-23 06:18:18',1,'2015-10-21 21:07:51',NULL,'0000-00-00 00:00:00'),
	(11,'009','yusuf','1dc485b4e573826275e518765ddb6671',1,0,2,'','2015-10-21 21:10:49',1,'2015-10-21 21:08:12',NULL,'0000-00-00 00:00:00'),
	(12,'008','mamad','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-21 21:08:32',1,'2015-10-21 21:08:32',NULL,'0000-00-00 00:00:00'),
	(13,'010','asep','1dc485b4e573826275e518765ddb6671',1,1,8,'4f8a6ed3df2a1ce1ff13807512673806','2015-12-14 07:02:40',1,'2015-10-21 21:08:51',1,'2015-12-14 07:02:20'),
	(14,'011','fitri','1dc485b4e573826275e518765ddb6671',1,NULL,1,NULL,'2015-10-21 21:09:10',1,'2015-10-21 21:09:10',1,'2015-10-21 21:10:26'),
	(15,'014','soni','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-21 21:09:31',1,'2015-10-21 21:09:31',NULL,'0000-00-00 00:00:00'),
	(16,'015','jamal','1dc485b4e573826275e518765ddb6671',0,NULL,1,NULL,'2015-10-21 21:09:45',1,'2015-10-21 21:09:45',NULL,'0000-00-00 00:00:00'),
	(17,'005','nani','1dc485b4e573826275e518765ddb6671',1,1,2,'074e6b15016f0a1b906d3507f0a76129','2015-10-21 23:00:55',1,'2015-10-21 21:10:05',1,'2015-10-21 22:59:00'),
	(18,'016','pusdiklat','1dc485b4e573826275e518765ddb6671',1,1,7,'42dbe40b85ae573085401cc7a98eca27','2016-01-03 21:23:02',1,'2015-12-11 15:32:29',NULL,'0000-00-00 00:00:00'),
	(19,'017','udiklat','1dc485b4e573826275e518765ddb6671',1,0,8,'','2015-12-30 07:41:18',1,'2015-12-11 15:32:55',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_webcam
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_webcam`;

CREATE TABLE `wb_webcam` (
  `id_webcam` int(11) NOT NULL AUTO_INCREMENT,
  `type_webcam` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `status_online` int(11) DEFAULT NULL,
  `information` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_webcam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
