/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.9-MariaDB : Database - pembelajaran_digital
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pembelajaran_digital` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pembelajaran_digital`;

/*Table structure for table `wb_bank_soal` */

DROP TABLE IF EXISTS `wb_bank_soal`;

CREATE TABLE `wb_bank_soal` (
  `id_bank_soal` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelajaran` varchar(50) DEFAULT NULL,
  `jenis_soal` varchar(20) DEFAULT NULL,
  `pertanyaan` text,
  `a` varchar(300) DEFAULT NULL,
  `b` varchar(300) DEFAULT NULL,
  `c` varchar(300) DEFAULT NULL,
  `d` varchar(300) DEFAULT NULL,
  `e` varchar(300) DEFAULT NULL,
  `jawaban_pg` varchar(2) DEFAULT NULL,
  `jawaban_essay` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bank_soal`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_class` */

DROP TABLE IF EXISTS `wb_class`;

CREATE TABLE `wb_class` (
  `id_class` varchar(50) NOT NULL,
  `id_pembelajaran` varchar(50) NOT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_class`,`created_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_document` */

DROP TABLE IF EXISTS `wb_document`;

CREATE TABLE `wb_document` (
  `id_document` varchar(50) NOT NULL,
  `id_pembelajaran` varchar(50) DEFAULT NULL,
  `document` varchar(150) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_document`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_document_class` */

DROP TABLE IF EXISTS `wb_document_class`;

CREATE TABLE `wb_document_class` (
  `id_document_class` int(11) NOT NULL AUTO_INCREMENT,
  `id_document` varchar(50) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_document_class`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_jadwal_ujian` */

DROP TABLE IF EXISTS `wb_jadwal_ujian`;

CREATE TABLE `wb_jadwal_ujian` (
  `id_jadwal_ujian` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ujian` varchar(150) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `durasi` varchar(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_jadwal_ujian`),
  KEY `fk_sch_prd` (`id_periode`),
  CONSTRAINT `fk_sch_prd` FOREIGN KEY (`id_periode`) REFERENCES `wb_periode` (`id_periode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_jawaban` */

DROP TABLE IF EXISTS `wb_jawaban`;

CREATE TABLE `wb_jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `id_jadwal_ujian` int(11) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `jawaban_pg` varchar(50) DEFAULT NULL,
  `jawaban_essay` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_jawaban`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_level` */

DROP TABLE IF EXISTS `wb_level`;

CREATE TABLE `wb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_login_log` */

DROP TABLE IF EXISTS `wb_login_log`;

CREATE TABLE `wb_login_log` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `id_sesion` varchar(90) DEFAULT NULL,
  `id_pegawai` varchar(50) DEFAULT NULL,
  `ip_address` varchar(90) DEFAULT NULL,
  `login_start` datetime DEFAULT NULL,
  `login_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_menu` */

DROP TABLE IF EXISTS `wb_menu`;

CREATE TABLE `wb_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `menu_name` varchar(90) DEFAULT NULL,
  `menu_url` varchar(100) DEFAULT NULL,
  `menu_icon` varchar(100) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_menu_role` */

DROP TABLE IF EXISTS `wb_menu_role`;

CREATE TABLE `wb_menu_role` (
  `id_menu_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu_role`),
  KEY `FK_wb_menu_role` (`id_menu`),
  KEY `FK_wb_menu_level` (`id_level`),
  CONSTRAINT `FK_wb_menu_level` FOREIGN KEY (`id_level`) REFERENCES `wb_level` (`id_level`),
  CONSTRAINT `FK_wb_menu_role` FOREIGN KEY (`id_menu`) REFERENCES `wb_menu` (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_pegawai` */

DROP TABLE IF EXISTS `wb_pegawai`;

CREATE TABLE `wb_pegawai` (
  `nip` varchar(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pend_terakhir` varchar(50) NOT NULL,
  `jurusan_pend` varchar(40) NOT NULL,
  `grade` varchar(15) NOT NULL,
  `level` varchar(3) NOT NULL,
  `jabatan` varchar(40) NOT NULL,
  `ket_jabatan` text NOT NULL,
  `orgunit` varchar(30) NOT NULL,
  `nama_atasanlgsg` varchar(50) NOT NULL,
  `nama_manajersdm` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL,
  `jk_pegawai` enum('L','P') NOT NULL,
  `agama` varchar(15) NOT NULL,
  `tmp_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp_kntr` text NOT NULL,
  `no_hp` text NOT NULL,
  `kd_udiklat` varchar(2) NOT NULL,
  `kd_unit` varchar(2) NOT NULL,
  `kd_sunit` varchar(2) NOT NULL,
  `kd_ssunit` varchar(2) NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_pegawai_bak` */

DROP TABLE IF EXISTS `wb_pegawai_bak`;

CREATE TABLE `wb_pegawai_bak` (
  `nip` varchar(12) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pend_terakhir` enum('SMA/SMK','D3','S1','S2','S3') NOT NULL,
  `jurusan_pend` varchar(40) NOT NULL,
  `grade` varchar(15) NOT NULL,
  `level` varchar(3) NOT NULL,
  `jabatan` varchar(40) NOT NULL,
  `ket_jabatan` text NOT NULL,
  `orgunit` varchar(30) NOT NULL,
  `nama_atasanlgsg` varchar(50) NOT NULL,
  `nama_manajersdm` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL,
  `jk_pegawai` enum('L','P') NOT NULL,
  `agama` varchar(15) NOT NULL,
  `tmp_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp_kntr` text NOT NULL,
  `no_hp` text NOT NULL,
  `kd_udiklat` varchar(2) NOT NULL,
  `kd_unit` varchar(2) NOT NULL,
  `kd_sunit` varchar(2) NOT NULL,
  `kd_ssunit` varchar(2) NOT NULL,
  `photo` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_pegawai_x` */

DROP TABLE IF EXISTS `wb_pegawai_x`;

CREATE TABLE `wb_pegawai_x` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `sertifikasi` varchar(200) DEFAULT NULL,
  `jabatan` varchar(200) DEFAULT NULL,
  `skill` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_pembelajaran` */

DROP TABLE IF EXISTS `wb_pembelajaran`;

CREATE TABLE `wb_pembelajaran` (
  `id_pembelajaran` varchar(50) NOT NULL,
  `nama_pembelajaran` varchar(150) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_pembelajaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_periode` */

DROP TABLE IF EXISTS `wb_periode`;

CREATE TABLE `wb_periode` (
  `id_periode` varchar(50) NOT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `instruktur1` int(11) DEFAULT NULL,
  `instruktur2` int(11) DEFAULT NULL,
  `id_schedule` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_periode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_peserta` */

DROP TABLE IF EXISTS `wb_peserta`;

CREATE TABLE `wb_peserta` (
  `id_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_peserta`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_quiz` */

DROP TABLE IF EXISTS `wb_quiz`;

CREATE TABLE `wb_quiz` (
  `id_quiz` varchar(50) NOT NULL,
  `quiz` varchar(100) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_quiz`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_soal_class` */

DROP TABLE IF EXISTS `wb_soal_class`;

CREATE TABLE `wb_soal_class` (
  `id_soal_class` int(11) NOT NULL AUTO_INCREMENT,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_soal_class`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_test` */

DROP TABLE IF EXISTS `wb_test`;

CREATE TABLE `wb_test` (
  `nama` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_time_history` */

DROP TABLE IF EXISTS `wb_time_history`;

CREATE TABLE `wb_time_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_ujian` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_udiklat` */

DROP TABLE IF EXISTS `wb_udiklat`;

CREATE TABLE `wb_udiklat` (
  `kd_udiklat` varchar(2) NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `alamat_udiklat` text NOT NULL,
  `notelp_udiklat` text NOT NULL,
  PRIMARY KEY (`kd_udiklat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_ujian` */

DROP TABLE IF EXISTS `wb_ujian`;

CREATE TABLE `wb_ujian` (
  `id_ujian` varchar(50) NOT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `id_soal` varchar(50) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sesion` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_ujian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_ujian_history` */

DROP TABLE IF EXISTS `wb_ujian_history`;

CREATE TABLE `wb_ujian_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_ujian` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `wb_users` */

DROP TABLE IF EXISTS `wb_users`;

CREATE TABLE `wb_users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `passwords` varchar(120) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `islogin` tinyint(1) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_sesion` varchar(100) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_user`,`nip`),
  KEY `FK_wb_level` (`id_level`),
  CONSTRAINT `FK_wb_level` FOREIGN KEY (`id_level`) REFERENCES `wb_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Table structure for table `wb_webcam` */

DROP TABLE IF EXISTS `wb_webcam`;

CREATE TABLE `wb_webcam` (
  `id_webcam` int(11) NOT NULL AUTO_INCREMENT,
  `type_webcam` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `status_online` int(11) DEFAULT NULL,
  `information` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_webcam`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
