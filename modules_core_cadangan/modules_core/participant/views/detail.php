    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2" width="70px">NIP</th>
                                <th colspan="4">Information</th>
                            </tr>
                            <tr>
                                <th>Nama</th>
								<th>Email</th>
                                <th width="200px">Alamat</th>
								<th>Telp</th>
								<th width="100px">Action</th>
							</tr>
                        </thead>

                        <tbody>
					<?php foreach (@$participant->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->nip?></td>
							<td><?=$row->nama?></td>
							<td><?=$row->email?></td>
							<td><?=$row->alamat?></td>
							<td><?=$row->telp?></td>
                            <td>
								<a href="#edit_participant" data-uk-modal="{ center:true }" onclick="edits('<?=$row->id_peserta?>','<?=$row->id_class?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
								<a href="#" onclick="deleted('<?=$row->id_peserta?>','<?=$row->id_class?>')"class="uk-icon-button uk-icon-trash-o"></a>
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_participant" onclick="add('<?=$id_class?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_participant">
			<div id="modal-add"></div>
		</div>
		<div class="uk-modal" id="edit_participant">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id_peserta,id_class){
				uri = '<?=base_url()."participant/edit"?>';
				$('#new_participant').hide();
				$('#edit_participant').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id_peserta:id_peserta,id_class:id_class},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(id_class){
				uri = '<?=base_url()."participant/add"?>';
				$('#edit_participant').hide();
				$('#new_participant').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true,id_class:id_class},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id_peserta,id_class){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."participant/delete"?>';
					$.post(uri,{ajax:true,id_peserta:id_peserta},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								$('#list_data').html(loading);
								$.post("<?=$link?>",{id_class:id_class},function(data){
									$('#list_data').html(data);
								});
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
	</script>	
</html>