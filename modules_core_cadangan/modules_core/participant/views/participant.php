
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2" width="270px">Kelas</th>
                                <th colspan="4">Information</th>
                            </tr>
                            <tr>
                                <th>Start Date</th>
                                <th>End Date</th>
								<th>Status</th>
								<th width="100px">Peserta</th>
							</tr>
                        </thead>

                        <tbody>
					<?php foreach ($participant->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->class_name?></td>
							<td><?=$row->start_date?></td>
							<td><?=$row->end_date?></td>
							<td>
							<?php if($row->status=='finish'){?>
								<span class="uk-badge uk-badge-success">
							<?php }else{?>
								<span class="uk-badge uk-badge-primary">
							<?php }
								echo $row->status?></span></td>
                            <td>
								<a href="#" onclick="detail('<?=$row->id_class?>')" class="uk-icon-button uk-icon-cogs" title="detail">
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function detail(id_class){
				$('#list_data').html(loading);
				$.post('<?=base_url()."participant/detail"?>',{id_class:id_class},function(data) 
				{
					$('#list_data').html(data);
				});
			}
	</script>	
</html>