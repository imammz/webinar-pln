<?php

class menu_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	public function menu_parent()
	{
		$sql="	select * from wb_menu
				where id_parent=0
				order by menu_order";
		return $this->db->query($sql);
	}
	
	public function menu_child($id_menu)
	{
		$sql="	SELECT * 
				FROM wb_menu
				WHERE id_parent	='$id_menu'
				and id_parent<>0
				ORDER BY menu_order";
		return $this->db->query($sql);
	}
	
	function get_list_menu(){
		$sql = "SELECT * FROM wb_menu";
		return $this->db->query($sql);
	}
	
	function get_list_menu_level($id_level){
		$sql = "select b.id_menu,b.id_parent,a.id_level,b.menu_name,
					   b.menu_url,b.menu_icon,b.isactive,b.menu_order
				from wb_level a, wb_menu b, wb_menu_role c
				where a.id_level=c.id_level
				and b.id_menu=c.id_menu
				and b.isactive=1
				and a.isactive=1
				and a.id_level='$id_level'";
		return $this->db->query($sql);
	}
	
	function get_list_menu_parent_role($id_level){
		$sql = "select b.id_menu,b.id_parent,a.id_level,b.menu_name,
					   b.menu_url,b.menu_icon,c.isactive,b.menu_order
				from wb_level a, wb_menu b, wb_menu_role c
				where a.id_level=c.id_level
				and b.id_menu=c.id_menu
				and b.id_parent=0
				and a.id_level='$id_level'";
		return $this->db->query($sql);
	}
	
	
	function get_list_menu_parent($id_level){
		$sql = "select b.id_menu,b.id_parent,a.id_level,b.menu_name,
					   b.menu_url,b.menu_icon,b.isactive,b.menu_order
				from wb_level a, wb_menu b, wb_menu_role c
				where a.id_level=c.id_level
				and b.id_menu=c.id_menu
				and b.id_parent=0
				and a.id_level='$id_level'
				and b.isactive=1
				and c.isactive=1
				and a.isactive=1
				order by menu_order";
		return $this->db->query($sql);
	}
	
	function get_list_menu_child($id_level,$id_parent){
		$sql = "select b.id_menu,b.id_parent,a.id_level,b.menu_name,
					   b.menu_url,b.menu_icon,c.isactive,b.menu_order
				from wb_level a, wb_menu b, wb_menu_role c
				where a.id_level=c.id_level
				and b.id_menu=c.id_menu
				and c.isactive=1
				and b.id_parent='$id_parent'
				and a.id_level='$id_level'
				order by menu_order";
		return $this->db->query($sql);
	}
	
	function get_list_menu_child_role($id_level,$id_parent){
		$sql = "select b.id_menu,b.id_parent,a.id_level,b.menu_name,
					   b.menu_url,b.menu_icon,c.isactive,b.menu_order
				from wb_level a, wb_menu b, wb_menu_role c
				where a.id_level=c.id_level
				and b.id_menu=c.id_menu
				and b.id_parent='$id_parent'
				and c.id_level='$id_level'
				order by menu_order";
		return $this->db->query($sql);
	}
	
	
	function save_menu($id_parent,$menu_name,$menu_url,$menu_icon,$menu_order,$isactive,$created_by){
		$sql="insert into wb_menu(id_parent,menu_name,menu_url,menu_icon,menu_order,isactive,created_by,created_date)
			  values('$id_parent','$menu_name','$menu_url','$menu_icon','$menu_order','$isactive','$created_by',now())";
		 $this->db->query($sql);	
		/*
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Menu berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Menu gagal disimpan'));		
		}*/	
		
	}
	
	function get_menu_by_id($id){
		$sql = "SELECT * FROM wb_menu
				WHERE id_menu='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function inactive_menu_by_id($id){
		$sql = "update wb_menu set isactive=0
				WHERE id_menu='$id'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Menu berhasil noaktifkan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Menu gagal di nonaktifkan'));		
		}			
	}
	
	function update_menu($id_menu,$id_parent,$menu_name,$menu_url,$menu_icon,$menu_order,$isactive,$updated_by){
		$sql="update wb_menu set id_parent='$id_parent',menu_name='$menu_name',
			  menu_url='$menu_url',menu_icon='$menu_icon',menu_order='$menu_order',
			  isactive='$isactive',updated_by='$updated_by',updated_date=now()
			  WHERE id_menu='$id_menu'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Menu berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Menu gagal di simpan'));		
		}		  
	
	}
	
	function save_menu_role($id_level,$id_menu,$created_by){
		if ($id_level==1){
			$isactive=1;
		}else{
			$isactive=0;
		}
		$sql="insert into wb_menu_role(id_level,id_menu,isactive,created_by,created_date)
			  values('$id_level','$id_menu','$isactive','$created_by',now())";
		 $this->db->query($sql);	
		/*
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Role menu berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Role menu gagal disimpan'));		
		}*/	
	}
	
	function get_menu_id($menu_name,$menu_url,$menu_icon,$menu_order,$created_by){
		$sql = "SELECT id_menu FROM wb_menu
				WHERE menu_name='$menu_name'
				and menu_url='$menu_url'
				and menu_icon='$menu_icon'
				and menu_order='$menu_order'
				and created_by='$created_by'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	function delete_menu($id_menu){
		$sql="DELETE FROM wb_menu 
			  WHERE id_menu='$id_menu'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Menu berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Menu gagal didelete'));		
		}		  
	}
	
	function delete_menu_role($id_menu){
		$sql="DELETE FROM wb_menu_role 
			  WHERE id_menu='$id_menu'";
		$this->db->query($sql);	
		/*
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Menu berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Menu gagal didelete'));		
		}*/		  
	}
	
	function inactive_role_menu($id_level){
		$sql = "UPDATE wb_menu_role
				SET isactive=0
				WHERE id_level='$id_level'";
		return  $this->db->query($sql);
	
	}
	
	function update_role_menu($id_menu,$id_level,$isactive){
		$sql = "UPDATE wb_menu_role
				SET isactive='$isactive'
				WHERE id_level='$id_level'
				and id_menu='$id_menu'";
		return  $this->db->query($sql);
	
	}
	
	function get_list_level(){
		$sql = "select * from wb_level";
		return  $this->db->query($sql);
	}
	
	
}
