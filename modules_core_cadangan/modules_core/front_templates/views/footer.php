<!---FOOTER START-->
		<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
		
		<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables colVis-->
		<script src="<?=base_url()?>front_assets/bower_components/datatables-colvis/js/dataTables.colVis.js"></script>
		<!-- datatables tableTools-->
		<script src="<?=base_url()?>front_assets/bower_components/datatables-tabletools/js/dataTables.tableTools.js"></script>
		<!-- datatables custom integration -->
		<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>

		<!--  datatables functions -->
		<script src="<?=base_url()?>front_assets/assets/js/pages/plugins_datatables.min.js"></script>
   <script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    // load extra validators
    altair_forms.parsley_extra_validators();
    </script>
		<script src="<?=base_url()?>front_assets/bower_components/parsleyjs/dist/parsley.min.js"></script>
    <!-- jquery steps -->
    <script src="<?=base_url()?>front_assets/assets/js/custom/wizard_steps.min.js"></script>

    <!--  forms wizard functions -->
    <script src="<?=base_url()?>front_assets/assets/js/pages/forms_wizard.min.js"></script>
    
		
		<script src="<?=base_url()?>front_assets/assets/js/uikit_custom.min.js"></script>
		<script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/d3/d3.min.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/chartist/dist/chartist.min.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/peity/jquery.peity.min.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/countUp.js/countUp.min.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/handlebars/handlebars.min.js"></script>
        <script src="<?=base_url()?>front_assets/assets/js/custom/handlebars_helpers.min.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/clndr/src/clndr.js"></script>
        <script src="<?=base_url()?>front_assets/bower_components/fitvids/jquery.fitvids.js"></script>
        <script src="<?=base_url()?>front_assets/assets/js/pages/dashboard.min.js"></script>  
		
		
    
</body>
<!---FOOTER END-->