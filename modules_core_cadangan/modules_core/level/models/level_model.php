<?php

class level_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	
	function get_list_level(){
		$sql = "SELECT *
				FROM wb_level";
		return $this->db->query($sql);
	}
	
	
	function save_level($level_name,$isactive,$id_user){
		$sql="insert into wb_level(level_name,isactive,created_by,created_date)
			  values('$level_name','$isactive','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Level berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Level gagal disimpan'));		
		}
		
	}
	
	function get_level_by_id($id){
		$sql = "SELECT * FROM wb_level
				WHERE id_level='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	
	function update_level($id,$isactive,$level_name,$id_user){
		$sql="update wb_level set level_name='$level_name',
			  isactive='$isactive',updated_by='$id_user',updated_date=now()
			  WHERE id_level='$id'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Level berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Level gagal di simpan'));		
		}		  
	
	}
	
	
	function delete_level($id_level){
		$sql="DELETE FROM wb_level 
			  WHERE id_level='$id_level'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal didelete'));		
		}		  
	}
	
	function inactive_level($id_level){
		$sql="update wb_level set isactive='0'
			  WHERE id_level='$id_level'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil di nonaktifkan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal di nonaktifkan'));		
		}		  
	}
	
	function get_level_id($level_name,$isactive,$id_user){
		$sql = "SELECT distinct id_level FROM wb_level
				WHERE level_name='$level_name'
				and isactive='$isactive'
				and created_by='$id_user'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
}
