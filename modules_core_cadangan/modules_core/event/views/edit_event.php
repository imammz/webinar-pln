<script>
	var Script = function () {
	$.validator.setDefaults({ ignore: ":hidden:not(select)" });
    $.validator.setDefaults({
        submitHandler: function() {	
				//$("#message").html("<img src='loading.gif'/>");
				var postData = $('#formadd').serializeArray();
				var formURL = $('#formadd').attr("action");
				var btn = $('#btnsave');
				var css =  btn.attr("class");
				var text = btn.html();
				var loading ='<div align="center" class="loading"><img src="<?=base_url()?>front_assets/img/ajax-loader.gif"></div>';
				btn.html("Please wait..");
				btn.attr("class","btn btn-primary disabled");
				$.ajax({
					url : formURL,
					type: "POST",
					data : postData,
					dataType: "json",
					success:function(data) 
					{
						//alert(data.response);
						if(data.response == 'success'){
							var msg='<div class="alert alert-success alert-dismissable">Data event saved to database </div>';
							$("#message").html(msg);
							$('#message').alert();
								btn.html("Success...");
								$('#message').fadeTo(3000, 500).slideUp(500, function(){
								$('#message').hide();
								$('#myModal').modal('hide');
								$.post("<?=$link?>",function(data) 
									{
										$('#list_data').html(data);
										$('#data_table').dataTable({
											oLanguage: {
												sLoadingRecords: '<img src="<?=base_url()?>front_assets/img/ajax-loader.gif">'
											}					
										});
									});
							});
							
						}else if(data.response == 'failed'){
							var msg='<div class="alert alert-success alert-dismissable">Error when to try save data </div>';
							$("#message").html(msg);
							$('#message').alert();
								$('#message').fadeTo(2000, 500).slideUp(500, function(){
								$('#message').hide();
							});
						}
						
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
						var msg='<div class="alert alert-error alert-dismissable">'+errorThrown+'</div>';
						$("#message").html(msg);
						$('#message').alert();
							$('#message').fadeTo(2000, 500).slideUp(500, function(){
							$('#message').hide();
							
						});
						reloadlist();
					}
				});
		}
    });

    $().ready(function() {
        $("#formadd").validate({
            rules: {
                title: "required",
				start: "required",
				end: "required"
            },
            messages: {
                title: "Please enter title",
				start: "Please enter start date",
                end: "Please endte end date"
            },
			highlight: function(element) {
				$(element).closest('.control-group').addClass('error');
			},
			unhighlight: function(element) {
				$(element).closest('.control-group').removeClass('error');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			}
        });
    });
}();

	function reloadlist(){
	$('#list_data').html(loading);
	$.post("<?=$link?>",function(data) 
	{
		$('#list_data').html(data);
		$('#data_table').dataTable({
			oLanguage: {
				sLoadingRecords: '<img src="<?=base_url()?>front_assets/img/ajax-loader.gif">'
			}					
		});
	});
	}
	$('#start').datepicker({
				format: 'yyyy-mm-dd',
				timeFormat: 'hh:mm tt',
				autoclose: true
	});
	 
 $(function() {
    $('#start_date').datetimepicker({
      language: 'pt-BR'
    });
	
	$('#end_date').datetimepicker({
      language: 'pt-BR'
    });
  });
</script>

<div class="widget green">
	<div class="widget-title">
		<h4><i class="icon-reorder"></i>Edit siswa</h4>
	</div>
<div class="widget-body form">
	<!-- BEGIN FORM-->
	
	<!-- BEGIN FORM-->
	<form method="POST" action="event/update" class="form-horizontal" id="formadd">
		<div id="message"></div>
		<div class="control-group">
			<label class="control-label">Title</label>
			<div class="controls">
				<input type="text" class="form-control" id="title" name="title" value="<?=$title2;?>"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">Start Date</label>
			<div class="controls">
				<div id="start_date" class="input-append date" name="start">
					<input data-format="yyyy-MM-dd hh:mm:ss" type="text" id="start" name="start" value="<?=$start;?>"></input>
					<span class="add-on">
					  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
					  </i>
					</span>
				 </div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">End Date</label>
			<div class="controls">
				<div id="end_date" class="input-append date" name="start" >
					<input data-format="yyyy-MM-dd hh:mm:ss" type="text" id="end" name="end" value="<?=$end;?>"></input>
					<span class="add-on">
					  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
					  </i>
					</span>
				 </div>
			</div>
		</div>
		<input type="hidden" name="event_id" value="<?=$event_id?>"/>
		<div class="modal-footer">
				<button aria-hidden="true" data-dismiss="modal" class="btn">Close</button>
				<button class="btn btn-primary" id="btnsave">Save</button>
			</div>
	</form>
	<!-- END FORM-->
</div>
