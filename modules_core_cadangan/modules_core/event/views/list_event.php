<div class="widget red">
	<div class="widget-title">
		<h4><i class="icon-reorder"></i> <?=$title;?></h4>
		<span class="tools">
		<!--
			<span class="tools">
				<a class="icon-chevron-down" href="javascript:;"></a>
				<a class="icon-remove" href="javascript:;"></a>
				<a data-toggle="modal" class="icon-remove" role="button" href="#myModal1">Dialog</a>
			</span>
		-->	
	</div>
	<div class="widget-body">
		<div role="grid" class="dataTables_wrapper form-inline" id="data_table_wrapper">
			<div class="row-fluid">
			
				<table id="data_table" class="table table-striped table-bordered dataTable" aria-describedby="data_table_info">
					<thead>
					<tr role="row">
						<th>No</th>
						<th class="sorting" role="columnheader" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1"  aria-label="Name: activate to sort column ascending">Title</th>
						<th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1"  aria-label="eventnamve: activate to sort column ascending">Start Date</th>
						<th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1"  aria-label="Level: activate to sort column ascending">End date</th>
						
						<th class="hidden-phone sorting" role="columnheader" tabindex="0" aria-controls="data_table" rowspan="1" colspan="1"  aria-label="Action: activate to sort column ascending">Action</th>
					</thead>
						
					<tbody role="alert" aria-live="polite" aria-relevant="all">
					<?php 
					$no=0;
					foreach ($list_event->result() as $event){
					$no=$no+1;
					?>
					
						<tr class="gradeX odd">
							<td class=" "><?=$no;?></td>
							<td class=" "><?=$event->title;?></td>
							<td class="hidden-phone "><?=$event->start_date;?></td>
							<td class="hidden-phone "><?=$event->end_date;?></td>
							<td class="center hidden-phone ">
								<a data-toggle="modal" class="btn btn-warning" uri="<?=base_url()?>event/edit" role="button" id="btnadd" onclick="edit(<?=$event->event_id;?>)" >Edit</i></a> 
								<a data-toggle="modal" class="btn btn-danger" uri="<?=base_url()?>event/delete" role="button" id="btndelete" onclick="deleted(<?=$event->event_id;?>)">Delete</i></a>
							</td>
						</tr>
						
					<?php }	?>			
					</tbody>
				</table>
			</div>
		</div>	
	</div>
</div>
<!---MODAL-->
<div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" class="modal hide fade" id="myModal" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
		<h3 id="myModalLabel1">Event</h3>
	</div>
	<div class="modal-body" id="modal-body">
	</div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" class="modal hide fade" id="confirm" style="display: none;">
	<div class="modal-header">
		<button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
		<h3 id="myModalLabel1">Delete Confirm</h3>
	</div>
	<div class="modal-body">
		Are You Sure?
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="button" onClick="deleteId()" class="btn btn-danger">Delete</button>
	</div>
</div>

<script>
	var loading ='<div align="center" class="loading"><img src="<?=base_url()?>front_assets/img/ajax-loader.gif"></div>';
	var delete_id='';
	function edit(id){
	//event.preventDefault();
		mdl = $('#myModal');
		mdl.modal('show');
		uri = '<?=base_url()."event/edit"?>';
		//alert(uri);
		mdl.find("div[class=modal-body]").html(loading);
		$.post(uri,{ajax:true,id:id},function(data) {
			mdl.find("div[class=modal-body]").html(data);
		});
	}
	function deleted(id){
	//event.preventDefault();
		delete_id=id;
		mdl = $('#confirm');
		mdl.modal('show');
		uri = '<?=base_url()."event/delete"?>';

	}
	
	function deleteId(){
		//alert(delete_id);
		uri = '<?=base_url()."event/delete"?>';
		$.post(uri,{ajax:true,id:delete_id},function(data) {
			//mdl.find("div[class=modal-body]").html(data);
			//alert(data.response);
			mdl = $('#confirm');
			mdl.modal('hide');
			reloadData();
		});
		
	}
	function reloadData(){
		$('#list_data').html(loading);
		$.post("<?=$link?>",function(data) 
		{
			$('#list_data').html(data);
			$('#data_table').dataTable({
				oLanguage: {
					sLoadingRecords: '<img src="<?=base_url()?>front_assets/img/ajax-loader.gif">'
				}					
			});
		});
	}
</script>