<?php echo Modules::run('front_templates/front_templates/header'); ?>
	  
	    
      <!-- BEGIN PAGE -->
      <div id="main-content">
         <!-- BEGIN PAGE CONTAINER-->
         <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
               <div class="span12">

                  <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                   <h3 class="page-title">
                   </h3>
                   <ul class="breadcrumb">
                       <li>
                           <a href="#">Home</a>
                           <span class="divider">/</span>
                       </li>
                       <li class="active">
                           Academic Calendar
                       </li>
                   </ul>
                   <!-- END PAGE TITLE & BREADCRUMB-->
               </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                <!-- BEGIN widget-->
				<div class="span12" >
                <a data-toggle="modal" class="btn btn-primary" uri="<?=base_url()?>event/add" role="button" id="btnadd">Add Event <i class="icon-plus"></i></a> 
				</div>
				<div id="list_data"></div>

                <!-- END widget-->
                </div>
            </div>
            <!-- END PAGE CONTENT-->
         </div>
         <!-- END PAGE CONTAINER-->
      </div>
      <!-- END PAGE -->
	  <!-- MODAL-->
		<div aria-hidden="true" aria-labelledby="myModalLabel1" role="dialog" tabindex="-1" class="modal hide fade" id="myModal" style="display: none;">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">x</button>
				<h3 id="myModalLabel1">Event</h3>
			</div>
			<div class="modal-body" id="modal-body">
			</div>
		</div>
	  <!---END MODAL->
   </div>
   <!-- END CONTAINER -->

<?php echo Modules::run('front_templates/front_templates/footer'); ?>
	   <script>
		   var loading ='<div align="center" class="loading"><img src="<?=base_url()?>front_assets/img/ajax-loader.gif"></div>';
			
			$('#btnadd').click(function(event) {
				event.preventDefault();
				mdl = $('#myModal');
				mdl.modal('show');
				uri = $(this).attr('uri');
				//alert(uri);
				mdl.find("div[class=modal-body]").html(loading);
				$.post(uri,{ajax:true},function(data) {
					mdl.find("div[class=modal-body]").html(data);
				});
			});
			
			$('#btnsave').click(function(event) {
				event.preventDefault();
				alert('sdfs');
			});

			
		   $( document ).ready(function() {
				reloadlist();
			});

			function reloadlist(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",function(data) 
				{
					$('#list_data').html(data);
					$('#data_table').dataTable({
						oLanguage: {
							sLoadingRecords: '<img src="<?=base_url()?>front_assets/img/ajax-loader.gif">'
						}					
					});
				});
			}	
	   </script> 	  
   </body>
<!-- END BODY -->
</html>
	  