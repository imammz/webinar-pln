<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/jquery.form.js"></script>

<script type="application/javascript">
   jQuery(document).ready(function() {
	setTimeout(function() { 
		$("#form-edit-mtr").show(); 
		$("#before-edit-materi").hide();
	}, 100);
    $("#form-edit-mtr").validate({ // initialize the plugin
        // any other options,
        onkeyup: false,
        rules: {
            id_document:{
					required:true
				  },
			id_periode:{
					required:true
				  }	  
        },
		highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					}
    });

    $("#form-edit-mtr").ajaxForm({
        beforeSubmit: function () {
			$("#btnsave").prop('value', 'Process...'); 
			$('input[type="submit"]').prop('disabled', true);
            return $("#form-edit-mtr").valid(); // TRUE when form is valid, FALSE will cancel submit
        },
        success: function (data) {
			var data = eval('('+ data + ')');
			if(data.response == "success") {
				var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
				$("#edit_message_materi").html(msg);
				
				$('#edit_message_materi').fadeTo(3000, 500).slideUp(500, function(){
					$('#edit_message_materi').hide();
					reload_materi();
				});
				 
			}else{
				var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
				$("#edit_message_materi").html(msg);
				$("#btnsave").prop('value', 'Save'); 
				$('input[type="submit"]').prop('disabled', false);
			}
        },
		error: function(){
			var msg='<div class="uk-alert uk-alert-danger">ERROR: unable to upload files</div>';
			$("#edit_message_materi").html(msg);
			$("#btnsave").prop('value', 'Save'); 
			$('input[type="submit"]').prop('disabled', false);
		}
    });

});
</script>

<div class="uk-modal-dialog">
	<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div id="before-edit-materi"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form id="form-edit-mtr" method="post" action="<?=base_url()?>class_m/update_materi" style="display:none">
	<div id="edit_message_materi"></div>
	
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Periode</label>
			<select id="id_periode" name="id_periode" data-md-selectize>
				<option value="">Choose..</option>
				<?php 
				foreach ($periode->result() as $row){?>
					<option value="<?=$row->id_periode?>" <?php echo $materi['id_periode']==$row->id_periode? 'selected' : '';?>><?=$row->periode?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Document</label>
			<select id="id_document" name="id_document" data-md-selectize>
				<option value="">Choose..</option>
				<?php 
				foreach ($document->result() as $row){?>
					<option value="<?=$row->id_document?>" <?php echo $id_document==$row->id_document? 'selected' : '';?>><?=$row->file_name?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_class" value="<?=$id_class?>"/>
		<input type="hidden" name="id_document_class" value="<?=$id_document_class?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
