<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/jquery.form.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>

<script type="application/javascript">
	var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
   jQuery(document).ready(function() {
	setTimeout(function() { 
		$("#form-edit-essay").show(); 
		$("#before-edit-essay").hide();
	}, 100);
	
	$('#dt_default_soal_list').dataTable({
		oLanguage: {
			sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
		}					
	});

	$("#id_periode_soal").change(function(){
		var id_periode = $("#id_periode_soal").val();
		$('#data_soal_essay').html(loading)
		$.post("<?php echo base_url().'class_m/get_list_soal'?>",{id_class:'<?=$id_class?>',id_periode:id_periode,jenis_soal:'essay'},function(data){
			$('#data_soal_essay').html(data);

		});
	});
	});
	
	function pilih(id_bank_soal,id_periode,id_class){
		$.post("<?php echo base_url().'class_m/save_essay'?>",{id_class:id_class,id_periode:id_periode,id_bank_soal:id_bank_soal},function(data){
			//$('#data_soal_essay').html(data);
			$("#soal_"+id_bank_soal).prop('disabled', true);
			if(data=='success'){
				$("#soal_"+id_bank_soal).removeClass("uk-badge uk-badge-primary"); //versions newer than 1.6
				$("#soal_"+id_bank_soal).addClass("uk-badge uk-badge-default");
				$("#soal_"+id_bank_soal).prop('disabled', true); //versions newer than 1.6
			}else{
				$("#soal_"+id_bank_soal).prop('disabled', false);
				$("#soal_"+id_bank_soal).addClass("uk-badge uk-badge-primary");
			}
		});
	}
	
	function save_pilih(){
		reload_essay();
	}
	
</script>

<div class="uk-modal-dialog">
	<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div id="before-edit-essay"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form id="form-edit-essay" style="display:none">
	<div id="edit_message_essay"></div>
	
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Periode</label>
			<select id="id_periode_soal" name="id_periode" data-md-selectize>
				<option value="">Choose..</option>
				<?php 
				foreach ($periode->result() as $row){?>
					<option value="<?=$row->id_periode?>"><?=$row->periode?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<table id="dt_default_soal_list" class="uk-table" cellspacing="0" width="100%">
				<thead>
					 <tr>
						<th rowspan="2" width="">Periode</th>
						<th colspan="2"><b>Master soal</b></th>
					</tr>
					<tr>
						<th width="">Pertanyaan</th>
						<th width="100px">Action</th>
					</tr>
				</thead>
				<tbody id="data_soal_essay">
					
				</tbody>
			</table>
		</div>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_class" value="<?=$id_class?>"/>
		<button type="button" onclick="save_pilih()" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<button type="button" onclick="save_pilih()" class="md-btn md-btn-success">Save</button>
	</div>
</form>
</div>
