<?php

class class_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	
	function get_list_class(){
		$sql = "SELECT id_class,class_name,instruktur2,instruktur1,lokasi,keterangan,start_date,status, end_date,created_by,created_date,  
				updated_by,updated_date,
				
				case when start_date <= DATE_FORMAT(now(),'%Y-%m-%d') and end_date >= DATE_FORMAT(now(),'%Y-%m-%d')
						then 'inprogress'
					 when start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
					 when end_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
				end status
				FROM wb_class a,wb_class_name b
				where a.id_class_name=b.id_class_name";
		return $this->db->query($sql);
	}
	
	function get_list_class_user($id_user){
		$id_user     = $this->session->userdata("sesi_id_user");
		$sql ='';
		$sql .= "SELECT id_class,class_name,lokasi,keterangan,start_date,status, end_date,created_by,created_date,  
				updated_by,updated_date,
				
				case when start_date <= DATE_FORMAT(now(),'%Y-%m-%d') and end_date >= DATE_FORMAT(now(),'%Y-%m-%d')
						then 'inprogress'
					 when start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
					 when end_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
				end status
				FROM wb_class a, wb_class_name b
				where a.id_class_name = b.id_class_name";
				
		if($id_user==1||$id_user==7){
			$sql .= " order by created_date desc";
		}else{
			$sql .= " and created_by ='$id_user'
					order by created_date desc";
		}
		return $this->db->query($sql);
	}
	
	function get_class_name(){
		$sql = "select * from wb_class_name";
		return $this->db->query($sql);
	}
	
	function get_list_class_status2($status){
		$sql = "SELECT *
				FROM wb_class
				where status='active'";
		return $this->db->query($sql);
	}
	
	function get_list_class_inst($id_user){
		$sql = "SELECT *
				FROM wb_class
				where (instruktur1='$id_user' or instruktur2='$id_user')";
		return $this->db->query($sql);
	}
	function get_list_class_($id_user){
		$sql = "SELECT *
				FROM wb_class
				where (instruktur1='$id_user' or instruktur2='$id_user')";
		return $this->db->query($sql);
	}
	
	function get_list_class_status(){
		$sql = "SELECT *
				FROM wb_class";
		return $this->db->query($sql);
	}
	
	function save_class($id_class,$id_class_name,$keterangan,$instruktur1,$instruktur2,$start_date,$end_date,$lokasi, $id_user){
		$sql="insert into wb_class(id_class,id_class_name,keterangan,instruktur1,instruktur2,start_date,end_date,lokasi,created_by,created_date)
			  values('$id_class','$id_class_name','$keterangan','$instruktur1','$instruktur2','$start_date','$end_date','$lokasi','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Kelas berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Kelas gagal disimpan'));		
		}
		
	}
	
	function save_class_new($id_class,$id_class_name,$keterangan,$start_date,$end_date,$lokasi, $id_user){
		$sql="insert into wb_class(id_class,id_class_name,keterangan,start_date,end_date,lokasi,created_by,created_date)
			  values('$id_class','$id_class_name','$keterangan','$start_date','$end_date','$lokasi','$id_user',now())";
		return $this->db->query($sql);
	}
	
	function get_class_by_id($id){
		$sql = "SELECT * FROM wb_class
				WHERE id_class='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_class_name_by_id($id){
		$sql = "SELECT * FROM wb_class a,wb_class_name b
				WHERE a.id_class_name=b.id_class_name
				and id_class='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
		
	function update_class($id,$class_name,$keterangan,$instruktur1,$instruktur2,$start_date,$end_date,$lokasi, $id_user){
		$sql="update wb_class set id_class_name='$class_name',
			  keterangan='$keterangan',instruktur1='$instruktur1',instruktur2='$instruktur2',
			  start_date='$start_date',end_date='$end_date',lokasi='$lokasi',
			  updated_by='$id_user',updated_date=now()
			  WHERE id_class='$id'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Kelas berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Kelas gagal di simpan'));		
		}		  
	
	}
	
	
	function delete_class($id_class){
		$sql="DELETE FROM wb_class 
			  WHERE id_class='$id_class'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Kelas berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Kelas gagal didelete'));		
		}		  
	}
	
	function inactive_class($id_class){
		$sql="update wb_class set keterangan='0'
			  WHERE id_class='$id_class'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Kelas berhasil di nonaktifkan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Kelas gagal di nonaktifkan'));		
		}		  
	}
	
	
}
