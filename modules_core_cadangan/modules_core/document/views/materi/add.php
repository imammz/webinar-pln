<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/jquery.form.js"></script>

<script type="application/javascript">
   jQuery(document).ready(function() {
	setTimeout(function() { 
		$("#form-add-mtr").show(); 
		$("#before-add-materi").hide();
	}, 100);
	
    $("#form-add-mtr").validate({ // initialize the plugin
        // any other options,
        onkeyup: false,
        rules: {
            document:{
					required:true
				  },
			file:{
					required:true
				  }				  
        },
		highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					}
    });

    $("#form-add-mtr").ajaxForm({ // initialize the plugin
        // any other options,
        beforeSubmit: function () {
			$("#btnsave").prop('value', 'Process...'); 
			$('input[type="submit"]').prop('disabled', true);
            return $("#form-add-mtr").valid(); // TRUE when form is valid, FALSE will cancel submit
        },
        success: function (data) {
			var data = eval('('+ data + ')');
			if(data.response == "success") {
				var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
				$("#add_message_materi").html(msg);
				
				$('#add_message_materi').fadeTo(3000, 500).slideUp(500, function(){
					$('#add_message_materi').hide();
					reload_materi();
				});
				 
			}else{
				var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
				$("#add_message_materi").html(msg);
				$("#btnsave").prop('value', 'Save'); 
				$('input[type="submit"]').prop('disabled', false);
			}
        },
		error: function(){
			var msg='<div class="uk-alert uk-alert-danger">ERROR: unable to upload files</div>';
			$("#add_message_materi").html(msg);
			$("#btnsave").prop('value', 'Save'); 
			$('input[type="submit"]').prop('disabled', false);
		}
    });

});
</script>

<div class="uk-modal-dialog">
	<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div id="before-add-materi"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form id="form-add-mtr" method="post" action="<?=base_url()?>document/save_materi" enctype="multipart/form-data" style="display:none">
	<div id="add_message_materi"></div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Document</label>
		<input type="text" class="md-input" id="document" name="document"/>
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">File</label>
		<input type="file" class="md-input" id="file" name="file">
	</div>
	
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name='id_pembelajaran' value="<?=$id_pembelajaran?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>