<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<!--<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>-->
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/jquery.form.js"></script>

<script type="application/javascript">
   jQuery(document).ready(function() {
	setTimeout(function() { 
		$("#form-add-file").show(); 
		$("#before-add-file").hide();
	}, 100);
	
    $("#form-add-file").validate({ 
        onkeyup: false,
        rules: {
            document:{
					required:true
				  },
			  id_periode:{
				required:true
			  },
		  file:{
				required:true
			  }
        },
		highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					}
    });

    $("#form-add-file").ajaxForm({ 
        beforeSubmit: function () {
			$("#btnsave").prop('value', 'Process...'); 
			$('input[type="submit"]').prop('disabled', true);
            return $("#form-add-file").valid();
        },
        success: function (data) {
			var data = eval('('+ data + ')');
			if(data.response == "success") {
				var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
				$("#add_message_file").html(msg);
				
				$('#add_message_file').fadeTo(3000, 500).slideUp(500, function(){
					$('#add_message_file').hide();
					reload_file();
				});
				 
			}else{
				var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
				$("#add_message_file").html(msg);
				$("#btnsave").prop('value', 'Save'); 
				$('input[type="submit"]').prop('disabled', false);
			}
        },
		error: function(){
			var msg='<div class="uk-alert uk-alert-danger">ERROR: unable to upload files</div>';
			$("#add_message_file").html(msg);
			$("#btnsave").prop('value', 'Save'); 
			$('input[type="submit"]').prop('disabled', false);
		}
    });

});
</script>


<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div>
<div id="before-add-file"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form id="form-add-file" method="post" action="<?=base_url()?>document/save_file" enctype="multipart/form-data" style="display:none">
	<div id="add_message_file"></div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Document</label>
		<input type="text" class="md-input" id="document" name="document"/>
	</div>

	<div class="uk-margin-medium-bottom">
		<label for="task_title">File</label>
		<input type="file" class="md-input" id="file" name="file">
	</div>
	
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name='id_pembelajaran' value="<?=$id_pembelajaran?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
