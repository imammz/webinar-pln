<div id="delete_message_file"></div>
<table id="dt_default_file" class="uk-table" cellspacing="0" width="100%">
	<thead>
		 <tr>
			<th rowspan="2" width="">File</th>
			<th colspan="2"><b><?=$title?></b></th>
		</tr>
		<tr>
			<th width="">Nama File</th>
			<th width="100px">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($file){
	foreach($file->result() as $fl){?>	
			<tr>
				<td><?=$fl->document?></td>
				<td><a href="<?=base_url().'document/download/'.$fl->id_document?>" alt="Download"><?=$fl->file_name?></a></td>
				<td>
					<a href="#edit_file" data-uk-modal="{ center:true }" onclick="edit_file('<?=$fl->id_document?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
					<a href="#" onclick="delete_file('<?=$fl->id_document?>')"class="uk-icon-button uk-icon-trash-o"></a>
				</td>
			</tr>	
	<?php }
	}?>		
	</tbody>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_file" onclick="add_file('<?=$id_pembelajaran?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_file">
			<div id="modal-add-file"></div>
		</div>
		<div class="uk-modal" id="edit_file">
			<div id="modal-edit-file"></div>
		</div>
	</div> 
	<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>
	
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default_file').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_file(id){
				uri = '<?=base_url()."document/edit_file"?>';
				$('#new_file').hide();
				$('#edit_file').show();
				$('#modal-edit-file').html(loading);
				$.post(uri,{ajax:true,id_document:id,id_pembelajaran:'<?=$id_pembelajaran?>'},function(data) {
					$('#modal-edit-file').html(data);
				});
			}
			
			function add_file(id_pembelajaran){
				uri = '<?=base_url()."document/add_file"?>';
				$('#edit_file').hide();
				$('#new_file').show();
				$('#modal-add-file').html(loading);
				$.post(uri,{ajax:true,id_pembelajaran:id_pembelajaran},function(data) {
					$('#modal-add-file').html(data);
				});
			}
			
			function delete_file(id){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."document/delete_file"?>';
					$.post(uri,{ajax:true,id_file:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message_file").html(msg);
							
							$('#delete_message_file').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message_file').hide();
								
								reload_file();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message_file").html(msg);
						}
					});
				} 
			}
			
	</script>