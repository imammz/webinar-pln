<!---CONTENT START-->	
   <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large">
				<h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
                    <div class="md-card">
						<div id="loading"></div>
                        <div class="user_content" id="user_contents" style="display:none">
                             <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1'}">
                                <li><a href="#">Materi</a></li>
                                <li id="video"><a href="#">Video</a></li>
								<li><a href="#">File</a></li>
                            </ul>
                            <ul id="tabs_1" class="uk-switcher uk-margin">
							<!-- informasi kelas -->
							<!-- end informasi kelas -->
							
							<!-- materi -->
                                <li>
									<div id="data_materi"></div>	
								</li>
							<!-- end materi -->	
			
							<!--video-->
                                <li>
									<div id="data_video"></div>								
								</li>
							<!--end video-->	
							
							<!-- file-->
                                <li>
									<div id="data_file"></div>
								</li>
							<!--end file-->	
								
                            </ul>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>  
	<!-- common functions -->
    <script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?=base_url()?>front_assets/assets/js/uikit_custom.min.js"></script>
	<script>
		var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/></div>';
		$(document).ready(function() {
			//$('user_contents').uk-();
			//$('#video').addClass("uk-active");
			$('#loading').html(loading);
			$('#loading').fadeTo(50, 50).slideUp(50, function(){
				
				reload_materi();
				reload_video();
				reload_file();
				
				$('#waiting').hide();
				$('#loading').hide();
				$('#user_contents').show();
			});
		});
		


		function reload_materi(){
			$('#data_materi').html(loading);
			$.post("<?php echo base_url().'document/list_materi'?>",{id_pembelajaran:'<?=$id_pembelajaran?>'},function(data){
				$('#data_materi').html(data)
			});
		}	
		
		function reload_video(){
			$('#data_video').html(loading);
			$.post("<?php echo base_url().'document/list_video'?>",{id_pembelajaran:'<?=$id_pembelajaran?>'},function(data){
				$('#data_video').html(data)
			});
		}
		
		function reload_file(){
			$('#data_file').html(loading);
			$.post("<?php echo base_url().'document/list_file'?>",{id_pembelajaran:'<?=$id_pembelajaran?>'},function(data){
				$('#data_file').html(data)
			});
		}
		
	</script>
<!---CONTENT END-->