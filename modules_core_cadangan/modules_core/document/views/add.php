<?php echo Modules::run('front_templates/front_templates/header'); ?>
<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
    <div id="page_content">
        <div id="page_content_inner">

            <h2 class="heading_b uk-margin-bottom">Advanced Wizard</h2>

            <div class="md-card uk-margin-large-bottom">
                <div class="md-card-content">
                    <form class="uk-form-stacked" id="wizard_advanced_form">
                        <div id="wizard_advanced">

                            <!-- first section -->
                            <h3>Informasi Kelas</h3>
                            <section>
                                <h2 class="heading_a">
                                    Informasi Kelas
                                    <span class="sub-heading"></span>
                                </h2>
                                <hr class="md-hr"/>
                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <label for="class_name">Nama Kelas<span class="req">*</span></label>
                                        <input type="text" name="class_name" id="class_name" required class="md-input" />
                                    </div>
                                </div>
                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <label for="keterangan">Keterangan<span class="req">*</span></label>
                                        <input type="text" name="keterangan" id="keterangan" required class="md-input" />
                                    </div>
                                </div>
								<div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <label for="keterangan">Lokasi<span class="req">*</span></label>
                                        <input type="text" name="lokasi" id="lokasi" required class="md-input" />
                                    </div>
                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
									<div class="uk-width-medium-1-2 parsley-row">
                                        <label for="start_date">Start Date<span class="req">*</span></label>
                                        <input type="text" name="start_date" id="start_date" required class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
									<div class="uk-width-medium-1-2 parsley-row">
                                        <label for="end_date">End Date<span class="req">*</span></label>
                                        <input type="text" name="end_date" id="end_date" required class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
                                </div>
                            </section>

                            <!-- second section -->
                            <h3>Informasi Periode</h3>
                            <section>
                                <h2 class="heading_a">
                                    Informasi Periode
                                    <span class="sub-heading"></span>
                                </h2>
                                <hr class="md-hr"/>	 
								<!--
								<span class="uk-alert uk-alert-info">If you would like your registration renewals sent to an address other than your residence/business address, enter it below.</span>-->
                                <div class="uk-grid" data-uk-grid-margin>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_name">Periode</label>
                                        <input type="text" name="periode_name[]" id="periode_name" class="md-input"/>
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_start_date">Start Date</label>
                                        <input type="text" name="periode_start_date[]" id="periode_start_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_end_date">End Date</label>
                                        <input type="text" name="periode_end_date[]" id="periode_end_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
                                </div>
								<div class="uk-grid" data-uk-grid-margin>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_name">Periode</label>
                                        <input type="text" name="periode_name[]" id="periode_name" class="md-input"/>
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_start_date">Start Date</label>
                                        <input type="text" name="periode_start_date[]" id="periode_start_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_end_date">End Date</label>
                                        <input type="text" name="periode_end_date[]" id="periode_end_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
                                </div>
								<div class="uk-grid" data-uk-grid-margin>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_name">Periode</label>
                                        <input type="text" name="periode_name[]" id="periode_name" class="md-input"/>
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_start_date">Start Date</label>
                                        <input type="text" name="periode_start_date[]" id="periode_start_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_end_date">End Date</label>
                                        <input type="text" name="periode_end_date[]" id="periode_end_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
                                </div>
								<div class="uk-grid" data-uk-grid-margin>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_name">Periode</label>
                                        <input type="text" name="periode_name[]" id="periode_name" class="md-input"/>
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_start_date">Start Date</label>
                                        <input type="text" name="periode_start_date[]" id="periode_start_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_end_date">End Date</label>
                                        <input type="text" name="periode_end_date[]" id="periode_end_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
                                </div>
								<div class="uk-grid" data-uk-grid-margin>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_name">Periode</label>
                                        <input type="text" name="periode_name[]" id="periode_name" class="md-input"/>
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_start_date">Start Date</label>
                                        <input type="text" name="periode_start_date[]" id="periode_start_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
									<div class="uk-width-medium-1-3 parsley-row">
                                        <label for="periode_end_date">End Date</label>
                                        <input type="text" name="periode_end_date[]" id="periode_end_date" class="md-input" data-uk-datepicker="{format:'YYYY-MM-DD'}" />
                                    </div>
                                </div>
                            </section>

                            <!-- third section -->
                            <h3>Informasi Instruktur</h3>
                            <section>
                                <h2 class="heading_a">
                                    Informasi Instruktur
                                    <span class="sub-heading"></span>
                                </h2>
                                <hr class="md-hr"/>
								<div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
										<label for="instruktur1">Instruktur 1</label>
										<select id="instruktur1" name="instruktur1"  required >
											<option value="">Select...</option>
											<?php 
											foreach ($instruktur->result() as $row){?>
												<option value="<?=$row->id_user?>"><?=$row->nama?></option>
											<?php }?>
										</select>
									</div>
                                </div>
								<div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
										<label for="instruktur2">Instruktur 2</label>
										<select id="instruktur2" name="instruktur2">
											<option value="">Select...</option>
											<?php 
											foreach ($instruktur->result() as $row){?>
												<option value="<?=$row->id_user?>"><?=$row->nama?></option>
											<?php }?>
										</select>
									</div>
                                </div>
                            </section>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    
    <!-- common functions -->
    <script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?=base_url()?>front_assets/assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- parsley (validation) -->
    <script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    // load extra validators
    altair_forms.parsley_extra_validators();
    </script>
    <script src="<?=base_url()?>front_assets/bower_components/parsleyjs/dist/parsley.min.js"></script>
    <!-- jquery steps -->
    <script src="<?=base_url()?>front_assets/assets/js/custom/wizard_steps.min.js"></script>

    <!--  forms wizard functions -->
    <!--<script src="<?php //base_url()?>front_assets/assets/js/pages/forms_wizard.min.js"></script>-->
	<script>
		$(function() {
				altair_wizard.advanced_wizard(), altair_wizard.vertical_wizard()
			}), altair_wizard = {
				content_height: function(t, i) {
					var a = $(t).find(".step-" + i).actual("outerHeight");
					$(t).children(".content").animate({
						height: a
					}, 280, bez_easing_swiftOut)
				},
				advanced_wizard: function() {
					var t = $("#wizard_advanced"),
						i = $("#wizard_advanced_form");
					t.length && (t.steps({
						headerTag: "h3",
						bodyTag: "section",
						transitionEffect: "slideLeft",
						trigger: "change",
						onInit: function(i, a) {
							altair_wizard.content_height(t, a), altair_md.checkbox_radio($(".wizard-icheck")), altair_uikit.reinitialize_grid_margin(), setTimeout(function() {
								$window.resize()
							}, 100)
						},
						onStepChanged: function(i, a) {
							altair_wizard.content_height(t, a), setTimeout(function() {
								$window.resize()
							}, 100)
						},
						onStepChanging: function(i, a, n) {
							var e = t.find(".body.current").attr("data-step"),
								r = $('.body[data-step="' + e + '"]');
							return r.find("[data-parsley-id]").each(function() {
								$(this).parsley().validate()
							}), $window.resize(), r.find(".md-input-danger").length ? !1 : !0
						},
						onFinished: function() {
							var t = JSON.stringify(i.serializeObject(), null, 2);
							//alert(i.serializeObject());
							$.post('<?php echo base_url('class_m/save_class'); ?>', $('#wizard_advanced_form').serialize(), function(data) {
								//UIkit.modal.alert("<p>Wizard data:</p><pre>" + t + "</pre>")
								var data = eval('('+ data + ')');
								if(data.response == "success") {
									UIkit.modal.alert("<p>"+data.msg+"</p>");
									//window.location.href = "<?php echo base_url('class_m')?>";
								}else{
									UIkit.modal.alert("<p>"+data.msg+"</p>");
								}
							});
						}
					}), $window.on("debouncedresize", function() {
						var i = t.find(".body.current").attr("data-step");
						altair_wizard.content_height(t, i)
					}), i.parsley().on("form:validated", function() {
						setTimeout(function() {
							altair_md.update_input(i.find(".md-input")), $window.resize()
						}, 100)
					}).on("field:validated", function(i) {
						var a = $(i.$element);
						setTimeout(function() {
							altair_md.update_input(a);
							var i = t.find(".body.current").attr("data-step");
							altair_wizard.content_height(t, i)
						}, 100)
					}))
				},
				vertical_wizard: function() {
					var t = $("#wizard_vertical");
					t.length && t.steps({
						headerTag: "h3",
						bodyTag: "section",
						enableAllSteps: !0,
						enableFinishButton: !1,
						transitionEffect: "slideLeft",
						stepsOrientation: "vertical",
						onInit: function(i, a) {
							altair_wizard.content_height(t, a)
						},
						onStepChanged: function(i, a) {
							altair_wizard.content_height(t, a)
						}
					})
				}
			};
	</script>
    