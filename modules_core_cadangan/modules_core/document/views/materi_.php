<!---CONTENT START-->	
<div id="page_content">
        <div id="page_content_inner">

            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin="">
                        <div class="uk-width-medium-3-10">
                            <label for="product_search_name">Nama File</label>
                            <input type="text" class="md-input" id="product_search_name">
                        </div>
                        <div class="uk-width-medium-1-10">
                            <label for="product_search_price">Judul</label>
                            <input type="text" class="md-input" id="product_search_price">
                        </div>
                        <div class="uk-width-medium-3-10">
                            <div class="uk-margin-small-top">
                                <select id="product_search_status" data-md-selectize multiple data-md-selectize-bottom>
                                    <option value="">Kelas</option>
                                    <option value="1">Kelas 1</option>
                                    <option value="2">Kelas 2</option>
                                    <option value="3">Kelas 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="uk-width-medium-2-10 uk-text-center">
                            <a href="#" class="md-btn md-btn-primary uk-margin-small-top">Filter</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Nama File</th>
                                            <th>Judul</th>
                                            <th>Fasilitator</th>
                                            <th>Kelas</th>
											<th>Sesi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img class="img_thumb" src="<?=base_url()?>front_assets/assets/img/ecommerce/s6_edge_1.jpg" alt=""></td>
                                            <td class="uk-text-large uk-text-nowrap">
											<a href="ecommerce_product_details.html">
											File Belajar Mengajar</a></td>
                                            <td class="uk-text-nowrap">Mengajar Dengan Cara Terbaik</td>
                                            <td>Imam Mudzakir</td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-warning">Kelas 3</span></td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-success">Sesi 1</span></td>
                                            <td class="uk-text-nowrap">
                                                   <a href="#"><i class="material-icons">cloud_download</i></a>
                                            </td>
                                        </tr>
                                       <tr>
                                            <td><img class="img_thumb" src="<?=base_url()?>front_assets/assets/img/ecommerce/s6_edge_1.jpg" alt=""></td>
                                            <td class="uk-text-large uk-text-nowrap">
											<a href="ecommerce_product_details.html">
											File Belajar Mengajar</a></td>
                                            <td class="uk-text-nowrap">Mengajar Dengan Cara Terbaik</td>
                                            <td>Imam Mudzakir</td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-warning">Kelas 3</span></td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-success">Sesi 1</span></td>
                                            <td class="uk-text-nowrap">
                                                   <a href="#"><i class="material-icons">cloud_download</i></a>
                                            </td>
                                        </tr>
										 <tr>
                                            <td><img class="img_thumb" src="<?=base_url()?>front_assets/assets/img/ecommerce/s6_edge_1.jpg" alt=""></td>
                                            <td class="uk-text-large uk-text-nowrap">
											<a href="ecommerce_product_details.html">
											File Belajar Mengajar</a></td>
                                            <td class="uk-text-nowrap">Mengajar Dengan Cara Terbaik</td>
                                            <td>Imam Mudzakir</td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-warning">Kelas 3</span></td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-success">Sesi 1</span></td>
                                            <td class="uk-text-nowrap">
                                                   <a href="#"><i class="material-icons">cloud_download</i></a>
                                            </td>
                                        </tr>
										 <tr>
                                            <td><img class="img_thumb" src="<?=base_url()?>front_assets/assets/img/ecommerce/s6_edge_1.jpg" alt=""></td>
                                            <td class="uk-text-large uk-text-nowrap">
											<a href="ecommerce_product_details.html">
											File Belajar Mengajar</a></td>
                                            <td class="uk-text-nowrap">Mengajar Dengan Cara Terbaik</td>
                                            <td>Imam Mudzakir</td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-warning">Kelas 3</span></td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-success">Sesi 1</span></td>
                                            <td class="uk-text-nowrap">
                                                <a href="#"><i class="material-icons">cloud_download</i></a>
                                            </td>
                                        </tr>
										 <tr>
                                            <td><img class="img_thumb" src="<?=base_url()?>front_assets/assets/img/ecommerce/s6_edge_1.jpg" alt=""></td>
                                            <td class="uk-text-large uk-text-nowrap">
											<a href="ecommerce_product_details.html">
											File Belajar Mengajar</a></td>
                                            <td class="uk-text-nowrap">Mengajar Dengan Cara Terbaik</td>
                                            <td>Imam Mudzakir</td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-warning">Kelas 3</span></td>
                                            <td class="uk-text-nowrap"><span class="uk-badge uk-badge-success">Sesi 1</span></td>
                                            <td class="uk-text-nowrap">
                                                   <a href="#"><i class="material-icons">cloud_download</i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <ul class="uk-pagination uk-margin-medium-top uk-margin-medium-bottom">
                                <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
                                <li class="uk-active"><span>1</span></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><span>&hellip;</span></li>
                                <li><a href="#">20</a></li>
                                <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<!---CONTENT END-->