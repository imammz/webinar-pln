<!---CONTENT START-->	
<div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_a uk-margin-bottom">Video List In Class</h3>
            <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-large-1-3 uk-grid-width-xlarge-1-4 uk-text-center uk-sortable sortable-handler" id="dashboard_sortable_cards" data-uk-sortable data-uk-grid-margin>
                <div>
                    <div class="md-card md-card-hover-img">
                        <div class="md-card-head uk-text-center uk-position-relative">
                            <img class="md-card-head-img" src="<?=base_url()?>front_assets/assets/img/ecommerce/video.jpg" alt=""/>
                        </div>
                        <div class="md-card-content">
                            <h4 class="heading_c uk-margin-bottom">
                                Kelas Video 1
                                <span class="sub-heading">Sesi 1</span>
                            </h4>
                            <p align="left">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquid autem cupiditate harum illum&hellip;</p>
                            <a class="md-btn md-btn-success" href="ecommerce_product_details.html">Detail</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<!---CONTENT END-->