
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2">File Name</th>
                                <th colspan="4">File Information</th>
                            </tr>
                            <tr>
                                <th>Judul</th>
								<th>Fasilitator</th>
								<th>Kelas</th>
								<th>Sesi</th>
								<th>Action</th>
							</tr>
                        </thead>

                        <tbody>
					<?php foreach ($document->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->file_name?></td>
							<td><?=$row->document?></td>
							<td><?=$row->nama?></td>
							<td><?=$row->class?></td>
							<td><?=$row->periode?></td>
                            <td>
								<a href="<?=base_url().'document/download/'.$row->id_document?>" class="uk-icon-button uk-icon-download"></a>
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
	</script>	
</html>