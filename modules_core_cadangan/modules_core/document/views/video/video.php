<div id="delete_message_video"></div>
<table id="dt_default_video" class="uk-table" cellspacing="0" width="100%">
	<thead>
		 <tr>
			<th rowspan="2" width="">Video</th>
			<th colspan="2"><b>Master Video</b></th>
		</tr>
		<tr>
			<th width="">Nama File</th>
			<th width="150px">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($video){
	foreach($video->result() as $vide){?>	
			<tr>
				<td><?=$vide->document?></td>
				<td><a href="<?=base_url().'document/download/'.$vide->id_document?>" alt="Download"><?=$vide->file_name?></a></td>
				<td>
					<a href="#edit_video" data-uk-modal="{ center:true }" onclick="edit_video('<?=$vide->id_document?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
					<a href="#" onclick="delete_video('<?=$vide->id_document?>')"class="uk-icon-button uk-icon-trash-o"></a>
				</td>
			</tr>	
	<?php }
	}?>		
	</tbody>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_video" onclick="add_video('<?=$id_pembelajaran?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_video">
			<div id="modal-add-vid"></div>
		</div>
		<div class="uk-modal" id="edit_video">
			<div id="modal-edit-vid"></div>
		</div>
	</div> 
	<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>
	
		<script>
			$( document ).ready(function() {
				
				$('#dt_default_video').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_video(id){
				uri = '<?=base_url()."document/edit_video"?>';
				$('#new_video').hide();
				$('#edit_video').show();
				$('#modal-edit-vid').html(loading);
				$.post(uri,{ajax:true,id_document:id,id_pembelajaran:'<?=$id_pembelajaran?>'},function(data) {
					$('#modal-edit-vid').html(data);
				});
			}
			
			function add_video(id_pembelajaran){
				uri = '<?=base_url()."document/add_video"?>';
				$('#edit_video').hide();
				$('#new_video').show();
				$('#modal-add-vid').html(loading);
				$.post(uri,{ajax:true,id_pembelajaran:id_pembelajaran},function(data) {
					$('#modal-add-vid').html(data);
				});
			}
			
			function delete_video(id){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."document/delete_video"?>';
					$.post(uri,{ajax:true,id_video:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message_video").html(msg);
							
							$('#delete_message_video').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message_video').hide();
								
								reload_video();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message_video").html(msg);
						}
					});
				} 
			}
			
	</script>