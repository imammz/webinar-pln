<?php

class document extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model('document/document_model');
		$this->load->helper('download');
		date_default_timezone_set('Asia/Jakarta');
    }
    
	function index(){
		$this->login_model->is_login();
        $data['id_pembelajaran'] = $this->input->post('id_pembelajaran');
		$nama_pembelajaran = $this->input->post('nama_pembelajaran');
        $id_user          = $this->session->userdata("sesi_id_user");
        $data['title']    = 'Materi '.$nama_pembelajaran;
        $data['link']     = base_url() . "document/document";
        $this->load->view('document', $data);
		
	}
    public function list_materi()
    {
        $this->login_model->is_login();
        $id_pembelajaran         = $this->input->post('id_pembelajaran');
        $data['materi']   = $this->document_model->get_list_document($id_pembelajaran, 'materi');
        $data['title']    = 'Master Materi';
        $data['id_pembelajaran'] = $id_pembelajaran;
        $data['link']     = base_url() . "class_m/list_materi";
        $this->load->view('materi/materi', $data);
    }
    
    public function add_materi()
    {
        $this->login_model->is_login();
        $id_pembelajaran         = $this->input->post('id_pembelajaran');
        $data['title']    = 'Add Materi';
        $data['id_pembelajaran'] = $id_pembelajaran;
        $data['link']     = base_url() . "class_m/list_materi";
        $this->load->view('materi/add', $data);
    }
    
    public function edit_materi()
    {
        $this->login_model->is_login();
        $data['id_document']       = $this->input->post('id_document');
        $data['materi']  = $this->document_model->get_document_by_id($data['id_document']);
        $data['title']   = 'Edit Materi';
        $data['link']    = base_url() . "class_m/list_materi";
        $this->load->view('materi/edit', $data);
    }
    
    public function save_materi()
    {
        $this->login_model->is_login();
        $id_document = 'MTR' . date('ymdHis');
        $id_pembelajaran    = $this->input->post('id_pembelajaran');
        $document    = $this->input->post('document');
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'png|jpg|doc|xls|pdf';
        $config['max_size']      = '1000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true
                );
                $this->document_model->save_document($id_document, $document, $name, $id_pembelajaran, 'materi', $path . $upload_data['file_name'], $id_user);
            }
        } else {
            $this->document_model->save_document($id_document, $document, '', $id_pembelajaran, 'materi', $path, $id_user);
        }
    }
    
    public function update_materi()
    {
        $this->login_model->is_login();
        $id_document = $this->input->post('id_document');
		$document = $this->input->post('document');
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'png|jpg|doc|xls|pdf';
        $config['max_size']      = '10000000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true
                );
                $this->document_model->update_document($id_document, $document, $name, 'materi', $path . $upload_data['file_name'], $id_user);
            }
        } else {
            $this->document_model->update_document($id_document, $document, '', 'materi', $path, $id_user);
        }
    }
    
    public function delete_materi()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_document');
        $this->document_model->delete_document($id);
    }
    
    
    public function list_video()
    {
        $this->login_model->is_login();
        $id_pembelajaran         = $this->input->post('id_pembelajaran');
        $data['video']    = $this->document_model->get_list_document($id_pembelajaran, 'video');
        $data['title']    = 'Master video';
        $data['id_pembelajaran'] = $id_pembelajaran;
        $data['link']     = base_url() . "class_m/list_video";
        $this->load->view('video/video', $data);
    }
    
    public function add_video()
    {
        $this->login_model->is_login();
        $id_pembelajaran         = $this->input->post('id_pembelajaran');
        $data['title']    = 'Add video';
        $data['id_pembelajaran'] = $id_pembelajaran;
        $data['link']     = base_url() . "class_m/list_video";
        $this->load->view('video/add', $data);
    }
    
    public function edit_video()
    {
        $this->login_model->is_login();
        $id_video        = $this->input->post('id_document');
        $id_pembelajaran        = $this->input->post('id_pembelajaran');
        $data['video']   = $this->document_model->get_document_by_id($id_video);
        $data['title']   = 'Edit video';
        $data['link']    = base_url() . "class_m/list_video";
        $this->load->view('video/edit', $data);
    }
    
    public function save_video()
    {
		
        $this->login_model->is_login();
        $id_document = 'VD' . date('ymdHis');
        $id_pembelajaran    = $this->input->post('id_pembelajaran');
        $document    = $this->input->post('document');
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name 		 = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'mp4|avi|3gp|mp3|wap';
        $config['encrypt_name']  = TRUE;
		$config['max_size'] 	 = '204800000000000000000000';
        $config['remove_spaces'] = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true
                );
                $this->document_model->save_document($id_document, $document, $name, $id_pembelajaran,'video', $path . $upload_data['file_name'], $id_user);
            }
        } else {
            $this->document_model->save_document($id_document, $document, '', $id_pembelajaran, 'video', $path, $id_user);
        }
    }
    
    public function update_video()
    {
        $this->login_model->is_login();
        
        ini_set('upload_max_filesize', '256M');
        ini_set('post_max_size', '256M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
        
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'avi|mpg|3gp|mp4';
        $config['max_size']      = '100000000000000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true
                );
                $this->document_model->update_document($id_document, $document, $name, 'video', $path . $upload_data['file_name'], $id_user);
            }
        } else {
            $this->document_model->update_document($id_document, $document, '', 'video', $path, $id_user);
        }
    }
    
    public function delete_video()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_video');
        $this->document_model->delete_document($id);
    }
    
    public function download()
    {
		ini_set('memory_limit', '-1');
        $id_document = $this->uri->segment(3);
        $doc         = $this->document_model->get_document_by_id($id_document);
        $data        = file_get_contents($doc['path']);
        ini_set('memory_limit', '300M'); 
		set_time_limit(0);
		force_download($doc['file_name'], $data);
    }
    
    public function list_file()
    {
        $this->login_model->is_login();
        $id_pembelajaran         = $this->input->post('id_pembelajaran');
        $data['file']     = $this->document_model->get_list_document($id_pembelajaran, 'file');
        $data['title']    = 'Master File';
        $data['id_pembelajaran'] = $id_pembelajaran;
        $data['link']     = base_url() . "class_m/list_file";
        $this->load->view('file/file', $data);
    }
    
    public function add_file()
    {
        $this->login_model->is_login();
        $id_pembelajaran         = $this->input->post('id_pembelajaran');
        $data['title']    = 'Add File';
        $data['id_pembelajaran'] = $id_pembelajaran;
        $data['link']     = base_url() . "class_m/list_file";
        $this->load->view('file/add', $data);
    }
    
    public function edit_file()
    {
        $this->login_model->is_login();
        $id_file         = $this->input->post('id_document');
        $id_pembelajaran        = $this->input->post('id_pembelajaran');
        $data['file']    = $this->document_model->get_document_by_id($id_file);
        $data['title']   = 'Edit File';
        $data['link']    = base_url() . "class_m/list_file";
        $this->load->view('file/edit', $data);
    }
    
    public function save_file()
    {
        $this->login_model->is_login();
        $id_document = 'FL' . date('ymdHis');
        $id_pembelajaran    = $this->input->post('id_pembelajaran');
        $document    = $this->input->post('document');
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'png|jpg|doc|xls|pdf';
        $config['max_size']      = '10000000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true
                );
                $this->document_model->save_document($id_document, $document, $name, $id_pembelajaran, 'file', $path . $upload_data['file_name'], $id_user);
            }
        } else {
            $this->document_model->save_document($id_document, $document, '', $id_pembelajaran, 'file', $path, $id_user);
        }
    }
    
    
    
    public function update_file()
    {
        $this->login_model->is_login();
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'png|jpg|doc|xls|pdf';
        $config['max_size']      = '10000000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true
                );
                $this->document_model->update_document($id_document, $document, $name, 'file', $path . $upload_data['file_name'], $id_user);
            }
        } else {
            $this->document_model->update_document($id_document, $document, '', 'file', $path, $id_user);
        }
    }
    
    public function delete_file()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_file');
        $this->document_model->delete_document($id);
    }
	
	
	/*
    public function index()
	{ 	
		exit();
		
	}
	

	public function file()
	{ 	
		$data['link']   = base_url()."document/list_file"; 
		$data['title']  ='document';
		
		$this->load->view('preview',$data);
	}

	public function list_file() 
	{
		$this->login_model->is_login();
		$data['title'] 		='File';
		$data['document'] = $this->document_model->get_all_document_class('file');
		//print_r($data['document']->result());
		$data['link'] 		= base_url()."document/list_file"; 
		$this->load->view('file', $data);        
    }
	
	public function materi()
	{ 	
		$this->login_model->is_login();
		$data['link']   = base_url()."document/list_materi"; 
		$data['title']  ='Materi';
		
		$this->load->view('preview',$data);
	}

	public function list_materi() 
	{
		$this->login_model->is_login();
		$data['title'] 		='Materi';
		$data['document'] = $this->document_model->get_all_document_class('materi');
		//print_r($data['document']->result());
		$data['link'] 		= base_url()."document/list_materi"; 
		$this->load->view('materi', $data);        
    }
	
	public function video()
	{ 	
		$this->login_model->is_login();
		$data['link']   = base_url()."document/list_video"; 
		$data['title']  ='Materi';
		
		$this->load->view('preview',$data);
	}

	public function list_video() 
	{
		$this->login_model->is_login();
		$data['title'] 		='Video';
		$data['document'] = $this->document_model->get_all_document_class('video');
		//print_r($data['document']->result());
		$data['link'] 		= base_url()."document/list_video"; 
		$this->load->view('video', $data);        
    }
	
	function download(){
		//ini_set('memory_limit', '300M'); 
		set_time_limit(0);
		ini_set('memory_limit', '-1');
		$id_document = $this->uri->segment(3);
        $doc         = $this->document_model->get_document_by_id($id_document);
        $data        = file_get_contents($doc['path']);
        force_download($doc['file_name'], $data);
	}
	
	
	
*/	
}

?>