<?php

class document extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");

		date_default_timezone_set('Asia/Jakarta');
    }
    
    public function index()
	{ 	
		exit();
		
	}
	
	public function video()
	{ 	
		$data['link'] = base_url()."document/list_video"; 
		$data['title']='document';
		$this->load->view('preview',$data);	
	}

	public function list_video() 
	{
		//$this->login_model->checkPrivilege();
		$data['title'] 		='document';
		$data['link'] 		= base_url()."document/list_video"; 
		$this->load->view('video', $data);        
    }
	
	public function file()
	{ 	
		$data['link']   = base_url()."document/list_file"; 
		$data['title']  ='document';
		
		$this->load->view('preview',$data);
	}

	public function list_file() 
	{
		//$this->login_model->checkPrivilege();
		$data['title'] 		='document';
		$data['link'] 		= base_url()."document/list_file"; 
		$this->load->view('file', $data);        
    }
	
	
	
	
}

?>