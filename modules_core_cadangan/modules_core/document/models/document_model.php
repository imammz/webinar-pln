<?php

class document_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
		$this->load->helper('download');
        
	}
	
	function get_class_file($jenis_file,$id_class,$id_periode){
		$sql ="SELECT b.`nama_pembelajaran`,a.* 
				FROM wb_document a,wb_pembelajaran b
				WHERE a.`id_pembelajaran`=b.`id_pembelajaran`
				AND type='$jenis_file'
				and a.id_document not in (
					select id_document from wb_document_class
					where id_class ='$id_class'
					and id_periode ='$id_periode'
				)";
		return $this->db->query($sql);		
	}
	function get_all_list_document($jenis_file){
		$sql=" SELECT * FROM wb_document 
			   WHERE type='$jenis_file'
			   order by created_date desc";
		return $this->db->query($sql);
	}
	function get_list_document($id_pembelajaran,$jenis_file){
		$sql=" SELECT * FROM wb_document 
			   WHERE id_pembelajaran='$id_pembelajaran'
			   AND type='$jenis_file'
			   order by created_date desc";
		return $this->db->query($sql);
	}
	
	
	function get_document_by_id($id_document){
		$sql=" SELECT * FROM wb_document 
			   WHERE id_document='$id_document'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	function save_document($id_document,$document,$file_name,$id_pembelajaran,$type,$path,$id_user){
		if($file_name){
			$sql="insert into wb_document (id_document,document,file_name,id_pembelajaran,type,path,created_by,created_date)
				  values('$id_document','$document','$file_name','$id_pembelajaran','$type','$path','$id_user',now())";	
		}else{
			$sql="insert into wb_document (id_document,document,type,created_by,created_date)
			  values('$id_document','$document','$type','$id_user',now())";
		}	
		
		$return =  $this->db->query($sql);
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil simpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}	
	}
	
	function update_document($id_document,$document,$file_name,$type,$path,$id_user){
		if($file_name){
		$sql="update wb_document set document='$document',file_name='$file_name',
			  type='$type',path='$path',updated_by='$id_user',updated_date=now()
			  where id_document = '$id_document'";
		}else{
			$sql="update wb_document set document='$document',
			  type='$type',updated_by='$id_user',updated_date=now()
			  where id_document = '$id_document'";
		}	  
		$return =  $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil diupdate'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal diupdate'));		
		}	
	}
	
	function delete_document($id_document){
	   $doc  = $this->document_model->get_document_by_id($id_document);
	   $path = $doc['path'];
		$sql="DELETE FROM wb_document
			  WHERE id_document='$id_document'";
		$return =  $this->db->query($sql);	

		if($return){
			@unlink($path);
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil didelete'));			
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal didelete'));		
		}		  
	}
	
	public function get_document_class($id_class,$type){
		$sql="	SELECT * FROM wb_document_class a, wb_document b, wb_periode c
				WHERE  a.`id_document`=b.`id_document`
				AND a.`id_periode`=c.`id_periode`
				AND b.type='$type'
				AND a.id_class='$id_class'
				ORDER BY a.created_date desc";
		return $this->db->query($sql);
	}
	function get_class_periode_document($id_document_class){
		$sql="	SELECT * FROM wb_document_class a, wb_document b, wb_periode c
				WHERE  a.`id_document`=b.`id_document`
				AND a.`id_periode`=c.`id_periode`
				AND a.id_document_class='$id_document_class'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	function download_document($id_document){
		$sql="	SELECT * FROM wb_document a
				WHERE a.id_document='$id_document'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	public function get_document_class_periode($id_class,$type,$id_periode){
		$sql="	SELECT * FROM wb_document_class a, wb_document b
				WHERE  a.`id_document`=b.`id_document`
				AND b.type='$type'
				AND a.id_class='$id_class'
				AND a.id_periode='$id_periode'
				ORDER BY a.created_date desc";
		return $this->db->query($sql);
	}
	
	function update_document_class($id_document_class,$id_document,$id_periode,$id_user){
		$sql ="update wb_document_class set id_document ='$id_document',id_periode='$id_periode',
			   updated_by='$id_user',updated_date =now()
			   where id_document_class='$id_document_class' ";
		$return =  $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil diupdate'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal diupdate'));		
		}	   
	}
	
	function save_document_class($id_class,$id_document,$id_periode,$id_user){
		$sql ="insert into wb_document_class(id_class,id_document,id_periode,created_by,created_date)
			   VALUES('$id_class','$id_document','$id_periode','$id_user',now())";
		$return =  $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function save_document_class_new($id_class,$id_document,$id_periode,$id_user){
		$sql ="insert into wb_document_class(id_class,id_document,id_periode,created_by,created_date)
			   VALUES('$id_class','$id_document','$id_periode','$id_user',now())";
		$return =  $this->db->query($sql);	
		if($return){
			echo "success";		
		}else{
			echo "failed";		
		}
	}
	
	function delete_document_class($id_document_class){
		$sql = "delete from wb_document_class where id_document_class ='$id_document_class'";
		$return =  $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	/*
	public function get_jenis_materi_cls($id_materi)
	{
		$sql="	select * from wb_jenis_materi
				where id_materi='$id_materi'
				order by created_date";
		return $this->db->query($sql);
	}
	
	public function get_document_periode($id_periode,$type)
	{
		$sql="	select * from wb_document
				where id_periode='$id_periode'
				and type='$type'
				order by created_date";
		return $this->db->query($sql);
	}
	
	public function get_document_periode_class($type,$id_periode)
	{
		$sql="	select * from wb_document
				where id_periode='$id_periode'
				and type='$type'
				order by created_date";
		return $this->db->query($sql);
	}
	
	public function get_materi_periode_class($id_pembelajaran)
	{
		$sql="	select * from wb_materi
				where id_pembelajaran = '$id_pembelajaran'
				order by created_date";
		return $this->db->query($sql);
	}
	
	public function get_document_class($id_pembelajaran,$type)
	{
		$sql="	select * from wb_document a,wb_periode b
				where a.id_pembelajaran='$id_pembelajaran'
				and a.id_periode=b.id_periode
				and a.type='$type'
				order by a.created_date";
		return $this->db->query($sql);
	}
	
	public function get_all_document_class($type)
	{
		$sql="	select xx.*,yy.class,ww.nama from (
					select a.id_document,a.type,a.document,a.file_name,a.path,c.id_pembelajaran,periode 
					from wb_document a,
						wb_periode c
					where a.id_periode=c.id_periode
					) xx,wb_class yy,wb_users zz,wb_pegawai ww
				 where xx.id_pembelajaran=yy.id_pembelajaran
				and yy.instruktur1=zz.id_user
				and ww.nip=zz.nip
				and type='$type'";
		return $this->db->query($sql);
	}
	
	public function get_all_document()
	{
		$sql="	select xx.*,yy.class,ww.nama from (
					select a.id_document,a.type,a.document,a.file_name,a.path,c.id_pembelajaran,periode,c.end_date
					from wb_document a,
						wb_periode c
					where a.id_periode=c.id_periode
					) xx,wb_class yy,wb_users zz,wb_pegawai ww
				 where xx.id_pembelajaran=yy.id_pembelajaran
				and yy.instruktur1=zz.id_user
				and ww.nip=zz.nip";
		return $this->db->query($sql);
	}
	
	public function get_document_class_periode($id_pembelajaran,$id_periode,$type)
	{
		$sql="	select * from wb_document
				where id_periode='$id_periode'
				and id_pembelajaran='$id_pembelajaran'
				and type='$type'
				order by created_date";
		return $this->db->query($sql);
	}
	
	
	function get_list_document(){
		$sql = "SELECT * FROM wb_document";
		return $this->db->query($sql);
	}
	
	
	function get_document_by_id($id){
		$sql = "SELECT * FROM wb_document
				WHERE id_document='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function delete_document($id_document){
	   $doc  = $this->document_model->get_document_by_id($id_document);
	   $path = $doc['path'];
		$sql="DELETE FROM wb_document
			  WHERE id_document='$id_document'";
		$return =  $this->db->query($sql);	

		if($return){
			@unlink($path);
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil didelete'));			
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal didelete'));		
		}		  
	}

	function save_document($id_document,$document,$file_name,$id_pembelajaran,$id_periode,$type,$path,$id_user){
		if($file_name){
			$sql="insert into wb_document (id_document,document,file_name,id_pembelajaran,id_periode,type,path,created_by,created_date)
				  values('$id_document','$document','$file_name','$id_pembelajaran','$id_periode','$type','$path','$id_user',now())";	
		}else{
			$sql="insert into wb_document (id_document,document,id_periode,type,created_by,created_date)
			  values('$id_document','$document','$id_periode','$type','$id_user',now())";
		}	
		
		$return =  $this->db->query($sql);
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil simpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}	
	}
	
	function update_document($id_document,$document,$file_name,$id_periode,$type,$path,$id_user){
		if($file_name){
		$sql="update wb_document set document='$document',id_periode='$id_periode',file_name='$file_name',
			  type='$type',path='$path',updated_by='$id_user',updated_date=now()
			  where id_document = '$id_document'";
		}else{
			$sql="update wb_document set document='$document',id_periode='$id_periode',
			  type='$type',updated_by='$id_user',updated_date=now()
			  where id_document = '$id_document'";
		}	  
		$return =  $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil diupdate'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal diupdate'));		
		}	
	}*/
}
