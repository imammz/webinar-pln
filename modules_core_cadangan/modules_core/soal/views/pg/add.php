<!--<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>-->
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div>
<div id="before-edit-pg"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="add_myform_pg" style="display:none">
	<div id="add_message-pg"></div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Peratanyaan</label>
		<textarea class="md-input" id="pertanyaan" name="pertanyaan"></textarea> 
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan A</label>
		<input  type="text" class="md-input" id="pilihan_a" name="pilihan_a">
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan B</label>
		<input type="text" class="md-input" id="pilihan_b" name="pilihan_b">  
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan C</label>
		<input type="text" class="md-input" id="pilihan_c" name="pilihan_c">  
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan D</label>
		<input type="text" class="md-input" id="pilihan_d" name="pilihan_d">  
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan E</label>
		<input type="text" class="md-input" id="pilihan_e" name="pilihan_e">  
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Jawaban</label>
	</div>	
	<div class="uk-margin-medium-bottom">
		A<input type="radio" class="md-input" id="jawaban" name="jawaban" value="a" />&nbsp;
		B<input type="radio" class="md-input" id="jawaban" name="jawaban" value="b" />&nbsp;
		C<input type="radio" class="md-input" id="jawaban" name="jawaban" value="c" />&nbsp;
		D<input type="radio" class="md-input" id="jawaban" name="jawaban" value="d" />&nbsp;
		E<input type="radio" class="md-input" id="jawaban" name="jawaban" value="e" />&nbsp;
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_pembelajaran" value="<?=$id_pembelajaran?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#add_myform_pg").show(); 
				$("#before-edit-pg").hide();
			}, 100);
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#add_myform_pg').validate({
				rules:{
						pertanyaan:{
							required:true
						},
						pilihan_a:{
							required:true
						},
						pilihan_b:{
							required:true
						},
						pilihan_c:{
							required:true
						},
						pilihan_d:{
							required:true
						},
						pilihan_e:{
							required:true
						},
						jawaban:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$("#btnsave").prop('value', 'Process'); 
					$('input[type="submit"]').prop('disabled', true);
					$.post('<?php echo base_url('soal/save_pg'); ?>', $('#add_myform_pg').serialize(), function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#add_message-pg").html(msg);
							
							$('#add_message-pg').fadeTo(3000, 500).slideUp(500, function(){
								$('#add_message-pg').hide();
								reload_pg();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#add_message-pg").html(msg);
							$("#btnsave").prop('value', 'Save'); 
							$('input[type="submit"]').prop('disabled', false);
						}
					});
				}
			})
		});
</script>
