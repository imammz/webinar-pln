<!---CONTENT START-->	
   <div id="page_content">
        <div id="page_content_inner">
			<h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large">
                    <div class="md-card">
                        <div class="user_content" id="user_contents">
                             <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1'}">
                                <li><a href="#">Pilihan Ganda</a>
									<div align="center" id="loading_pg">
										<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/>
									</div>
								</li>
								<li><a href="#">Essay</a>
									<div align="center" id="loading_essay">
										<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/>
									</div>
								</li>
                            </ul>
                            <ul id="tabs_1" class="uk-switcher uk-margin">
							
							<!--pg-->
								<li>
									<div id="data_pg"></div>	
								</li>

							<!--end pg-->
							
							<!-- essay -->
                                <li>
									<div id="data_essay"></div>	
								</li>
							<!-- end essay -->
								
                            </ul>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>  
	<!-- common functions -->
    <script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?=base_url()?>front_assets/assets/js/uikit_custom.min.js"></script>
	<script>
		var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/></div>';
		$(document).ready(function() {
			reload_pg();
			reload_essay();
				
		});
		
		function reload_pg(){
			$('#data_pg').html(loading);
			$.post("<?php echo base_url().'soal/list_pg'?>",{id_pembelajaran:'<?=$id_pembelajaran?>',jenis:'pg'},function(data){
				$('#data_pg').html(data);
				$('#loading_pg').hide();
			});
		}

		function reload_essay(){
			$('#data_essay').html(loading);
			$.post("<?php echo base_url().'soal/list_essay'?>",{id_pembelajaran:'<?=$id_pembelajaran?>',jenis:'essay'},function(data){
				$('#data_essay').html(data);
				$('#loading_essay').hide();
			});
		}	
		
		
	</script>
<!---CONTENT END-->