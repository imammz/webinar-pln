<?php

class soal_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	function get_all_soal($id_class,$periode,$jenis_soal){
		 $sql ="SELECT a.*,d.nama_pembelajaran
				FROM wb_bank_soal a,wb_pembelajaran d
				WHERE a.id_pembelajaran =d.id_pembelajaran
				AND a.jenis_soal='$jenis_soal'
				AND a.`id_bank_soal` NOT IN 
				(
					SELECT b.id_bank_soal FROM `wb_soal_class` b, wb_bank_soal c
					WHERE b.`id_bank_soal`=c.`id_bank_soal`
					AND b.id_class='$id_class'
					AND id_periode='$periode'
					AND c.`jenis_soal`='$jenis_soal'		
				)";
		return $this->db->query($sql);		
	}
	
	function get_soal_class_periode($id_class,$periode,$jenis_soal){
		$sql =" SELECT b.`id_soal_class`,c.`class`,d.`periode`,d.`id_periode`,a.*
				FROM wb_bank_soal a, wb_soal_class b,wb_class c,wb_periode d
				WHERE a.`id_bank_soal` =b.`id_bank_soal`
				AND b.`id_class`=c.`id_class`
				AND b.`id_periode`=d.`id_periode`
				and c.id_class='$id_class'
				and d.id_periode='$periode'
				and a.jenis_soal='$jenis_soal'";
		return $this->db->query($sql);		
	}
	
	function get_soal_class($id_class,$jenis_soal){
		$sql =" SELECT b.`id_soal_class`,e.`class_name`,d.`periode`,d.`id_periode`,a.*
				FROM wb_bank_soal a, wb_soal_class b,wb_class c,wb_periode d,wb_class_name e
				WHERE a.`id_bank_soal` =b.`id_bank_soal`
				AND b.`id_class`=c.`id_class`
				AND b.`id_periode`=d.`id_periode`
				and c.id_class_name=e.id_class_name
				and c.id_class='$id_class'
				and a.jenis_soal='$jenis_soal'";
		return $this->db->query($sql);		
	}
	function get_list_soal(){
		$sql = "SELECT b.id_pembelajaran,a.* 
				FROM wb_bank_soal a, wb_pembelajaran b
				WHERE a.`id_pembelajaran`=b.id_pembelajaran
				ORDER BY a.id_pembelajaran,a.jenis_soal";
		return $this->db->query($sql);
	}
	
	function get_soal_pembelajaran($id_pembelajaran,$jenis_soal){
		$sql = "SELECT *
				FROM wb_bank_soal 
				WHERE id_pembelajaran='$id_pembelajaran'
				and jenis_soal='$jenis_soal'
				order by created_date desc";
		return $this->db->query($sql);
	}
	
	
	function update_bank_soal($id_bank_soal,$id_pembelajaran,$jenis_soal,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="update wb_bank_soal set jenis_soal='$jenis_soal',pertanyaan='$pertanyaan ',a='$a' ,b='$b', 				
			  c='$c',d='$d',e='$e', jawaban_pg='$jawaban_pg',jawaban_essay='$jawaban_essay',updated_by='$id_user',updated_date=now()
			  where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function get_bank_soal_by_id($id_bank_soal){
    	$sql = "select b.* from wb_bank_soal b
				where b.id_bank_soal='$id_bank_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	function save_soal_class($id_class,$id_bank_soal,$id_periode,$id_user){
		$sql = "DELETE from wb_soal_class
				WHERE id_class='$id_class'
				AND id_periode='$id_periode'
				AND id_bank_soal='$id_bank_soal'"; 
		$this->db->query($sql);
		
		$sql ="INSERT INTO wb_soal_class(id_class,id_periode,id_bank_soal,created_by,created_date)
			   values('$id_class','$id_periode','$id_bank_soal','$id_user',now())";
		$return = $this->db->query($sql);
		if($return){
			echo 'success';		
		}else{
			echo 'failed';		
		}	   
	}
	
	function delete_soal_class($id){
		$sql ="DELETE from wb_soal_class
			   where id_soal_class='$id'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}	   
	}
	
	////
	/*
	function get_list_soal_instruktur($id_user){
		$sql = "select a.*,b.class,c.periode,b.created_by,b.instruktur1,b.instruktur2
				from wb_soal a, wb_class b, wb_periode c
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and (instruktur1='$id_user' or instruktur2='$id_user')";
		return $this->db->query($sql);
	}
	
	
	function get_soal_instruktur_by_id($id_pembelajaran){
		$sql = "select a.*,b.class,c.periode,b.created_by,b.instruktur1,b.instruktur2
				from wb_soal a, wb_class b, wb_periode c
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_pembelajaran='$id_pembelajaran'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_list_bank_soal($id_pembelajaran,$categori){
		$sql = "select b.*,a.soal from wb_soal a, wb_bank_soal b
				where a.id_pembelajaran=b.id_pembelajaran
				and b.id_pembelajaran='$id_pembelajaran'
				and b.jenis_soal='$categori'";
		return $this->db->query($sql);
	}
	
	function get_list_bank_soal_lmt($id_pembelajaran,$categori,$num,$offset){
		$sql = "select b.*,a.soal from wb_soal a, wb_bank_soal b
				where a.id_pembelajaran=b.id_pembelajaran
				and b.id_pembelajaran='$id_pembelajaran'
				and b.jenis_soal='$categori'
				limit $offset,$num";
		return $this->db->query($sql);
	}
	
	
	
	function get_soal_class_periode($id_class,$id_periode){
		$sql ="select * from wb_soal 
		      where id_class='$id_class'
		      and id_periode='$id_periode'";
		return $this->db->query($sql);
	}
	
	function get_soal_class($id_class){
		$sql ="select a.*,b.class from wb_soal a,wb_class b 
		      where a.id_class=b.id_class
			  and b.status='active'
			  and a.id_class='$id_class'";
		return $this->db->query($sql);
	}
	
	
	function save_soal($id_pembelajaran,$soal,$id_class,$id_periode,$id_user){
		$sql="insert into wb_soal(id_pembelajaran,soal,id_class,id_periode,created_by,created_date)
			  values('$id_pembelajaran','$soal','$id_class','$id_periode','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_soal($id_pembelajaran,$soal,$id_class,$id_periode,$id_user){
		$sql="update wb_soal set soal='$soal',id_class='$id_class',id_periode='$id_periode',updated_by='$id_user',updated_date=now()
			  where id_pembelajaran='$id_pembelajaran'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	
	function delete_soal($id_pembelajaran){
		$sql="delete from wb_soal where id_pembelajaran='$id_pembelajaran'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	
	function save_bank_soal($id_pembelajaran,$jenis_soal,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="insert into wb_bank_soal(id_pembelajaran,jenis_soal,pertanyaan,a,b,c,d,e,jawaban_pg,jawaban_essay,created_by,created_date)
			  values('$id_pembelajaran','$jenis_soal','$pertanyaan','$a','$b','$c','$d','$e','$jawaban_pg','$jawaban_essay','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	
	
	function delete_bank_soal($id_bank_soal){
		$sql="delete from wb_bank_soal where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal sihapus'));		
		}
	}*/
	
}
