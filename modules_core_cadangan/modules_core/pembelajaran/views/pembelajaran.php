
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
								<th rowspan="2">Kode Pembelajaran</th>
                                <th colspan="2">Information</th>
                            </tr>
                            <tr>
                                <th>Nama Pembelajaran</th>
								<th width="100px">Action</th>
							</tr>
                        </thead>

                        <tbody>
					<?php foreach ($pembelajaran->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->id_pembelajaran?></td>
							<td><?=$row->nama_pembelajaran?></td>
                            <td>
								<a href="#" onclick="detail_pembelajaran('<?=$row->id_pembelajaran?>','<?=$row->nama_pembelajaran?>')" class="uk-icon-button uk-icon-cogs" title="List pembelajaran <?=$row->nama_pembelajaran?>"></a>&nbsp;
								<a href="#" onclick="detail_file('<?=$row->id_pembelajaran?>','<?=$row->nama_pembelajaran?>')" class="uk-icon-button uk-icon-file" title="List File <?=$row->nama_pembelajaran?>"></a>&nbsp;
								<a href="#edit_pembelajaran" data-uk-modal="{ center:true }" onclick="edits('<?=$row->id_pembelajaran?>')" class="uk-icon-button uk-icon-edit" title="Edit <?=$row->nama_pembelajaran?>"></a>&nbsp;
								<a href="#" onclick="deleted('<?=$row->id_pembelajaran?>')"class="uk-icon-button uk-icon-trash-o" title="Delete <?=$row->nama_pembelajaran?>"></a>
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_pembelajaran" onclick="add()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_pembelajaran">
			<div id="modal-add"></div>
		</div>
		<div class="uk-modal" id="edit_pembelajaran">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id_pembelajaran,id_class){
				uri = '<?=base_url()."pembelajaran/edit"?>';
				$('#new_pembelajaran').hide();
				$('#edit_pembelajaran').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id_pembelajaran:id_pembelajaran,id_class:id_class},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(){
				uri = '<?=base_url()."pembelajaran/add"?>';
				$('#edit_pembelajaran').hide();
				$('#new_pembelajaran').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id_pembelajaran){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."pembelajaran/delete"?>';
					$.post(uri,{ajax:true,id_pembelajaran:id_pembelajaran},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
			function detail_pembelajaran(id_pembelajaran,nama_pembelajaran){
				$('#list_data').html(loading);
				$.post('<?=base_url()."pembelajaran/detail"?>',{id_pembelajaran:id_pembelajaran,nama_pembelajaran:nama_pembelajaran},function(data) 
				{
					$('#list_data').html(data);
				});
			}
			function detail_file(id_pembelajaran,nama_pembelajaran){
				$('#list_data').html(loading);
				$.post('<?=base_url()."document"?>',{id_pembelajaran:id_pembelajaran,nama_pembelajaran:nama_pembelajaran},function(data) 
				{
					$('#list_data').html(data);
				});
			}
	</script>	
</html>