<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
	<div id="add_message"></id>
</div>
<div id="before-add-pembelajaran"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="add_myform" style="display:none">
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Kode Pembelajaran</label>
		<input type="text" class="md-input" id="id_pembelajaran" name="id_pembelajaran"/>
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Nama Pembelajaran</label>
		<input type="text" class="md-input" id="nama_pembelajaran" name="nama_pembelajaran"/>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#add_myform").show(); 
				$("#before-add-pembelajaran").hide();
			}, 100);
			
			function reloadlist(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",function(data){
					$('#list_data').html(data);
				});
			}
			
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#add_myform').validate({
				rules:{
						nama_pembelajaran:{
							required:true
						},
						id_pembelajaran:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$("#btnsave").prop('value', 'Process...'); 
					$('input[type="submit"]').prop('disabled', true);
					$.post('<?php echo base_url('pembelajaran/save'); ?>', $('#add_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						$('#btnsave').attr('disabled');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#add_message").html(msg);
							
							$('#add_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#add_message').hide();
								$('#add_user').hide();
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#add_message").html(msg);
							$("#btnsave").prop('value', 'Save'); 
							$('input[type="submit"]').prop('disabled', false);
						}
					});
				}
			})
		});
</script>
