<?php

class pembelajaran_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	function get_list_soal(){
		$sql = "SELECT b.id_pembelajaran,a.* 
				FROM wb_bank_soal a, wb_pembelajaran b
				WHERE a.`id_pembelajaran`=b.id_pembelajaran
				ORDER BY a.id_pembelajaran,a.jenis_soal";
		return $this->db->query($sql);
	}
	
	function get_list_pembelajaran(){
		$sql = "SELECT b.* 
				FROM wb_pembelajaran b";
		return $this->db->query($sql);
	}
	function save_pembelajaran($id_pembelajaran, $nama_pembelajaran, $id_user){
		$sql="insert into wb_pembelajaran(id_pembelajaran,nama_pembelajaran,created_by,created_date)
			  values('$id_pembelajaran','$nama_pembelajaran','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_pembelajaran($id_pembelajaran_old,$id_pembelajaran, $nama_pembelajaran, $id_user){
		$sql="update wb_pembelajaran set id_pembelajaran='$id_pembelajaran',nama_pembelajaran='$nama_pembelajaran',
			  updated_by='$id_user',updated_date=now()
			  where id_pembelajaran='$id_pembelajaran_old'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function delete_pembelajaran($id_pembelajaran){
		$sql="delete from wb_pembelajaran where id_pembelajaran='$id_pembelajaran'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil delete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal delete'));		
		}
	}
	
	function get_pembelajaran_by_id($id_pembelajaran){
		$sql = "select * from wb_pembelajaran where id_pembelajaran='$id_pembelajaran'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	/*
	////
	function get_list_soal_instruktur($id_user){
		$sql = "select a.*,b.class,c.periode,b.created_by,b.instruktur1,b.instruktur2
				from wb_soal a, wb_class b, wb_periode c
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and (instruktur1='$id_user' or instruktur2='$id_user')";
		return $this->db->query($sql);
	}
	
	
	function get_soal_instruktur_by_id($id_soal){
		$sql = "select a.*,b.class,c.periode,b.created_by,b.instruktur1,b.instruktur2
				from wb_soal a, wb_class b, wb_periode c
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_soal='$id_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_list_bank_soal($id_soal,$categori){
		$sql = "select b.*,a.soal from wb_soal a, wb_bank_soal b
				where a.id_soal=b.id_soal
				and b.id_soal='$id_soal'
				and b.categori_quiz='$categori'";
		return $this->db->query($sql);
	}
	
	function get_list_bank_soal_lmt($id_soal,$categori,$num,$offset){
		$sql = "select b.*,a.soal from wb_soal a, wb_bank_soal b
				where a.id_soal=b.id_soal
				and b.id_soal='$id_soal'
				and b.categori_quiz='$categori'
				limit $offset,$num";
		return $this->db->query($sql);
	}
	
	function get_bank_soal_by_id($id_bank_soal){
		$sql = "select b.* from wb_bank_soal b
				where b.id_bank_soal='$id_bank_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	function get_soal_class_periode($id_class,$id_periode){
		$sql ="select * from wb_soal 
		      where id_class='$id_class'
		      and id_periode='$id_periode'";
		return $this->db->query($sql);
	}
	
	function get_soal_class($id_class){
		$sql ="select a.*,b.class from wb_soal a,wb_class b 
		      where a.id_class=b.id_class
			  and b.status='active'
			  and a.id_class='$id_class'";
		return $this->db->query($sql);
	}
	
	
	function save_soal($id_soal,$soal,$id_class,$id_periode,$id_user){
		$sql="insert into wb_soal(id_soal,soal,id_class,id_periode,created_by,created_date)
			  values('$id_soal','$soal','$id_class','$id_periode','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_soal($id_soal,$soal,$id_class,$id_periode,$id_user){
		$sql="update wb_soal set soal='$soal',id_class='$id_class',id_periode='$id_periode',updated_by='$id_user',updated_date=now()
			  where id_soal='$id_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	
	function delete_soal($id_soal){
		$sql="delete from wb_soal where id_soal='$id_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	
	function save_bank_soal($id_soal,$categori_quiz,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="insert into wb_bank_soal(id_soal,categori_quiz,pertanyaan,a,b,c,d,e,jawaban_pg,jawaban_essay,created_by,created_date)
			  values('$id_soal','$categori_quiz','$pertanyaan','$a','$b','$c','$d','$e','$jawaban_pg','$jawaban_essay','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_bank_soal($id_bank_soal,$id_soal,$categori_quiz,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="update wb_bank_soal set categori_quiz='$categori_quiz',pertanyaan='$pertanyaan ',a='$a' ,b='$b', 				
			  c='$c',d='$d',e='$e', jawaban_pg='$jawaban_pg',jawaban_essay='$jawaban_essay',updated_by='$id_user',updated_date=now()
			  where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function delete_bank_soal($id_bank_soal){
		$sql="delete from wb_bank_soal where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal sihapus'));		
		}
	}*/
	
}
