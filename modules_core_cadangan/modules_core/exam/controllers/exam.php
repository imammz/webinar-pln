<?php
class Exam extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->load->model(array(
            'exam/exam_model',
            'pegawai/pegawai_model',
            'periode/periode_model',
            'document/document_model',
            'class_m/class_model',
            'soal/soal_model'
        ));
        $this->load->library('pagination');
    }
    
    public function index()
    {
        $this->login_model->is_login();
        $data['title'] = 'Master Exam';
        $data['link']  = base_url() . "exam/list_data";
        $this->load->view('preview', $data);
    }
    
    public function list_data()
    {
        $this->login_model->is_login();
        $id_user       = $this->session->userdata("sesi_id_user");
        $data['exam']  = $this->exam_model->get_list_exam_perserta($id_user);
        $data['title'] = 'List Exam';
        $data['link']  = base_url() . "exam/list_data";
        $this->load->view('exam', $data);
    }
    
    public function add()
    {
        $this->login_model->is_login();
        $id_user       = $this->session->userdata("sesi_id_user");
        //$data['id_Exam'] = $this->input->post('id_Exam');
        $data['class'] = $this->class_model->get_list_class_inst($id_user);
        $data['title'] = 'Add Exam';
        $data['link']  = base_url() . "exam/list_data";
        $this->load->view('add', $data);
    }
    
    public function edit()
    {
        $this->login_model->is_login();
        $id_user         = $this->session->userdata("sesi_id_user");
        $id_Exam         = $this->input->post('id_Exam');
        $id_class        = $this->input->post('id_class');
        $data['Exam']    = $this->exam_model->get_Exam_instruktur_by_id($id_Exam);
        $data['periode'] = $this->periode_model->get_periode_class($id_class);
        $data['class']   = $this->class_model->get_list_class_inst($id_user);
        $data['title']   = 'Edit Nama Exam';
        $data['link']    = base_url() . "exam/list_data";
        $this->load->view('edit', $data);
    }
    
    public function update()
    {
        $this->login_model->is_login();
        $id_Exam    = $this->input->post('id_Exam');
        $Exam       = $this->input->post('Exam');
        $id_class   = $this->input->post('id_class');
        $id_periode = $this->input->post('id_periode');
        $id_user    = $this->session->userdata("sesi_id_user");
        $this->exam_model->update_Exam($id_Exam, $Exam, $id_class, $id_periode, $id_user);
    }
    
    public function save()
    {
        $this->login_model->is_login();
        $id_Exam    = 'SL' . date('ymdHis');
        $id_class   = $this->input->post('id_class');
        $id_periode = $this->input->post('id_periode');
        $Exam       = $this->input->post('Exam');
        $id_user    = $this->session->userdata("sesi_id_user");
        
        $this->exam_model->save_Exam($id_Exam, $Exam, $id_class, $id_periode, $id_user);
    }
    
    function get_periode_class()
    {
        $id_class = $this->input->get('id_class');
        $data     = $this->periode_model->get_periode_class($id_class);
        $arrkab   = array();
        foreach ($data->result() as $list) {
            array_push($arrkab, $list);
        }
        echo json_encode($arrkab);
        exit;
    }
    
    function get_exam()
    {
        $id_class   = $this->input->get('id_class');
        $id_periode = $this->input->get('id_periode');
        $data       = $this->exam_model->get_Exam_class_periode($id_class, $id_periode);
        $arrkab     = array();
        foreach ($data->result() as $list) {
            array_push($arrkab, $list);
        }
        echo json_encode($arrkab);
        exit;
    }
    
    public function delete()
    {
        $this->login_model->is_login();
        $id_Exam = $this->input->post('id_Exam');
        $this->exam_model->delete_Exam($id_Exam);
    }
    
    public function detail()
    {
        $this->login_model->is_login();
        $durasi = '';
        
        $id_soal = $this->fungsi->decrypt($this->uri->segment(3));
        $id_user = $this->session->userdata("sesi_id_user");
        
        $data['exam']  = $this->exam_model->get_ujian_soal($id_soal);
        $data['class'] = $this->class_model->get_class_by_id($data['exam']['id_class']);
       // $last          = $this->exam_model->get_last_time($id_user, $data['exam']['id_ujian'], $data['exam']['sesion']);
        //if ($last) {
        //   $durasi = $last['sisa_waktu'];
        //} else {
          $durasi = $data['exam']['durasi'];
        //}

        $last   = $this->exam_model->get_last_ujian($id_user,$data['exam']['id_ujian']);
		if($last){
			$durasi=0;
		}else{
			$this->exam_model->save_last_ujian($id_user,$data['exam']['id_ujian']);
		}

        $data['durasi']  = $durasi;
        $data['title']   = 'Ujian '.$data['class']['class'].' Sesion '.$data['exam']['periode'];
        $data['link']    = base_url() . "exam/detail";
        $data['id_soal'] = $id_soal;
        $this->load->view('detail', $data);
    }
    
    
    public function list_pg()
    {
        $this->login_model->is_login();
        $id_soal         = $this->input->post('id_soal');
        $page            = $this->input->post('page');
        $perpage         = 1;
        $data['total']   = $this->soal_model->get_list_bank_soal($id_soal, 'pg')->num_rows();
        $data['soal']    = $this->soal_model->get_list_bank_soal_lmt($id_soal, 'pg', $perpage, $page == '' ? 0 : $page);
        $data['page']    = $page;
        $data['title']   = 'Master PG';
        $data['id_soal'] = $id_soal;
        $data['link']    = base_url() . "exam/list_pg";
        $this->load->view('pg/pg', $data);
    }
    
    public function list_essay()
    {
        $this->login_model->is_login();
        $id_soal         = $this->input->post('id_soal');
        $page            = $this->input->post('page');
        $perpage         = 1;
        $data['total']   = $this->soal_model->get_list_bank_soal($id_soal, 'essay')->num_rows();
        $data['soal']    = $this->soal_model->get_list_bank_soal_lmt($id_soal, 'essay', $perpage, $page == '' ? 0 : $page);
        $data['page']    = $page;
        $data['title']   = 'Master Essay';
        $data['id_soal'] = $id_soal;
        $data['link']    = base_url() . "exam/list_essay";
        $this->load->view('essay/essay', $data);
    }
    function last_time()
    {
        $id_ujian = $this->input->post('id_ujian');
        $sesion   = $this->input->post('sesion');
        $time     = $this->input->post('times');
        $time     = substr($time, 0, strpos($time, ":")) + substr($time, -2) / 60;
        $id_user  = $this->session->userdata("sesi_id_user");
        $this->exam_model->save_last_time($id_user, $id_ujian, $sesion, $time);
        sleep(3);
        
    }
    
    function response()
    {
        $id_soal      = $this->input->post('id_soal');
        $id_bank_soal = $this->input->post('id_bank_soal');
        $jenis        = $this->input->post('jenis');
        $jawab        = $this->input->post('jawab');
        $id_user      = $this->session->userdata('sesi_id_user');
        
        $this->exam_model->jawab($id_soal, $id_bank_soal, $jenis, $jawab, $id_user);
    }
	
	function score(){
		$id_soal = $this->input->post('id_soal');
		$id_user = $this->session->userdata('sesi_id_user');
		$data['soal'] = $this->exam_model->get_ujian_soal($id_soal);
		$data['score_pg'] = $this->exam_model->score_pg($id_soal,$id_user);
		$data['score_essay'] = $this->exam_model->score_essay($id_soal,$id_user);
		$this->load->view('score',$data);
	}
    
}

?>