<?php

class exam_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	function get_list_exam(){
		$sql = "select d.id_soal,d.soal,a.id_class,a.class,b.id_periode,b.periode,c.durasi,c.sesion,c.start_date,
				case when c.start_date = DATE_FORMAT(now(),'%Y-%m-%d')
						then 'ongoing'
						 when c.start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
						 when  c.start_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
					end status
				 from wb_class a, wb_periode b, wb_ujian c, wb_soal d
				where a.id_class=b.id_class
				and b.id_periode=c.id_periode
				and b.id_class=c.id_class
				and c.id_soal=d.id_soal
				order by c.start_date";
		return $this->db->query($sql);
	}
	
	function get_list_exam_perserta($id_user){
		$sql = "select d.id_soal,d.soal,a.id_class,a.class,b.id_periode,b.periode,c.durasi,c.sesion,c.start_date,
				case when c.start_date = DATE_FORMAT(now(),'%Y-%m-%d')
						then 'ongoing'
						 when c.start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
						 when  c.start_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
					end status
				 from wb_class a, wb_periode b, wb_ujian c, wb_soal d,wb_peserta e
				where a.id_class=b.id_class
				and b.id_periode=c.id_periode
				and b.id_class=c.id_class
				and c.id_soal=d.id_soal
				and a.id_class=e.id_class
				and e.id_user='$id_user'
				order by c.start_date";
		return $this->db->query($sql);
	}
	
	function get_soal_instruktur_by_id($id_soal){
		$sql = "select a.*,b.class,c.periode,b.created_by,b.instruktur1,b.instruktur2
				from wb_soal a, wb_class b, wb_periode c
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_soal='$id_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_list_bank_soal($id_soal,$categori){
		$sql = "select b.*,a.soal from wb_soal a, wb_bank_soal b
				where a.id_soal=b.id_soal
				and b.id_soal='$id_soal'
				and b.categori_quiz='$categori'";
		return $this->db->query($sql);
	}
	
	function get_bank_soal_by_id($id_bank_soal){
		$sql = "select b.* from wb_bank_soal b
				where b.id_bank_soal='$id_bank_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	function get_soal_class_periode($id_class,$id_periode){
		$sql ="select * from wb_soal 
		      where id_class='$id_class'
		      and id_periode='$id_periode'";
		return $this->db->query($sql);
	}
	
	function get_soal_class($id_class){
		$sql ="select a.*,b.class from wb_soal a,wb_class b 
		      where a.id_class=b.id_class
			  and b.status='active'
			  and a.id_class='$id_class'";
		return $this->db->query($sql);
	}
	
	
	function save_soal($id_soal,$soal,$id_class,$id_periode,$id_user){
		$sql="insert into wb_soal(id_soal,soal,id_class,id_periode,created_by,created_date)
			  values('$id_soal','$soal','$id_class','$id_periode','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_soal($id_soal,$soal,$id_class,$id_periode,$id_user){
		$sql="update wb_soal set soal='$soal',id_class='$id_class',id_periode='$id_periode',updated_by='$id_user',updated_date=now()
			  where id_soal='$id_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	
	function delete_soal($id_soal){
		$sql="delete from wb_soal where id_soal='$id_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	
	function save_bank_soal($id_soal,$categori_quiz,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="insert into wb_bank_soal(id_soal,categori_quiz,pertanyaan,a,b,c,d,e,jawaban_pg,jawaban_essay,created_by,created_date)
			  values('$id_soal','$categori_quiz','$pertanyaan','$a','$b','$c','$d','$e','$jawaban_pg','$jawaban_essay','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_bank_soal($id_bank_soal,$id_soal,$categori_quiz,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="update wb_bank_soal set categori_quiz='$categori_quiz',pertanyaan='$pertanyaan ',a='$a' ,b='$b', 				
			  c='$c',d='$d',e='$e', jawaban_pg='$jawaban_pg',jawaban_essay='$jawaban_essay',updated_by='$id_user',updated_date=now()
			  where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function delete_bank_soal($id_bank_soal){
		$sql="delete from wb_bank_soal where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal sihapus'));		
		}
	}
	
	function get_ujian_soal($id_soal){
		$sql = "select a.*,b.class,c.periode,c.start_date,c.end_date,f.nama,f.jabatan
				from wb_ujian a,wb_class b,wb_periode c,wb_users e,wb_pegawai f
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_soal='$id_soal'
				and b.instruktur1=e.id_user
				and e.nip=f.nip";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
		
	}
	
	function save_last_time($id_user,$id_ujian){
		$sql = "select * from wb_ujian_history 
				where id_user='$id_user'
				and id_ujian='$id_ujian'";
		
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			$sql = "update wb_ujian_history set sisa_waktu ='$time'
					where id_user='$id_user'
					and id_ujian='$id_ujian'
					and sesion='$sesion'";
		}else{
			$sql = "insert into wb_ujian_history(id_user,id_ujian,sesion,sisa_waktu,created_date) values('$id_user','$id_ujian','$sesion','$time',now())";
		}	
			return $this->db->query($sql);
	}
	
	function save_last_ujian($id_user,$id_ujian){
		$sql = "insert into wb_ujian_history(id_user,id_ujian,created_date) values('$id_user','$id_ujian',now())";
		return $this->db->query($sql);
	}
	
	function get_last_ujian($id_user,$id_ujian){
		 $sql = "select * from wb_ujian_history 
				where id_user='$id_user'
				and id_ujian='$id_ujian'";
		$hasil = $this->db->query($sql);	
		//if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		//}
	}
	
	function jawab($id_soal,$id_bank_soal,$jenis,$jawab,$id_user){
		$sql = "delete from wb_jawaban
				where id_soal='$id_soal'
				and id_bank_soal='$id_bank_soal'
				and jenis='$jenis'
				and created_by='$id_user'";
		$this->db->query($sql);
		
		if($jenis=='pg'){			
			$sql = "insert into wb_jawaban(jenis,id_soal,id_bank_soal,jawaban_pg,created_by,created_date)
				    values('$jenis','$id_soal','$id_bank_soal','$jawab','$id_user',now())";
		}else{
			$sql = "insert into wb_jawaban(jenis,id_soal,id_bank_soal,jawaban_essay,created_by,created_date)
				    values('$jenis','$id_soal','$id_bank_soal','$jawab','$id_user',now())";
		}
		
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disipan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function get_jawaban_user($id_user,$jenis,$id_soal,$id_bank_soal){
		$sql = "select jawaban_pg,jawaban_essay from wb_jawaban 
				where created_by='$id_user'
				and jenis='$jenis'
				and id_soal='$id_soal'
				and id_bank_soal='$id_bank_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function score_pg($id_soal,$id_user){
		$sql ="select count(jawaban_pg) as total_soal,sum(total) as total_benar
			   from (select b.id_soal,a.jawaban_pg,b.jawaban_pg as jawab,
						case when a.jawaban_pg=b.jawaban_pg then 1
						else 0
						end as total
						 from wb_jawaban a, wb_bank_soal b
						where a.id_bank_soal=b.id_bank_soal
						and a.created_by='$id_user'
						and a.id_soal='$id_soal'
						and a.jenis='pg'
					)sx";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	function score_essay($id_soal,$id_user){
		$sql ="select count(jawaban_pg) as total_soal,sum(total) as total_benar
			   from (select b.id_soal,a.jawaban_pg,b.jawaban_pg as jawab,
						case when a.jawaban_pg=b.jawaban_pg then 1
						else 0
						end as total
						 from wb_jawaban a, wb_bank_soal b
						where a.id_bank_soal=b.id_bank_soal
						and a.created_by='$id_user'
						and a.id_soal='$id_soal'
						and a.jenis='essay'
					)sx";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
}
