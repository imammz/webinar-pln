<div id="message_pg"></div>
<div class="md-card">
	<div class="md-card-content">
		<div class="uk-grid">
			<div class="uk-width-1-1">
				<div class="uk-form-row">
			<?php	
				
				if($soal){
					foreach($soal->result() as $sl){
					$id_user = $this->session->userdata('sesi_id_user');
					$jawab   = $this->exam_model->get_jawaban_user($id_user,'pg',$id_soal,$sl->id_bank_soal);
			?>	
						<p><?=$sl->pertanyaan?></p>
						a. <input type="radio" id="jawaban" name="jawaban" value="a" <?php echo $jawab['jawaban_pg']=="a"? "checked":""?>/><?=$sl->a?></br>
						b. <input type="radio" id="jawaban" name="jawaban" value="b" <?php echo $jawab['jawaban_pg']=="b"? "checked":""?>/><?=$sl->b?></br>
						c. <input type="radio" id="jawaban" name="jawaban" value="c" <?php echo $jawab['jawaban_pg']=="c"? "checked":""?>/><?=$sl->c?></br>
						d. <input type="radio" id="jawaban" name="jawaban" value="d" <?php echo $jawab['jawaban_pg']=="d"? "checked":""?>/><?=$sl->d?></br>
						e. <input type="radio" id="jawaban" name="jawaban" value="e" <?php echo $jawab['jawaban_pg']=="e"? "checked":""?>/><?=$sl->e?></br>
						<input type="hidden" id="id_bank_soal" value="<?=$sl->id_bank_soal?>"/>
						<div style="margin-top:20px">
							<button class="md-btn md-btn md-btn-danger" onclick="jawab_pg()">Jawab</button/>
						</div>	
					</div>
					
					<?php }
				}?>

				<div align="center">
					<button class="md-btn md-btn md-btn-primary" onclick="prev('<?=$total?>','<?=$page-1?>')">Prev</button/><button class="md-btn md-btn md-btn-primary" onclick="next('<?=$total?>','<?=$page+1?>')">Next</button/>
				</div>

			</div>
		</div>
	</div>
</div>
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			
			function jawab_pg(){
				jawab = $('input[name="jawaban"]:checked').val();
				id_bank_soal = $('#id_bank_soal').val();
				var r = confirm("Apakah anda yakin?");
					if (r == true) {
						//return true;
						$.post("<?php echo base_url().'exam/response'?>",{id_soal:'<?=$id_soal?>',jenis:'pg',id_bank_soal:id_bank_soal,jawab:jawab},function(data){});
					}
			}
			
			function next(total,page){
				if(page>=total){
					$("#message_pg").html()
					var msg='<div class="uk-alert uk-alert-warning">Tidak ada soal lagi !</div>';
					$("#message_pg").html(msg);
				}else{
					$('#data_pg').html(loading);
					$.post("<?php echo base_url().'exam/list_pg'?>",{id_soal:'<?=$id_soal?>',jenis:'pg',page:page},function(data){
						$('#data_pg').html(data);
						$('#loading_pg').hide();
					});
					
				}
			}
			
			function prev(total,page){
				if(page>-1){
					$('#data_pg').html(loading);
					$.post("<?php echo base_url().'exam/list_pg'?>",{id_soal:'<?=$id_soal?>',jenis:'pg',page:page},function(data){
						$('#data_pg').html(data);
						$('#loading_pg').hide();
					});
				}else{
					$("#message_pg").html()
					var msg='<div class="uk-alert uk-alert-warning">Tidak ada soal sebelumnnya !</div>';
					$("#message_pg").html(msg);
				}
			}
			
			
	</script>