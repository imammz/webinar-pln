<div id="message_essay"></div>
<div class="md-card">
	<div class="md-card-content">
		<div class="uk-grid">
			<div class="uk-width-1-1">
				<div class="uk-form-row">
			<?php	
				if($soal){
				foreach($soal->result() as $sl){
				$id_user = $this->session->userdata('sesi_id_user');
				$jawab   = $this->exam_model->get_jawaban_user($id_user,'essay',$id_soal,$sl->id_bank_soal);
			?>	
					<p><?=$sl->pertanyaan?></p>
					<textarea name='jawaban_essay' id="jawaban_essay" cols="50" rows="10" placeholder="Jawaban anda"><?=$jawab['jawaban_essay']?></textarea></br>
					<div style="margin-top:20px">
						<button class="md-btn md-btn md-btn-danger" onclick="jawab_essay()">Jawab</button/>
					</div>
					<input type="hidden" id="id_bank" value="<?=$sl->id_bank_soal?>"/>
				</div>
				
				<?php }
				}?>
			
				<div align="center">
					<button class="md-btn md-btn md-btn-primary" onclick="prev_essay('<?=$total?>','<?=$page-1?>')">Prev</button/><button class="md-btn md-btn md-btn-primary" onclick="next_essay('<?=$total?>','<?=$page+1?>')">Next</button/>
				</div>	
			</div>
		</div>
	</div>
</div>
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			
			function jawab_essay(){
				jawab = $('#jawaban_essay').val();
				id_bank_soal = $('#id_bank').val();
				var r = confirm("Apakah anda yakin?"+jawab);
					if (r == true) {
						//return true;
						$.post("<?php echo base_url().'exam/response'?>",{id_soal:'<?=$id_soal?>',jenis:'essay',id_bank_soal:id_bank_soal,jawab:jawab},function(data){});
					}
			}
			
			function next_essay(total,page){
				if(page>=total){
					$("#message_essay").html()
					var msg='<div class="uk-alert uk-alert-warning">Tidak ada soal lagi !</div>';
					$("#message_essay").html(msg);
				}else{
					$('#data_essay').html(loading);
					$.post("<?php echo base_url().'exam/list_essay'?>",{id_soal:'<?=$id_soal?>',jenis:'essay',page:page},function(data){
						$('#data_essay').html(data);
						$('#loading_essay').hide();
					});
					
				}
			}
			
			function prev_essay(total,page){
				if(page>-1){
					$('#data_essay').html(loading);
					$.post("<?php echo base_url().'exam/list_essay'?>",{id_soal:'<?=$id_soal?>',jenis:'essay',page:page},function(data){
						$('#data_essay').html(data);
						$('#loading_essay').hide();
					});
				}else{
					$("#message_essay").html()
					var msg='<div class="uk-alert uk-alert-warning">Tidak ada soal sebelumnnya !</div>';
					$("#message_essay").html(msg);
				}
			}
			
			
	</script>