	<div class="uk-margin-medium-bottom">
		<h3 align="center">Hasil Test Ujian Kelas <?=$soal['class']?></h3>
	</div>
	<div class="uk-grid" data-uk-grid-margin>
		<div class="uk-width-small-1-5">
			<div class="uk-margin-bottom">
				<span class="uk-text-muted uk-text-small uk-text-italic">Fasilitator:</span>
				<address>
					<p><strong><?=$soal['nama']?></strong></p>
					<p>Pengajar 1</p>
					<p><?=$soal['jabatan']?></p>
				</address>
			</div>
		</div>
		<?php 
			if($score_pg['total_soal']==0){
				$score_pg['total_soal']=1;
			}
		?>
		<div class="uk-width-small-2-5">
			<span class="uk-text-muted uk-text-small uk-text-italic">Result Pilihan Ganda:</span>
			<p class="heading_b uk-text-success"><?=round(($score_pg['total_benar']/$score_pg['total_soal'])*100)?></p>
			<p class="uk-text-small uk-text-muted uk-margin-top-remove">
				<?php
					if(($score_pg['total_benar']/$score_pg['total_soal'])*100>=60){
						echo 'LULUS';
					}else{
						echo 'TIDAK LULUS';
					}
				?>
			</p>
		</div>
		<div class="uk-width-small-2-5">
			<span class="uk-text-muted uk-text-small uk-text-italic">Result Essay:</span>
			<p class="heading_b uk-text-success"></p>
			<p class="uk-text-small uk-text-muted uk-margin-top-remove">0</p>
		</div>
	</div>
	<div class="uk-grid uk-margin-large-bottom">
		<div class="uk-width-1-1">
			<div class="uk-width-small-1-10">
				<div class="uk-margin-bottom">
					<span class="uk-text-muted uk-text-small uk-text-italic">Kelas:</span>
					<address>
						<p><strong><?=$soal['class']?></strong></p>
					</address>
				</div>
			</div>
		</div>
		<div class="uk-width-1-1">
			<div class="uk-width-small-1-10">
				<div class="uk-margin-bottom">
					<span class="uk-text-muted uk-text-small uk-text-italic">Periode:</span>
					<address>
						<p><strong><?=$soal['periode']?></strong></p>
					</address>
					<div class="uk-margin-medium-bottom">
								<span class="uk-text-muted uk-text-small uk-text-italic">Start Data:</span> <?=$soal['start_date']?>
								<br/>
								<span class="uk-text-muted uk-text-small uk-text-italic">End Date:</span>  <?=$soal['end_date']?>
					</div>
				</div>
			</div>
		</div>
	</div>