
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2" width="200px">Soal</th>
                                <th colspan="4">Information</th>
                            </tr>
                            <tr>
                                <th>Class</th>
                                <th>Periode</th>
								<th>Durasi</th>
                                <th>Sesi</th>
							    <th>Start Date</th>
                                <th>Status</th>
								<th>Mulai Ujian</th>
							</tr>
                        </thead>
                        <tbody>
					<?php foreach ($exam->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->soal?></td>
							<td><?=$row->class?></td>
							<td><?=$row->periode?></td>
							<td><?=$row->durasi?></td>
							<td><?=$row->sesion?></td>
							<td><?=$row->start_date?></td>
							<td>
							<?php if($row->status=='schedule'){?>
								<span class="uk-badge uk-badge-primary">
							<?php }else if($row->status=='ongoing'){?>
								<span class="uk-badge uk-badge-success">
							<?php }else{?>
								<span class="uk-badge uk-badge-danger">
								
							<?php } ?>
								<?php echo $row->status?></span></td>
                            <td>
								<button  onclick="detail('<?=$this->fungsi->encrypt($row->id_soal)?>')"  class="uk-icon-button uk-icon-book" title="Start"  <?php echo $row->status<>'ongoing'?'disabled':'';?>></a>
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id_soal,id_class){
				uri = '<?=base_url()."soal/edit"?>';
				$('#new_soal').hide();
				$('#edit_soal').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id_soal:id_soal,id_class:id_class},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(){
				uri = '<?=base_url()."soal/add"?>';
				$('#edit_soal').hide();
				$('#new_soal').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id_soal){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."soal/delete"?>';
					$.post(uri,{ajax:true,id_soal:id_soal},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
			function detail(id_soal){
				var r = confirm("Perhatian !!! \n- Anda akan diarahkan ke soal ujian, Gunakan waktu sebaik baiknya \n- Jawablah pertanyaan dengan benar \n -Jangan menekan F5 atau refresh halaman pada saat ujian, jika hal ini dilakukan anda dianggap selesai melakukan test");
				if (r == true) {
					/*
					$('#list_data').html(loading);
					$.post('<?=base_url()."soal/detail"?>',{id_soal:id_soal},function(data) 
					{
						window.open(data, '_blank');
					});*/
					var url = '<?=base_url()."exam/detail/"?>'+id_soal;
					//window.open(url, '_blank');
					//window.location(url);
					window.location.href = url;


					
				} 
				
			}
	</script>	
</html>