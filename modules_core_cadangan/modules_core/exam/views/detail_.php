<?php echo Modules::run('front_templates/front_templates/header'); ?>
<!---CONTENT START-->	
   <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large">
					<div class="md-card">
						<div class="md-card-content">
							<div class="uk-grid">
								<div class="uk-width-1-1">
									<div class="uk-form-row">
										<p><?=$title?></p>
										<div align="center">
										 <span id="time" style="font-size:40px;color:red"></span>
										 </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="md-card">
                        <div class="user_content" id="user_contents">
                             <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1'}">
                                <li><a href="#">Pilihan Ganda</a>
									<div align="center" id="loading_pg">
										<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/>
									</div>
								</li>
								<li><a href="#">Essay</a>
									<div align="center" id="loading_essay">
										<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/>
									</div>
								</li>
                            </ul>
                            <ul id="tabs_1" class="uk-switcher uk-margin">
							
							<!--pg-->
								<li>
									<div id="data_pg"></div>	
								</li>

							<!--end pg-->
							
							<!-- essay -->
                                <li>
									<div id="data_essay"></div>	
								</li>
							<!-- end essay -->
								
                            </ul>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>  
	<!-- common functions -->
   <?php echo Modules::run('front_templates/front_templates/footer'); ?>
   
   
	<script>
		function startTimer(duration, display) {
			var timer = duration, minutes, seconds;
			setInterval(function () {
				minutes = parseInt(timer / 60, 10)
				seconds = parseInt(timer % 60, 10);

				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;

				display.textContent = minutes + ":" + seconds;

				if (--timer < 0) {
					timer = duration;
				}else{
					if(timer==0){
						//alert(timer);
						duration=0;
					}
				}
			}, 1000);
			
		}

		window.onload = function () {
			var fiveMinutes = 60 * 1,
				display = document.querySelector('#time');
			startTimer(fiveMinutes, display);
		};
		
	</script>
	
	
<!---CONTENT END-->