<?php echo Modules::run('front_templates/front_templates/header'); ?>
<!---CONTENT START-->	
   <div id="page_content">
        <div id="page_content_inner">
			<h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large">
					<div class="md-card" id="timer">
						<div class="md-card-content">
							<div class="uk-grid">
								<div class="uk-width-1-1">
									<div class="uk-form-row">
										<div align="center">
										 <span style="font-size:40px;color:blue">Waktu anda </span><span id="time" style="font-size:40px;color:red">wait...</span>
										 </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <div class="md-card">
                        <div class="user_content" id="user_contents">
							 <div align="right">
								<button class="md-btn md-btn md-btn-success" name="finish" onclick="finish()">Selesai</button>
							 </div>
                             <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1'}">
                                <li><a href="#">Pilihan Ganda</a>
									<div align="center" id="loading_pg">
										<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/>
									</div>
								</li>
								<li><a href="#">Essay</a>
									<div align="center" id="loading_essay">
										<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/>
									</div>
								</li>
                            </ul>
                            <ul id="tabs_1" class="uk-switcher uk-margin">
							
							<!--pg-->
								<li>
									<div id="data_pg"></div>	
								</li>

							<!--end pg-->
							
							<!-- essay -->
                                <li>
									<div id="data_essay"></div>	
								</li>
							<!-- end essay -->
								
                            </ul>
                        </div>
						<div class="user_content" id="user_result" style="display:none">
							<div class="uk-margin-medium-bottom">
                <span class="uk-text-muted uk-text-small uk-text-italic">Start Data:</span> <?=$exam['start_date']?>
                <br/>
                <span class="uk-text-muted uk-text-small uk-text-italic">End Date:</span>  <?=$exam['end_date']?>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-3-5">
                    <div class="uk-margin-bottom">
                        <span class="uk-text-muted uk-text-small uk-text-italic">Fasilitator:</span>
                        <address>
                            <p><strong>Taufan Arfianto</strong></p>
                            <p>Pengajar 1</p>
                            <p>Staff Kementerian A</p>
                        </address>
                    </div>
                </div>
                <div class="uk-width-small-2-5">
                    <span class="uk-text-muted uk-text-small uk-text-italic">Result:</span>
                    <p class="heading_b uk-text-success">78</p>
                    <p class="uk-text-small uk-text-muted uk-margin-top-remove">LULUS</p>
                </div>
            </div>
            <div class="uk-grid uk-margin-large-bottom">
                <div class="uk-width-1-1">
                    <table class="uk-table">
                        <thead>
                        <tr class="uk-text-upper">
                            <th>Description Materi</th>
                        </tr>
                        </thead>
                        <tbody>
                       
                            <tr class="uk-table-middle">
                                <td>
                                    <span class="uk-text-large"> <?=$class['class']?></span>
                                </td>
                            </tr>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
	<!-- common functions -->
	 <?php echo Modules::run('front_templates/front_templates/footer'); ?>
    
    <!-- uikit functions -->

	<script>	
		var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/></div>';
		$(document).ready(function() {
			//alert('');
			reload_pg();
			reload_essay();
			var durasi='<?=$durasi?>';
			//alert(durasi);
			if(durasi <= 0){
				$("#user_contents").hide();
				$("#user_result").show();
			}
				
		});
		
		function reload_pg(){
			$('#data_pg').html(loading);
			$.post("<?php echo base_url().'exam/list_pg'?>",{id_soal:'<?=$id_soal?>',jeni:'pg',page:0},function(data){
				$('#data_pg').html(data);
				$('#loading_pg').hide();
			});
		}

		function reload_essay(){
			$('#data_essay').html(loading);
			$.post("<?php echo base_url().'exam/list_essay'?>",{id_soal:'<?=$id_soal?>',jenis:'essay',page:0},function(data){
				$('#data_essay').html(data);
				$('#loading_essay').hide();
			});
		}	
		
		function finish(){
			var r = confirm("Apakah anda yakin akan mengakhiri test ini?");
				if (r == true) {
					$("#user_contents").hide();
					$("#user_result").show();
					$("#timer").hide();
				}
		}
		
		
		function startTimer(duration, display) {
			var timer = duration, minutes, seconds;
			setInterval(function () {
				minutes = parseInt(timer / 60, 10)
				seconds = parseInt(timer % 60, 10);

				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;

				display.textContent = minutes + ":" + seconds;

				if (--timer < 0) {
					timer = duration;
				}else{
					if(timer==0){
						//alert(timer); buat disabled form nya
						duration=0;
						$("#user_contents").hide();
						$("#user_result").show();
						/*
						$('#user_result').html(loading);
						$.post("<?php echo base_url().'exam/list_result'?>",{id_soal:'<?=$id_soal?>',jenis:'essay',page:0},function(data){
							$('#user_result').html(data);
						});*/
					}
				}
			}, 1000);
			
		}

		window.onload = function () {
			var durasi ='<?=$durasi?>';
			//alert(durasi);
			var fiveMinutes = 60 * durasi;
				display = document.querySelector('#time');
			startTimer(fiveMinutes, display);
		};
		
		$(window).unload( function () {
		  times = $("#time").text();
		  $.ajax({
				url: '<?php echo base_url().'exam/last_time'?>',
				type: "POST",
				data: "times="+ times+"&id_ujian="+"<?=$exam['id_ujian']?>&sesion="+"<?=$exam['sesion']?>",
				cache: true,
				success: function(response){

				}
				});
	    } );
	</script>
	
<!---CONTENT END-->