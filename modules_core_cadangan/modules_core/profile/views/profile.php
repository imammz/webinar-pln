<div id="page_content">
	<div id="page_content_inner">
		<div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
			<div class="uk-width-large">
				<div class="md-card">
					<div class="user_heading">
						<div class="user_heading_avatar">
							<a href="#edit_photo" data-uk-modal="{ center:true }" onclick="change(<?=$this->session->userdata('sesi_id_user')?>)"><img src="<?=$profile['photo']?>"/></a>
						</div>
						<div class="user_heading_content">
							<h2 class="heading_b uk-margin-bottom"><span class="uk-text-truncate"><?=$profile['nama']?></span>
							<span class="sub-heading"><?=$profile['skill']?></span>
							</h2>
							<span class="sub-heading"><?=$profile['jabatan']?></span>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<div class="uk-grid uk-grid-large" data-uk-grid-margin>
                <div class="uk-width-xLarge-3-8  uk-width-large-5-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
								Profile
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <div class="uk-overflow-container">
								<table class="uk-table uk-table-hover">
									<tbody>
									<tr>
										<th width="100px">NIP</th>
										<td><?=$profile['nip']?></td>
									</tr>
									<tr>
										<th width="100px">Tanggal Lahir</th>
										<td><?=$profile['tgl_lahir']?></td>
									</tr>
									<tr>
										<th width="100px">Alamat</th>
										<td><?=$profile['alamat']?></td>
									</tr>
									<tr>
										<th width="100px">Telp</th>
										<td><?=$profile['telp']?></td>
									</tr>
									<tr>
										<th width="100px">Email</th>
										<td><?=$profile['email']?></td>
									</tr>
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-xLarge-5-10  uk-width-large-5-10">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <h3 class="md-card-toolbar-heading-text">
								Kelas
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <div class="uk-overflow-container">
								<table class="uk-table uk-table-hover">
									<header>
										<th>Kelas</th>
										<th>Status</th>
										<th>Start Date</th>
										<th>End Date</th>
									</header>
									<tbody>
								<?php foreach($class->result() as $row){?>	
									<tr>
										<td width=""><?=$row->class;?></td>
										<td>
											<?php if($row->status=='inprogress'){?>
													<span class="uk-badge uk-badge-warning"><?=$row->status?></span>
											<?php }else if($row->status=='schedule'){?>
													<span class="uk-badge uk-badge-primary"><?=$row->status?></span>
											<?php }else{?>
													<span class="uk-badge uk-badge-success"><?=$row->status?></span>
											<?php } ?>
										</td>
										<td><?=$row->start_date;?></td>
										<td><?=$row->end_date;?></td>
									</tr>
								<?php } ?>	
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
				</div>
			</div>
				<div class="uk-modal" id="edit_photo">
					<div id="modal-edit-photo"></div>
				</div>
		</div>
</div>	
<script>
	var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
	function change(id){
		//alert(id);
		uri = '<?=base_url()."profile/change_photo"?>';
		$('#modal-edit-photo').html(loading);
		$.post(uri,{ajax:true,id_user:id},function(data) {
			
			$('#modal-edit-photo').html(data);
		});
	}
</script>