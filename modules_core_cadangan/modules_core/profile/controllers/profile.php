<?php

class profile extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model('profile/profile_model');
		date_default_timezone_set('Asia/Jakarta');
    }
    
    public function index(){ 	
		$this->login_model->is_login();
		$data['link'] = base_url()."profile/list_data"; 
		$data['title']='Profile';
		
		$this->load->view('preview',$data);
		
	}
	
	public function list_data() {
		$this->login_model->is_login();
		$id = $this->session->userdata('sesi_id_user');
		$data['profile']    = $this->profile_model->get_profile($id);
		$data['class']		= $this->profile_model->get_my_class($id); 
		$data['title'] 		='Profile';
		$data['link'] 		= base_url()."profile/list_data"; 
		$this->load->view('profile', $data);        
    }
	
	function change_photo(){
		$this->login_model->is_login();
        $id_user         = $this->input->post('id_user');
        $data['profile']  = $this->profile_model->get_profile($id_user);
        $data['title']    = 'Change Photo';
        $data['link']     = base_url() . "profile/list_data";
        $this->load->view('edit_photo', $data);
	}
	
	function update_photo(){
        $this->login_model->is_login();
        $id_user     = $this->session->userdata("sesi_id_user");
        $photo 	 = $_FILES["photo"]["name"];
		
        $config['upload_path']   = './front_assets/profile/';
        $config['allowed_types'] = 'png|jpg';
        $config['max_size']      = '10000000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/profile/";
        //die($photo);
        if ($photo) {
      
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('photo')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/profile/thumbs/'),
                    'maintain_ration' => true
                );
				 $path . $upload_data['file_name'];
                 $this->profile_model->update_photo($id_user,$path . $upload_data['file_name']);
            }
        }
	}
	
	function change_password(){
		$this->login_model->is_login();
        $id_user         = $this->input->post('id_user');
        $data['profile']  = $this->profile_model->get_profile($id_user);
        $data['title']    = 'Change Password';
        $data['link']     = base_url() . "profile/list_data";
        $this->load->view('edit_pass', $data);
	}
	
	function update_password(){
		$this->login_model->is_login();
		$id_user  = $this->session->userdata("sesi_id_user");
		$old	  = $this->input->post('old');
		$old      = $this->fungsi->Md5AddSecret($old);
		$password = $this->input->post('pass2');
		$password = $this->fungsi->Md5AddSecret($password);
		$this->profile_model->update_password($id_user,$old,$password);
	}
}

?>