<?php

class user extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model(array('user/user_model','level/level_model'));
    }
    
    public function index()
	{ 	
		$this->login_model->is_login();
		$data['title'] 		='Master User';
		$data['link'] = base_url()."user/list_data"; 
		$this->load->view('preview',$data);
	}
	
	public function list_data() 
	{
		$this->login_model->is_login();
		$data['user']	= $this->user_model->get_list_user();
		$data['title'] 	='Master User';
		$data['link'] 	= base_url()."user/list_data"; 
		$this->load->view('user', $data);        
    }
	
	public function add()
	{
		$this->login_model->is_login();
		$data['pegawai']= $this->user_model->get_list_pegawai();
		$data['level']	= $this->level_model->get_list_level();
		$data['title'] 		= 'Add User';
		$data['link'] = base_url()."user/list_data"; 
		$this->load->view('add', $data); 
	}
	
	public function edit()
	{
		
		$this->login_model->is_login();
		
		$id				= $this->input->post('id');
		$data['user']	= $this->user_model->get_user_by_id($id);
		$data['pegawai']= $this->user_model->get_list_pegawai2();
		$data['level']	= $this->level_model->get_list_level();
		$data['title'] 	= 'Edit User';
		$data['link'] 	= base_url()."user/list_data"; 
		$this->load->view('edit', $data); 
	}
	
	public function update()
	{	
		$this->login_model->is_login();
		$id				= $this->input->post('id');
		$id_level		= $this->input->post('id_level');
		//$nip			= $this->input->post('nip');
		$passwords		= $this->input->post('passwords');
		$username		= $this->input->post('username');
		$isactive 		= $this->input->post('isactive');
		$id_user		= $this->session->userdata("sesi_id_user");
		//save user
		//$this->user_model->update_user($id,$username,$nip,$isactive,$passwords,$id_level,$id_user);
		$this->user_model->update_user($id,$username,$isactive,$passwords,$id_level,$id_user);
	}
	public function save()
	{	
		$this->login_model->is_login();
		$id_level		= $this->input->post('id_level');
		$nip			= $this->input->post('nip');
		$passwords		= $this->fungsi->Md5AddSecret($this->input->post('passwords'));
		$username		= $this->input->post('username');
		$isactive 		= $this->input->post('isactive');
		$id_user		= $this->session->userdata("sesi_id_user");
		//save user
		$this->user_model->save_user($username,$nip,$isactive,$passwords,$id_level,$id_user);
	}
	
	//inactive aja jangan di delete
	public function delete()
	{	
		$this->login_model->is_login();
		$id	= $this->input->post('id');
		$this->user_model->update_user($id);
	
	}
	
}

?>