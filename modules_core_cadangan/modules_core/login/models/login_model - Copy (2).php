<?php

class login_model extends  CI_Model {
	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		
	}
	
	function get_user_login($username,$password){
		$sql="	select a.id_user,b.nip,b.nama,a.id_level 
				from wb_users a, wb_pegawai b
				where a.nip=b.nip
				and a.isactive=1
				and a.username='$username'
				and a.passwords='$password'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array(); 							 
		}
	}
	
	function check_locked($username){
		$sql="	select a.username,a.isactive 
				from wb_users a
				where a.username='$username'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array(); 							 
		}
	}
	
	function is_login(){
		$controller = $this->uri->segment(1);
	//	die($controller);
		if($controller=='login' ||$controller==''){
			if($this->session->userdata("sesi_islogin")==TRUE){
				$id_sesion  = $this->session->userdata('sesi_id_sesion');
			    $sesion  	= $this->check_sesion($id_sesion);
				
				if($sesion['total']<1){
					$this->session->sess_destroy();
					redirect('/login', 'refresh');
					exit;
				}else{
					//tambahin check privileges
					redirect('/home', 'refresh');
				}
			}
		}else{
			//die($controller);
			if($this->session->userdata("sesi_islogin")==TRUE){
				$id_sesion  = $this->session->userdata('sesi_id_sesion');
				$sesion  	= $this->check_sesion($id_sesion);
				//echo $sesion['total'];
				if($sesion['total']<1){
					
					$this->session->sess_destroy();
					redirect('/login', 'refresh');
					exit;
				}
			}else{
				
				$this->session->sess_destroy();
				redirect('/login', 'refresh');
				exit;
			}
		}
	}
	
	function check_sesion($id_sesion){
		$sql = "select count(1) as total
				from wb_users where id_sesion='$id_sesion'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array(); 							 
		}		
	}
	
	function check_menu_allowed($id_level,$menu_url){
		$sql = "select count(1)
				from wb_level a, wb_menu b, wb_menu_role c
				where a.id_level=c.id_level
				and b.id_menu=c.id_menu
				and b.menu_url='$menu_url'
				and a.id_level='$id_level'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array(); 							 
		}
	}       
	
	function update_sesion($id_sesion,$id_user){
		$sql = "update wb_users set id_sesion ='$id_sesion',islogin=1,last_login=now()
			    where id_user='$id_user'";
		$hasil = $this->db->query($sql);
		
	}
	function update_sesion_logout($id_user){
		$sql = "update wb_users set id_sesion ='',islogin=0
			    where id_user='$id_user'";
		$hasil = $this->db->query($sql);
		
	}
	
}
