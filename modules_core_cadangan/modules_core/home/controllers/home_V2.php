<?php

class Home extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model('home_model');
    }
    
    public function index()
	{
		$this->login_model->checkPrivilege();
		$data['title']		 		="Home";
		$data['kindegarten'] 		= $this->home_model->getListPlan(1);
		$data['primary_cambridge'] 	= $this->home_model->getListPlan(2);
		$data['primary_national'] 	= $this->home_model->getListPlan(3);
		$data['development'] 		= $this->home_model->getListPlan(4);
		$data['info']				= $this->home_model->getInfo($this->session->userdata("sesi_id_karyawan"));
		$this->load->view('home_view',$data);
		
	}
	
	function changes_images(){
		//$this->login_model->checkPrivilege();
		$this->load->view('home/changes_images');
	}
	
	function changes_signature(){
		//$this->login_model->checkPrivilege();
		$this->load->view('home/changes_signature');
	}
	
	function save_images(){
		$config['upload_path']   = './front_assets/profile/';
        $config['allowed_types'] = 'png|jpg';
        $config['max_size']      = '1000';
        
        $this->load->library('upload', $config);
        
        if (!$this->upload->do_upload('filename')) {
            $upload_error = array(
                $error = $this->upload->display_errors()
            );
            echo json_encode(array('response' => 'failed', 'msg' => $error));	
        } else {
			
			 $upload_data         = $this->upload->data();
				$config = array(
				'source_image' => $upload_data['full_path'],
				'new_image' => realpath(APPPATH . '../front_assets/profile/thumbs/'),
				'maintain_ration' => true,
				'width' => 87,
				'height' => 130
			);

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			$nama_filenya = $upload_data['file_name'];
			
			$img = $this->home_model->getImage($this->session->userdata("sesi_id_karyawan"));
			@unlink($img['photo']);
			$file_name = './front_assets/profile/'.$_FILES['filename']['name'];
			$this->home_model->updateImages($this->session->userdata("sesi_id_karyawan"),$file_name);
            echo json_encode(array('response' => 'success', 'msg' => 'Berhasil','files'=>$file_name));	
        }	
	}
	
	function save_signature(){
		$config['upload_path']   = './front_assets/signature/';
        $config['allowed_types'] = 'png|jpg';
        $config['max_size']      = '1000';
        
        $this->load->library('upload', $config);
        
        if (!$this->upload->do_upload('filename')) {
            $upload_error = array(
                $error = $this->upload->display_errors()
            );
            echo json_encode(array('response' => 'failed', 'msg' => $error));	
        } else {
			
			 $upload_data         = $this->upload->data();
				$config = array(
				'source_image' => $upload_data['full_path'],
				'new_image' => realpath(APPPATH . '../front_assets/signature/thumbs/'),
				'maintain_ration' => true,
				'width' => 130,
				'height' => 85
			);

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
            $img = $this->home_model->getImage($this->session->userdata("sesi_id_karyawan"));
			@unlink($img['signature']);
			$nama_filenya = $upload_data['file_name'];
			$file_name = './front_assets/signature/'.$_FILES['filename']['name'];
			$this->home_model->updateSignature($this->session->userdata("sesi_id_karyawan"),$file_name);
            echo json_encode(array('response' => 'success', 'msg' => 'Berhasil','files'=>$file_name));	
        }		
	}

        

}

?>