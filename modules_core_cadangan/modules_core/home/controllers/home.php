<?php

class home extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model('document/document_model');
		date_default_timezone_set('Asia/Jakarta');
    }
    
    public function index(){ 	
		$this->login_model->is_login();
		$data['link'] = base_url()."home/list_data"; 
		$data['title']='Home';
		
		$this->load->view('preview',$data);
		
	}
	
	public function list_data() {
		$this->login_model->is_login();
		$data['title'] 		='Home';
		$data['link'] 		= base_url()."home/list_data"; 
		$this->load->view('home', $data);        
    }
}

?>