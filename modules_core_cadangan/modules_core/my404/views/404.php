
    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div class="uk-width-8-10 uk-container-center">
						<p class="heading_b">404! Page not found</p>
						<p class="uk-text-large">
							The requested URL <span class="uk-text-muted">/some_url</span> was not found on this server.
						</p>
						<a href="#" onclick="history.go(-1);return false;">Go back to previous page</a>
					</div>
                </div>
            </div>

        </div>
    </div>
</html>