<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no"/>
        <link rel="icon" type="image/png" href="<?= base_url() ?>front_assets/assets/img/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?= base_url() ?>front_assets/assets/img/favicon-32x32.png" sizes="32x32">
        <title>PLN Web Binar System</title>
        <!-- fullcalendar -->
        <link rel="stylesheet" href="<?= base_url() ?>front_assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>front_assets/bower_components/weather-icons/css/weather-icons.css" media="all">
        <link rel="stylesheet" href="<?= base_url() ?>front_assets/bower_components/metrics-graphics/dist/metricsgraphics.css">
        <link rel="stylesheet" href="<?= base_url() ?>front_assets/bower_components/chartist/dist/chartist.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>front_assets/bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
        <link rel="stylesheet" href="<?= base_url() ?>front_assets/assets/icons/flags/flags.min.css" media="all">
        <link rel="stylesheet" href="<?= base_url() ?>front_assets/assets/css/main.min.css" media="all">
        <script src="<?php echo base_url('player/AC_RunActiveContent.js') ?>" language="javascript"></script>	
        <link href="http://vjs.zencdn.net/5.0.0/video-js.css" rel="stylesheet">

        <!-- If you'd like to support IE8 -->
        <script src="http://vjs.zencdn.net/ie8/1.1.0/videojs-ie8.min.js"></script>


    </head>
    <body class="sidebar_main">
        <!---HEADER START-->
        <header id="header_main">
            <div class="header_main_content">
                <nav class="uk-navbar">
                    <!-- main sidebar switch -->
                    <a href="#" id="sidebar_main_toggle">
                        <span class="sSwitchIcon"></span>
                    </a>
                </nav>
            </div>
        </header>

        <div id="page_content">
            <div id="page_heading">
                <h1><?= $class['class'] ?></h1>
                <span class="uk-text-muted uk-text-upper uk-text-small">Periode 1</span>
            </div>
            <div id="page_content_inner">

                <div class="uk-grid uk-grid-large" data-uk-grid-margin>
                    <div class="uk-width-xLarge-3-8  uk-width-large-3-10">
                        
                        <?php 
                        
                $user = $this->db->where('id_user',$this->session->userdata('sesi_id_user'))->get('wb_users');
                $username = $user->row_array();
                $all_user = $this->db->where('id_user != "'.$this->session->userdata('sesi_id_user').'" AND id_level != 1')->get('wb_users');
                $all_username = $all_user->result_array();
                        
                      
                        ?>
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Instructure Page
                                </h3>
                            </div>
                        <?php  if($username['id_level']=='1') {  ?>    
                            <div class="md-card-content">
                                <div class="uk-margin-bottom uk-text-center">
                                    <object width="320" height="320" data="<?php echo base_url('player/admin.swf?username=' .$username['username']) ?>"></object> 
                                </div>
                            </div>
                        <?php } else { ?>
                            
                            <div class="md-card-content">
                                <div class="uk-margin-bottom uk-text-center">
                                    <object width="320" height="320" data="<?php echo base_url('player/user.swf?username=admin') ?>"></object> 
                                </div>
                            </div>
                            
                        <?php } ?>
                        </div>
                        <?php if($username['id_level']!='1') {  ?>
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    My Page
                                </h3>
                            </div>
                            <div class="md-card-content">
                                <div class="uk-margin-bottom uk-text-center">
                                    <object width="320" height="320" data="<?php echo base_url('player/admin.swf?username=' . $username['username']) ?>"></object> 
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                    </div>
                    
                    
                    
                    <div class="uk-width-xLarge-5-10  uk-width-large-5-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Slide Presentation
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                
                                <div class="uk-grid uk-grid-divider uk-grid-medium">
                                    <div class="uk-width-large-1-1">
                                        <div class="md-card-content">
                                            <div class="uk-margin-bottom uk-text-center">

                                                <iframe src="https://docs.google.com/presentation/d/1Ny107zfzJXFS4-WzJhBn48KTtNbN0RcYUHpwKaJLjBs/embed?start=true&loop=false&delayms=3000" frameborder=0" width="480" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
                                            </div>
                                        </div>	
                                    </div>
                                    <div class="uk-width-large-1-1">
                                        <p>
                                            <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">Information</span>
                                            <span class="uk-badge uk-badge-success">Slide 1 - Presentasi Tentang</span><br/>
                                        </p>
                                        <hr class="uk-grid-divider">
                                        <p>
                                            <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">Description</span>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam necessitatibus suscipit velit voluptatibus! Ab accusamus ad adipisci alias aliquid at atque consectetur, dicta dignissimos, distinctio dolores esse fugiat iste laborum libero magni maiores maxime modi nemo neque, nesciunt nisi nulla optio placeat quas quia quibusdam quis saepe sit ullam!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-xLarge-2-8  uk-width-large-2-10">
                                                <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Class Information
                                </h3>
                            </div>
                            <div class="md-card-content uk-margin-bottom">
                                <div>
                                    <p>Dudung SHT</p>
                                    <p>Trafo Class</p>
                                    <p>Periode 1</p>
                                    <p>2015-07-8    ----   2015-07-30</p>
                                    <p>Lokasi Diklat Jakarta Area 1</p>
                                    <p>Peserta</p>
                                </div>
                            </div>
                        </div>
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    List Friend
                                </h3><br/>
                            </div>
                            <div class="md-card-content">
                                <div class="md-list-outside-wrapper">
                                    <ul class="md-list md-list-addon md-list-outside" id="chat_user_list">
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?= base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Kusnadi</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-danger"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?= base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Taufan Arfianto</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Non Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?= base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Mamad Tohiri</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?= base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Daud Pulung</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?= base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Mila Rahma</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?= base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Fuadi Julian</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?= base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Dimas Galih</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?= base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Sidiq Permana</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php foreach ($all_username as $row) { ?>  

                                <div>
                                    <div class="md-card md-card-hover md-card-overlay">
                                        <div class="md-card-content" style="min-height: 300px;">
                                            <div class="epc_chart" data-percent="100"  data-bar-color="#03a9f4">
                                                 <object width="320" height="320" data="<?php echo base_url('player/user.swf?username=' . $row['username']) ?>"></object> 
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            <?php } ?>
                    
                    
                    <div class="uk-width-xLarge-8-10  uk-width-large-10-10">
                        <div class="md-card">

                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Calendar And Schedule
                                </h3>
                            </div

                            <div class="md-card-content large-padding">
                                <div class="md-card-content">
                                    <div id="calendar_selectable"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="md-fab-wrapper">
            <div id="my_camera"></div>
        </div>

        <!-- common functions -->
        <script src="<?= base_url() ?>front_assets/assets/js/common.min.js"></script>
        <!-- uikit functions -->
        <script src="<?= base_url() ?>front_assets/assets/js/uikit_custom.min.js"></script>
        <!-- altair common functions/helpers -->
        <script src="<?= base_url() ?>front_assets/assets/js/altair_admin_common.min.js"></script>

        <!-- page specific plugins -->
        <!-- fullcalendar -->
        <script src="<?= base_url() ?>front_assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>front_assets/webcam/webcam.js"></script>
        <!--  calendar functions -->
        <script src="<?= base_url() ?>front_assets/assets/js/pages/plugins_fullcalendar.min.js"></script>
        <!-- Configure a few settings and attach camera -->
        <script language="JavaScript">
            Webcam.set({
                width: 320,
                height: 240,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
            Webcam.attach('#my_camera');
        </script>

        <!-- Code to handle taking the snapshot and displaying it locally -->
        <script language="JavaScript">
            function take_snapshot() {
                // take snapshot and get image data
                Webcam.snap(function (data_uri) {
                    // display results in page
                    document.getElementById('results').innerHTML =
                            '<h2>Here is your image:</h2>' +
                            '<img src="' + data_uri + '"/>';
                });
            }
        </script>