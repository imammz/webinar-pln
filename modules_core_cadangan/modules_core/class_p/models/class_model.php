<?php

class class_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	
	function get_list_class(){
		$sql = "SELECT *
				FROM wb_class";
		return $this->db->query($sql);
	}
	
	function get_list_class_peserta($id_user){
		$sql = "select a.* from wb_class a,wb_peserta b
				where a.id_class=b.id_class
				and b.id_user='$id_user'";
		return $this->db->query($sql);
	}
	
	function save_class($class_name,$keterangan,$instruktur1,$instruktur2,$start_date,$end_date,$lokasi, $id_user){
		$sql="insert into wb_class(class,keterangan,instruktur1,instruktur2,start_date,end_date,lokasi,created_by,created_date)
			  values('$class_name','$keterangan','$instruktur1','$instruktur2','$start_date','$end_date','$lokasi','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Class berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Class gagal disimpan'));		
		}
		
	}
	
	function get_class_by_id($id){
		$sql = "SELECT * FROM wb_class
				WHERE id_class='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	
	function update_class($id,$class_name,$keterangan,$instruktur1,$instruktur2,$start_date,$end_date,$lokasi, $id_user){
		$sql="update wb_class set class='$class_name',
			  keterangan='$keterangan',instruktur1='$instruktur1',instruktur2='$instruktur2',
			  start_date='$start_date',end_date='$end_date',lokasi='$lokasi',
			  updated_by='$id_user',updated_date=now()
			  WHERE id_class='$id'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Class berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Class gagal di simpan'));		
		}		  
	
	}
	
	
	function delete_class($id_class){
		$sql="DELETE FROM wb_class 
			  WHERE id_class='$id_class'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Class berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Class gagal didelete'));		
		}		  
	}
	
	function inactive_class($id_class){
		$sql="update wb_class set keterangan='0'
			  WHERE id_class='$id_class'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Class berhasil di nonaktifkan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Class gagal di nonaktifkan'));		
		}		  
	}
	

}
