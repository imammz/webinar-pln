<?php

class exam_schd_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	function get_list_examp_schd(){
		$sql = "select a.*,
					case when a.start_date = DATE_FORMAT(now(),'%Y-%m-%d')
						then 'ongoing'
						 when a.start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
						 when  a.start_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
					end status,
					b.class,c.periode,d.soal
				 from wb_ujian a, wb_class b,wb_periode c,wb_soal d
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_soal=d.id_soal
				order by a.start_date desc";
		return $this->db->query($sql);
	}
	
	function get_list_examp_schd_peserta($id_user){
		$sql = "select a.*,
					case when a.start_date = DATE_FORMAT(now(),'%Y-%m-%d')
						then 'ongoing'
						 when a.start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
						 when  a.start_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
					end status,
					b.class,c.periode,d.soal
				 from wb_ujian a, wb_class b,wb_periode c,wb_soal d,wb_peserta e
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_soal=d.id_soal
				and b.id_class=e.id_class
				and e.id_user='$id_user'
				order by a.start_date desc";
		return $this->db->query($sql);
	}
	
	function get_list_exam_schd_by_id($id_ujian){
		$sql = "select a.*,b.class,c.periode,d.soal
				 from wb_ujian a, wb_class b,wb_periode c,wb_soal d
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_soal=d.id_soal
				and a.id_ujian='$id_ujian'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}	
	}
	
	function update_exam_schd($id_ujian,$id_class,$id_periode,$id_soal,$durasi,$sesion,$start_date,$id_user){
		$sql="update wb_ujian set id_class='$id_class',id_periode='$id_periode',id_soal='$id_soal',
			  durasi='$durasi',sesion='$sesion',start_date='$start_date',updated_by='$id_user', updated_date=now()
			  where id_ujian='$id_ujian'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function save_exam_schd($id_ujian,$id_class,$id_periode,$id_soal,$durasi,$sesion,$start_date,$id_user){
		$sql="insert into wb_ujian(id_ujian,id_class,id_periode,id_soal,durasi,sesion,start_date,created_by,created_date)
			  values('$id_ujian','$id_class','$id_periode','$id_soal','$durasi','$sesion','$start_date','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function delete_exam_schd($id_ujian){
		$sql="delete from wb_ujian where id_ujian='$id_ujian'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	function get_soal_instruktur_by_id($id_soal){
		$sql = "select a.*,b.class,c.periode,b.created_by,b.instruktur1,b.instruktur2
				from wb_soal a, wb_class b, wb_periode c
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_soal='$id_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_list_bank_soal($id_soal,$categori){
		$sql = "select b.*,a.soal from wb_soal a, wb_bank_soal b
				where a.id_soal=b.id_soal
				and b.id_soal='$id_soal'
				and b.categori_quiz='$categori'";
		return $this->db->query($sql);
	}
	
	function get_bank_soal_by_id($id_bank_soal){
		$sql = "select b.* from wb_bank_soal b
				where b.id_bank_soal='$id_bank_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	
	function save_soal($id_soal,$soal,$id_class,$id_periode,$id_user){
		$sql="insert into wb_soal(id_soal,soal,id_class,id_periode,created_by,created_date)
			  values('$id_soal','$soal','$id_class','$id_periode','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_soal($id_soal,$soal,$id_class,$id_periode,$id_user){
		$sql="update wb_soal set soal='$soal',id_class='$id_class',id_periode='$id_periode',updated_by='$id_user',updated_date=now()
			  where id_soal='$id_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	
	function delete_soal($id_soal){
		$sql="delete from wb_soal where id_soal='$id_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	
	function save_bank_soal($id_soal,$categori_quiz,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="insert into wb_bank_soal(id_soal,categori_quiz,pertanyaan,a,b,c,d,e,jawaban_pg,jawaban_essay,created_by,created_date)
			  values('$id_soal','$categori_quiz','$pertanyaan','$a','$b','$c','$d','$e','$jawaban_pg','$jawaban_essay','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_bank_soal($id_bank_soal,$id_soal,$categori_quiz,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="update wb_bank_soal set categori_quiz='$categori_quiz',pertanyaan='$pertanyaan ',a='$a' ,b='$b', 				
			  c='$c',d='$d',e='$e', jawaban_pg='$jawaban_pg',jawaban_essay='$jawaban_essay',updated_by='$id_user',updated_date=now()
			  where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function delete_bank_soal($id_bank_soal){
		$sql="delete from wb_bank_soal where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal sihapus'));		
		}
	}
	
}
