<style>
	.uk-datepicker { width: 20em; padding: .2em .2em 0; display: none; z-index: 2000 !important;}
</style>
<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
	<div id="edit_message"></id>
</div>
<div id="before-edit-exam_schd"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="edit_myform" style="display:none">
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Class</label>
		</div>
		<select id="id_class" name="id_class">
				<option value="">Choose..</option>
			<?php 
			foreach ($class->result() as $row){?>
				<option value="<?=$row->id_class?>" <?php echo $exam_schd['id_class']==$row->id_class ? 'selected':''?>><?=$row->class?></option>
				
			<?php }?>
		</select>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Periode</label>
		</div>
		<select id="id_periode" name="id_periode">
			<?php 
			foreach ($periode->result() as $row){?>
				<option value="<?=$row->id_periode?>" <?php echo $exam_schd['id_periode']==$row->id_periode ? 'selected':''?>><?=$row->periode?></option>
				
			<?php }?>
		</select>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Soal</label>
		</div>
		<select id="id_soal" name="id_soal">
			<?php 
			foreach ($soal->result() as $row){?>
				<option value="<?=$row->id_soal?>" <?php echo $exam_schd['id_soal']==$row->id_soal ? 'selected':''?>><?=$row->soal?></option>
				
			<?php }?>
		</select>
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Durasi</label>
		<input type="number" class="md-input" id="durasi" name="durasi" value="<?=$exam_schd['durasi']?>"/>
	</div>
	<div class="uk-margin-medium-bottom">
		<label class="uk-form">Start Date</label>
		<input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'YYYY-MM-DD'}" value="<?=$exam_schd['start_date']?>">
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Sesion</label>
		<input type="number" class="md-input" id="sesion" name="sesion" value="<?=$exam_schd['sesion']?>"/>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_ujian" value="<?=$exam_schd['id_ujian']?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnupdate" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#edit_myform").show(); 
				$("#before-edit-exam_schd").hide();
			}, 10);
			
			function reloadlist(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",function(data){
					$('#list_data').html(data);
				});
			}
			
			$("#id_class").change(function(){
				var id_class = $("#id_class").val();
				$.ajax({
					url: "soal/get_periode_class",
					data: "id_class="+id_class,
					cache: false,
					 dataType: 'json',
					success: function(msg){
						$('#id_periode').html('');
						$.each(msg, function(index, row) {
							$('#id_periode').append('<option value='+row.id_periode+' >'+row.periode+'</option>');
					   });
					  
					}
				});
			  });
			  
			$("#id_periode").change(function(){
				var id_periode = $("#id_periode").val();
				var id_class   = $("#id_class").val();
				$.ajax({
					url: "soal/get_soal",
					data: "id_class="+id_class+"&id_periode="+id_periode,
					cache: false,
					 dataType: 'json',
					success: function(msg){
						$('#id_soal').html('');
						$.each(msg, function(index, row) {
							$('#id_soal').append('<option value='+row.id_soal+' >'+row.soal+'</option>');
					   });
					  
					}
				});
			  })  
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#edit_myform').validate({
				rules:{
						exam_schd:{
							required:true
						},
						id_periode:{
							required:true
						},
						id_class:{
							required:true
						},
						id_soal:{
							required:true
						},
						durasi:{
							required:true
						},
						start_date:{
							required:true
						},
						sesion:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$("#btnupdate").prop('value', 'Process...'); 
					$('input[type="submit"]').prop('disabled', true);
					$.post('<?php echo base_url('exam_schd/update'); ?>', $('#edit_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						$('#btnupdate').attr('disabled');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#edit_message").html(msg);
							
							$('#edit_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#edit_message').hide();
								$('#edit_user').hide();
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#edit_message").html(msg);
							$("#btnupdate").prop('value', 'Save'); 
							$('input[type="submit"]').prop('disabled', false);
						}
					});
				}
			})
		});
</script>
