    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2" width="200px">Class</th>
                                <th colspan="4">Information</th>
                            </tr>
                            <tr>
                                <th>Periode</th>
                                <th>Soal</th>
								<th>Durasi</th>
								<th>Tanggal Ujian</th>
								<th>Status</th>
							<?php if($this->session->userdata('sesi_id_level')==1){?>	
								<th width="80px">Action</th>
							<?php } ?>	
							</tr>
                        </thead>

                        <tbody>
					<?php foreach ($exam_schd->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->class?></td>
							<td><?=$row->periode?></td>
							<td><?=$row->soal?></td>
							<td><?=$row->durasi?></td>
							<td><?=$row->start_date?></td>
							<td>
							<?php if($row->status=='schedule'){?>
								<span class="uk-badge uk-badge-primary">
							<?php }else if($row->status=='ongoing'){?>
								<span class="uk-badge uk-badge-success">
							<?php }else{?>
								<span class="uk-badge uk-badge-danger">
								
							<?php } ?>
							<?php echo $row->status?></span></td>
							<?php if($this->session->userdata('sesi_id_level')<>2){?>
                            <td>
								<a href="#edit_exam_schd" data-uk-modal="{ center:true }" onclick="edits('<?=$row->id_ujian?>','<?=$row->id_class?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
								<a href="#" onclick="deleted('<?=$row->id_ujian?>')"class="uk-icon-button uk-icon-trash-o"></a>
							</td>
							<?php } ?>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
		<?php if($this->session->userdata('sesi_id_level')==1){?>
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_exam_schd" onclick="add()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<?php } ?>
		<div class="uk-modal" id="new_exam_schd">
			<div id="modal-add"></div>
		</div>
		<div class="uk-modal" id="edit_exam_schd">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id_ujian,id_class){
				uri = '<?=base_url()."exam_schd/edit"?>';
				$('#new_exam_schd').hide();
				$('#edit_exam_schd').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id_ujian:id_ujian,id_class:id_class},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(){
				uri = '<?=base_url()."exam_schd/add"?>';
				$('#edit_exam_schd').hide();
				$('#new_exam_schd').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id_ujian){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."exam_schd/delete"?>';
					$.post(uri,{ajax:true,id_ujian:id_ujian},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
		
	</script>	
</html>