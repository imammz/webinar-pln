<?php

class employee_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}

	function get_list_pegawai(){
		$sql = "SELECT * 
				FROM wb_pegawai";
		return $this->db->query($sql);
	}
	

	
	function save_user($username,$nip,$isactive,$passwords,$id_level,$id_user){
		$sql="insert into wb_pegawai(username,nip,passwords,id_level,isactive,created_by,created_date)
			  values('$username','$nip','$passwords','$id_level','$isactive','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal disimpan'));		
		}
		
	}
	
	function get_user_by_id($id){
		$sql = "SELECT * FROM wb_pegawai
				WHERE id_user='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_pegawai_user_by_id($id){
		$sql = "SELECT a.* FROM wb_pegawai a, wb_users b
				WHERE a.nip=b.nip
				and b.id_user='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	
	function update_user($id,$username,$isactive,$passwords,$id_level,$id_user){
		$sql="update wb_pegawai set username='$username',passwords='$passwords',
			  id_level='$id_level',isactive='$isactive',updated_by='$id_user',updated_date=now()
			  WHERE id_user='$id'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal di simpan'));		
		}		  
	
	}
	
	
	function delete_user($id_user){
		$sql="DELETE FROM wb_pegawai 
			  WHERE id_user='$id_user'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal didelete'));		
		}		  
	}
	
	function inactive_user($id_user){
		$sql="update wb_pegawai set isactive='0'
			  WHERE id_user='$id_user'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil di nonaktifkan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal di nonaktifkan'));		
		}		  
	}
	
	
}
