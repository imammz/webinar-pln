<?php

class employee extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model(array('employee/employee_model','level/level_model'));
    }
    
    public function index()
	{ 	
		$this->login_model->is_login();
		$data['title'] 		='Master Peawai';
		$data['link'] = base_url()."employee/list_data"; 
		$this->load->view('preview',$data);
	}
	
	public function list_data() 
	{
		$this->login_model->is_login();
		$data['employee']	= $this->employee_model->get_list_pegawai();
		$data['title'] 	='Master Pegawai';
		$data['link'] 	= base_url()."employee/list_data"; 
		$this->load->view('employee', $data);        
    }
	
	public function add()
	{
		$this->login_model->is_login();
		$data['pegawai']= $this->employee_model->get_list_pegawai();
		$data['level']	= $this->level_model->get_list_level();
		$data['title'] 		= 'Add employee';
		$data['link'] = base_url()."employee/list_data"; 
		$this->load->view('add', $data); 
	}
	
	public function edit()
	{
		
		$this->login_model->is_login();
		
		$id				= $this->input->post('id');
		$data['employee']	= $this->employee_model->get_employee_by_id($id);
		$data['pegawai']= $this->employee_model->get_list_pegawai2();
		$data['level']	= $this->level_model->get_list_level();
		$data['title'] 	= 'Edit employee';
		$data['link'] 	= base_url()."employee/list_data"; 
		$this->load->view('edit', $data); 
	}
	
	public function update()
	{	
		$this->login_model->is_login();
		$id				= $this->input->post('id');
		$id_level		= $this->input->post('id_level');
		//$nip			= $this->input->post('nip');
		$passwords		= $this->input->post('passwords');
		$employeename		= $this->input->post('employeename');
		$isactive 		= $this->input->post('isactive');
		$id_employee		= $this->session->employeedata("sesi_id_employee");
		//save employee
		//$this->employee_model->update_employee($id,$employeename,$nip,$isactive,$passwords,$id_level,$id_employee);
		$this->employee_model->update_employee($id,$employeename,$isactive,$passwords,$id_level,$id_employee);
	}
	public function save()
	{	
		$this->login_model->is_login();
		$id_level		= $this->input->post('id_level');
		$nip			= $this->input->post('nip');
		$passwords		= $this->fungsi->Md5AddSecret($this->input->post('passwords'));
		$employeename		= $this->input->post('employeename');
		$isactive 		= $this->input->post('isactive');
		$id_employee		= $this->session->employeedata("sesi_id_employee");
		//save employee
		$this->employee_model->save_employee($employeename,$nip,$isactive,$passwords,$id_level,$id_employee);
	}
	
	//inactive aja jangan di delete
	public function delete()
	{	
		$this->login_model->is_login();
		$id	= $this->input->post('id');
		$this->employee_model->update_employee($id);
	
	}
	
}

?>