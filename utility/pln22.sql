
insert  into `wb_class`(`id_class`,`class`,`instruktur2`,`instruktur1`,`lokasi`,`keterangan`,`start_date`,`end_date`,`created_by`,`created_date`,`updated_by`,`updated_date`) values ('CL001','TRAFO',5,2,'BSD','apa aja','2015-01-01','2015-03-03',NULL,'2015-09-16 04:05:56',1,'2015-09-16 04:05:56');

/*Data for the table `wb_document` */

insert  into `wb_document`(`id_document`,`document`,`type`,`path`,`id_periode`,`created_by`,`created_date`,`updated_by`,`updated_date`) values ('DOC001','BOOK1','file',NULL,'PRD002',NULL,'2015-09-10 02:23:56',NULL,'0000-00-00 00:00:00'),('DOC002','VIDEO1','video',NULL,'PRD001',NULL,'2015-09-10 02:24:29',NULL,'0000-00-00 00:00:00');

/*Data for the table `wb_jawaban` */

/*Data for the table `wb_jawaban_peserta` */

/*Data for the table `wb_jenis_materi` */

insert  into `wb_jenis_materi`(`id_jenis`,`jenis`,`id_materi`,`created_by`,`created_date`,`updated_by`,`updated_date`) values ('JNS001','JENIS 1','MTR001',NULL,'2015-09-10 02:29:14',NULL,'0000-00-00 00:00:00'),('JNS002','JENIS 2','MTR001',NULL,'2015-09-10 02:29:31',NULL,'0000-00-00 00:00:00');

/*Data for the table `wb_jenis_quiz` */

/*Data for the table `wb_level` */

insert  into `wb_level`(`id_level`,`level_name`,`created_by`,`created_date`,`updated_by`,`updated_date`,`isactive`) values (1,'Admin',NULL,'2015-09-09 00:19:58',NULL,NULL,1),(2,'Peserta',NULL,'2015-09-09 00:20:08',1,'2015-09-14',0),(6,'Instruktur',1,'2015-09-15 00:14:01',NULL,NULL,1);

/*Data for the table `wb_login_log` */

/*Data for the table `wb_materi` */

insert  into `wb_materi`(`id_materi`,`materi`,`path`,`id_class`,`id_periode`,`created_by`,`created_date`,`updated_by`,`updated_date`) values ('MTR001','Trafo',NULL,'CL001',NULL,NULL,'2015-09-09 00:41:16',NULL,'0000-00-00 00:00:00');

/*Data for the table `wb_menu` */

insert  into `wb_menu`(`id_menu`,`id_parent`,`menu_name`,`menu_url`,`menu_icon`,`menu_order`,`isactive`,`created_by`,`created_date`,`updated_by`,`updated_date`) values (1,0,'Home','home','windows',1,1,'1',NULL,'1','2015-09-14 02:19:13'),(2,0,'Master Class','class_m','briefcase',2,1,'1',NULL,'1','2015-09-16 20:34:50'),(3,0,'Document','document','file',4,1,'1',NULL,'1','2015-09-16 20:34:08'),(4,3,'video','document/video','file-movie-o',1,1,'1',NULL,'1','2015-09-14 01:18:59'),(5,3,'File','document/file','file-pdf-o',2,1,'1',NULL,NULL,NULL),(6,3,'Materi','document/materi','file-word-o',3,1,'1',NULL,NULL,NULL),(7,0,'Nilai','nilai','newspaper-o',4,1,'1',NULL,NULL,NULL),(8,0,'Report','report','bar-chart',5,1,'1',NULL,NULL,NULL),(9,0,'Administration','','user',50,1,'1',NULL,NULL,NULL),(10,9,'Menu','menu','bars',1,1,'1',NULL,NULL,NULL),(11,9,'level','level','bookmark-o',2,1,'1',NULL,NULL,NULL),(12,9,'User','user','users',3,1,NULL,NULL,NULL,NULL),(13,9,'Reset Password','user/reset','edit',4,1,NULL,NULL,NULL,NULL),(15,14,'Instruktur','instruktur','user-plus',1,1,NULL,NULL,NULL,NULL),(16,14,'Peserta','peserta','user-md',2,1,NULL,NULL,NULL,NULL),(17,0,'Class (Peserta)','class_p','users',3,1,'1','2015-09-16 20:31:32','1','2015-09-16 23:46:41');

/*Data for the table `wb_menu_role` */

insert  into `wb_menu_role`(`id_menu_role`,`id_level`,`id_menu`,`isactive`,`created_by`,`created_date`,`updated_by`,`updated_date`) values (1,1,1,1,'1',NULL,NULL,NULL),(2,1,2,1,'1',NULL,NULL,NULL),(3,1,3,1,'1',NULL,NULL,NULL),(4,1,4,1,'1',NULL,NULL,NULL),(5,1,5,1,'1',NULL,NULL,NULL),(6,1,6,1,'1',NULL,NULL,NULL),(7,1,7,1,'1',NULL,NULL,NULL),(8,1,8,1,'1',NULL,NULL,NULL),(9,1,9,1,'1',NULL,NULL,NULL),(10,1,10,1,NULL,NULL,NULL,NULL),(11,1,11,1,NULL,NULL,NULL,NULL),(12,1,12,1,NULL,NULL,NULL,NULL),(13,1,13,1,NULL,NULL,NULL,NULL),(15,1,15,1,NULL,NULL,NULL,NULL),(16,1,16,1,NULL,NULL,NULL,NULL),(21,2,1,1,'1',NULL,NULL,NULL),(22,2,2,1,'1',NULL,NULL,NULL),(23,2,3,1,'1',NULL,NULL,NULL),(24,2,4,0,'1',NULL,NULL,NULL),(25,2,5,0,'1',NULL,NULL,NULL),(26,2,6,0,'1',NULL,NULL,NULL),(27,2,7,0,'1',NULL,NULL,NULL),(28,2,8,0,'1',NULL,NULL,NULL),(29,2,9,0,'1',NULL,NULL,NULL),(30,2,10,0,NULL,NULL,NULL,NULL),(31,2,11,0,NULL,NULL,NULL,NULL),(32,2,12,0,NULL,NULL,NULL,NULL),(33,2,13,0,NULL,NULL,NULL,NULL),(35,2,15,0,NULL,NULL,NULL,NULL),(36,2,16,0,NULL,NULL,NULL,NULL),(40,6,1,1,'1','2015-09-15 00:17:05',NULL,NULL),(41,6,2,1,'1','2015-09-15 00:17:05',NULL,NULL),(42,6,3,0,'1','2015-09-15 00:17:05',NULL,NULL),(43,6,4,0,'1','2015-09-15 00:17:05',NULL,NULL),(44,6,5,0,'1','2015-09-15 00:17:05',NULL,NULL),(45,6,6,0,'1','2015-09-15 00:17:05',NULL,NULL),(46,6,7,1,'1','2015-09-15 00:17:05',NULL,NULL),(47,6,8,1,'1','2015-09-15 00:17:05',NULL,NULL),(48,6,9,0,'1','2015-09-15 00:17:05',NULL,NULL),(49,6,10,0,'1','2015-09-15 00:17:05',NULL,NULL),(50,6,11,0,'1','2015-09-15 00:17:05',NULL,NULL),(51,6,12,0,'1','2015-09-15 00:17:05',NULL,NULL),(52,6,13,0,'1','2015-09-15 00:17:05',NULL,NULL),(54,6,15,0,'1','2015-09-15 00:17:05',NULL,NULL),(55,6,16,0,'1','2015-09-15 00:17:05',NULL,NULL),(56,1,17,1,'1','2015-09-16 20:31:32',NULL,NULL),(57,2,17,0,'1','2015-09-16 20:31:32',NULL,NULL),(58,6,17,0,'1','2015-09-16 20:31:32',NULL,NULL);

/*Data for the table `wb_pegawai` */

insert  into `wb_pegawai`(`id_pegawai`,`nip`,`nama`,`alamat`,`telp`,`email`,`tgl_lahir`,`photo`,`sertifikasi`,`skill`,`created_by`,`created_date`,`updated_by`,`updated_date`) values (1,'001','KUSNADI',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),(2,'002','Dudung Aja',NULL,'08567237237','dudung@yahoo.com',NULL,NULL,NULL,'Elektronika',NULL,'0000-00-00 00:00:00',NULL,NULL),(3,'003','imam',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00',NULL,NULL);

/*Data for the table `wb_pegawaix` */

insert  into `wb_pegawaix`(`id_pegawai`,`nip`,`nama`,`no_hp`,`email`,`tgl_lahir`,`photo`,`created_by`,`created_date`,`updated_by`,`updated_date`) values (1,'001','KUSNADI','88888','ADI','2015-01-01',NULL,'1','2015-09-10 02:10:35',NULL,NULL),(2,'002','duDUNG',NULL,NULL,NULL,NULL,NULL,'2015-09-14 18:09:19',NULL,NULL);

/*Data for the table `wb_periode` */

insert  into `wb_periode`(`id_periode`,`periode`,`start_date`,`end_date`,`id_class`,`id_schedule`,`created_by`,`created_date`,`updated_by`,`updated_date`) values ('PRD001','PERIODE1','2015-09-10 02:21:52','0000-00-00 00:00:00','CL001',NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00'),('PRD002','PERIODE2','2015-09-10 02:22:07','0000-00-00 00:00:00','CL001',NULL,NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00');

/*Data for the table `wb_peserta` */

insert  into `wb_peserta`(`id_peserta`,`id_user`,`id_class`,`created_by`,`created_date`,`updated_by`,`updated_date`) values ('PST001',1,'CL001',NULL,'2015-09-10 23:51:02',NULL,'0000-00-00 00:00:00');

/*Data for the table `wb_quiz` */

/*Data for the table `wb_schedule` */

insert  into `wb_schedule`(`id_schedule`,`schedule_name`,`id_class`,`id_periode`,`created_by`,`created_date`,`updated_by`,`updated_date`) values ('SCH001','Sch 1','CL001',NULL,NULL,NULL,NULL,NULL);

/*Data for the table `wb_ujian` */

/*Data for the table `wb_users` */

insert  into `wb_users`(`id_user`,`nip`,`username`,`passwords`,`isactive`,`islogin`,`id_level`,`id_sesion`,`last_login`,`created_by`,`created_date`,`updated_by`,`updated_date`) values (1,'001','admin','1dc485b4e573826275e518765ddb6671',1,1,1,'5e6a3737107fec424a7bcd062efe66e5','2015-09-16 22:07:03',NULL,'0000-00-00 00:00:00',1,'2015-09-14 23:37:38'),(2,'002','dudung','1dc485b4e573826275e518765ddb6671',1,1,6,'c1d541e767dba3a517cf225fb2202d14','2015-09-16 20:25:58',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00'),(5,'003','imam','1dc485b4e573826275e518765ddb6671',1,0,6,NULL,'2015-09-14 23:01:47',1,'2015-09-14 23:01:47',NULL,'0000-00-00 00:00:00');

/*Data for the table `wb_webcam` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
