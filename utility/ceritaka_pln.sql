-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 11, 2015 at 12:49 PM
-- Server version: 5.1.73-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ceritaka_pln`
--

-- --------------------------------------------------------

--
-- Table structure for table `wb_bank_soal`
--

CREATE TABLE IF NOT EXISTS `wb_bank_soal` (
  `id_bank_soal` int(11) NOT NULL AUTO_INCREMENT,
  `id_soal` varchar(50) DEFAULT NULL,
  `categori_quiz` varchar(20) DEFAULT NULL,
  `pertanyaan` text,
  `a` varchar(300) DEFAULT NULL,
  `b` varchar(300) DEFAULT NULL,
  `c` varchar(300) DEFAULT NULL,
  `d` varchar(300) DEFAULT NULL,
  `e` varchar(300) DEFAULT NULL,
  `jawaban_pg` varchar(2) DEFAULT NULL,
  `jawaban_essay` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bank_soal`),
  KEY `FK_wb_bank_soal` (`id_soal`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `wb_bank_soal`
--

INSERT INTO `wb_bank_soal` (`id_bank_soal`, `id_soal`, `categori_quiz`, `pertanyaan`, `a`, `b`, `c`, `d`, `e`, `jawaban_pg`, `jawaban_essay`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 'SL001', 'pg', 'Menurut deret tribolistrik, jika emas (Au) digosok dengan kain wol, maka emas akan ? ', 'bermuatan positif', 'bermuatan negatif', 'netral', 'mungkin positif, mungkin negatif', 'tidak ada jawaban yang benar', 'a', '', '2015-09-26 18:37:15', NULL, '2015-10-06 15:54:08', 5),
(3, 'SL001', 'pg', 'Sebuah benda akan bermuatan positif bila? ', 'kelebihan elektron', 'kekurangan elektron', 'kekurangan proton', 'jumlah proton sama dengan jumlah elektron', 'jawaban a dan c benar', 'a', '', '2015-09-27 13:02:35', 5, '2015-10-06 15:55:25', 5),
(4, 'SL001', 'essay', 'Jika di dalam suatu benda terdapat keseimbangan antara jumlah proton dengan jumlah elektron, maka benda tersebut? ', '', '', '', '', '', '', 'netral,,,,,,,, ', '2015-09-27 13:38:39', NULL, '2015-10-06 15:57:16', 5),
(5, 'SL001', 'pg', 'Sebuah benda akan bermuatan negatif bila ?', 'kelebihan elektron', 'kekurangan elektron', 'kekurangan proton', 'jumlah proton sama dengan jumlah elektron', 'semua pilihan salah', 'a', '', '2015-10-06 15:56:28', 5, '0000-00-00 00:00:00', NULL),
(6, 'SL001', 'essay', 'Jika dua muatan listrik sejenis didekatkan akan tolak-menolak dan bila tidak sejenis didekatkan akan tarik-menarik. Pernyataan tersebut sesuai dengan', '', '', '', '', '', '', 'Hukum ,,,,', '2015-10-06 15:57:43', 5, '0000-00-00 00:00:00', NULL),
(7, 'SL150926190328', 'pg', 'Pada hukum Coulomb besar gaya tarik atau gaya tolak antara dua muatan berbanding terbalik dengan?', 'besar muatan masing-masing', 'kuadrat muatan masing-masing', 'jarak antara dua muatan', 'kuadrat jarak antara dua muatan', 'kuadrat jarak antara dua muatan', 'a', '', '2015-10-06 15:59:07', 5, '0000-00-00 00:00:00', NULL),
(8, 'SL150926190328', 'pg', 'Satuan sistem internasional muatan listrik adala?', 'coulomb', 'ampere', 'farad', 'mikrocoulomb', 'watt', 'a', '', '2015-10-06 15:59:58', 5, '0000-00-00 00:00:00', NULL),
(9, 'SL150926190328', 'essay', 'Gelas dan mika akan mendapat muatan negatif bila digosok dengan bulu kelinci. Apakah muatan negatif tersebut mempunyai intensitas yang sama? Jelaskan!', '', '', '', '', '', '', '?', '2015-10-06 16:00:36', 5, '0000-00-00 00:00:00', NULL),
(10, 'SL150926190328', 'essay', 'Dua buah muatan listrik yang tidak sejenis didekatkan akan tarikmenarik dan apabila semakin didekatkan gaya tarik akan semakin besar, mengapa demikian?', '', '', '', '', '', '', '?', '2015-10-06 16:00:52', 5, '0000-00-00 00:00:00', NULL),
(11, 'SL150926190328', 'essay', 'Muatan +Q1 dan muatan -Q2 pada jarak R tarik menarik dengan gaya sebesar F. Jika jarak kedua muatan dibuat menjadi 3/4 R berapa besar gaya tarik menarik kedua muatan tersebut?', '', '', '', '', '', '', '?', '2015-10-06 16:01:05', 5, '0000-00-00 00:00:00', NULL),
(12, 'SL151021170635', 'pg', 'Berkas cahaya monokromatik 600 nm digunakan untuk menyinari suatu kisi difraksi secara tegak lurus. Jika jarak terang ke terang yang berdekatan adalah 1 cm dan jarak antar kisi adalah 45 ?m maka layar terletak dari kisi sejauh....', '30 cm', '40 cm', '50 cm', '60 cm', '90 cm', 'a', '', '2015-10-21 15:09:02', 1, '0000-00-00 00:00:00', NULL),
(13, 'SL151021170635', 'pg', 'Cahaya sebuah sumber dengan panjang gelombang sebesar 590 nm melalui dua celah sempit hingga terjadi pola-pola pada layar yang berjarak 1 m. Jika jarak antara garis gelap pertama dengan garis terang pertama adalah 2,95 mm, maka lebar celah adalah....', '0,1 mm', '0,2 mm', '0,3 mm', '0,4mm', '0,5 mm', 'b', '', '2015-10-21 15:09:55', 1, '0000-00-00 00:00:00', NULL),
(14, 'SL151021170635', 'pg', 'Percobaan lenturan cahaya pada kisi-kisi dapat diketahui bahwa penyimpangan cahaya biru lebih kecil dari cahaya kuning. Sedangkan penyimpangan kuning lebih kecil dari pada penyimpangan cahaya merah. Urutan cahaya dari panjang gelombang yang besar ke yang kecil adalah …', 'biru–kuning –merah', 'kuning–merah–biru ', 'merah–biru–kuning ', 'merah–kuning–biru', 'kuning–biru–merah ', 'd', '', '2015-10-21 15:10:48', 1, '0000-00-00 00:00:00', NULL),
(15, 'SL151021170635', 'pg', 'Pada percobaan Young, dua celah berjarak 1 mm diletak kan pada jarak 1 meter dari sebuah layar. Bila jarak terdekat antara pola interferensi garis terang pertama dan garis terang kesebelas adalah 4 mm, maka panjang gelombang cahaya yang menyinari adalah… ', '1.000 Å ', '2.000 Å ', '3.000 Å ', '4.000 Å ', '1.000 Å ', 'b', '', '2015-10-21 15:11:25', 1, '0000-00-00 00:00:00', NULL),
(16, 'SL151021170635', 'essay', 'Seberkas cahaya dengan panjang gelombang 5000 Å jatuh tegak lurus pada pada suatu kisi yang terdiri dari 5000 garis tiap cm. Sudut bias orde kedua yang terjadi adalah....', '', '', '', '', '', '', 'xx', '2015-10-21 15:12:16', 1, '0000-00-00 00:00:00', NULL),
(17, 'SL151021170635', 'essay', 'Berkas cahaya monokromatik 600 nm digunakan untuk menyinari suatu kisi difraksi secara tegak lurus. Jika jarak terang ke terang yang berdekatan adalah 1 cm dan jarak antar kisi adalah 45 ?m maka layar terletak dari kisi sejauh....', '', '', '', '', '', '', 'yy', '2015-10-21 15:12:30', 1, '0000-00-00 00:00:00', NULL),
(18, 'SL151021170635', 'essay', ' Dua celah yang berjarak 1 mm disinari cahaya merah dengan panjang gelombang 6,5 x 10?7 m. Sebuah layar diletakkan 1 m dari celah untuk mengamati pola-pola yang terjadi. Jarak antara gelap pertama dengan terang ketiga adalah....', '', '', '', '', '', '', 'xx', '2015-10-21 15:12:41', 1, '0000-00-00 00:00:00', NULL),
(19, 'SL151022122406', 'pg', 'untuk jalan kedepan lewat mana yang bener', 'Kesamping', 'kedepan', 'kebalakang', 'kesamping kiri', 'kesamping depan', 'b', '', '2015-10-22 05:25:21', 5, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_categori_quiz`
--

CREATE TABLE IF NOT EXISTS `wb_categori_quiz` (
  `id_categori_quiz` int(11) NOT NULL AUTO_INCREMENT,
  `categori_quiz` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_categori_quiz`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wb_class`
--

CREATE TABLE IF NOT EXISTS `wb_class` (
  `id_class` varchar(50) NOT NULL,
  `class` varchar(150) NOT NULL,
  `instruktur2` int(11) DEFAULT NULL,
  `instruktur1` int(11) DEFAULT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_class`),
  KEY `FK_wb_class_ins` (`instruktur1`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_class`
--

INSERT INTO `wb_class` (`id_class`, `class`, `instruktur2`, `instruktur1`, `lokasi`, `keterangan`, `start_date`, `status`, `end_date`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
('CL001', 'Listrik Statis', 10, 5, 'Jln. pulau seribu, tangerang selatan', 'Mempelajari listrik statis', '2015-09-01', 'active', '2015-10-24', 5, '2015-10-21 14:54:00', 1, '2015-10-21 14:54:00'),
('CLS151010183751', 'Interferensi Difraksi Cahaya', 9, 10, 'BDS', 'Interferensi Difraksi Cahaya', '2015-10-01', NULL, '2016-01-30', 1, '2015-10-21 15:13:20', 1, '2015-10-21 15:13:20');

-- --------------------------------------------------------

--
-- Table structure for table `wb_document`
--

CREATE TABLE IF NOT EXISTS `wb_document` (
  `id_document` varchar(50) NOT NULL,
  `document` varchar(150) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `id_class` varchar(100) DEFAULT NULL,
  `id_periode` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_document`),
  KEY `FK_wb_document` (`id_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_document`
--

INSERT INTO `wb_document` (`id_document`, `document`, `file_name`, `type`, `path`, `id_class`, `id_periode`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
('DOC001', 'FILE 1', 'Apress - Beginning Android (2009).pdf', 'file', './front_assets/file/89deae6674c0089000c1e1166966935a.pdf', 'CL001', 'PRD001', NULL, '2015-09-09 19:23:56', 5, '2015-09-27 14:22:32'),
('FL150927162307', 'FILE 2', 'Apress - Android Essentials (2008).pdf', 'file', './front_assets/file/e8c1020281c447ef52691116169c4fef.pdf', 'CL001', 'PRD002', 5, '2015-09-27 14:23:07', NULL, '0000-00-00 00:00:00'),
('FL151002193725', 'adsad', 'Transkip2.jpg', 'file', './front_assets/file/24689291946480cb014b40d775664483.jpg', 'CL001', 'PRD001', 1, '2015-10-02 17:37:25', NULL, '0000-00-00 00:00:00'),
('MAT00X', 'MATERI 1', '1.1 Mengenal Lingkup Pemrograman.pdf', 'materi', './front_assets/file/0813dad61be61c27052e8cf15bae7276.pdf', 'CL001', 'PRD001', NULL, '2015-09-19 14:54:44', 1, '2015-10-02 17:25:06'),
('MTR150927161413', 'MATERI 2', '1.2 Dasar-Dasar Pemrograman.pdf', 'materi', './front_assets/file/277fa6e7208d819245e0b26616ccdf53.pdf', 'CL001', 'PRD002', 5, '2015-09-27 14:14:13', NULL, '0000-00-00 00:00:00'),
('MTR151002192817', 'aqw', 'Cert.jpg', 'materi', './front_assets/file/e1d819d3558ad1cd30fbfce0c5869ef6.jpg', 'CL001', 'PRD002', 1, '2015-10-02 17:28:17', NULL, '0000-00-00 00:00:00'),
('MTR151002193527', 'asdadasdwwww', 'Transkip2.jpg', 'materi', './front_assets/file/eb3a7fb8f2b06e35bc1167a71432b29f.jpg', 'CL001', 'PRD001', 1, '2015-10-02 17:35:27', NULL, '0000-00-00 00:00:00'),
('VD150927161826', 'VIDEO 1', '#1 - Android Studio Tutorial   Php Mysql Login and Register.mp4', 'video', './front_assets/file/5ba2debe1c42b8bf416cf4b461c8bfcb.mp4', 'CL001', 'PRD001', 5, '2015-09-27 14:18:31', 1, '2015-09-29 16:24:24'),
('VD150927161931', 'VIDEO 2', '#2 - Android Studio Tutorial   Php Mysql Login and Register.mp4', 'video', './front_assets/file/8ab36c8c1f01a031cbb7edb4f5b7d0de.mp4', 'CL001', 'PRD002', 5, '2015-09-27 14:19:35', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wb_jawaban`
--

CREATE TABLE IF NOT EXISTS `wb_jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(20) DEFAULT NULL,
  `id_soal` varchar(50) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `jawaban_pg` varchar(50) DEFAULT NULL,
  `jawaban_essay` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_jawaban`),
  KEY `FK_wb_jawaban_quiz` (`id_soal`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `wb_jawaban`
--

INSERT INTO `wb_jawaban` (`id_jawaban`, `jenis`, `id_soal`, `id_bank_soal`, `jawaban_pg`, `jawaban_essay`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(10, 'essay', 'SL150926190328', 11, NULL, 'asdadasd', 2, '2015-10-20 15:22:47', NULL, NULL),
(11, 'essay', 'SL150926190328', 10, NULL, 'asdad', 2, '2015-10-20 15:23:06', NULL, NULL),
(13, 'essay', 'SL150926190328', 9, NULL, 'asdadsad 12313', 2, '2015-10-20 15:41:48', NULL, NULL),
(17, 'essay', 'SL001', 4, NULL, 'zz', 2, '2015-10-20 15:55:06', NULL, NULL),
(18, 'essay', 'SL001', 6, NULL, 'dd', 2, '2015-10-20 15:55:13', NULL, NULL),
(36, 'pg', 'SL001', 1, 'c', NULL, 2, '2015-10-22 05:26:23', NULL, NULL),
(37, 'pg', 'SL001', 3, 'c', NULL, 2, '2015-10-22 05:26:27', NULL, NULL),
(23, 'pg', 'SL001', 5, 'b', NULL, 2, '2015-10-20 18:24:50', NULL, NULL),
(27, 'pg', 'SL150926190328', 7, 'a', NULL, 2, '2015-10-21 12:49:15', NULL, NULL),
(28, 'pg', 'SL150926190328', 8, 'a', NULL, 2, '2015-10-21 12:49:19', NULL, NULL),
(33, 'pg', 'SL151021170635', 12, 'a', NULL, 2, '2015-10-22 04:56:16', NULL, NULL),
(30, 'pg', 'SL151021170635', 13, 'a', NULL, 2, '2015-10-21 15:48:24', NULL, NULL),
(34, 'pg', 'SL151021170635', 14, 'e', NULL, 2, '2015-10-22 04:56:29', NULL, NULL),
(32, 'pg', 'SL151021170635', 15, 'e', NULL, 2, '2015-10-21 15:48:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_jawaban_peserta`
--

CREATE TABLE IF NOT EXISTS `wb_jawaban_peserta` (
  `id_jawaban_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` varchar(50) DEFAULT NULL,
  `id_quiz` varchar(50) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `jawaban` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_jawaban_peserta`),
  KEY `FK_wb_jawaban_peserta` (`id_peserta`),
  KEY `FK_wb_jawaban_quiz` (`id_quiz`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wb_jenis_materi`
--

CREATE TABLE IF NOT EXISTS `wb_jenis_materi` (
  `id_jenis` varchar(50) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `id_materi` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_jenis`),
  KEY `FK_wb_jenis_materi` (`id_materi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_jenis_materi`
--

INSERT INTO `wb_jenis_materi` (`id_jenis`, `jenis`, `id_materi`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
('JNS001', 'JENIS 1', 'MTR001', NULL, '2015-09-09 19:29:14', NULL, '0000-00-00 00:00:00'),
('JNS002', 'JENIS 2', 'MTR001', NULL, '2015-09-09 19:29:31', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wb_jenis_quiz`
--

CREATE TABLE IF NOT EXISTS `wb_jenis_quiz` (
  `id_jenis_quis` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_quiz` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_quis`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wb_level`
--

CREATE TABLE IF NOT EXISTS `wb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id_level`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `wb_level`
--

INSERT INTO `wb_level` (`id_level`, `level_name`, `created_by`, `created_date`, `updated_by`, `updated_date`, `isactive`) VALUES
(1, 'Admin', NULL, '2015-09-08 17:19:58', NULL, NULL, 1),
(2, 'Peserta', NULL, '2015-09-08 17:20:08', 1, '2015-09-26', 1),
(6, 'Instruktur', 1, '2015-09-14 17:14:01', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wb_login_log`
--

CREATE TABLE IF NOT EXISTS `wb_login_log` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `id_sesion` varchar(90) DEFAULT NULL,
  `id_pegawai` varchar(50) DEFAULT NULL,
  `ip_address` varchar(90) DEFAULT NULL,
  `login_start` datetime DEFAULT NULL,
  `login_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wb_materi`
--

CREATE TABLE IF NOT EXISTS `wb_materi` (
  `id_materi` varchar(50) NOT NULL,
  `materi` varchar(200) NOT NULL,
  `path` varchar(100) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_materi`),
  KEY `FK_wb_materi` (`id_class`),
  KEY `FK_wb_mater` (`id_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_materi`
--

INSERT INTO `wb_materi` (`id_materi`, `materi`, `path`, `id_class`, `id_periode`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
('MTR001', 'Trafo', NULL, 'CL001', NULL, NULL, '2015-09-08 17:41:16', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wb_menu`
--

CREATE TABLE IF NOT EXISTS `wb_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `menu_name` varchar(90) DEFAULT NULL,
  `menu_url` varchar(100) DEFAULT NULL,
  `menu_icon` varchar(100) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `wb_menu`
--

INSERT INTO `wb_menu` (`id_menu`, `id_parent`, `menu_name`, `menu_url`, `menu_icon`, `menu_order`, `isactive`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 0, 'Home', 'home', 'xE871', 1, 1, '1', NULL, '1', '2015-11-02 09:19:41'),
(2, 0, 'Master Kelas', 'class_m', 'xE1BD', 2, 1, '1', NULL, '1', '2015-10-17 15:39:00'),
(3, 0, 'Document', 'document', 'xE24D', 6, 1, '1', NULL, '1', '2015-10-17 15:40:51'),
(4, 3, 'video', 'document/video', 'file-movie-o', 1, 1, '1', NULL, '1', '2015-09-13 18:18:59'),
(5, 3, 'File', 'document/file', 'file-pdf-o', 2, 1, '1', NULL, NULL, NULL),
(6, 3, 'Materi', 'document/materi', 'file-word-o', 3, 1, '1', NULL, NULL, NULL),
(7, 0, 'Nilai', 'nilai', 'xE53E', 10, 1, '1', NULL, '1', '2015-10-17 15:41:50'),
(8, 0, 'Report', 'report', 'xE85C', 20, 1, '1', NULL, '1', '2015-10-17 15:39:34'),
(9, 0, 'Administration', 'a', 'xE8C0', 30, 1, '1', NULL, '1', '2015-10-17 16:36:16'),
(10, 9, 'Menu', 'menu', 'bars', 1, 1, '1', NULL, NULL, NULL),
(11, 9, 'level', 'level', 'bookmark-o', 2, 1, '1', NULL, NULL, NULL),
(12, 9, 'User', 'user', 'users', 3, 1, NULL, NULL, NULL, NULL),
(13, 9, 'Reset Password', 'user/reset', 'edit', 4, 1, NULL, NULL, NULL, NULL),
(15, 14, 'Instruktur', 'instruktur', 'user-plus', 1, 1, NULL, NULL, NULL, NULL),
(16, 14, 'Peserta', 'peserta', 'user-md', 2, 1, NULL, NULL, NULL, NULL),
(17, 0, 'Kelas (Peserta)', 'class_p', 'xE8CB', 5, 1, '1', '2015-09-16 13:31:32', '1', '2015-10-17 15:48:24'),
(18, 0, 'Soal', 'soal', 'xE0B9', 9, 1, '1', '2015-09-26 08:12:24', '1', '2015-10-17 15:45:01'),
(19, 0, 'Jadwal Ujian', 'exam_schd', 'xE8D2', 7, 1, '1', '2015-09-28 13:00:25', '1', '2015-10-17 15:44:39'),
(20, 0, 'Ujian', 'exam', 'xE32A', 8, 1, '1', '2015-09-29 17:13:10', '1', '2015-10-17 15:46:42'),
(21, 0, 'Master Peserta', 'participant', 'xE87B', 4, 1, '1', '2015-09-30 15:15:13', '1', '2015-10-17 15:43:40'),
(22, 0, 'Master Pegawai', 'employee', 'xE87C', 3, 1, '1', '2015-09-30 17:37:05', '1', '2015-10-17 15:42:52'),
(23, 0, 'Profile', 'profile', 'xE87C', 40, 1, '1', '2015-10-17 16:36:49', '1', '2015-10-17 16:37:25');

-- --------------------------------------------------------

--
-- Table structure for table `wb_menu_role`
--

CREATE TABLE IF NOT EXISTS `wb_menu_role` (
  `id_menu_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu_role`),
  KEY `FK_wb_menu_role` (`id_menu`),
  KEY `FK_wb_menu_level` (`id_level`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `wb_menu_role`
--

INSERT INTO `wb_menu_role` (`id_menu_role`, `id_level`, `id_menu`, `isactive`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, 1, 1, 1, '1', NULL, NULL, NULL),
(2, 1, 2, 1, '1', NULL, NULL, NULL),
(3, 1, 3, 1, '1', NULL, NULL, NULL),
(4, 1, 4, 1, '1', NULL, NULL, NULL),
(5, 1, 5, 1, '1', NULL, NULL, NULL),
(6, 1, 6, 1, '1', NULL, NULL, NULL),
(7, 1, 7, 1, '1', NULL, NULL, NULL),
(8, 1, 8, 1, '1', NULL, NULL, NULL),
(9, 1, 9, 1, '1', NULL, NULL, NULL),
(10, 1, 10, 1, NULL, NULL, NULL, NULL),
(11, 1, 11, 1, NULL, NULL, NULL, NULL),
(12, 1, 12, 1, NULL, NULL, NULL, NULL),
(13, 1, 13, 1, NULL, NULL, NULL, NULL),
(15, 1, 15, 1, NULL, NULL, NULL, NULL),
(16, 1, 16, 1, NULL, NULL, NULL, NULL),
(21, 2, 1, 1, '1', NULL, NULL, NULL),
(22, 2, 2, 0, '1', NULL, NULL, NULL),
(23, 2, 3, 0, '1', NULL, NULL, NULL),
(24, 2, 4, 0, '1', NULL, NULL, NULL),
(25, 2, 5, 0, '1', NULL, NULL, NULL),
(26, 2, 6, 0, '1', NULL, NULL, NULL),
(27, 2, 7, 0, '1', NULL, NULL, NULL),
(28, 2, 8, 0, '1', NULL, NULL, NULL),
(29, 2, 9, 0, '1', NULL, NULL, NULL),
(30, 2, 10, 0, NULL, NULL, NULL, NULL),
(31, 2, 11, 0, NULL, NULL, NULL, NULL),
(32, 2, 12, 0, NULL, NULL, NULL, NULL),
(33, 2, 13, 0, NULL, NULL, NULL, NULL),
(35, 2, 15, 0, NULL, NULL, NULL, NULL),
(36, 2, 16, 0, NULL, NULL, NULL, NULL),
(40, 6, 1, 1, '1', '2015-09-14 17:17:05', NULL, NULL),
(41, 6, 2, 1, '1', '2015-09-14 17:17:05', NULL, NULL),
(42, 6, 3, 1, '1', '2015-09-14 17:17:05', NULL, NULL),
(43, 6, 4, 1, '1', '2015-09-14 17:17:05', NULL, NULL),
(44, 6, 5, 1, '1', '2015-09-14 17:17:05', NULL, NULL),
(45, 6, 6, 1, '1', '2015-09-14 17:17:05', NULL, NULL),
(46, 6, 7, 0, '1', '2015-09-14 17:17:05', NULL, NULL),
(47, 6, 8, 1, '1', '2015-09-14 17:17:05', NULL, NULL),
(48, 6, 9, 0, '1', '2015-09-14 17:17:05', NULL, NULL),
(49, 6, 10, 0, '1', '2015-09-14 17:17:05', NULL, NULL),
(50, 6, 11, 0, '1', '2015-09-14 17:17:05', NULL, NULL),
(51, 6, 12, 0, '1', '2015-09-14 17:17:05', NULL, NULL),
(52, 6, 13, 0, '1', '2015-09-14 17:17:05', NULL, NULL),
(54, 6, 15, 0, '1', '2015-09-14 17:17:05', NULL, NULL),
(55, 6, 16, 0, '1', '2015-09-14 17:17:05', NULL, NULL),
(56, 1, 17, 1, '1', '2015-09-16 13:31:32', NULL, NULL),
(57, 2, 17, 1, '1', '2015-09-16 13:31:32', NULL, NULL),
(58, 6, 17, 0, '1', '2015-09-16 13:31:32', NULL, NULL),
(59, 1, 18, 1, '1', '2015-09-26 08:12:24', NULL, NULL),
(60, 2, 18, 0, '1', '2015-09-26 08:12:24', NULL, NULL),
(61, 6, 18, 1, '1', '2015-09-26 08:12:24', NULL, NULL),
(62, 1, 19, 1, '1', '2015-09-28 13:00:25', NULL, NULL),
(63, 2, 19, 1, '1', '2015-09-28 13:00:25', NULL, NULL),
(64, 6, 19, 1, '1', '2015-09-28 13:00:25', NULL, NULL),
(65, 1, 20, 1, '1', '2015-09-29 17:13:10', NULL, NULL),
(66, 2, 20, 1, '1', '2015-09-29 17:13:10', NULL, NULL),
(67, 6, 20, 0, '1', '2015-09-29 17:13:10', NULL, NULL),
(68, 1, 21, 1, '1', '2015-09-30 15:15:13', NULL, NULL),
(69, 2, 21, 0, '1', '2015-09-30 15:15:13', NULL, NULL),
(70, 6, 21, 0, '1', '2015-09-30 15:15:13', NULL, NULL),
(71, 1, 22, 1, '1', '2015-09-30 17:37:05', NULL, NULL),
(72, 2, 22, 0, '1', '2015-09-30 17:37:05', NULL, NULL),
(73, 6, 22, 0, '1', '2015-09-30 17:37:06', NULL, NULL),
(74, 1, 23, 1, '1', '2015-10-17 16:36:49', NULL, NULL),
(75, 2, 23, 1, '1', '2015-10-17 16:36:49', NULL, NULL),
(76, 6, 23, 1, '1', '2015-10-17 16:36:49', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_pegawai`
--

CREATE TABLE IF NOT EXISTS `wb_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `sertifikasi` varchar(200) DEFAULT NULL,
  `jabatan` varchar(200) DEFAULT NULL,
  `skill` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `wb_pegawai`
--

INSERT INTO `wb_pegawai` (`id_pegawai`, `nip`, `nama`, `alamat`, `telp`, `email`, `tgl_lahir`, `photo`, `sertifikasi`, `jabatan`, `skill`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, '001', 'Kusnadi', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742001', 'Kusnadi@yahoo.com', NULL, './front_assets/profile/9419cdde5d53aa64ef5865ffdfb05888.png', NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(2, '002', 'Dudung', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742002', 'Dudung@yahoo.com', NULL, './front_assets/profile/24cfcd59349946d0ef4164f48b53b33e.png', NULL, 'Staff', 'Elektronika', NULL, '0000-00-00 00:00:00', NULL, NULL),
(3, '003', 'imam', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742003', 'imam@yahoo.com', NULL, './front_assets/profile/49fccd80b43614458674bc9f388b3f67.png', NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(4, '004', 'Intan', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742004', 'Intan@yahoo.com', NULL, './front_assets/profile/avatar_04.PNG', NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(5, '005', 'Nani', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742005', 'Nani@yahoo.com', NULL, './front_assets/profile/avatar_05.PNG', NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(6, '006', 'Emmy', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742006', 'Emmy@yahoo.com', NULL, './front_assets/profile/avatar_06.PNG', NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(7, '007', 'Joni', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742007', 'Joni@yahoo.com', NULL, './front_assets/profile/avatar_07.PNG', NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(8, '008', 'Mamad', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742008', 'Mamad@yahoo.com', NULL, NULL, NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(9, '009', 'yusuf', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742009', 'yusuf@yahoo.com', NULL, './front_assets/profile/5adf1f83568256154af85a166ab0f336.jpg', NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(10, '010', 'Asep', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742010', 'Asep@yahoo.com', NULL, NULL, NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(11, '011', 'Fitri', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742011', 'Fitri@yahoo.com', NULL, NULL, NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(12, '012', 'Dadang', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742012', 'Dadang@yahoo.com', NULL, NULL, NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(13, '013', 'Hardi', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742013', 'Hardi@yahoo.com', NULL, NULL, NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(14, '014', 'Soni', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742014', 'Soni@yahoo.com', NULL, NULL, NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL),
(15, '015', 'Jamal', 'Jl. Pulau Seribu NO. 12 A, BSD', '0856742015', 'Jamal@yahoo.com', NULL, NULL, NULL, 'Staff', NULL, NULL, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_periode`
--

CREATE TABLE IF NOT EXISTS `wb_periode` (
  `id_periode` varchar(50) NOT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_schedule` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_periode`),
  KEY `FK_wb_periode` (`id_class`),
  KEY `FK_wb_sch` (`id_schedule`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_periode`
--

INSERT INTO `wb_periode` (`id_periode`, `periode`, `start_date`, `end_date`, `id_class`, `id_schedule`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
('PRD001', 'PERIODE1', '2015-10-02', '2015-11-10', 'CL001', NULL, NULL, '0000-00-00 00:00:00', 1, '2015-10-10 18:02:34'),
('PRD002', 'PERIODE2', '2015-11-07', '2015-11-23', 'CL001', NULL, NULL, '0000-00-00 00:00:00', 1, '2015-10-06 16:03:26'),
('PRD15101018371', 'Periode 1', '2015-10-01', '2015-10-17', 'CLS151010183751', NULL, 1, '2015-10-10 16:37:51', NULL, '0000-00-00 00:00:00'),
('PRD15101018372', 'Periode 2', '2015-10-31', '2015-10-25', 'CLS151010183751', NULL, 1, '2015-10-10 16:37:51', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wb_peserta`
--

CREATE TABLE IF NOT EXISTS `wb_peserta` (
  `id_peserta` varchar(50) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_peserta`),
  KEY `FK_wb_peserta` (`id_class`),
  KEY `FK_wb_peserta_reg` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_peserta`
--

INSERT INTO `wb_peserta` (`id_peserta`, `id_user`, `id_class`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
('PST150930193058', 2, 'CL001', '1', '2015-10-21 14:15:17', '2', '2015-10-21 14:15:17'),
('PST150930203251', 6, 'CL001', '1', '2015-09-30 18:32:51', NULL, '0000-00-00 00:00:00'),
('PST151006191033', 7, 'CL001', '1', '2015-10-06 17:10:33', NULL, '0000-00-00 00:00:00'),
('PST151006191039', 8, 'CL001', '1', '2015-10-06 17:10:39', NULL, '0000-00-00 00:00:00'),
('PST151021162010', 6, 'CLS151010183751', '1', '2015-10-21 14:20:10', NULL, '0000-00-00 00:00:00'),
('PST151021162021', 7, 'CLS151010183751', '1', '2015-10-21 14:20:21', NULL, '0000-00-00 00:00:00'),
('PST151021162030', 12, 'CLS151010183751', '1', '2015-10-21 14:20:30', NULL, '0000-00-00 00:00:00'),
('PST151021163456', 17, 'CLS151010183751', '1', '2015-10-21 14:34:56', NULL, '0000-00-00 00:00:00'),
('PST151021165146', 2, 'CLS151010183751', '1', '2015-10-21 14:51:46', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wb_quiz`
--

CREATE TABLE IF NOT EXISTS `wb_quiz` (
  `id_quiz` varchar(50) NOT NULL,
  `quiz` varchar(100) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_quiz`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wb_schedule`
--

CREATE TABLE IF NOT EXISTS `wb_schedule` (
  `id_schedule` varchar(50) NOT NULL,
  `schedule_name` varchar(150) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_schedule`),
  KEY `FK_wb_schedule` (`id_class`),
  KEY `FK_wb_periode` (`id_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_schedule`
--

INSERT INTO `wb_schedule` (`id_schedule`, `schedule_name`, `id_class`, `id_periode`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
('SCH001', 'Sch 1', 'CL001', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_schedule_bak`
--

CREATE TABLE IF NOT EXISTS `wb_schedule_bak` (
  `id_schedule` varchar(50) NOT NULL,
  `schedule_name` varchar(150) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_schedule`),
  KEY `FK_wb_schedule` (`id_class`),
  KEY `FK_wb_periode` (`id_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wb_soal`
--

CREATE TABLE IF NOT EXISTS `wb_soal` (
  `id_soal` varchar(50) NOT NULL,
  `soal` varchar(200) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_soal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_soal`
--

INSERT INTO `wb_soal` (`id_soal`, `soal`, `id_class`, `id_periode`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
('SL001', 'Listrik Statis 1', 'CL001', 'PRD001', '2015-09-26 04:42:57', NULL, '2015-10-06 15:52:29', 5),
('SL150926190328', 'Listrik Statis 2', 'CL001', 'PRD002', '2015-09-26 17:03:28', 5, '2015-10-06 15:58:07', 5),
('SL151021170635', 'Cahaya', 'CLS151010183751', 'PRD15101018371', '2015-10-21 15:06:35', 1, '2015-10-21 15:25:45', 1),
('SL151022122406', 'Jalan ke depan lewat mana', 'CL001', 'PRD002', '2015-10-22 05:24:06', 5, '0000-00-00 00:00:00', NULL),
('SL151022140125', 'test', 'CLS151010183751', 'PRD15101018371', '2015-10-22 07:01:25', 5, '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wb_test`
--

CREATE TABLE IF NOT EXISTS `wb_test` (
  `nama` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_test`
--

INSERT INTO `wb_test` (`nama`) VALUES
('199.76666666667'),
('199.83333333333'),
('199.85'),
('199.93333333333'),
('199.95'),
('195.33333333333'),
('0'),
('0.016666666666667'),
('0.016666666666667');

-- --------------------------------------------------------

--
-- Table structure for table `wb_time_history`
--

CREATE TABLE IF NOT EXISTS `wb_time_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_ujian` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wb_ujian`
--

CREATE TABLE IF NOT EXISTS `wb_ujian` (
  `id_ujian` varchar(50) NOT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `id_soal` varchar(50) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sesion` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_ujian`),
  KEY `FK_wb_ujian` (`id_soal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wb_ujian`
--

INSERT INTO `wb_ujian` (`id_ujian`, `id_class`, `id_periode`, `id_soal`, `durasi`, `start_date`, `end_date`, `sesion`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
('UJN001', 'CL001', 'PRD001', 'SL001', 100, '2015-10-22', '0000-00-00', '1', NULL, '0000-00-00 00:00:00', 1, '2015-10-22 04:43:31'),
('UJN150929185337', 'CL001', 'PRD002', 'SL150926190328', 100, '2015-11-07', NULL, '1', 1, '2015-09-29 16:53:37', 1, '2015-11-07 15:52:10'),
('UJN151022114503', 'CLS151010183751', 'PRD15101018371', 'SL151021170635', 90, '2015-10-22', NULL, '1', 1, '2015-10-22 04:45:03', NULL, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `wb_ujian_history`
--

CREATE TABLE IF NOT EXISTS `wb_ujian_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_ujian` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `wb_ujian_history`
--

INSERT INTO `wb_ujian_history` (`id`, `id_user`, `id_ujian`, `created_date`) VALUES
(17, 2, 'UJN150929185337', '2015-11-07 15:53:45');

-- --------------------------------------------------------

--
-- Table structure for table `wb_users`
--

CREATE TABLE IF NOT EXISTS `wb_users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `passwords` varchar(120) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `islogin` tinyint(1) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_sesion` varchar(100) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_user`,`nip`),
  KEY `FK_wb_level` (`id_level`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `wb_users`
--

INSERT INTO `wb_users` (`id_user`, `nip`, `username`, `passwords`, `isactive`, `islogin`, `id_level`, `id_sesion`, `last_login`, `created_by`, `created_date`, `updated_by`, `updated_date`) VALUES
(1, '001', 'admin', '3d47bd12248e350d8d1fe95eaa2e74d3', 1, 1, 1, 'ddcbb61c6f18965d09b546dee07f3376', '2015-11-10 04:42:37', NULL, '0000-00-00 00:00:00', 1, '2015-10-28 07:32:17'),
(2, '002', 'dudung', '1dc485b4e573826275e518765ddb6671', 1, 1, 2, 'd8f6786d683cf75e23fb4fde1b5d8298', '2015-11-07 15:50:41', NULL, '0000-00-00 00:00:00', 2, '2015-10-17 14:37:12'),
(5, '003', 'imam', '1dc485b4e573826275e518765ddb6671', 1, 1, 6, '0b69e13cafde9eec7aa45777c802cc80', '2015-11-10 00:55:54', 1, '2015-09-14 16:01:47', NULL, '0000-00-00 00:00:00'),
(6, '004', 'sopo', '1dc485b4e573826275e518765ddb6671', 1, 1, 2, 'b6f0b904cbe5f05469b4ad768f16d577', '2015-10-20 18:34:34', NULL, '0000-00-00 00:00:00', NULL, '0000-00-00 00:00:00'),
(7, '006', 'emmy', '1dc485b4e573826275e518765ddb6671', 1, NULL, 2, NULL, '2015-10-06 17:09:53', 1, '2015-10-06 17:09:53', NULL, '0000-00-00 00:00:00'),
(8, '007', 'joni', '1dc485b4e573826275e518765ddb6671', 1, NULL, 2, NULL, '2015-10-06 17:10:14', 1, '2015-10-06 17:10:14', NULL, '0000-00-00 00:00:00'),
(9, '012', 'dadang', '1dc485b4e573826275e518765ddb6671', 1, 0, 6, '', '2015-10-22 03:09:38', 1, '2015-10-21 14:07:20', NULL, '0000-00-00 00:00:00'),
(10, '013', 'hardi', '1dc485b4e573826275e518765ddb6671', 1, NULL, 6, NULL, '2015-10-21 14:07:51', 1, '2015-10-21 14:07:51', NULL, '0000-00-00 00:00:00'),
(11, '009', 'yusuf', '1dc485b4e573826275e518765ddb6671', 1, 0, 2, '', '2015-10-21 14:10:49', 1, '2015-10-21 14:08:12', NULL, '0000-00-00 00:00:00'),
(12, '008', 'mamad', '1dc485b4e573826275e518765ddb6671', 1, NULL, 2, NULL, '2015-10-21 14:08:32', 1, '2015-10-21 14:08:32', NULL, '0000-00-00 00:00:00'),
(13, '010', 'asep', '1dc485b4e573826275e518765ddb6671', 1, NULL, 2, NULL, '2015-10-21 14:08:51', 1, '2015-10-21 14:08:51', NULL, '0000-00-00 00:00:00'),
(14, '011', 'fitri', '1dc485b4e573826275e518765ddb6671', 1, NULL, 1, NULL, '2015-10-21 14:09:10', 1, '2015-10-21 14:09:10', 1, '2015-10-21 14:10:26'),
(15, '014', 'soni', '1dc485b4e573826275e518765ddb6671', 1, NULL, 2, NULL, '2015-10-21 14:09:31', 1, '2015-10-21 14:09:31', NULL, '0000-00-00 00:00:00'),
(16, '015', 'jamal', '1dc485b4e573826275e518765ddb6671', 0, NULL, 1, NULL, '2015-10-21 14:09:45', 1, '2015-10-21 14:09:45', NULL, '0000-00-00 00:00:00'),
(17, '005', 'nani', '1dc485b4e573826275e518765ddb6671', 1, 1, 2, '074e6b15016f0a1b906d3507f0a76129', '2015-10-21 16:00:55', 1, '2015-10-21 14:10:05', 1, '2015-10-21 15:59:00');

-- --------------------------------------------------------

--
-- Table structure for table `wb_webcam`
--

CREATE TABLE IF NOT EXISTS `wb_webcam` (
  `id_webcam` int(11) NOT NULL AUTO_INCREMENT,
  `type_webcam` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `status_online` int(11) DEFAULT NULL,
  `information` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_webcam`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
