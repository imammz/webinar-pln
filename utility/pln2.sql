
USE `mutiarah_pln`;

/*Table structure for table `wb_bank_soal` */

DROP TABLE IF EXISTS `wb_bank_soal`;

CREATE TABLE `wb_bank_soal` (
  `id_bank_soal` int(11) NOT NULL AUTO_INCREMENT,
  `id_quiz` varchar(50) DEFAULT NULL,
  `id_categori_quiz` int(11) DEFAULT NULL,
  `essay` text,
  `A` text,
  `b` text,
  `c` text,
  `d` text,
  `e` text,
  `jawaban` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_bank_soal`),
  KEY `FK_wb_bank_soal` (`id_quiz`),
  KEY `FK_wb_bank_category` (`id_categori_quiz`),
  CONSTRAINT `FK_wb_bank_category` FOREIGN KEY (`id_categori_quiz`) REFERENCES `wb_categori_quiz` (`id_categori_quiz`),
  CONSTRAINT `FK_wb_bank_soal` FOREIGN KEY (`id_quiz`) REFERENCES `wb_quiz` (`id_quiz`)
) ;

/*Table structure for table `wb_categori_quiz` */

DROP TABLE IF EXISTS `wb_categori_quiz`;

CREATE TABLE `wb_categori_quiz` (
  `id_categori_quiz` int(11) NOT NULL AUTO_INCREMENT,
  `categori_quiz` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_categori_quiz`)
) ;

/*Table structure for table `wb_class` */

DROP TABLE IF EXISTS `wb_class`;

CREATE TABLE `wb_class` (
  `id_class` varchar(50) NOT NULL,
  `class` varchar(150) NOT NULL,
  `instruktur2` int(11) DEFAULT NULL,
  `instruktur1` int(11) DEFAULT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_class`),
  KEY `FK_wb_class_ins` (`instruktur1`)
) ;

/*Table structure for table `wb_document` */

DROP TABLE IF EXISTS `wb_document`;

CREATE TABLE `wb_document` (
  `id_document` varchar(50) NOT NULL,
  `document` varchar(150) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `id_periode` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_document`),
  KEY `FK_wb_document` (`id_periode`),
  CONSTRAINT `FK_wb_document` FOREIGN KEY (`id_periode`) REFERENCES `wb_periode` (`id_periode`)
) ;

/*Table structure for table `wb_jawaban` */

DROP TABLE IF EXISTS `wb_jawaban`;

CREATE TABLE `wb_jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `id_bank_soal` int(11) NOT NULL,
  `jawaban` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_jawaban`,`id_bank_soal`),
  KEY `FK_wb_jawaban` (`id_bank_soal`),
  CONSTRAINT `FK_wb_jawaban` FOREIGN KEY (`id_bank_soal`) REFERENCES `wb_bank_soal` (`id_bank_soal`)
) ;

/*Table structure for table `wb_jawaban_peserta` */

DROP TABLE IF EXISTS `wb_jawaban_peserta`;

CREATE TABLE `wb_jawaban_peserta` (
  `id_jawaban_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` varchar(50) DEFAULT NULL,
  `id_quiz` varchar(50) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `jawaban` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_jawaban_peserta`),
  KEY `FK_wb_jawaban_peserta` (`id_peserta`),
  KEY `FK_wb_jawaban_quiz` (`id_quiz`),
  CONSTRAINT `FK_wb_jawaban_peserta` FOREIGN KEY (`id_peserta`) REFERENCES `wb_peserta` (`id_peserta`),
  CONSTRAINT `FK_wb_jawaban_quiz` FOREIGN KEY (`id_quiz`) REFERENCES `wb_quiz` (`id_quiz`)
) ;

/*Table structure for table `wb_jenis_materi` */

DROP TABLE IF EXISTS `wb_jenis_materi`;

CREATE TABLE `wb_jenis_materi` (
  `id_jenis` varchar(50) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `id_materi` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_jenis`),
  KEY `FK_wb_jenis_materi` (`id_materi`),
  CONSTRAINT `FK_wb_jenis_materi` FOREIGN KEY (`id_materi`) REFERENCES `wb_materi` (`id_materi`)
) ;

/*Table structure for table `wb_jenis_quiz` */

DROP TABLE IF EXISTS `wb_jenis_quiz`;

CREATE TABLE `wb_jenis_quiz` (
  `id_jenis_quis` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_quiz` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_quis`)
) ;

/*Table structure for table `wb_level` */

DROP TABLE IF EXISTS `wb_level`;

CREATE TABLE `wb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id_level`)
) ;

/*Table structure for table `wb_login_log` */

DROP TABLE IF EXISTS `wb_login_log`;

CREATE TABLE `wb_login_log` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `id_sesion` varchar(90) DEFAULT NULL,
  `id_pegawai` varchar(50) DEFAULT NULL,
  `ip_address` varchar(90) DEFAULT NULL,
  `login_start` datetime DEFAULT NULL,
  `login_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ;

/*Table structure for table `wb_materi` */

DROP TABLE IF EXISTS `wb_materi`;

CREATE TABLE `wb_materi` (
  `id_materi` varchar(50) NOT NULL,
  `materi` varchar(200) NOT NULL,
  `path` varchar(100) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_materi`),
  KEY `FK_wb_materi` (`id_class`),
  KEY `FK_wb_mater` (`id_periode`),
  CONSTRAINT `FK_wb_mater` FOREIGN KEY (`id_periode`) REFERENCES `wb_periode` (`id_periode`)
) ;

/*Table structure for table `wb_menu` */

DROP TABLE IF EXISTS `wb_menu`;

CREATE TABLE `wb_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `menu_name` varchar(90) DEFAULT NULL,
  `menu_url` varchar(100) DEFAULT NULL,
  `menu_icon` varchar(100) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ;

/*Table structure for table `wb_menu_role` */

DROP TABLE IF EXISTS `wb_menu_role`;

CREATE TABLE `wb_menu_role` (
  `id_menu_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu_role`),
  KEY `FK_wb_menu_role` (`id_menu`),
  KEY `FK_wb_menu_level` (`id_level`),
  CONSTRAINT `FK_wb_menu_level` FOREIGN KEY (`id_level`) REFERENCES `wb_level` (`id_level`),
  CONSTRAINT `FK_wb_menu_role` FOREIGN KEY (`id_menu`) REFERENCES `wb_menu` (`id_menu`)
) ;

/*Table structure for table `wb_pegawai` */

DROP TABLE IF EXISTS `wb_pegawai`;

CREATE TABLE `wb_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `sertifikasi` varchar(200) DEFAULT NULL,
  `skill` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`)
) ;

/*Table structure for table `wb_pegawaix` */

DROP TABLE IF EXISTS `wb_pegawaix`;

CREATE TABLE `wb_pegawaix` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`,`nip`)
);

/*Table structure for table `wb_periode` */

DROP TABLE IF EXISTS `wb_periode`;

CREATE TABLE `wb_periode` (
  `id_periode` varchar(50) NOT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `id_class` varchar(50) DEFAULT NULL,
  `id_schedule` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_periode`),
  KEY `FK_wb_periode` (`id_class`),
  KEY `FK_wb_sch` (`id_schedule`),
  CONSTRAINT `FK_wb_sch` FOREIGN KEY (`id_schedule`) REFERENCES `wb_schedule` (`id_schedule`)
) ;

/*Table structure for table `wb_peserta` */

DROP TABLE IF EXISTS `wb_peserta`;

CREATE TABLE `wb_peserta` (
  `id_peserta` varchar(50) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_peserta`),
  KEY `FK_wb_peserta` (`id_class`),
  KEY `FK_wb_peserta_reg` (`id_user`),
  CONSTRAINT `FK_wb_peserta` FOREIGN KEY (`id_class`) REFERENCES `wb_class` (`id_class`),
  CONSTRAINT `FK_wb_peserta_reg` FOREIGN KEY (`id_user`) REFERENCES `wb_users` (`id_user`)
) ;

/*Table structure for table `wb_quiz` */

DROP TABLE IF EXISTS `wb_quiz`;

CREATE TABLE `wb_quiz` (
  `id_quiz` varchar(50) NOT NULL,
  `quiz` varchar(100) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_quiz`)
) ;

/*Table structure for table `wb_schedule` */

DROP TABLE IF EXISTS `wb_schedule`;

CREATE TABLE `wb_schedule` (
  `id_schedule` varchar(50) NOT NULL,
  `schedule_name` varchar(150) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_schedule`),
  KEY `FK_wb_schedule` (`id_class`),
  KEY `FK_wb_periode` (`id_periode`),
  CONSTRAINT `FK_wb_schedule` FOREIGN KEY (`id_class`) REFERENCES `wb_class` (`id_class`)
) ;

/*Table structure for table `wb_ujian` */

DROP TABLE IF EXISTS `wb_ujian`;

CREATE TABLE `wb_ujian` (
  `id_ujian` varchar(50) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_quiz` varchar(50) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_jenis_quiz` int(11) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sesion` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_ujian`),
  KEY `FK_wb_ujian_class` (`id_class`),
  KEY `FK_wb_ujian` (`id_quiz`),
  KEY `FK_wb_period_ujian` (`id_periode`),
  KEY `FK_wb_jenis` (`id_jenis_quiz`),
  CONSTRAINT `FK_wb_jenis` FOREIGN KEY (`id_jenis_quiz`) REFERENCES `wb_jenis_quiz` (`id_jenis_quis`),
  CONSTRAINT `FK_wb_period_ujian` FOREIGN KEY (`id_periode`) REFERENCES `wb_periode` (`id_periode`),
  CONSTRAINT `FK_wb_ujian` FOREIGN KEY (`id_quiz`) REFERENCES `wb_quiz` (`id_quiz`),
  CONSTRAINT `FK_wb_ujian_class` FOREIGN KEY (`id_class`) REFERENCES `wb_class` (`id_class`)
) ;

/*Table structure for table `wb_users` */

DROP TABLE IF EXISTS `wb_users`;

CREATE TABLE `wb_users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `passwords` varchar(120) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `islogin` tinyint(1) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_sesion` varchar(100) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_user`,`nip`),
  KEY `FK_wb_level` (`id_level`),
  CONSTRAINT `FK_wb_level` FOREIGN KEY (`id_level`) REFERENCES `wb_level` (`id_level`)
) ;

/*Table structure for table `wb_webcam` */

DROP TABLE IF EXISTS `wb_webcam`;

CREATE TABLE `wb_webcam` (
  `id_webcam` int(11) NOT NULL AUTO_INCREMENT,
  `type_webcam` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `status_online` int(11) DEFAULT NULL,
  `information` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_webcam`)
) ;

