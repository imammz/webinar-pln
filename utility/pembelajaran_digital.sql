# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: pembelajaran_digital
# Generation Time: 2015-11-12 08:31:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table fasilitator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fasilitator`;

CREATE TABLE `fasilitator` (
  `id_fasilitator` int(11) NOT NULL AUTO_INCREMENT,
  `nama_fasilitator` varchar(50) NOT NULL,
  `original_fasilitator` varchar(1) NOT NULL,
  `kategori_fasilitator` varchar(15) NOT NULL,
  `kel_fasilitator` enum('PTE','PNT') NOT NULL,
  `bidang_fasilitator` text NOT NULL,
  `sub_bidangfas` text NOT NULL,
  `level_fas` varchar(20) NOT NULL,
  `sme` enum('Ya','Tidak') NOT NULL,
  `accessor` enum('Ya','Tidak') NOT NULL,
  `retired` enum('Ya','Tidak') NOT NULL,
  `dentur` enum('Ya','Tidak') NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nip_fasilitator` varchar(12) NOT NULL,
  PRIMARY KEY (`id_fasilitator`),
  KEY `id_fasilitator` (`id_fasilitator`),
  KEY `id_fasilitator_2` (`id_fasilitator`),
  KEY `id_fasilitator_3` (`id_fasilitator`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `fasilitator` WRITE;
/*!40000 ALTER TABLE `fasilitator` DISABLE KEYS */;

INSERT INTO `fasilitator` (`id_fasilitator`, `nama_fasilitator`, `original_fasilitator`, `kategori_fasilitator`, `kel_fasilitator`, `bidang_fasilitator`, `sub_bidangfas`, `level_fas`, `sme`, `accessor`, `retired`, `dentur`, `username`, `password`, `nip_fasilitator`)
VALUES
	(1,'WAHYU BINTORO','','','PTE','DISTRIBUSI','DISTRIBUSI','SENIOR','Tidak','Tidak','Tidak','','','','6893005L'),
	(2,'MOHAMMAD IKHFAN','','','PTE','RENEWABLE ENERGY','RENEWABLE ENERGY','BASIC','Tidak','Tidak','Tidak','','','','8006246Z'),
	(3,'SRI WAHYUDI','','','PNT','SUMBER DAYA MANUSIA','SUMBER DAYA MANUSIA','SENIOR','Tidak','Tidak','Tidak','','','','5979001K3'),
	(4,'SAIFUL JUSUF','','','PNT','SUMBER DAYA MANUSIA','SUMBER DAYA MANUSIA','SENIOR','Tidak','Tidak','Tidak','','','','6083024P'),
	(5,'SRI ATMI SUKMANASARI BUDIASIH','','','PNT','MANAJEMEN PEMBELAJARAN','MANAJEMEN PEMBELAJARAN','SENIOR','Tidak','Tidak','Tidak','','','','6084002T'),
	(6,'KARMEN TARIGAN','','','PNT','KNOWLEDGE MANAGEMENT','KNOWLEDGE MANAGEMENT','SENIOR','Tidak','Tidak','Tidak','','','','6188002P2B'),
	(7,'SONI ASMAUL FUADI','','','','DISTRIBUSI','DISTRIBUSI / OPHARDIS','SENIOR','','','','','','','6895002D'),
	(8,'FARIEDS','','','PTE','PEMBANGKITAN','Pembangkitan / mesin diesel','BASIC','Tidak','Tidak','Tidak','','','','8208267Z'),
	(9,'RIDWAN','','','PTE','PEMBANGKITAN','Pembangkitan / Operasi PLTD','BASIC','Tidak','Tidak','Tidak','','','','6487005D'),
	(10,'SUMARDIONO','','','PTE','PENYALURAN','PENYALURAN / PEMELIHARAAN TRANSMISI','ASSOCIATE','Tidak','Tidak','Tidak','','','','6184713K3');

/*!40000 ALTER TABLE `fasilitator` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table file_vl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `file_vl`;

CREATE TABLE `file_vl` (
  `id_filevl` int(11) NOT NULL AUTO_INCREMENT,
  `path_file` text NOT NULL,
  `judul_file` varchar(40) NOT NULL,
  `kd_materi` varchar(12) NOT NULL,
  PRIMARY KEY (`id_filevl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `file_vl` WRITE;
/*!40000 ALTER TABLE `file_vl` DISABLE KEYS */;

INSERT INTO `file_vl` (`id_filevl`, `path_file`, `judul_file`, `kd_materi`)
VALUES
	(1,'Cara Menghitung Kebutuhan Bandwidth.docx','File Penunjang Tenaga Uap 1','B111044');

/*!40000 ALTER TABLE `file_vl` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table komentarvl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `komentarvl`;

CREATE TABLE `komentarvl` (
  `id_komentarvl` int(11) NOT NULL AUTO_INCREMENT,
  `nip_pegawai` varchar(12) NOT NULL,
  `komentar` text NOT NULL,
  `wktkomen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_sesi` int(11) NOT NULL,
  PRIMARY KEY (`id_komentarvl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `komentarvl` WRITE;
/*!40000 ALTER TABLE `komentarvl` DISABLE KEYS */;

INSERT INTO `komentarvl` (`id_komentarvl`, `nip_pegawai`, `komentar`, `wktkomen`, `id_sesi`)
VALUES
	(1,'','a','2015-10-28 06:22:04',1),
	(2,'','mila\r\n','2015-10-28 06:31:16',1);

/*!40000 ALTER TABLE `komentarvl` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table link_vl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `link_vl`;

CREATE TABLE `link_vl` (
  `id_linkvl` int(11) NOT NULL,
  `link` text NOT NULL,
  `judul_link` varchar(40) NOT NULL,
  `kd_materi` varchar(12) NOT NULL,
  PRIMARY KEY (`id_linkvl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `link_vl` WRITE;
/*!40000 ALTER TABLE `link_vl` DISABLE KEYS */;

INSERT INTO `link_vl` (`id_linkvl`, `link`, `judul_link`, `kd_materi`)
VALUES
	(0,'pln.co.id','Tenaga Uap','B111044');

/*!40000 ALTER TABLE `link_vl` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_accessor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_accessor`;

CREATE TABLE `m_accessor` (
  `id_accessor` int(11) NOT NULL AUTO_INCREMENT,
  `ket_accessor` varchar(2) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nip_accessor` varchar(12) NOT NULL,
  PRIMARY KEY (`id_accessor`),
  KEY `id_accessor` (`id_accessor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `m_accessor` WRITE;
/*!40000 ALTER TABLE `m_accessor` DISABLE KEYS */;

INSERT INTO `m_accessor` (`id_accessor`, `ket_accessor`, `username`, `password`, `nip_accessor`)
VALUES
	(1,'0','tasyasal','plnpusdi','5984004T'),
	(2,'0','dodihendra','pik1234','6994018D'),
	(3,'0','yudasagung','pik1234','86112159Z');

/*!40000 ALTER TABLE `m_accessor` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_bid_fasilitator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_bid_fasilitator`;

CREATE TABLE `m_bid_fasilitator` (
  `kd_bidfasil` varchar(2) NOT NULL,
  `bidang_fasil` text NOT NULL,
  `kd_subbidfasil` varchar(3) NOT NULL,
  `subbid_fasil` text NOT NULL,
  KEY `kd_subbidfasil` (`kd_subbidfasil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_log`;

CREATE TABLE `m_log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `nip_pegawai` varchar(12) NOT NULL,
  `nis_siswa` varchar(15) NOT NULL,
  `login` datetime NOT NULL,
  `logout` datetime NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `m_log` WRITE;
/*!40000 ALTER TABLE `m_log` DISABLE KEYS */;

INSERT INTO `m_log` (`id_log`, `nip_pegawai`, `nis_siswa`, `login`, `logout`)
VALUES
	(1,'5980092R','','2015-08-18 08:05:05','2015-08-18 12:11:10');

/*!40000 ALTER TABLE `m_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_nilai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_nilai`;

CREATE TABLE `m_nilai` (
  `id_nilai` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` float NOT NULL,
  `nip_pegawai` varchar(12) NOT NULL,
  `nis_siswa` varchar(15) NOT NULL,
  `status` varchar(20) NOT NULL,
  `id_sesi` varchar(8) NOT NULL,
  PRIMARY KEY (`id_nilai`),
  KEY `id_nilai` (`id_nilai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_pendalaman_materi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_pendalaman_materi`;

CREATE TABLE `m_pendalaman_materi` (
  `id_pendmat` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pembelajaran` varchar(1) NOT NULL,
  `kd_materi` varchar(12) NOT NULL,
  `judul_materi` text NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `tglmulai` date NOT NULL,
  `tglselesai` text NOT NULL,
  `nilai` float NOT NULL,
  `ket` text NOT NULL,
  `nip_pendmat` varchar(12) NOT NULL,
  PRIMARY KEY (`id_pendmat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_pendidikan_fasilitator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_pendidikan_fasilitator`;

CREATE TABLE `m_pendidikan_fasilitator` (
  `id_pendfas` int(11) NOT NULL AUTO_INCREMENT,
  `kd_materi` varchar(12) NOT NULL,
  `judul_materi` varchar(10) NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `lembaga_pend` text NOT NULL,
  `ket_lempend` text NOT NULL,
  `tglmulai` date NOT NULL,
  `tglselesai` date NOT NULL,
  `nomor_sert` varchar(30) NOT NULL,
  `tgl_sert` date NOT NULL,
  `nilai` float NOT NULL,
  `ket` text NOT NULL,
  `nip_pend` varchar(12) NOT NULL,
  PRIMARY KEY (`id_pendfas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_pengembangan_materi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_pengembangan_materi`;

CREATE TABLE `m_pengembangan_materi` (
  `id_pengmat` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pembelajaran` varchar(1) NOT NULL,
  `kd_materi` varchar(12) NOT NULL,
  `judul_materi` text NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `tglmulai` date NOT NULL,
  `tglselesai` text NOT NULL,
  `ket` text NOT NULL,
  `nip_pengmat` varchar(12) NOT NULL,
  PRIMARY KEY (`id_pengmat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_prajabatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_prajabatan`;

CREATE TABLE `m_prajabatan` (
  `nis_siswa` varchar(15) NOT NULL,
  `angkatan_prajab` varchar(4) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `alamat_siswa` text NOT NULL,
  `pend_terakhir` enum('SMA/SMK','D3','S1') NOT NULL,
  `jurusan_pend` varchar(40) NOT NULL,
  `jk_siswa` enum('L','P') NOT NULL,
  `agama` varchar(15) NOT NULL,
  `tmp_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp_kntr` text NOT NULL,
  `no_hp` text NOT NULL,
  PRIMARY KEY (`nis_siswa`),
  KEY `id_siswa` (`nis_siswa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `m_prajabatan` WRITE;
/*!40000 ALTER TABLE `m_prajabatan` DISABLE KEYS */;

INSERT INTO `m_prajabatan` (`nis_siswa`, `angkatan_prajab`, `nama_siswa`, `alamat_siswa`, `pend_terakhir`, `jurusan_pend`, `jk_siswa`, `agama`, `tmp_lahir`, `tgl_lahir`, `email`, `no_telp_kntr`, `no_hp`)
VALUES
	('12345','2011','Rina Lulu','Jakarta Selatan','','Teknik','P','Islam','Jakarta','1997-04-18','rina@gmail.com','02178654321','081876543291');

/*!40000 ALTER TABLE `m_prajabatan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_refreshment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_refreshment`;

CREATE TABLE `m_refreshment` (
  `id_ref` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pembelajaran` varchar(1) NOT NULL,
  `kd_materi` varchar(12) NOT NULL,
  `judul_materi` text NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `tglmulai` date NOT NULL,
  `tglselesai` date NOT NULL,
  `nomor_sert` varchar(30) NOT NULL,
  `tgl_sert` date NOT NULL,
  `nilai` float NOT NULL,
  `ket` text NOT NULL,
  `nip_ref` varchar(12) NOT NULL,
  PRIMARY KEY (`id_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_riwayat_accessor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_riwayat_accessor`;

CREATE TABLE `m_riwayat_accessor` (
  `id_riwacc` int(11) NOT NULL AUTO_INCREMENT,
  `id_sesi` varchar(8) NOT NULL,
  `id_accessor` int(11) NOT NULL,
  `lastupdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_riwacc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_riwayat_belajar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_riwayat_belajar`;

CREATE TABLE `m_riwayat_belajar` (
  `id_riwbel` int(11) NOT NULL,
  `nip_pegawai` varchar(12) NOT NULL,
  `nis_siswa` varchar(15) NOT NULL,
  `id_sesi` varchar(8) NOT NULL,
  `status` enum('lulus','tidak lulus') NOT NULL,
  PRIMARY KEY (`id_riwbel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `m_riwayat_belajar` WRITE;
/*!40000 ALTER TABLE `m_riwayat_belajar` DISABLE KEYS */;

INSERT INTO `m_riwayat_belajar` (`id_riwbel`, `nip_pegawai`, `nis_siswa`, `id_sesi`, `status`)
VALUES
	(0,'5980092R','','VL050115','lulus');

/*!40000 ALTER TABLE `m_riwayat_belajar` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_riwayat_komp_fasilitator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_riwayat_komp_fasilitator`;

CREATE TABLE `m_riwayat_komp_fasilitator` (
  `id_riwkomp_fasil` int(11) NOT NULL AUTO_INCREMENT,
  `id_fasilitator` int(11) NOT NULL,
  `bidang_fasilitator` varchar(30) NOT NULL,
  `sertifikat` enum('Sertifikat','Belum') NOT NULL,
  PRIMARY KEY (`id_riwkomp_fasil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_riwayat_mengajar
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_riwayat_mengajar`;

CREATE TABLE `m_riwayat_mengajar` (
  `id_riwmeng` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pembelajaran` varchar(1) NOT NULL,
  `kd_materi` varchar(12) NOT NULL,
  `judul_materi` varchar(50) NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `lembaga_pend` text NOT NULL,
  `ket_lempend` text NOT NULL,
  `tglmulai` date NOT NULL,
  `tglselesai` date NOT NULL,
  `nilai_evallv1` float NOT NULL,
  `ket` text NOT NULL,
  `nip_riwmeng` varchar(12) NOT NULL,
  PRIMARY KEY (`id_riwmeng`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_s_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_s_unit`;

CREATE TABLE `m_s_unit` (
  `kd_sunit` varchar(2) NOT NULL,
  `nama_sunit` text NOT NULL,
  `alamat_sunit` text NOT NULL,
  `notelp_sunit` text NOT NULL,
  `kd_unit` varchar(3) NOT NULL,
  PRIMARY KEY (`kd_sunit`),
  KEY `kd_sunit` (`kd_sunit`),
  KEY `kd_sunit_2` (`kd_sunit`),
  KEY `kd_sunit_3` (`kd_sunit`),
  KEY `kd_sunit_4` (`kd_sunit`),
  KEY `kd_sunit_5` (`kd_sunit`),
  KEY `kd_sunit_6` (`kd_sunit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `m_s_unit` WRITE;
/*!40000 ALTER TABLE `m_s_unit` DISABLE KEYS */;

INSERT INTO `m_s_unit` (`kd_sunit`, `nama_sunit`, `alamat_sunit`, `notelp_sunit`, `kd_unit`)
VALUES
	('AA','Kantor Perwakilan Jakarta','Jl. Gatot SUbroto Kav. 18, Jakarta 12950','(021)5250751','AAD'),
	('AB','Unit Workshop I Citarum','Jl. Raya Dayeuh Kolot KM.09, BANDUNG 40257','(022)5202929','AAD'),
	('AC','Unit Workshop II Bandung','Jl. Banten No.10, BANDUNG 40272','(022)7208176','AAD'),
	('AD','Unit Workshop III Surabaya','Jl. Ngagel Timur No. 16, SURABAYA 60285','(031)5023731','AAD'),
	('AE','Unit Pelaksana Pemeliharaan I Merak','Jl. Pulorida PO.BOX 6, DS SURALAYA, KEC.PULOMERAK MERAK, BANTEN 42456','(0254)571265','AAD'),
	('AF','Unit Pelaksana Pemeliharaan II Semarang','Jl. Siliwangi No. 379, Semarang 56146','(024)7608250','AAD'),
	('AG','Unit Pelaksana Pemeliharaan III Jakarta','Jl. Raya Bekasi Timur KM.17, Jakarta Timur 13930','(021)4613230','AAD'),
	('AH','Unit Produksi Bali','Jl. Gunung Mandalawangi No.15, Denpasar 80119','(0361)484313','AAD'),
	('AI','Sub Unit Pelaksana Pemeliharaan I Bangkinang','Jl. Raya Sumbar Riau KM.15, Rantau Berangin DS.Merangin BANGKINANG, PEKAN BARU, RIAU ','(0762)7000120','AAD'),
	('AJ','Sub Unit Workshop II Cijedil','Jl. Raya Cijedil DS.Gasol Kec.Cugenang, Cianjur ','(0263)261430','AAD'),
	('AK','Unit Supervisi Konstruksi Jakarta Raya, Jawa Barat dan Banten (USK JJBB)','Jl. Letjen Sutoyo No.1, Cililitan , Jakarta Timur 13640','(021)8011575','AAF'),
	('AL','Unit Supervisi Konstruksi Jawa Timur, Bali, dan Nusa Tenggara (USK JTBN)','Jl. Ketintang Baru No.1-3, Surabaya, 60231','(031)8273456','AAF'),
	('AM','Unit Supervisi Konstruksi Jawa Tengah dan DI. Yogyakarta (USK JTJG)','Jl. Slamet No.1, Candi Baru, Semarang 50232','(024)8507542','AAF'),
	('AN','Unit Supervisi Konstruksi Sumatera Utara, Aceh, dan Riau (USK SUAR)','Jl. Kolonel Yos Sudarso KM.8, Tanjung Mulia Medan 20241','(061)6645621','AAF'),
	('AO','Unit Supervisi Konstruksi Sumatera  Bagian Selatan (USK SBSL)','Jl. Sapta Marga No.8, Bukit Sangkal, Palembang 30114','(0711)817754','AAF'),
	('AP','APB DKI Jakarta Dan Banten','Jl. Mayjen Sutoyo No. 1, Jakarta Timur 13640','(021)8001915','AAH'),
	('AQ','APB Jawa Barat','Jl. Moh Toha Km. 4, Komplek GI Cigereleng Bandung 40255','(022)5201723','AAH'),
	('AR','APB Jawa Tengah Dan DIY','Jl. Jend. Sudirman Km.23, Komplek GI Ungaran Ungaran 50501','(024)6921221','AAH'),
	('AS','APB Jawa Timur','Jl. Suningrat No. 45, Taman Sidoarjo Surabaya 61257','(031)7882113','AAH'),
	('AT','APB Bali','Jl. Abian Base, Mengwi, Badung, Bali','(0361)410979','AAH'),
	('AU','UNIT PELAYANAN TRANSMISI (UPT) PT. PLN (Persero) UPT TANJUNG KARANG','Jl. S.Parman No.221 Padang Sumatera Barat 25135','(0751)7054688','AAI'),
	('AV','PT. PLN (Persero) UPT PALEMBANG','Jl. MP Mangku Negara No.1 A, Palembang 30127','(0711)814516','AAI'),
	('AW','PT. PLN (Persero) UPT Bengkulu','Jl. Jawa No.7, Kel.Sukamerindu, BENGKULU 38119','(0736)343878','AAI'),
	('AX','PT. PLN (Persero) UPT PADANG','Jl. By Pass KM.06, Lubuk Begalung, PADANG 25221','(0751)71650','AAI'),
	('AY','PT. PLN (Persero) UPT PEKANBARU','Jl. Garuda Sakti KM.13, Kec. Tampanraya, PEKANBARU 28293','(0761)3038886','AAI'),
	('AZ','PT. PLN (Persero) UPT PEMATANG SIANTAR','Jl. Sangnawaluh KM.4,5, Pematang Siantar 21151','(0622)7550874','AAI'),
	('BA','PT. PLN (Persero) UPT MEDAN','Jl. Listrik No.12, Kel./Kec. Medan Petisah, Medan 20112','(061)4578800','AAI'),
	('BB','PT. PLN (Persero) UPT BANDA ACEH','Jl. Soekarno Hatta, Desa Lamreueng, Banda Aceh 23352','(0651)48962','AAI'),
	('BC','PT. PLN (Persero) UPB SUMBAGTENG','Jl. Ir. H Sutami Batang Tapakis, Kec. Sintuk Toboh Gadang, Kab. Padang Pariaman, Sumatera Barat 25581','(0751)96163','AAI');

/*!40000 ALTER TABLE `m_s_unit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_sertifikasi_fasilitator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_sertifikasi_fasilitator`;

CREATE TABLE `m_sertifikasi_fasilitator` (
  `id_serfas` int(11) NOT NULL AUTO_INCREMENT,
  `akreditasi` varchar(10) NOT NULL,
  `lembaga_sert` text NOT NULL,
  `no_sert` varchar(30) NOT NULL,
  `judul_sert` text NOT NULL,
  `tanggal_sert` date NOT NULL,
  `berlakumulai` date NOT NULL,
  `berakhir` date NOT NULL,
  `ket` text NOT NULL,
  `nip_sertfas` varchar(12) NOT NULL,
  PRIMARY KEY (`id_serfas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_ss_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_ss_unit`;

CREATE TABLE `m_ss_unit` (
  `id_ssunit` int(11) NOT NULL,
  `kd_ssunit` varchar(2) NOT NULL,
  `nama_ssunit` text NOT NULL,
  `alamat_ssunit` text NOT NULL,
  `notelp_ssunit` text NOT NULL,
  `kd_sunit` varchar(2) NOT NULL,
  PRIMARY KEY (`kd_ssunit`),
  KEY `kd_ssunit` (`kd_ssunit`),
  KEY `kd_ssunit_2` (`kd_ssunit`),
  KEY `kd_ssunit_3` (`kd_ssunit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table m_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_unit`;

CREATE TABLE `m_unit` (
  `kd_unit` varchar(3) NOT NULL,
  `nama_unit` text NOT NULL,
  `alamat_unit` text NOT NULL,
  `notelp_unit` text NOT NULL,
  `kd_udiklat` varchar(2) NOT NULL,
  PRIMARY KEY (`kd_unit`),
  KEY `kd_unit` (`kd_unit`),
  KEY `kd_unit_2` (`kd_unit`),
  KEY `kd_unit_3` (`kd_unit`),
  KEY `kd_unit_4` (`kd_unit`),
  KEY `kd_unit_5` (`kd_unit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `m_unit` WRITE;
/*!40000 ALTER TABLE `m_unit` DISABLE KEYS */;

INSERT INTO `m_unit` (`kd_unit`, `nama_unit`, `alamat_unit`, `notelp_unit`, `kd_udiklat`)
VALUES
	('AAA','PT. PLN (Persero)- Kantor Pusat','Jl. Trunojoyo Blok M I-135, Jakarta, 12160','(021)7261875',''),
	('AAB','PT. PLN (Persero) PENELITIAN DAN PENGEMBANGAN KETENAGALISTRIKAN','Jl. Duren Tiga No. 102 Jakarta Selatan 12760','(021)7973774',''),
	('AAC','PT. PLN (Persero) PUSAT PENDIDIKAN DAN PELATIHAN','jL. Harsono RM No. 59, Ragunan-PasarMinggu, Jakarta 12550','(021)7811292','UJ'),
	('AAD','PT. PLN (Persero) PUSAT ENJINIIRING KETENAGALISTRIKAN ','Jl. Aipda KS.Tubun I/2, Petamburan, Jakarta 11420','(021)5640141',''),
	('AAE','PT. PLN (Persero) PUSAT PEMELIHARAAN KETENAGALISTRIKAN','Jl. Raya Dayeuhkolot KM.09 Bandung 40257','(022)5202929',''),
	('AAF','PT. PLN (Persero) JASA MANAJEMEN KONSTRUKSI','Jl. Raya Pluit Utara No. 2B, Muara Karang Jakarta 14450','(021)66695219',''),
	('AAG','PT. PLN (Persero) JASA SERTIFIKASI','Jl.Laboratorium Duren Tiga Jakarta 12760','(021)7900034',''),
	('AAH','PT. PLN (Persero) PENYALURAN DAN PUSAT PENGATUR BEBAN (P3B) Jawa-Bali','Jl. Krukut-Limo, Cinere, Jakarta Selatan 16514','(021)7543566',''),
	('AAI','PT. PLN (Persero) PENYALURAN DAN PUSAT PENGATUR BEBAN (P3B) Sumatera','Jl. S.Parman No. 221 Padang, Sumatera Barat 25135','(0751)7054688',''),
	('AAJ','PT. PLN (Persero) WILAYAH ACEH','Jl. Tgk. H. M. Daud Beureuh No.172, Lamprit BANDA ACEH 23243','(0651)22188',''),
	('AAK','PT. PLN (Persero) WILAYAH SUMATERA UTARA','Jl. K.L.Yos Sudarso No.284, Medan, 20115','(061)6615155',''),
	('AAL','PT. PLN (Persero) WILAYAH SUMATERA BARAT','Jl. Dr. Wahidin No.8, Padang 25121','(0751)33446',''),
	('AAM','PT. PLN (Persero) WILAYAH RIAU DAN KEP RI','Jl. Dr. Setiabudi No. 57 Pekanbaru 28141','(0761)855840',''),
	('AAN','PT. PLN (Persero) WILAYAH SUMATERA SELATAN JAMBI DAN BENGKULU','Jl. Kapten A. Rivai NO. 37 Palembang 30129','(0711)358355',''),
	('AAO','PT. PLN (Persero) WILAYAH BANGKA BELITUNG','Jl. Soekarno Hatta KM. 5, PangkalPinang 33171','(0717)4393000',''),
	('AAP','PT. PLN (Persero) WILAYAH LAMPUNG','Jl. ZA. Pagar Alam No.5 Rajabasa, Bandar Lampung 35144','(0721)774868',''),
	('AAQ','PT. PLN (Persero) WILAYAH KALIMANTAN BARAT','Jl. Adisucipto KM 7.3 Seiraya, Kuburaya, Pontianak 78391','(0561)722037',''),
	('AAR','PT. PLN (Persero) WILAYAH KALIMANTAN SELATAN DAN KALIMANTAN TENGAH','Jl. Panglima Batur Barat No.1 Banjarbaru, Kalimantan Selatan','(0511)4772520',''),
	('AAS','PT. PLN (Persero) WILAYAH KALIMANTAN TIMUR','Jl. MT. Haryono No.384, Balikpapan 76114 ','(0542)871840',''),
	('AAT','PT. PLN (Persero) WILAYAH SULAWESI UTARA, SULAWESI TENGAH, DAN GORONTALO','Jl. Bethesda No.32, Manado 95116','(0431)863644',''),
	('AAU','PT. PLN (Persero) WILAYAH SULAWESI SELATAN, SULAWESI TENGGARA, DAN SULAWESI BARAT ','Jl. Letjend Hertasning Blok B, 1206 Panakkukang, Makassar 90222','(0411)444488',''),
	('AAV','PT. PLN (Persero) WILAYAH MALUKU','Jl. Diponegoro No.2, Ambon 97127','(0911)311810',''),
	('AAW','PT. PLN (Persero) WILAYAH PAPUA','Jl. Jend. A.Yani No.18, Jayapura 99111','(0967)533891',''),
	('AAX','PT. PLN (Persero) WILAYAH NUSA TENGGARA BARAT','Jl.Langko No.26-27 Ampenan 83114, Mataram','(0370)643123',''),
	('AAY','PT. PLN (Persero) WILAYAH NUSA TENGGARA TIMUR','Jl. Piet A. Tallo II No. 101, Kupang 85228','(0380)8554005',''),
	('AAZ','PT. PLN (Persero) DISTRIBUSI JAWA TIMUR','Jl. Embong Trengguli No.19-21 Surabaya 60271','(031)5491179',''),
	('ABA','PT. PLN (Persero) DISTRIBUSI JAWA TENGAH DAN DI. YOGYAKARTA','Jl. Teuku Umar No. 47, SEMARANG','(024)8411991',''),
	('ABB','PT. PLN (Persero) DISTRIBUSI JAWA BARAT DAN BANTEN','Jl. Asia Afrika No. 63, BANDUNG 40111','(022)4230747',''),
	('ABC','PT. PLN (Persero) DISTRIBUSI JAKARTA RAYA DAN TANGERANG','Jl. M.I.R. Rais No.1, JAKARTA 10110','(021)34505000',''),
	('ABD','PT. PLN (Persero) PEMBANGKIT LONTAR','Jl. Setiabudi No. 96 SEMARANG SELATAN, SEMARANG 50269','(024)7465795','');

/*!40000 ALTER TABLE `m_unit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table m_upskill_fasilitator
# ------------------------------------------------------------

DROP TABLE IF EXISTS `m_upskill_fasilitator`;

CREATE TABLE `m_upskill_fasilitator` (
  `id_upskillfas` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pembelajaran` varchar(1) NOT NULL,
  `kd_materi` varchar(12) NOT NULL,
  `judul_materi` text NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `tglmulai` date NOT NULL,
  `tglselesai` date NOT NULL,
  `nomor_sert` varchar(30) NOT NULL,
  `tgl_sert` date NOT NULL,
  `nilai` float NOT NULL,
  `ket` text NOT NULL,
  `nip_upskillfas` varchar(12) NOT NULL,
  PRIMARY KEY (`id_upskillfas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table materi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `materi`;

CREATE TABLE `materi` (
  `kd_materi` varchar(13) NOT NULL,
  `kel_materi` varchar(30) NOT NULL,
  `judul_materi` text NOT NULL,
  `bid_materi` varchar(20) NOT NULL,
  `edisi` varchar(5) NOT NULL,
  `revisi` varchar(5) NOT NULL,
  `jenis_pembelajaran` varchar(1) NOT NULL,
  `sifat_pembelajaran` varchar(1) NOT NULL,
  `pohon_profesi` varchar(3) NOT NULL,
  `urutan_registrasi` varchar(2) NOT NULL,
  `level_kompetensi` varchar(1) NOT NULL,
  `kd_academy` varchar(2) NOT NULL,
  `klasifikasi_pembelajaran` varchar(1) NOT NULL,
  `kd_metode` varchar(2) NOT NULL,
  PRIMARY KEY (`kd_materi`),
  KEY `kd_materi` (`kd_materi`),
  KEY `kd_materi_2` (`kd_materi`),
  KEY `kd_materi_3` (`kd_materi`),
  KEY `kd_materi_4` (`kd_materi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `materi` WRITE;
/*!40000 ALTER TABLE `materi` DISABLE KEYS */;

INSERT INTO `materi` (`kd_materi`, `kel_materi`, `judul_materi`, `bid_materi`, `edisi`, `revisi`, `jenis_pembelajaran`, `sifat_pembelajaran`, `pohon_profesi`, `urutan_registrasi`, `level_kompetensi`, `kd_academy`, `klasifikasi_pembelajaran`, `kd_metode`)
VALUES
	('B111013','ICT','Pengoperasian Boiler','Teknik','01','','B','1','11','01','3','','',''),
	('B111023','ICT','Pengoperasian HRSG','Teknik','01','','B','1','11','02','3','','',''),
	('B111033','ICT','Pengoperasian Turbin UAP','Teknik','09','01','B','1','11','03','3','','',''),
	('B111044','Digital','Pengoperasian PLTU','Teknik','09','01','B','1','11','04','4','','M',''),
	('B111053','Digital','K2/K3 PLTD ','Teknik','09','01','B','1','11','05','3','','',''),
	('B112012','Digital','Perencanaan Sistem Tenaga Listrik','Teknik','09','01','B','1','12','01','2','','',''),
	('B112022','ICT','Transaksi Tenaga Listrik','Teknik','10','02','B','1','12','02','2','','',''),
	('B112033','Digital','Analisa Sistem Tenaga Listrik','Teknik','09','01','B','1','12','03','3','','',''),
	('B112043','ICT','Perencanaan Produksi Energi Listrik','Teknik','09','01','B','1','12','04','3','','',''),
	('B112053','Digital','Pengaturan Operasi Sistem Real Time','Teknik','10','02','B','1','12','05','3','','',''),
	('B112123','ICT','Pekerjaan Dalam Keadaan Bertegangan pada SUTT/SUTET','Teknik','09','01','B','1','12','12','3','','M',''),
	('B112472','Digital','Pengenalan PDKB SUTT / SUTET','Teknik','10','00','B','1','12','47','2','','M',''),
	('B112643','ICT','Keselamatan Kerja pada PDKB untuk SUTT/SUTET Pelaksana Pekerjaan','Teknik','11','00','B','1','12','64','3','','M',''),
	('B112653','ICT','Pengawas Keselamatan Kerja pada PDKB SUTT/SUTET','Teknik','11','00','B','1','12','65','3','','M',''),
	('B112663','ICT','PDKB Gardu Induk','Teknik','11','00','B','1','12','66','3','','M',''),
	('B113013','Digital','Pengoperasian JTR dan Sambungan Pelayanan TR','Teknik','09','01','B','1','13','01','3','','',''),
	('B113023','ICT','Pengoperasian JTM','Teknik','09','01','B','1','13','02','3','','',''),
	('B113033','ICT','Pengoperasian Gardu Distribusi','Teknik','09','01','B','1','13','03','3','','',''),
	('B113043','ICT','Pengoperasian Kubikel 20 KV','Teknik','09','01','B','1','13','04','3','','',''),
	('B113053','ICT','Pengaturan Operasi Distribusi Tanpa SCADA','Teknik','09','01','B','1','13','05','3','','',''),
	('B114373','ICT','SIP3','Non Teknik','09','00','B','1','14','37','3','','',''),
	('B114732','ICT','AP2T untuk Supervisori Atas dan Manajemen Dasar','Non Teknik','11','00','B','1','14','73','2','','',''),
	('B114743','ICT','AP2T Pelayanan Pelanggan untuk Supervisori Dasar dan Pelaksana','Non Teknik','11','00','B','1','14','74','3','','',''),
	('B114753','ICT','AP2T Pembacaan Meter & Perhitungan Tagihan Listrik untuk Supervisori Dasar dan Pelaksana','Non Teknik','11','00','B','1','14','75','3','','',''),
	('B122013','ICT','Manajemen Konstruksi untuk Manajer Dasar','Teknik','13','00','B','1','22','01','3','','M',''),
	('B122063','ICT','Manajemen Konstruksi untuk Site Supervision','Teknik','13','00','B','1','22','06','3','','M',''),
	('B122093','ICT','Manajemen Konstruksi untuk Manajer Menengah','Teknik','13','00','B','1','22','09','3','','M',''),
	('B122103','ICT','Pengadaan Tanah','Teknik','13','00','B','1','22','10','3','','',''),
	('B122113','ICT','Perijinan Pembangunan Infrastruktur Ketenagalistrikan','Teknik','13','00','B','1','22','11','3','','',''),
	('B123456MVL','ADMINISTRASI','ADMINISTRASI PERKANTORAN ','Non Teknik','R0009','0','B','1','23','45','6','','M','VL'),
	('B214482','ICT','Contact Center 123','Non Teknik','10','00','B','2','14','48','2','','','');

/*!40000 ALTER TABLE `materi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table metode_pembelajaran
# ------------------------------------------------------------

DROP TABLE IF EXISTS `metode_pembelajaran`;

CREATE TABLE `metode_pembelajaran` (
  `kd_metode` varchar(2) NOT NULL,
  `metode` varchar(20) NOT NULL,
  PRIMARY KEY (`kd_metode`),
  KEY `id_metode` (`kd_metode`),
  KEY `kd_metode` (`kd_metode`),
  KEY `kd_metode_2` (`kd_metode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `metode_pembelajaran` WRITE;
/*!40000 ALTER TABLE `metode_pembelajaran` DISABLE KEYS */;

INSERT INTO `metode_pembelajaran` (`kd_metode`, `metode`)
VALUES
	('EL','E-Learning'),
	('FL','Forum Learning'),
	('IT','In Class Training'),
	('ML','Mobile Learning'),
	('VL','Video Learning'),
	('WB','Webinar'),
	('WS','Workshop');

/*!40000 ALTER TABLE `metode_pembelajaran` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pegawai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `nip_pegawai` varchar(12) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `alamat_pegawai` text NOT NULL,
  `pend_terakhir` enum('SMA/SMK','D3','S1','S2','S3') NOT NULL,
  `jurusan_pend` varchar(40) NOT NULL,
  `grade` varchar(15) NOT NULL,
  `level` varchar(3) NOT NULL,
  `jabatan` varchar(40) NOT NULL,
  `ket_jabatan` text NOT NULL,
  `orgunit` varchar(30) NOT NULL,
  `nama_atasanlgsg` varchar(50) NOT NULL,
  `nama_manajersdm` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL,
  `jk_pegawai` enum('L','P') NOT NULL,
  `agama` varchar(15) NOT NULL,
  `tmp_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_telp_kntr` text NOT NULL,
  `no_hp` text NOT NULL,
  `kd_udiklat` varchar(2) NOT NULL,
  `kd_unit` varchar(2) NOT NULL,
  `kd_sunit` varchar(2) NOT NULL,
  `kd_ssunit` varchar(2) NOT NULL,
  PRIMARY KEY (`nip_pegawai`),
  KEY `nip_pegawai` (`nip_pegawai`),
  KEY `nip_pegawai_2` (`nip_pegawai`),
  KEY `nip_pegawai_3` (`nip_pegawai`),
  KEY `nip_pegawai_4` (`nip_pegawai`),
  KEY `nip_pegawai_5` (`nip_pegawai`),
  KEY `nip_pegawai_6` (`nip_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pegawai` WRITE;
/*!40000 ALTER TABLE `pegawai` DISABLE KEYS */;

INSERT INTO `pegawai` (`nip_pegawai`, `nama_pegawai`, `alamat_pegawai`, `pend_terakhir`, `jurusan_pend`, `grade`, `level`, `jabatan`, `ket_jabatan`, `orgunit`, `nama_atasanlgsg`, `nama_manajersdm`, `status`, `jk_pegawai`, `agama`, `tmp_lahir`, `tgl_lahir`, `email`, `no_telp_kntr`, `no_hp`, `kd_udiklat`, `kd_unit`, `kd_sunit`, `kd_ssunit`)
VALUES
	('5980092R','SUDARWANTO','','SMA/SMK','Listrik','OPT04','18','DEPUTI MANAJER','PEMBELAJARAN PENUNJANG KORPORAT TEKNIK','SUBID PBLJRN PNJ KORP TEKNIK','TASYA SALIM','','Aktif','L','','Tegal','1959-12-09','sudarwanto.5980092R@pln.co.id','0217865432','08587654321','','','',''),
	('5984004T','TASYA SALIM','Jakarta Selatan','S1','Psikologi','ADV01','12','MANAJER BIDANG','PEMBELAJARAN NON TEKNIK','BID PBLJRN NON TEKNIK','OKTO RINALDI S.','','Aktif','P','Islam','Jakarta','1959-12-28','tasya.salim@pln.co.id','0218764321','081323456789','','','',''),
	('5985004D','A KRISTIANTO','','S2','Hukum (Notariat)','INT03','16','MANAJER BIDANG ','MANAJER BIDANG PERENCANAAN DAN TEKNOLOGI INFORMASI','BID REN DAN TI','OKTO RINALDI S.','','Aktif','L','','BANGKALAN','1959-11-28','kristianto@pln.co.id','','','','','',''),
	('6083103B','SUKIRNO','','S2','Teknik Listrik','OPT02','19','MANAJER UNIT','MANAJER UNIT PENDIDIKAN DAN PELATIHAN (ACADEMY) PANDAAN','UDIKLAT (ACADEMY) PANDAAN','OKTO RINALDI S.','','Aktif','L','','Blitar','1960-12-13','sukirno.pusdiklat@pln.co.id','','','','','',''),
	('6084002T','SRI ATMI SUKMANASARI BUDIASIH','','S2','Manajemen Bisnis','ADV02','16','SUPERVISOR','SUPERVISOR PELAYANAN PESERTA PEMBELAJARAN','PT PLN (PERSERO) PUSDIKLAT','INDRIYANI WIDAYATI','','Aktif','P','','SALATIGA','1960-06-01','listyaningyun@pln.co.id','','','','','','');

/*!40000 ALTER TABLE `pegawai` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pesanvl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pesanvl`;

CREATE TABLE `pesanvl` (
  `id_pesanvl` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) NOT NULL,
  `email` varchar(25) NOT NULL,
  `pesan` text NOT NULL,
  PRIMARY KEY (`id_pesanvl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pesanvl` WRITE;
/*!40000 ALTER TABLE `pesanvl` DISABLE KEYS */;

INSERT INTO `pesanvl` (`id_pesanvl`, `nama`, `email`, `pesan`)
VALUES
	(1,'Mila','rizkanurmila@outlook.co.i','Bagaimana cara bergabung ke vl pembangkit?');

/*!40000 ALTER TABLE `pesanvl` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table peserta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `peserta`;

CREATE TABLE `peserta` (
  `id_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_sesi` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nip_pegawai` varchar(12) NOT NULL,
  PRIMARY KEY (`id_peserta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `peserta` WRITE;
/*!40000 ALTER TABLE `peserta` DISABLE KEYS */;

INSERT INTO `peserta` (`id_peserta`, `id_sesi`, `username`, `password`, `nip_pegawai`)
VALUES
	(1,4,'subroto','peserta123','5980092R'),
	(2,5,'sukikir','peserta123','6083103B'),
	(3,3,'tasyaselalusenang','peserta123','5984004T'),
	(4,6,'sri','peserta1234','6084002T');

/*!40000 ALTER TABLE `peserta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sesi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sesi`;

CREATE TABLE `sesi` (
  `id_sesi` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `mulai` date NOT NULL,
  `selesai` date NOT NULL,
  `kd_materi` varchar(12) NOT NULL,
  `kd_udiklat` varchar(2) NOT NULL,
  `id_fasilitator` int(11) NOT NULL,
  `kd_metode` varchar(2) NOT NULL,
  PRIMARY KEY (`id_sesi`),
  KEY `id_sesi` (`id_sesi`),
  KEY `id_sesi_2` (`id_sesi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `sesi` WRITE;
/*!40000 ALTER TABLE `sesi` DISABLE KEYS */;

INSERT INTO `sesi` (`id_sesi`, `nama`, `mulai`, `selesai`, `kd_materi`, `kd_udiklat`, `id_fasilitator`, `kd_metode`)
VALUES
	(1,'Pembelajaran Video PLTU 1','2015-10-27','2015-10-30','B111044','AA',4,'VL');

/*!40000 ALTER TABLE `sesi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table udiklat
# ------------------------------------------------------------

DROP TABLE IF EXISTS `udiklat`;

CREATE TABLE `udiklat` (
  `kd_udiklat` varchar(2) NOT NULL,
  `nama_udiklat` varchar(50) NOT NULL,
  `alamat_udiklat` text NOT NULL,
  `notelp_udiklat` text NOT NULL,
  PRIMARY KEY (`kd_udiklat`),
  KEY `nama_udiklat` (`nama_udiklat`),
  KEY `nama_udiklat_2` (`nama_udiklat`),
  KEY `nama_udiklat_3` (`nama_udiklat`),
  KEY `nama_udiklat_4` (`nama_udiklat`),
  KEY `kd_udiklat` (`kd_udiklat`),
  KEY `kd_udiklat_2` (`kd_udiklat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `udiklat` WRITE;
/*!40000 ALTER TABLE `udiklat` DISABLE KEYS */;

INSERT INTO `udiklat` (`kd_udiklat`, `nama_udiklat`, `alamat_udiklat`, `notelp_udiklat`)
VALUES
	('AA','Udiklat Jakarta','Jl. Raya S.Parman, Slipi, Jakarta Barat','(021)8976542'),
	('AB','Udiklat Bogor','Jl. Raya Puncak KM.72, Bogor 16750','(0251)8254545'),
	('AC','Udiklat Pandaan','Jl.Raya Surabaya-Malang KM.50, Pandaan Pasuruan 67156','(0343)631688'),
	('AD','Udiklat Suralaya','Jl. Raya Kompl. PLTU Suralaya No. 7 Merak, 42439, Jawa Barat','(0254)571218'),
	('AE','Udiklat Semarang','Jl. Raya Kedung Mundu, Salak Utama, Semarang Timur 5001','(024)6718316'),
	('AF','Udiklat Tuntungan','jl. Lapangan Golf Tuntungan No. 35 Pancarbatu - MEDAN','(061)8360855'),
	('AG','Udiklat Makassar','Jl. Malino 377- PO BOX 1182 Makassar','(0411)865390'),
	('AH','Udiklat Padang','Jl. Padang Bukit Tinggi KM. 37 Lubuk Agung - Sumatera Barat','(0751)98768'),
	('AI','Udiklat Palembang','Jl. Bendungan No. 22 Sekip Palembang ','(0711)322342'),
	('AJ','Udiklat Banjarbaru','Jl. A.Yani KM.32 Loktabat, Banjarbaru Kalimantan Selatan','(0511)4777357'),
	('AK','Assessment Center','JL. Harsono RM No.59, Ragunan-PasarMinggu, Jakarta, 12550','(021)7811390'),
	('AL','Unit Sertifikasi','JL. Harsono RM No.59, Ragunan-PasarMinggu, Jakarta, 12550','(021)7811292'),
	('AM','Museum Listrik Energi Baru','JL. Raya Taman Mini','(021)8413451');

/*!40000 ALTER TABLE `udiklat` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `tipe_user` enum('fasilitator','peserta','accessor','admin','penyelenggara') NOT NULL,
  `nip_user` varchar(12) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id_user`, `username`, `password`, `tipe_user`, `nip_user`)
VALUES
	(1,'tasyasal','plnpusdi','admin',''),
	(2,'mila','12345','peserta',''),
	(3,'rizka','abcde','fasilitator','');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_vl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_vl`;

CREATE TABLE `user_vl` (
  `id_uservl` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `tipe_user` enum('peserta','fasilitator') NOT NULL,
  `nip_pegawai` varchar(12) NOT NULL,
  `id_sesi` int(11) NOT NULL,
  PRIMARY KEY (`id_uservl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_vl` WRITE;
/*!40000 ALTER TABLE `user_vl` DISABLE KEYS */;

INSERT INTO `user_vl` (`id_uservl`, `username`, `password`, `tipe_user`, `nip_pegawai`, `id_sesi`)
VALUES
	(1,'wanto','12345','peserta','5980092R',1);

/*!40000 ALTER TABLE `user_vl` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table vl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vl`;

CREATE TABLE `vl` (
  `id_vl` int(11) NOT NULL AUTO_INCREMENT,
  `kd_materi` varchar(12) NOT NULL,
  `path_vl` text NOT NULL,
  `judul_vl` varchar(30) NOT NULL,
  PRIMARY KEY (`id_vl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `vl` WRITE;
/*!40000 ALTER TABLE `vl` DISABLE KEYS */;

INSERT INTO `vl` (`id_vl`, `kd_materi`, `path_vl`, `judul_vl`)
VALUES
	(1,'B111044','a.mp4','Video PLTU 1');

/*!40000 ALTER TABLE `vl` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_bank_soal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_bank_soal`;

CREATE TABLE `wb_bank_soal` (
  `id_bank_soal` int(11) NOT NULL AUTO_INCREMENT,
  `id_soal` varchar(50) DEFAULT NULL,
  `categori_quiz` varchar(20) DEFAULT NULL,
  `pertanyaan` text,
  `a` varchar(300) DEFAULT NULL,
  `b` varchar(300) DEFAULT NULL,
  `c` varchar(300) DEFAULT NULL,
  `d` varchar(300) DEFAULT NULL,
  `e` varchar(300) DEFAULT NULL,
  `jawaban_pg` varchar(2) DEFAULT NULL,
  `jawaban_essay` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bank_soal`),
  KEY `FK_wb_bank_soal` (`id_soal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_bank_soal` WRITE;
/*!40000 ALTER TABLE `wb_bank_soal` DISABLE KEYS */;

INSERT INTO `wb_bank_soal` (`id_bank_soal`, `id_soal`, `categori_quiz`, `pertanyaan`, `a`, `b`, `c`, `d`, `e`, `jawaban_pg`, `jawaban_essay`, `created_date`, `created_by`, `updated_date`, `updated_by`)
VALUES
	(1,'SL001','pg','Menurut deret tribolistrik, jika emas (Au) digosok dengan kain wol, maka emas akan ? ','bermuatan positif','bermuatan negatif','netral','mungkin positif, mungkin negatif','tidak ada jawaban yang benar','a','','2015-09-26 18:37:15',NULL,'2015-10-06 15:54:08',5),
	(3,'SL001','pg','Sebuah benda akan bermuatan positif bila? ','kelebihan elektron','kekurangan elektron','kekurangan proton','jumlah proton sama dengan jumlah elektron','jawaban a dan c benar','a','','2015-09-27 13:02:35',5,'2015-10-06 15:55:25',5),
	(4,'SL001','essay','Jika di dalam suatu benda terdapat keseimbangan antara jumlah proton dengan jumlah elektron, maka benda tersebut? ','','','','','','','netral,,,,,,,, ','2015-09-27 13:38:39',NULL,'2015-10-06 15:57:16',5),
	(5,'SL001','pg','Sebuah benda akan bermuatan negatif bila ?','kelebihan elektron','kekurangan elektron','kekurangan proton','jumlah proton sama dengan jumlah elektron','semua pilihan salah','a','','2015-10-06 15:56:28',5,'0000-00-00 00:00:00',NULL),
	(6,'SL001','essay','Jika dua muatan listrik sejenis didekatkan akan tolak-menolak dan bila tidak sejenis didekatkan akan tarik-menarik. Pernyataan tersebut sesuai dengan','','','','','','','Hukum ,,,,','2015-10-06 15:57:43',5,'0000-00-00 00:00:00',NULL),
	(7,'SL150926190328','pg','Pada hukum Coulomb besar gaya tarik atau gaya tolak antara dua muatan berbanding terbalik dengan?','besar muatan masing-masing','kuadrat muatan masing-masing','jarak antara dua muatan','kuadrat jarak antara dua muatan','kuadrat jarak antara dua muatan','a','','2015-10-06 15:59:07',5,'0000-00-00 00:00:00',NULL),
	(8,'SL150926190328','pg','Satuan sistem internasional muatan listrik adala?','coulomb','ampere','farad','mikrocoulomb','watt','a','','2015-10-06 15:59:58',5,'0000-00-00 00:00:00',NULL),
	(9,'SL150926190328','essay','Gelas dan mika akan mendapat muatan negatif bila digosok dengan bulu kelinci. Apakah muatan negatif tersebut mempunyai intensitas yang sama? Jelaskan!','','','','','','','?','2015-10-06 16:00:36',5,'0000-00-00 00:00:00',NULL),
	(10,'SL150926190328','essay','Dua buah muatan listrik yang tidak sejenis didekatkan akan tarikmenarik dan apabila semakin didekatkan gaya tarik akan semakin besar, mengapa demikian?','','','','','','','?','2015-10-06 16:00:52',5,'0000-00-00 00:00:00',NULL),
	(11,'SL150926190328','essay','Muatan +Q1 dan muatan -Q2 pada jarak R tarik menarik dengan gaya sebesar F. Jika jarak kedua muatan dibuat menjadi 3/4 R berapa besar gaya tarik menarik kedua muatan tersebut?','','','','','','','?','2015-10-06 16:01:05',5,'0000-00-00 00:00:00',NULL),
	(12,'SL151021170635','pg','Berkas cahaya monokromatik 600 nm digunakan untuk menyinari suatu kisi difraksi secara tegak lurus. Jika jarak terang ke terang yang berdekatan adalah 1 cm dan jarak antar kisi adalah 45 ?m maka layar terletak dari kisi sejauh....','30 cm','40 cm','50 cm','60 cm','90 cm','a','','2015-10-21 15:09:02',1,'0000-00-00 00:00:00',NULL),
	(13,'SL151021170635','pg','Cahaya sebuah sumber dengan panjang gelombang sebesar 590 nm melalui dua celah sempit hingga terjadi pola-pola pada layar yang berjarak 1 m. Jika jarak antara garis gelap pertama dengan garis terang pertama adalah 2,95 mm, maka lebar celah adalah....','0,1 mm','0,2 mm','0,3 mm','0,4mm','0,5 mm','b','','2015-10-21 15:09:55',1,'0000-00-00 00:00:00',NULL),
	(14,'SL151021170635','pg','Percobaan lenturan cahaya pada kisi-kisi dapat diketahui bahwa penyimpangan cahaya biru lebih kecil dari cahaya kuning. Sedangkan penyimpangan kuning lebih kecil dari pada penyimpangan cahaya merah. Urutan cahaya dari panjang gelombang yang besar ke yang kecil adalah ','birukuning merah','kuningmerahbiru ','merahbirukuning ','merahkuningbiru','kuningbirumerah ','d','','2015-10-21 15:10:48',1,'0000-00-00 00:00:00',NULL),
	(15,'SL151021170635','pg','Pada percobaan Young, dua celah berjarak 1 mm diletak kan pada jarak 1 meter dari sebuah layar. Bila jarak terdekat antara pola interferensi garis terang pertama dan garis terang kesebelas adalah 4 mm, maka panjang gelombang cahaya yang menyinari adalah ','1.000 Å ','2.000 Å ','3.000 Å ','4.000 Å ','1.000 Å ','b','','2015-10-21 15:11:25',1,'0000-00-00 00:00:00',NULL),
	(16,'SL151021170635','essay','Seberkas cahaya dengan panjang gelombang 5000 Å jatuh tegak lurus pada pada suatu kisi yang terdiri dari 5000 garis tiap cm. Sudut bias orde kedua yang terjadi adalah....','','','','','','','xx','2015-10-21 15:12:16',1,'0000-00-00 00:00:00',NULL),
	(17,'SL151021170635','essay','Berkas cahaya monokromatik 600 nm digunakan untuk menyinari suatu kisi difraksi secara tegak lurus. Jika jarak terang ke terang yang berdekatan adalah 1 cm dan jarak antar kisi adalah 45 ?m maka layar terletak dari kisi sejauh....','','','','','','','yy','2015-10-21 15:12:30',1,'0000-00-00 00:00:00',NULL),
	(18,'SL151021170635','essay',' Dua celah yang berjarak 1 mm disinari cahaya merah dengan panjang gelombang 6,5 x 10?7 m. Sebuah layar diletakkan 1 m dari celah untuk mengamati pola-pola yang terjadi. Jarak antara gelap pertama dengan terang ketiga adalah....','','','','','','','xx','2015-10-21 15:12:41',1,'0000-00-00 00:00:00',NULL),
	(19,'SL151022122406','pg','untuk jalan kedepan lewat mana yang bener','Kesamping','kedepan','kebalakang','kesamping kiri','kesamping depan','b','','2015-10-22 05:25:21',5,'0000-00-00 00:00:00',NULL);

/*!40000 ALTER TABLE `wb_bank_soal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_categori_quiz
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_categori_quiz`;

CREATE TABLE `wb_categori_quiz` (
  `id_categori_quiz` int(11) NOT NULL AUTO_INCREMENT,
  `categori_quiz` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_categori_quiz`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table wb_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_class`;

CREATE TABLE `wb_class` (
  `id_class` varchar(50) NOT NULL,
  `class` varchar(150) NOT NULL,
  `instruktur2` int(11) DEFAULT NULL,
  `instruktur1` int(11) DEFAULT NULL,
  `lokasi` varchar(200) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_class`),
  KEY `FK_wb_class_ins` (`instruktur1`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_class` WRITE;
/*!40000 ALTER TABLE `wb_class` DISABLE KEYS */;

INSERT INTO `wb_class` (`id_class`, `class`, `instruktur2`, `instruktur1`, `lokasi`, `keterangan`, `start_date`, `status`, `end_date`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('CL001','Listrik Statis',10,5,'Jln. pulau seribu, tangerang selatan','Mempelajari listrik statis','2015-09-01','active','2015-10-24',5,'2015-10-21 14:54:00',1,'2015-10-21 14:54:00'),
	('CLS151010183751','Interferensi Difraksi Cahaya',9,10,'BDS','Interferensi Difraksi Cahaya','2015-10-01',NULL,'2016-01-30',1,'2015-10-21 15:13:20',1,'2015-10-21 15:13:20');

/*!40000 ALTER TABLE `wb_class` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_document
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_document`;

CREATE TABLE `wb_document` (
  `id_document` varchar(50) NOT NULL,
  `document` varchar(150) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `id_class` varchar(100) DEFAULT NULL,
  `id_periode` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_document`),
  KEY `FK_wb_document` (`id_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_document` WRITE;
/*!40000 ALTER TABLE `wb_document` DISABLE KEYS */;

INSERT INTO `wb_document` (`id_document`, `document`, `file_name`, `type`, `path`, `id_class`, `id_periode`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('DOC001','FILE 1','Apress - Beginning Android (2009).pdf','file','./front_assets/file/89deae6674c0089000c1e1166966935a.pdf','CL001','PRD001',NULL,'2015-09-09 19:23:56',5,'2015-09-27 14:22:32'),
	('FL150927162307','FILE 2','Apress - Android Essentials (2008).pdf','file','./front_assets/file/e8c1020281c447ef52691116169c4fef.pdf','CL001','PRD002',5,'2015-09-27 14:23:07',NULL,'0000-00-00 00:00:00'),
	('FL151002193725','adsad','Transkip2.jpg','file','./front_assets/file/24689291946480cb014b40d775664483.jpg','CL001','PRD001',1,'2015-10-02 17:37:25',NULL,'0000-00-00 00:00:00'),
	('MAT00X','MATERI 1','1.1 Mengenal Lingkup Pemrograman.pdf','materi','./front_assets/file/0813dad61be61c27052e8cf15bae7276.pdf','CL001','PRD001',NULL,'2015-09-19 14:54:44',1,'2015-10-02 17:25:06'),
	('MTR150927161413','MATERI 2','1.2 Dasar-Dasar Pemrograman.pdf','materi','./front_assets/file/277fa6e7208d819245e0b26616ccdf53.pdf','CL001','PRD002',5,'2015-09-27 14:14:13',NULL,'0000-00-00 00:00:00'),
	('MTR151002192817','aqw','Cert.jpg','materi','./front_assets/file/e1d819d3558ad1cd30fbfce0c5869ef6.jpg','CL001','PRD002',1,'2015-10-02 17:28:17',NULL,'0000-00-00 00:00:00'),
	('MTR151002193527','asdadasdwwww','Transkip2.jpg','materi','./front_assets/file/eb3a7fb8f2b06e35bc1167a71432b29f.jpg','CL001','PRD001',1,'2015-10-02 17:35:27',NULL,'0000-00-00 00:00:00'),
	('VD150927161826','VIDEO 1','#1 - Android Studio Tutorial   Php Mysql Login and Register.mp4','video','./front_assets/file/5ba2debe1c42b8bf416cf4b461c8bfcb.mp4','CL001','PRD001',5,'2015-09-27 14:18:31',1,'2015-09-29 16:24:24'),
	('VD150927161931','VIDEO 2','#2 - Android Studio Tutorial   Php Mysql Login and Register.mp4','video','./front_assets/file/8ab36c8c1f01a031cbb7edb4f5b7d0de.mp4','CL001','PRD002',5,'2015-09-27 14:19:35',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_document` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_jawaban
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_jawaban`;

CREATE TABLE `wb_jawaban` (
  `id_jawaban` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(20) DEFAULT NULL,
  `id_soal` varchar(50) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `jawaban_pg` varchar(50) DEFAULT NULL,
  `jawaban_essay` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_jawaban`),
  KEY `FK_wb_jawaban_quiz` (`id_soal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_jawaban` WRITE;
/*!40000 ALTER TABLE `wb_jawaban` DISABLE KEYS */;

INSERT INTO `wb_jawaban` (`id_jawaban`, `jenis`, `id_soal`, `id_bank_soal`, `jawaban_pg`, `jawaban_essay`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(10,'essay','SL150926190328',11,NULL,'asdadasd',2,'2015-10-20 15:22:47',NULL,NULL),
	(11,'essay','SL150926190328',10,NULL,'asdad',2,'2015-10-20 15:23:06',NULL,NULL),
	(13,'essay','SL150926190328',9,NULL,'asdadsad 12313',2,'2015-10-20 15:41:48',NULL,NULL),
	(17,'essay','SL001',4,NULL,'zz',2,'2015-10-20 15:55:06',NULL,NULL),
	(18,'essay','SL001',6,NULL,'dd',2,'2015-10-20 15:55:13',NULL,NULL),
	(36,'pg','SL001',1,'c',NULL,2,'2015-10-22 05:26:23',NULL,NULL),
	(37,'pg','SL001',3,'c',NULL,2,'2015-10-22 05:26:27',NULL,NULL),
	(23,'pg','SL001',5,'b',NULL,2,'2015-10-20 18:24:50',NULL,NULL),
	(27,'pg','SL150926190328',7,'a',NULL,2,'2015-10-21 12:49:15',NULL,NULL),
	(28,'pg','SL150926190328',8,'a',NULL,2,'2015-10-21 12:49:19',NULL,NULL),
	(33,'pg','SL151021170635',12,'a',NULL,2,'2015-10-22 04:56:16',NULL,NULL),
	(30,'pg','SL151021170635',13,'a',NULL,2,'2015-10-21 15:48:24',NULL,NULL),
	(34,'pg','SL151021170635',14,'e',NULL,2,'2015-10-22 04:56:29',NULL,NULL),
	(32,'pg','SL151021170635',15,'e',NULL,2,'2015-10-21 15:48:38',NULL,NULL);

/*!40000 ALTER TABLE `wb_jawaban` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_jawaban_peserta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_jawaban_peserta`;

CREATE TABLE `wb_jawaban_peserta` (
  `id_jawaban_peserta` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` varchar(50) DEFAULT NULL,
  `id_quiz` varchar(50) DEFAULT NULL,
  `id_bank_soal` int(11) DEFAULT NULL,
  `jawaban` text,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_jawaban_peserta`),
  KEY `FK_wb_jawaban_peserta` (`id_peserta`),
  KEY `FK_wb_jawaban_quiz` (`id_quiz`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table wb_jenis_materi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_jenis_materi`;

CREATE TABLE `wb_jenis_materi` (
  `id_jenis` varchar(50) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `id_materi` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_jenis`),
  KEY `FK_wb_jenis_materi` (`id_materi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_jenis_materi` WRITE;
/*!40000 ALTER TABLE `wb_jenis_materi` DISABLE KEYS */;

INSERT INTO `wb_jenis_materi` (`id_jenis`, `jenis`, `id_materi`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('JNS001','JENIS 1','MTR001',NULL,'2015-09-09 19:29:14',NULL,'0000-00-00 00:00:00'),
	('JNS002','JENIS 2','MTR001',NULL,'2015-09-09 19:29:31',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_jenis_materi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_jenis_quiz
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_jenis_quiz`;

CREATE TABLE `wb_jenis_quiz` (
  `id_jenis_quis` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_quiz` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_quis`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table wb_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_level`;

CREATE TABLE `wb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(100) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `isactive` int(11) DEFAULT '1',
  PRIMARY KEY (`id_level`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_level` WRITE;
/*!40000 ALTER TABLE `wb_level` DISABLE KEYS */;

INSERT INTO `wb_level` (`id_level`, `level_name`, `created_by`, `created_date`, `updated_by`, `updated_date`, `isactive`)
VALUES
	(1,'Admin',NULL,'2015-09-08 17:19:58',NULL,NULL,1),
	(2,'Peserta',NULL,'2015-09-08 17:20:08',1,'2015-09-26',1),
	(6,'Instruktur',1,'2015-09-14 17:14:01',NULL,NULL,1);

/*!40000 ALTER TABLE `wb_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_login_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_login_log`;

CREATE TABLE `wb_login_log` (
  `id_login` int(11) NOT NULL AUTO_INCREMENT,
  `id_sesion` varchar(90) DEFAULT NULL,
  `id_pegawai` varchar(50) DEFAULT NULL,
  `ip_address` varchar(90) DEFAULT NULL,
  `login_start` datetime DEFAULT NULL,
  `login_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id_login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table wb_materi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_materi`;

CREATE TABLE `wb_materi` (
  `id_materi` varchar(50) NOT NULL,
  `materi` varchar(200) NOT NULL,
  `path` varchar(100) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_materi`),
  KEY `FK_wb_materi` (`id_class`),
  KEY `FK_wb_mater` (`id_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_materi` WRITE;
/*!40000 ALTER TABLE `wb_materi` DISABLE KEYS */;

INSERT INTO `wb_materi` (`id_materi`, `materi`, `path`, `id_class`, `id_periode`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('MTR001','Trafo',NULL,'CL001',NULL,NULL,'2015-09-08 17:41:16',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_materi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_menu`;

CREATE TABLE `wb_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `menu_name` varchar(90) DEFAULT NULL,
  `menu_url` varchar(100) DEFAULT NULL,
  `menu_icon` varchar(100) DEFAULT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_menu` WRITE;
/*!40000 ALTER TABLE `wb_menu` DISABLE KEYS */;

INSERT INTO `wb_menu` (`id_menu`, `id_parent`, `menu_name`, `menu_url`, `menu_icon`, `menu_order`, `isactive`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,0,'Home','home','xE871',1,1,'1',NULL,'1','2015-11-02 09:19:41'),
	(2,0,'Master Kelas','class_m','xE1BD',2,1,'1',NULL,'1','2015-10-17 15:39:00'),
	(3,0,'Document','document','xE24D',6,1,'1',NULL,'1','2015-10-17 15:40:51'),
	(4,3,'video','document/video','file-movie-o',1,1,'1',NULL,'1','2015-09-13 18:18:59'),
	(5,3,'File','document/file','file-pdf-o',2,1,'1',NULL,NULL,NULL),
	(6,3,'Materi','document/materi','file-word-o',3,1,'1',NULL,NULL,NULL),
	(7,0,'Nilai','nilai','xE53E',10,1,'1',NULL,'1','2015-10-17 15:41:50'),
	(8,0,'Report','report','xE85C',20,1,'1',NULL,'1','2015-10-17 15:39:34'),
	(9,0,'Administration','a','xE8C0',30,1,'1',NULL,'1','2015-10-17 16:36:16'),
	(10,9,'Menu','menu','bars',1,1,'1',NULL,NULL,NULL),
	(11,9,'level','level','bookmark-o',2,1,'1',NULL,NULL,NULL),
	(12,9,'User','user','users',3,1,NULL,NULL,NULL,NULL),
	(13,9,'Reset Password','user/reset','edit',4,1,NULL,NULL,NULL,NULL),
	(15,14,'Instruktur','instruktur','user-plus',1,1,NULL,NULL,NULL,NULL),
	(16,14,'Peserta','peserta','user-md',2,1,NULL,NULL,NULL,NULL),
	(17,0,'Kelas (Peserta)','class_p','xE8CB',5,1,'1','2015-09-16 13:31:32','1','2015-10-17 15:48:24'),
	(18,0,'Soal','soal','xE0B9',9,1,'1','2015-09-26 08:12:24','1','2015-10-17 15:45:01'),
	(19,0,'Jadwal Ujian','exam_schd','xE8D2',7,1,'1','2015-09-28 13:00:25','1','2015-10-17 15:44:39'),
	(20,0,'Ujian','exam','xE32A',8,1,'1','2015-09-29 17:13:10','1','2015-10-17 15:46:42'),
	(21,0,'Master Peserta','participant','xE87B',4,1,'1','2015-09-30 15:15:13','1','2015-10-17 15:43:40'),
	(22,0,'Master Pegawai','employee','xE87C',3,1,'1','2015-09-30 17:37:05','1','2015-10-17 15:42:52'),
	(23,0,'Profile','profile','xE87C',40,1,'1','2015-10-17 16:36:49','1','2015-10-17 16:37:25');

/*!40000 ALTER TABLE `wb_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_menu_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_menu_role`;

CREATE TABLE `wb_menu_role` (
  `id_menu_role` int(11) NOT NULL AUTO_INCREMENT,
  `id_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu_role`),
  KEY `FK_wb_menu_role` (`id_menu`),
  KEY `FK_wb_menu_level` (`id_level`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_menu_role` WRITE;
/*!40000 ALTER TABLE `wb_menu_role` DISABLE KEYS */;

INSERT INTO `wb_menu_role` (`id_menu_role`, `id_level`, `id_menu`, `isactive`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,1,1,1,'1',NULL,NULL,NULL),
	(2,1,2,1,'1',NULL,NULL,NULL),
	(3,1,3,1,'1',NULL,NULL,NULL),
	(4,1,4,1,'1',NULL,NULL,NULL),
	(5,1,5,1,'1',NULL,NULL,NULL),
	(6,1,6,1,'1',NULL,NULL,NULL),
	(7,1,7,1,'1',NULL,NULL,NULL),
	(8,1,8,1,'1',NULL,NULL,NULL),
	(9,1,9,1,'1',NULL,NULL,NULL),
	(10,1,10,1,NULL,NULL,NULL,NULL),
	(11,1,11,1,NULL,NULL,NULL,NULL),
	(12,1,12,1,NULL,NULL,NULL,NULL),
	(13,1,13,1,NULL,NULL,NULL,NULL),
	(15,1,15,1,NULL,NULL,NULL,NULL),
	(16,1,16,1,NULL,NULL,NULL,NULL),
	(21,2,1,1,'1',NULL,NULL,NULL),
	(22,2,2,0,'1',NULL,NULL,NULL),
	(23,2,3,0,'1',NULL,NULL,NULL),
	(24,2,4,0,'1',NULL,NULL,NULL),
	(25,2,5,0,'1',NULL,NULL,NULL),
	(26,2,6,0,'1',NULL,NULL,NULL),
	(27,2,7,0,'1',NULL,NULL,NULL),
	(28,2,8,0,'1',NULL,NULL,NULL),
	(29,2,9,0,'1',NULL,NULL,NULL),
	(30,2,10,0,NULL,NULL,NULL,NULL),
	(31,2,11,0,NULL,NULL,NULL,NULL),
	(32,2,12,0,NULL,NULL,NULL,NULL),
	(33,2,13,0,NULL,NULL,NULL,NULL),
	(35,2,15,0,NULL,NULL,NULL,NULL),
	(36,2,16,0,NULL,NULL,NULL,NULL),
	(40,6,1,1,'1','2015-09-14 17:17:05',NULL,NULL),
	(41,6,2,1,'1','2015-09-14 17:17:05',NULL,NULL),
	(42,6,3,1,'1','2015-09-14 17:17:05',NULL,NULL),
	(43,6,4,1,'1','2015-09-14 17:17:05',NULL,NULL),
	(44,6,5,1,'1','2015-09-14 17:17:05',NULL,NULL),
	(45,6,6,1,'1','2015-09-14 17:17:05',NULL,NULL),
	(46,6,7,0,'1','2015-09-14 17:17:05',NULL,NULL),
	(47,6,8,1,'1','2015-09-14 17:17:05',NULL,NULL),
	(48,6,9,0,'1','2015-09-14 17:17:05',NULL,NULL),
	(49,6,10,0,'1','2015-09-14 17:17:05',NULL,NULL),
	(50,6,11,0,'1','2015-09-14 17:17:05',NULL,NULL),
	(51,6,12,0,'1','2015-09-14 17:17:05',NULL,NULL),
	(52,6,13,0,'1','2015-09-14 17:17:05',NULL,NULL),
	(54,6,15,0,'1','2015-09-14 17:17:05',NULL,NULL),
	(55,6,16,0,'1','2015-09-14 17:17:05',NULL,NULL),
	(56,1,17,1,'1','2015-09-16 13:31:32',NULL,NULL),
	(57,2,17,1,'1','2015-09-16 13:31:32',NULL,NULL),
	(58,6,17,0,'1','2015-09-16 13:31:32',NULL,NULL),
	(59,1,18,1,'1','2015-09-26 08:12:24',NULL,NULL),
	(60,2,18,0,'1','2015-09-26 08:12:24',NULL,NULL),
	(61,6,18,1,'1','2015-09-26 08:12:24',NULL,NULL),
	(62,1,19,1,'1','2015-09-28 13:00:25',NULL,NULL),
	(63,2,19,1,'1','2015-09-28 13:00:25',NULL,NULL),
	(64,6,19,1,'1','2015-09-28 13:00:25',NULL,NULL),
	(65,1,20,1,'1','2015-09-29 17:13:10',NULL,NULL),
	(66,2,20,1,'1','2015-09-29 17:13:10',NULL,NULL),
	(67,6,20,0,'1','2015-09-29 17:13:10',NULL,NULL),
	(68,1,21,1,'1','2015-09-30 15:15:13',NULL,NULL),
	(69,2,21,0,'1','2015-09-30 15:15:13',NULL,NULL),
	(70,6,21,0,'1','2015-09-30 15:15:13',NULL,NULL),
	(71,1,22,1,'1','2015-09-30 17:37:05',NULL,NULL),
	(72,2,22,0,'1','2015-09-30 17:37:05',NULL,NULL),
	(73,6,22,0,'1','2015-09-30 17:37:06',NULL,NULL),
	(74,1,23,1,'1','2015-10-17 16:36:49',NULL,NULL),
	(75,2,23,1,'1','2015-10-17 16:36:49',NULL,NULL),
	(76,6,23,1,'1','2015-10-17 16:36:49',NULL,NULL);

/*!40000 ALTER TABLE `wb_menu_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_pegawai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_pegawai`;

CREATE TABLE `wb_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `tgl_lahir` varchar(50) DEFAULT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `sertifikasi` varchar(200) DEFAULT NULL,
  `jabatan` varchar(200) DEFAULT NULL,
  `skill` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_pegawai` WRITE;
/*!40000 ALTER TABLE `wb_pegawai` DISABLE KEYS */;

INSERT INTO `wb_pegawai` (`id_pegawai`, `nip`, `nama`, `alamat`, `telp`, `email`, `tgl_lahir`, `photo`, `sertifikasi`, `jabatan`, `skill`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,'001','Kusnadi','Jl. Pulau Seribu NO. 12 A, BSD','0856742001','Kusnadi@yahoo.com',NULL,'./front_assets/profile/9419cdde5d53aa64ef5865ffdfb05888.png',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(2,'002','Dudung','Jl. Pulau Seribu NO. 12 A, BSD','0856742002','Dudung@yahoo.com',NULL,'./front_assets/profile/24cfcd59349946d0ef4164f48b53b33e.png',NULL,'Staff','Elektronika',NULL,'0000-00-00 00:00:00',NULL,NULL),
	(3,'003','imam','Jl. Pulau Seribu NO. 12 A, BSD','0856742003','imam@yahoo.com',NULL,'./front_assets/profile/49fccd80b43614458674bc9f388b3f67.png',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(4,'004','Intan','Jl. Pulau Seribu NO. 12 A, BSD','0856742004','Intan@yahoo.com',NULL,'./front_assets/profile/avatar_04.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(5,'005','Nani','Jl. Pulau Seribu NO. 12 A, BSD','0856742005','Nani@yahoo.com',NULL,'./front_assets/profile/avatar_05.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(6,'006','Emmy','Jl. Pulau Seribu NO. 12 A, BSD','0856742006','Emmy@yahoo.com',NULL,'./front_assets/profile/avatar_06.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(7,'007','Joni','Jl. Pulau Seribu NO. 12 A, BSD','0856742007','Joni@yahoo.com',NULL,'./front_assets/profile/avatar_07.PNG',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(8,'008','Mamad','Jl. Pulau Seribu NO. 12 A, BSD','0856742008','Mamad@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(9,'009','yusuf','Jl. Pulau Seribu NO. 12 A, BSD','0856742009','yusuf@yahoo.com',NULL,'./front_assets/profile/5adf1f83568256154af85a166ab0f336.jpg',NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(10,'010','Asep','Jl. Pulau Seribu NO. 12 A, BSD','0856742010','Asep@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(11,'011','Fitri','Jl. Pulau Seribu NO. 12 A, BSD','0856742011','Fitri@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(12,'012','Dadang','Jl. Pulau Seribu NO. 12 A, BSD','0856742012','Dadang@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(13,'013','Hardi','Jl. Pulau Seribu NO. 12 A, BSD','0856742013','Hardi@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(14,'014','Soni','Jl. Pulau Seribu NO. 12 A, BSD','0856742014','Soni@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL),
	(15,'015','Jamal','Jl. Pulau Seribu NO. 12 A, BSD','0856742015','Jamal@yahoo.com',NULL,NULL,NULL,'Staff',NULL,NULL,'0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `wb_pegawai` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_periode
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_periode`;

CREATE TABLE `wb_periode` (
  `id_periode` varchar(50) NOT NULL,
  `periode` varchar(100) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_schedule` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_periode`),
  KEY `FK_wb_periode` (`id_class`),
  KEY `FK_wb_sch` (`id_schedule`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_periode` WRITE;
/*!40000 ALTER TABLE `wb_periode` DISABLE KEYS */;

INSERT INTO `wb_periode` (`id_periode`, `periode`, `start_date`, `end_date`, `id_class`, `id_schedule`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('PRD001','PERIODE1','2015-10-02','2015-11-10','CL001',NULL,NULL,'0000-00-00 00:00:00',1,'2015-10-10 18:02:34'),
	('PRD002','PERIODE2','2015-11-07','2015-11-23','CL001',NULL,NULL,'0000-00-00 00:00:00',1,'2015-10-06 16:03:26'),
	('PRD15101018371','Periode 1','2015-10-01','2015-10-17','CLS151010183751',NULL,1,'2015-10-10 16:37:51',NULL,'0000-00-00 00:00:00'),
	('PRD15101018372','Periode 2','2015-10-31','2015-10-25','CLS151010183751',NULL,1,'2015-10-10 16:37:51',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_periode` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_peserta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_peserta`;

CREATE TABLE `wb_peserta` (
  `id_peserta` varchar(50) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_peserta`),
  KEY `FK_wb_peserta` (`id_class`),
  KEY `FK_wb_peserta_reg` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_peserta` WRITE;
/*!40000 ALTER TABLE `wb_peserta` DISABLE KEYS */;

INSERT INTO `wb_peserta` (`id_peserta`, `id_user`, `id_class`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('PST150930193058',2,'CL001','1','2015-10-21 14:15:17','2','2015-10-21 14:15:17'),
	('PST150930203251',6,'CL001','1','2015-09-30 18:32:51',NULL,'0000-00-00 00:00:00'),
	('PST151006191033',7,'CL001','1','2015-10-06 17:10:33',NULL,'0000-00-00 00:00:00'),
	('PST151006191039',8,'CL001','1','2015-10-06 17:10:39',NULL,'0000-00-00 00:00:00'),
	('PST151021162010',6,'CLS151010183751','1','2015-10-21 14:20:10',NULL,'0000-00-00 00:00:00'),
	('PST151021162021',7,'CLS151010183751','1','2015-10-21 14:20:21',NULL,'0000-00-00 00:00:00'),
	('PST151021162030',12,'CLS151010183751','1','2015-10-21 14:20:30',NULL,'0000-00-00 00:00:00'),
	('PST151021163456',17,'CLS151010183751','1','2015-10-21 14:34:56',NULL,'0000-00-00 00:00:00'),
	('PST151021165146',2,'CLS151010183751','1','2015-10-21 14:51:46',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_peserta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_quiz
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_quiz`;

CREATE TABLE `wb_quiz` (
  `id_quiz` varchar(50) NOT NULL,
  `quiz` varchar(100) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_quiz`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table wb_schedule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_schedule`;

CREATE TABLE `wb_schedule` (
  `id_schedule` varchar(50) NOT NULL,
  `schedule_name` varchar(150) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_schedule`),
  KEY `FK_wb_schedule` (`id_class`),
  KEY `FK_wb_periode` (`id_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_schedule` WRITE;
/*!40000 ALTER TABLE `wb_schedule` DISABLE KEYS */;

INSERT INTO `wb_schedule` (`id_schedule`, `schedule_name`, `id_class`, `id_periode`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('SCH001','Sch 1','CL001',NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `wb_schedule` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_schedule_bak
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_schedule_bak`;

CREATE TABLE `wb_schedule_bak` (
  `id_schedule` varchar(50) NOT NULL,
  `schedule_name` varchar(150) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_schedule`),
  KEY `FK_wb_schedule` (`id_class`),
  KEY `FK_wb_periode` (`id_periode`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table wb_soal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_soal`;

CREATE TABLE `wb_soal` (
  `id_soal` varchar(50) NOT NULL,
  `soal` varchar(200) DEFAULT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_soal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_soal` WRITE;
/*!40000 ALTER TABLE `wb_soal` DISABLE KEYS */;

INSERT INTO `wb_soal` (`id_soal`, `soal`, `id_class`, `id_periode`, `created_date`, `created_by`, `updated_date`, `updated_by`)
VALUES
	('SL001','Listrik Statis 1','CL001','PRD001','2015-09-26 04:42:57',NULL,'2015-10-06 15:52:29',5),
	('SL150926190328','Listrik Statis 2','CL001','PRD002','2015-09-26 17:03:28',5,'2015-10-06 15:58:07',5),
	('SL151021170635','Cahaya','CLS151010183751','PRD15101018371','2015-10-21 15:06:35',1,'2015-10-21 15:25:45',1),
	('SL151022122406','Jalan ke depan lewat mana','CL001','PRD002','2015-10-22 05:24:06',5,'0000-00-00 00:00:00',NULL),
	('SL151022140125','test','CLS151010183751','PRD15101018371','2015-10-22 07:01:25',5,'0000-00-00 00:00:00',NULL);

/*!40000 ALTER TABLE `wb_soal` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_test`;

CREATE TABLE `wb_test` (
  `nama` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_test` WRITE;
/*!40000 ALTER TABLE `wb_test` DISABLE KEYS */;

INSERT INTO `wb_test` (`nama`)
VALUES
	('199.76666666667'),
	('199.83333333333'),
	('199.85'),
	('199.93333333333'),
	('199.95'),
	('195.33333333333'),
	('0'),
	('0.016666666666667'),
	('0.016666666666667');

/*!40000 ALTER TABLE `wb_test` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_time_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_time_history`;

CREATE TABLE `wb_time_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_ujian` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



# Dump of table wb_ujian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_ujian`;

CREATE TABLE `wb_ujian` (
  `id_ujian` varchar(50) NOT NULL,
  `id_class` varchar(50) DEFAULT NULL,
  `id_periode` varchar(50) DEFAULT NULL,
  `id_soal` varchar(50) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sesion` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_ujian`),
  KEY `FK_wb_ujian` (`id_soal`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_ujian` WRITE;
/*!40000 ALTER TABLE `wb_ujian` DISABLE KEYS */;

INSERT INTO `wb_ujian` (`id_ujian`, `id_class`, `id_periode`, `id_soal`, `durasi`, `start_date`, `end_date`, `sesion`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	('UJN001','CL001','PRD001','SL001',100,'2015-10-22','0000-00-00','1',NULL,'0000-00-00 00:00:00',1,'2015-10-22 04:43:31'),
	('UJN150929185337','CL001','PRD002','SL150926190328',100,'2015-11-07',NULL,'1',1,'2015-09-29 16:53:37',1,'2015-11-07 15:52:10'),
	('UJN151022114503','CLS151010183751','PRD15101018371','SL151021170635',90,'2015-10-22',NULL,'1',1,'2015-10-22 04:45:03',NULL,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `wb_ujian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_ujian_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_ujian_history`;

CREATE TABLE `wb_ujian_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_ujian` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_ujian_history` WRITE;
/*!40000 ALTER TABLE `wb_ujian_history` DISABLE KEYS */;

INSERT INTO `wb_ujian_history` (`id`, `id_user`, `id_ujian`, `created_date`)
VALUES
	(17,2,'UJN150929185337','2015-11-07 15:53:45');

/*!40000 ALTER TABLE `wb_ujian_history` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_users`;

CREATE TABLE `wb_users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `passwords` varchar(120) DEFAULT NULL,
  `isactive` tinyint(1) DEFAULT NULL,
  `islogin` tinyint(1) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_sesion` varchar(100) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_user`,`nip`),
  KEY `FK_wb_level` (`id_level`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `wb_users` WRITE;
/*!40000 ALTER TABLE `wb_users` DISABLE KEYS */;

INSERT INTO `wb_users` (`id_user`, `nip`, `username`, `passwords`, `isactive`, `islogin`, `id_level`, `id_sesion`, `last_login`, `created_by`, `created_date`, `updated_by`, `updated_date`)
VALUES
	(1,'001','admin','3d47bd12248e350d8d1fe95eaa2e74d3',1,1,1,'ddcbb61c6f18965d09b546dee07f3376','2015-11-10 04:42:37',NULL,'0000-00-00 00:00:00',1,'2015-10-28 07:32:17'),
	(2,'002','dudung','1dc485b4e573826275e518765ddb6671',1,1,2,'d8f6786d683cf75e23fb4fde1b5d8298','2015-11-07 15:50:41',NULL,'0000-00-00 00:00:00',2,'2015-10-17 14:37:12'),
	(5,'003','imam','1dc485b4e573826275e518765ddb6671',1,1,6,'0b69e13cafde9eec7aa45777c802cc80','2015-11-10 00:55:54',1,'2015-09-14 16:01:47',NULL,'0000-00-00 00:00:00'),
	(6,'004','sopo','1dc485b4e573826275e518765ddb6671',1,1,2,'b6f0b904cbe5f05469b4ad768f16d577','2015-10-20 18:34:34',NULL,'0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00'),
	(7,'006','emmy','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-06 17:09:53',1,'2015-10-06 17:09:53',NULL,'0000-00-00 00:00:00'),
	(8,'007','joni','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-06 17:10:14',1,'2015-10-06 17:10:14',NULL,'0000-00-00 00:00:00'),
	(9,'012','dadang','1dc485b4e573826275e518765ddb6671',1,0,6,'','2015-10-22 03:09:38',1,'2015-10-21 14:07:20',NULL,'0000-00-00 00:00:00'),
	(10,'013','hardi','1dc485b4e573826275e518765ddb6671',1,NULL,6,NULL,'2015-10-21 14:07:51',1,'2015-10-21 14:07:51',NULL,'0000-00-00 00:00:00'),
	(11,'009','yusuf','1dc485b4e573826275e518765ddb6671',1,0,2,'','2015-10-21 14:10:49',1,'2015-10-21 14:08:12',NULL,'0000-00-00 00:00:00'),
	(12,'008','mamad','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-21 14:08:32',1,'2015-10-21 14:08:32',NULL,'0000-00-00 00:00:00'),
	(13,'010','asep','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-21 14:08:51',1,'2015-10-21 14:08:51',NULL,'0000-00-00 00:00:00'),
	(14,'011','fitri','1dc485b4e573826275e518765ddb6671',1,NULL,1,NULL,'2015-10-21 14:09:10',1,'2015-10-21 14:09:10',1,'2015-10-21 14:10:26'),
	(15,'014','soni','1dc485b4e573826275e518765ddb6671',1,NULL,2,NULL,'2015-10-21 14:09:31',1,'2015-10-21 14:09:31',NULL,'0000-00-00 00:00:00'),
	(16,'015','jamal','1dc485b4e573826275e518765ddb6671',0,NULL,1,NULL,'2015-10-21 14:09:45',1,'2015-10-21 14:09:45',NULL,'0000-00-00 00:00:00'),
	(17,'005','nani','1dc485b4e573826275e518765ddb6671',1,1,2,'074e6b15016f0a1b906d3507f0a76129','2015-10-21 16:00:55',1,'2015-10-21 14:10:05',1,'2015-10-21 15:59:00');

/*!40000 ALTER TABLE `wb_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wb_webcam
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wb_webcam`;

CREATE TABLE `wb_webcam` (
  `id_webcam` int(11) NOT NULL AUTO_INCREMENT,
  `type_webcam` varchar(50) DEFAULT NULL,
  `hostname` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `status_online` int(11) DEFAULT NULL,
  `information` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_webcam`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
