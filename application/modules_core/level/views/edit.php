<div class="uk-modal-dialog">
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
	<div id="edit_message"></id>
</div>

<form class="form-horizontal" id="edit_myform">
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Level Name</label>
		<input type="text" class="md-input" id="level_name" name="level_name" value="<?=$level['level_name']?>"/>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="val_select" class="uk-form-label">Active</label>
			<select id="isactive" name="isactive" required data-md-selectize>
				<option value="">Choose..</option>
				<option value="1" <?php echo $level['isactive']==1 ? 'selected':''?>>Active</option>
				<option value="0" <?php echo $level['isactive']==0 ? 'selected':''?>>Inactiva</option>
			</select>
		</div>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id" value="<?=$level['id_level']?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnupdate" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>
<script>

		$(document).ready(function() {
				
			function reloadlist(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",function(data){
					$('#list_data').html(data);
				});
			}	
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#edit_myform').validate({
				rules:{
						level_name:{
							required:true
						},
						 isactive:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$.post('<?php echo base_url('level/update'); ?>', $('#edit_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						$('#btnupdate').attr('disabled');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#edit_message").html(msg);
							
							$('#edit_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#edit_message').hide();
								$('#edit_user').hide();
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#edit_message").html(msg);
						}
					});
				}
			})
		});
</script>
