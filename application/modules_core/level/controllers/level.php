<?php

class level extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model(array('level/level_model'));
    }
    
    public function index()
	{ 	
		$this->login_model->is_login();
		$data['title'] 		='Master level';
		$data['link'] = base_url()."level/list_data"; 
		$this->load->view('preview',$data);
	}
	
	public function list_data() 
	{
		$this->login_model->is_login();
		$data['level']	= $this->level_model->get_list_level();
		$data['title'] 	='Master level';
		$data['link'] 	= base_url()."level/list_data"; 
		$this->load->view('level', $data);        
    }
	
	public function add()
	{
		$this->login_model->is_login();
		$data['title'] 		= 'Add level';
		$data['link'] = base_url()."level/list_data"; 
		$this->load->view('add', $data); 
	}
	
	public function edit()
	{
		$this->login_model->is_login();
		
		$id				= $this->input->post('id');
		$data['level']	= $this->level_model->get_level_by_id($id);
		$data['title'] 	= 'Edit level';
		$data['link'] 	= base_url()."level/list_data"; 
		$this->load->view('edit', $data); 
	}
	
	public function mapping(){
		$this->login_model->is_login();
		$id				= $this->input->post('id');
		$data['id_level'] = $id;
		$data['level']	= $this->level_model->get_level_by_id($id);
		$data['parent']	= $this->menu_model->get_list_menu_parent_role($id);
		$data['title']  = 'Mapping Level '.$data['level']['level_name'];
		$data['link'] 	= base_url()."level/mapping"; 
		$this->load->view('mapping', $data); 
	}
	
	public function update()
	{	
		$this->login_model->is_login();
		$id				= $this->input->post('id');
		$level_name		= $this->input->post('level_name');
		$isactive 		= $this->input->post('isactive');
		$id_user		= $this->session->userdata("sesi_id_level");
		$this->level_model->update_level($id,$isactive,$level_name,$id_user);
	}
	
	public function save()
	{	
		$this->login_model->is_login();
		$level_name		= $this->input->post('level_name');
		$isactive 		= $this->input->post('isactive');
		$id_user		= $this->session->userdata("sesi_id_level");
		
		$this->level_model->save_level($level_name,$isactive,$id_user);
		
		$data  			= $this->level_model->get_level_id($level_name,$isactive,$id_user);
		
		$menu = $this->menu_model->get_list_menu();
		foreach ($menu->result() as $row){
			$this->menu_model->save_menu_role($data['id_level'],$row->id_menu,$id_user);
		}
	}
	
	function update_mapping()
	{	
		$this->login_model->is_login();
		$id_level = $this->input->post('id');
		
		$this->menu_model->inactive_role_menu($id_level);
	

		if(!empty($this->input->post('menu'))) {
			foreach($this->input->post('menu') as $check2) {
				 $menu = explode('|', $check2);
				 //echo $menu[0]; 
				// echo $menu[1];
				// echo "<br/>";
				
				$this->menu_model->update_role_menu($menu[0],$menu[1],1);
				//die();
			}
		}	
		echo json_encode(array('response' => 'success', 'msg' => 'Mapping berhasil di simpan'));
	}
	
	//inactive aja jangan di delete
	public function delete()
	{	
		$this->login_model->is_login();
		$id	= $this->input->post('id');
		$this->level_model->inactive_level($id);
	}
	
}

?>