
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2">Name</th>
                                <th colspan="5">Menu Information</th>
                            </tr>
                            <tr>
                                <th>Url</th>
                                <th>Icon</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
					<?php foreach ($parent->result() as $row){?>	
                        <tr>
                            <td><strong>:: <?=$row->menu_name?></strong></td>
                            <td><strong><?=$row->menu_url?></strong></td>
                            <td><strong><?=$row->menu_icon?></strong></td>
                            <td><?php if($row->isactive==1){ 
									echo '<span class="uk-badge uk-badge-warning">active</span';
								}else{ 
									echo '<span class="uk-badge uk-badge-danger">inctive</span';
								}?>
							</td>
                            <td>
								<a href="#edit_menu" data-uk-modal="{ center:true }" onclick="edits('<?=$row->id_menu?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
								<a href="#" onclick="deleted('<?=$row->id_menu?>')"class="uk-icon-button uk-icon-trash-o"></a>
							</td>
                        </tr>
					<?php
						if($row->id_parent==0){
							$child = $this->menu_model->menu_child($row->id_menu);
							foreach ($child->result() as $row2){
					?>
						<tr>
                            <td><?=$row2->menu_name?></td>
                            <td><?=$row2->menu_url?></td>
                            <td><?=$row->menu_icon?></td>
                            <td><?php if($row2->isactive==1){ 
									echo '<span class="uk-badge uk-badge-warning">active</span';
								}else{ 
									echo '<span class="uk-badge uk-badge-danger">inctive</span';
								}?>
							</td>
                            <td>
								<a href="#edit_menu" data-uk-modal="{ center:true }" onclick="edits('<?=$row2->id_menu?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
								<a href="#" onclick="deleted('<?=$row2->id_menu?>')"class="uk-icon-button uk-icon-trash-o"></a>	
							</td>
                        </tr>
					<?php					
							}
						}
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_menu" onclick="add()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_menu">
			<div id="modal-add"></div>
		</div>
		<div class="uk-modal" id="edit_menu">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id){
				uri = '<?=base_url()."menu/edit"?>';
				$('#new_menu').hide();
				$('#edit_menu').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(id){
				uri = '<?=base_url()."menu/add"?>';
				$('#edit_menu').hide();
				$('#new_menu').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."menu/delete"?>';
					$.post(uri,{ajax:true,id:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
	</script>	
</html>