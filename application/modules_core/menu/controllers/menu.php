<?php

class menu extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
    }
    
    public function index()
	{ 	
		$this->login_model->is_login();
		$data['title'] 		='Master Menu';
		$data['link'] = base_url()."menu/list_data"; 
		$this->load->view('preview',$data);
	}
	
	public function list_data() 
	{
		$this->login_model->is_login();
		$data['parent']	= $this->menu_model->menu_parent();
		$data['title'] 		='Master Menu';
		$data['link'] 		= base_url()."menu/list_data"; 
		$this->load->view('menu', $data);        
    }
	
	public function add()
	{
		$this->login_model->is_login();
		$data['option']	= $this->menu_model->get_list_menu();
		$data['title'] 		= 'Add Menu';
		$data['link'] = base_url()."menu/list_data"; 
		$this->load->view('add', $data); 
	}
	
	public function edit()
	{
		$this->login_model->is_login();
		
		$id				= $this->input->post('id');
		$data['menu']	= $this->menu_model->get_menu_by_id($id);
		$data['option']	= $this->menu_model->get_list_menu();
		$data['title'] 	= 'Edit Menu';
		$data['link'] 	= base_url()."menu/list_data"; 
		$this->load->view('edit', $data); 
	}
	
	public function update()
	{	
		$this->login_model->is_login();
		$id				= $this->input->post('id');
		$isactive		= $this->input->post('isactive');
		$menu_name		= $this->input->post('menu_name');
		$menu_order		= $this->input->post('menu_order');
		$menu_parent	= $this->input->post('menu_parent');
		$menu_url 		= $this->input->post('menu_url');
		$menu_icon 		= $this->input->post('menu_icon');
		$id_user		= $this->session->userdata("sesi_id_user");
		//save menu
		$this->menu_model->update_menu($id,$menu_parent,$menu_name,$menu_url,$menu_icon,$menu_order,$isactive,$id_user);
		
		//echo json_encode(array('response' => 'success', 'msg' => 'Berhasil'));
	}
	public function save()
	{	
		$this->login_model->is_login();
		$isactive		= $this->input->post('isactive');
		$menu_name		= $this->input->post('menu_name');
		$menu_order		= $this->input->post('menu_order');
		$menu_parent	= $this->input->post('menu_parent');
		$menu_url 		= $this->input->post('menu_url');
		$menu_icon 		= $this->input->post('menu_icon');
		$id_user		= $this->session->userdata("sesi_id_user");;
		//save menu
		$this->menu_model->save_menu($menu_parent,$menu_name,$menu_url,$menu_icon,$menu_order,$isactive,$id_user);
		$data  = $this->menu_model->get_menu_id($menu_name,$menu_url,$menu_icon,$menu_order,$id_user);
		$level = $this->menu_model->get_list_level();
		foreach ($level->result() as $row){
			$this->menu_model->save_menu_role($row->id_level,$data['id_menu'],$id_user);
		}
		
		echo json_encode(array('response' => 'success', 'msg' => 'Menu berhasil disimpan'));
	}
	
	public function updatea()
	{	
		$this->login_model->is_login();
		$name			= $_POST['name'];
		$parent			= $_POST['parent'];
		$menuorder		= $_POST['menuorder'];
		$url			= $_POST['url'];
		$controller		= $_POST['controller'];
		$icon			= $_POST['icon'];
		$active			= $_POST['active'];
		$menu_id		= $_POST['menu_id'];
		$input_by		= $this->session->userdata("sesi_user_id");
		$level_id		= $this->session->userdata("sesi_level_id");
		
		//save menu
		$this->menu_model->updateMenu($menu_id,$name,$parent,$menuorder,$controller,$url,$icon,$active,$input_by);
		
		//get lates menu_id
	//	$data = $this->menu_model->getMenuId($name,$url,$icon,$menuorder,$input_by);
		
		//assign menu to list level, if uid ==1 default active
		$list_level	= $this->level_model->getListLevel();
		
		$list_function = $this->menu_model->getFunctionId($menu_id);
		$function_ = array();
		$i = 0;
		if($list_function->num_rows()>0){
			foreach($list_function->result() as $list_function2):
				$function_[$i] = $list_function2->function_id;
			endforeach;
			$i++;
		}
		
	
		for($i=0; $i<count($function_name); $i++){
			//menghapus jika data hanya satu row dan function name tidak di isi nanti rubah update ya
			if($function_name[$i]==''){
				//delete function ->nanti update aja kalo dah fix jadi 0
				$this->menu_model->deleteFunction($function_old[$i]);
				foreach($function_ as $function2):
						//delete function ->nanti update aja kalo dah fix jadi 0
						$this->menu_model->deleteFunctionRole($function_old[$i]);
						//$this->main_mod->delete('mst_role_function','function_id',$function2);
					endforeach;
				
			}
			
			if($function_old[$i] == ''){
					//save
					//$this->menu_model->saveFunction($menu_id,$function_name,$function_alias,$input_by);
					$this->menu_model->saveFunction($menu_id,$function_name[$i],$function_alias[$i],$input_by);
					$role_menu 	= $this->menu_model->getRoleId($menu_id);//ok
					//add role funtion
					foreach ($role_menu->result() as $role_menus){
						//delete role funciton
						$function = $this->menu_model->getFunctionId($menu_id);
						
						foreach($function->result() as $functions){
							$this->menu_model->saveFunctionRole($role_menus->role_id,$functions->function_id,$input_by,$role_menus->level_id);
						}
					}
				}else{
				//	null;
										//updateFunction($function_id,$menu_id,$function_name,$function_alias,$input_by)
					$this->menu_model->updateFunction($function_old[$i],$menu_id,$function_name[$i],$function_alias[$i],$input_by);
					//updateFunction($function_id,$menu_id,$function_name,$function_alias,$input_by)
				}
		}	
		//jika datanya ada yang di delete
		@$deff_function = array_diff($function_,$function_old);
			if(count($deff_function)>0){
				foreach($deff_function as $deff_function2):
					
				echo	$this->menu_model->deleteFunction($deff_function2);
			
				echo	$this->menu_model->deleteFunctionRole($deff_function2);
					
				endforeach;
			}
		
		echo json_encode(array('response' => 'success', 'msg' => 'Berhasil'));
	}
	
	public function delete()
	{	
		$this->login_model->is_login();
		$id	= $this->input->post('id');
		$this->menu_model->delete_menu_role($id);
		$this->menu_model->delete_menu($id);
	
	}
	
}

?>