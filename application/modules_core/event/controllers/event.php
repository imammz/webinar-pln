<?php

class event extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
    	$this->load->model('event_model');
    }
    
    public function index()
    {
        $this->login_model->checkPrivilege();
        $data['link'] = base_url() . "event/list_data";
        $this->load->view('view_event', $data);
    }
    
    public function calendar()
    {
        $list_event= $this->event_model->getListEvent();
		foreach ($list_event->result() as $event){
			$data[]=array(
				'title'=>$event->title,
				'start'=>$event->start_date,
				'end'=>$event->end_date,
				);
		} 
		echo json_encode($data);		
    }
    public function list_data()
    {
        $this->login_model->checkPrivilege();
        $data['list_event'] = $this->event_model->getListEvent();
        $data['title']     = 'List Event';
       $data['link'] = base_url() . "event/list_data";
        $this->load->view('list_event', $data);
    }
    
    public function add()
    {
        $this->login_model->checkPrivilege();
        $data['title']            = 'Add';
       $data['link'] = base_url() . "event/list_data";
        $this->load->view('add_event', $data);
    }
    
    public function edit()
    {
        $this->login_model->checkPrivilege();
        $id                       = $_POST['id'];
        $data_event                = $this->event_model->getListEventId($id);
        $data['title2']         = $data_event['title'];
        $data['start']         = $data_event['start_date'];
        $data['end']          = $data_event['end_date'];
		$data['event_id']          = $data_event['event_id'];
        $data['title']            = 'Edit';
        $data['link'] = base_url() . "event/list_data";
        $this->load->view('edit_event', $data);
    }
    
    public function save()
    {
        $this->login_model->checkPrivilege();
        $title        = $_POST['title'];
        $start   = $_POST['start'];
        $end= $_POST['end'];
        $input_by    = $this->session->userdata("sesi_user_id");
		$this->event_model->saveEvent($title, $start, $end, $input_by);
    }
    
    public function update()
    {
        $this->login_model->checkPrivilege();
        $event_id          = $_POST['event_id'];
        $title        = $_POST['title'];
        $start   = $_POST['start'];
        $end= $_POST['end'];
        $update_by   = $this->session->userdata("sesi_user_id");
        $this->event_model->updateEvent($event_id, $title, $start, $end, $update_by);
    }
    
    public function delete()
    {
        $this->login_model->checkPrivilege();
        $id = $_POST['id'];
        $this->event_model->deleteEvent($id);
    }
    
}

?>