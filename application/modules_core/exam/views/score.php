	<div class="uk-margin-medium-bottom">
		<h3 align="center">Hasil Test Ujian</h3>
	</div>
	<div class="uk-grid" data-uk-grid-margin>

		<div class="uk-width-small-2-5">
			<span class="uk-text-muted uk-text-small uk-text-italic">Hasil Pilihan Ganda:</span>
			<p class="heading_b uk-text-success">Jumlah Soal  &nbsp;&nbsp;: <?=$score_pg['total_soal']?></p>
			<p class="heading_b uk-text-success">Jumlah Benar : <?=$score_pg['total_benar']?></p>

		</div>
		<div class="uk-width-small-2-5">
			<span class="uk-text-muted uk-text-small uk-text-italic">Hasil Essay:</span>
			<p class="heading_b uk-text-success"></p>
			<p class="uk-text-small uk-text-muted uk-margin-top-remove">0</p>
		</div>
	</div>