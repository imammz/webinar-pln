
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2" width="200px">Nama Ujian</th>
                                <th colspan="4">Information</th>
                            </tr>
                            <tr>
                                <th>Nama Pembelajaran</th>
                                <th>Periode</th>
								<th>Durasi</th>
							    <th>Start Date</th>
                                <th>Status</th>
								<th>Mulai Ujian</th>
							</tr>
                        </thead>
                        <tbody>
					<?php foreach ($exam->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->nama_ujian?></td>
							<td><?=$row->nama_pembelajaran?></td>
							<td><?=$row->periode?></td>
							<td><?=$row->durasi?></td>
							<td><?=$row->start_date?></td>
							<td>
							<?php if($row->status=='schedule'){?>
								<span class="uk-badge uk-badge-primary">
							<?php }else if($row->status=='ongoing'){?>
								<span class="uk-badge uk-badge-success">
							<?php }else{?>
								<span class="uk-badge uk-badge-danger">
								
							<?php } ?>
								<?php echo $row->status?></span></td>
                            <td>
								<button  onclick="detail('<?=$row->id_jadwal_ujian?>')"  class="uk-icon-button uk-icon-book" title="Start"  <?php echo $row->status<>'ongoing'?'disabled':'';?>></a>
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			
			
			function detail(id_jadwal_ujian){
				var r = confirm("Perhatian !!! \n- Anda akan diarahkan ke soal ujian, Gunakan waktu sebaik baiknya \n- Jawablah pertanyaan dengan benar \n -Jangan menekan F5 atau refresh halaman pada saat ujian, jika hal ini dilakukan anda dianggap selesai melakukan test");
				if (r == true) {
					
					var url = '<?=base_url()."exam/detail/"?>'+id_jadwal_ujian;
					window.open(url, '_blank');
					//window.location(url);
					//window.location.href = url;	
				} 
				
			}
	</script>	
</html>