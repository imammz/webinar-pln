<?php echo Modules::run('front_templates/front_templates/header'); ?>
<div id="list_data"></div>
<?php echo Modules::run('front_templates/front_templates/footer'); ?>

<script>
	var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64" height="64"/></div>';
	$(document).ready(function() {
		reloadlist();
	});

	function reloadlist(){
		$('#list_data').html(loading);
		$.post("<?=$link?>",function(data){
			$('#list_data').html(data)
		});
	}	
</script>

</html>