<!--<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>-->
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script>
	
			$(document).ready(function() {
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#change_password').validate({
				rules:{	old:{
								required:true
							  },
						 pass1:{
								required:true
							  },
						 pass2: {
								  equalTo: "#pass1"
								}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$.post('<?php echo base_url('profile/update_password'); ?>', $('#change_password').serialize(), function(data) {
						var data = eval('('+ data + ')');
						//alert(data.response);
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#err_message").html(msg);
							
							$('#err_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#err_message').hide();
								$('#modal-edit-pass').hide();
								window.location.reload();
							});
				 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#err_message").html(msg);
							$('#err_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#err_message').hide();
							});																
						}
					});
				}
			})
		});
	
	</script>

<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div>
<form id="change_password" method="post" action="<?=base_url()?>profile/update_password">
	<div id="err_message"></div>
	<div class="uk-form-row">
		<label for="login_username">Password Lama</label>
		<input class="md-input" type="password" id="old" name="old" />
	</div>
	<div class="uk-form-row">
		<label for="login_username">Password Baru</label>
		<input class="md-input" type="password" id="pass1" name="pass1" />
	</div>
	<div class="uk-form-row">
		<label for="login_username">Ulangi</label>
		<input class="md-input" type="password" id="pass2" name="pass2"/>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		 <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Submit</button>
	</div>
</form>
</div>
