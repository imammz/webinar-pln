<!--<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>-->
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/jquery.form.js"></script>

<script type="application/javascript">
   jQuery(document).ready(function() {
	 /*  
	setTimeout(function() { 
		$("#form-edit-photo").show(); 
		$("#before-edit-photo").hide();
	}, 100);*/
    $("#form-edit-photo").validate({ 
        onkeyup: false,
        rules: {
            photo:{
					required:true
				  }	  
        },
		highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					}
    });

    $("#form-edit-photo").ajaxForm({ 
        beforeSubmit: function () {
			$("#btnsave").prop('value', 'Process...'); 
			$('input[type="submit"]').prop('disabled', true);
            return $("#form-edit-photo").valid(); // TRUE when form is valid, FALSE will cancel submit
        },
        success: function (data) {
			var data = eval('('+ data + ')');
			if(data.response == "success") {
				var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
				$("#edit_message_photo").html(msg);
				
				$('#edit_message_photo').fadeTo(3000, 500).slideUp(500, function(){
					$('#edit_message_photo').hide();
					$('#modal-edit-photo').hide();
					//reload_photo();
					window.location.reload();
				});
				 
			}else{
				var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
				$("#edit_message_photo").html(msg);
				$("#btnsave").prop('value', 'Save'); 
				$('input[type="submit"]').prop('disabled', false);
			}
        },
		error: function(){
			var msg='<div class="uk-alert uk-alert-danger">ERROR: unable to upload photos</div>';
			$("#edit_message_photo").html(msg);
			$("#btnsave").prop('value', 'Save'); 
			$('input[type="submit"]').prop('disabled', false);	
		}
    });

});
</script>


<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div>
<form id="form-edit-photo" method="post" action="<?=base_url()?>profile/update_photo" enctype="multipart/form-data">
	<div id="edit_message_photo"></div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">photo</label>
		<input type="file" class="md-input" id="photo" name="photo">
	</div>
	
	<div class="uk-modal-footer uk-text-right">
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
