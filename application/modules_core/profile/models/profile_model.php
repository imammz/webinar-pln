<?php

class profile_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	
	function get_profile($id){
		$sql = "SELECT a.* FROM wb_pegawai a, wb_users b
				WHERE a.nip=b.nip
				and b.id_user='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_my_class($id_user){
		$sql = "select b.*,c.nama_pembelajaran,
				case when b.start_date <= DATE_FORMAT(now(),'%Y-%m-%d') and b.end_date >= DATE_FORMAT(now(),'%Y-%m-%d')
						then 'inprogress'
					 when b.start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
					 when b.end_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
				end status
				from wb_peserta a, wb_class b,wb_pembelajaran c
				where a.id_class=b.id_class
				and a.id_user='$id_user'
				and b.id_pembelajaran=c.id_pembelajaran
				order by b.start_date desc";
		return $this->db->query($sql);		
	}
	
	
	function update_user($id,$username,$isactive,$passwords,$id_level,$id_user){
		$sql="update wb_pegawai set username='$username',passwords='$passwords',
			  id_level='$id_level',isactive='$isactive',updated_by='$id_user',updated_date=now()
			  WHERE id_user='$id'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal di simpan'));		
		}		  
	
	}
	
	function update_photo($id_user,$photo){
		$sql="update wb_pegawai set photo='$photo'
			  WHERE nip= (select nip from wb_users where id_user='$id_user')";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal di simpan'));		
		}
	}
	
	function update_password($id_user,$old,$new){
		$sql1 ="select * from wb_users where passwords='$old' and id_user='$id_user'";
		$hasil = $this->db->query($sql1);	
		if($hasil->num_rows() > 0){
			$sql ="update wb_users set passwords='$new',updated_by='$id_user',updated_date=now()
				   where id_user='$id_user'";
				$return = $this->db->query($sql);	
				if($return){
					echo json_encode(array('response' => 'success', 'msg' => 'Password Berhasil di rubah'));		
				}else
				{
					echo json_encode(array('response' => 'failed', 'msg' => 'Password gagal di rubah'));		
				}	
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Password lama salah'));	
		}
	}
	
}
