<div id="delete_message_pg"></div>
<table id="dt_default_pg" class="uk-table" cellspacing="0" width="100%">
	<thead>
		 <tr>
			<th rowspan="2" width="">Periode</th>
			<th colspan="2"><b>Master Pg</b></th>
		</tr>
		<tr>
			<th width="">Pertanyaan</th>
			<th width="100px">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($pg){
	foreach($pg->result() as $mat){?>	
			<tr>
				<td><?=$mat->periode?></td>
				<td><?=$mat->pertanyaan?></td>
				<td>
					
					<a href="#" onclick="delete_pg('<?=$mat->id_soal_class?>')"class="uk-icon-button uk-icon-trash-o"></a>
				</td>
			</tr>	
	<?php }
	}?>		
	</tbody>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_pg" onclick="add_pg('<?=$id_class?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_pg">
		
				<div id="modal-add-pg"></div>
		</div>
		<div class="uk-modal" id="edit_pg">
			<button class="uk-modal-close uk-close" type="button"></button>
			<div id="modal-edit-pg"></div>
		</div>
	</div> 
	
	<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>
	
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default_pg').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_pg(id_soal_class){
				uri = '<?=base_url()."class_m/edit_pg"?>';
				$('#modal-edit-pg').html(loading);
				$.post(uri,{ajax:true,id_soal_class:id_soal_class,id_class:'<?=$id_class?>'},function(data) {
					$('#modal-edit-pg').html(data);
				});
			}
			
			function add_pg(id_class){
				$('#modal-add-pg').html(loading);
				uri = '<?=base_url()."class_m/add_pg"?>';
				$.post(uri,{ajax:true,id_class:id_class},function(data) {
					$('#modal-add-pg').html(data);
				});
			}
			
			function delete_pg(id_soal_class){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."class_m/delete_pg"?>';
					$.post(uri,{ajax:true,id_soal_class:id_soal_class},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message_pg").html(msg);
							
							$('#delete_message_pg').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message_pg').hide();
								
								reload_pg();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message_pg").html(msg);
						}
					});
				} 
			}
			
	</script>