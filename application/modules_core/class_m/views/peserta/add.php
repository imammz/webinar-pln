<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/jquery.form.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>

<script type="application/javascript">
	var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
   jQuery(document).ready(function() {
	setTimeout(function() { 
		$("#form-edit-peserta").show(); 
		$("#before-edit-peserta").hide();
	}, 100);
	
	$('#dt_default_peserta_list').dataTable({
		oLanguage: {
			sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
		}					
	});

	$("#id_periode_peserta").change(function(){
		var id_periode = $("#id_periode_peserta").val();
		$('#data_peserta_peserta').html(loading)
		$.post("<?php echo base_url().'class_m/get_list_peserta'?>",{id_class:'<?=$id_class?>',id_periode:id_periode,jenis_peserta:'peserta'},function(data){
			$('#data_peserta_peserta').html(data);

		});
	});
	});
	
	function pilih(id_user,id_class){
		$.post("<?php echo base_url().'class_m/save_peserta'?>",{id_class:id_class,id_user:id_user},function(data){
			//$('#data_peserta_peserta').html(data);
			//alert(data);
			$("#peserta_"+id_user).prop('disabled', true);
			if(data=='success'){
				$("#peserta_"+id_user).removeClass("uk-badge uk-badge-primary"); //versions newer than 1.6
				$("#peserta_"+id_user).addClass("uk-badge uk-badge-default");
				$("#peserta_"+id_user).prop('disabled', true); //versions newer than 1.6
			}else{
				$("#peserta_"+id_user).prop('disabled', false);
				$("#peserta_"+id_user).addClass("uk-badge uk-badge-primary");
			}
		});
	}
	
	function save_pilih(){
		reload_peserta();
	}
	
</script>

<div class="uk-modal-dialog">
	<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div id="before-edit-peserta"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form id="form-edit-peserta" style="display:none">
	<div id="edit_message_peserta"></div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<table id="dt_default_peserta_list" class="uk-table" cellspacing="0" width="100%">
				<thead>
					 <tr>
						<th rowspan="2" width="">Pilih</th>
						<th colspan="2"><b>Master peserta</b></th>
					</tr>
					<tr>
						<th width="">Nip</th>
						<th width="">Nama</th>
						<th width="">Alamat </th>
					</tr>
				</thead>
				<tbody id="data_peserta_peserta">
					<?php 
				if($peserta){
				foreach($peserta->result() as $mat){?>	
						<tr>
							<td><button type="button" id="peserta_<?=$mat->id_user?>" onclick="pilih('<?=$mat->id_user?>','<?=$id_class?>')" class="uk-badge uk-badge-primary"><i class="material-icons">&#xE145;</i></button></td>
							<td><?=$mat->nip?></td>
							<td><?=$mat->nama?></td>
							<td><?=$mat->alamat?></td>
						</tr>	
				<?php }}
				?>	
				</tbody>
			</table>
		</div>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_class" value="<?=$id_class?>"/>
		<button type="button" onclick="save_pilih()" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<!--<button type="button" onclick="save_pilih()" class="md-btn md-btn-success">Save</button>-->
	</div>
</form>
</div>
