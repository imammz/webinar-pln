
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2">Name</th>
                                <th colspan="4">User Information</th>
                            </tr>
                            <tr>
                                <th>Level</th>
                                <th>Isactive</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
					<?php foreach ($user->result() as $row){?>	
                        <tr>
                            <td><?=$row->nama?></td>
                            <td><?=$row->level_name?></td>
							<td><?php if($row->isactive==1){ 
									echo '<span class="uk-badge uk-badge-warning">active</span';
								}else{ 
									echo '<span class="uk-badge uk-badge-danger">inctive</span';
								}?>
							</td>
                            <td>
								<a href="#edit_user" data-uk-modal="{ center:true }" onclick="edits('<?=$row->id_user?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
								<a href="#" onclick="deleted('<?=$row->id_user?>')"class="uk-icon-button uk-icon-trash-o"></a>
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_user" onclick="add()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_user">
			<div id="modal-add"></div>
		</div>
		<div class="uk-modal" id="edit_user">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id){
				uri = '<?=base_url()."user/edit"?>';
				$('#new_user').hide();
				$('#edit_user').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(id){
				uri = '<?=base_url()."user/add"?>';
				$('#edit_user').hide();
				$('#new_user').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id){
				var r = confirm("Yakin akan di nonaktifkan?");
				if (r == true) {
					uri = '<?=base_url()."user/delete"?>';
					$.post(uri,{ajax:true,id:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
	</script>	
</html>