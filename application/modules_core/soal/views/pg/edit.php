<!--<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>-->
<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<script src="<?=base_url()?>front_assets/assets/js/tinymce.min.js"></script>

  <script>
  /*
	tinymce.init({
	  selector: "#pertanyaan",
	  height: 100,
	  plugins: [
		"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
		"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
	  ],

	  toolbar1: " bold italic underline strikethrough | alignleft aligncenter alignright alignjustify|cut copy paste  | bullist numlist",
	  toolbar2: "outdent indent blockquote | undo redo | forecolor backcolor",
	  toolbar3: "table | hr removeformat | subscript superscript | charmap",

	  menubar: false,
	  toolbar_items_size: 'small'
	});*/
  </script>

<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div>
<div id="before-edit"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="edit_myform" style="display:none">
	<div id="edit_message-pg"></div>
	<div class="uk-grid">
		<div class="uk-width-medium-1-1 parsley-row">
			<label for="class_name">Nama Pembelajaran<span class="req">*</span></label>
			<select id="id_pembelajaran" name="id_pembelajaran" required>
				<option value="">Pilih ...</option>
				<?php 
				foreach ($pembelajaran->result() as $row){?>
					<option value="<?=$row->id_pembelajaran?>" <?=$bs['id_pembelajaran']==$row->id_pembelajaran? 'selected':''?>><?=$row->nama_pembelajaran?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Peratanyaan</label>
		<textarea class="md-input" id="pertanyaan" name="pertanyaan"><?=$bs['pertanyaan']?></textarea> 
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan A</label>
		<input  type="text" class="md-input" id="pilihan_a" name="pilihan_a" value="<?=$bs['a']?>">
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan B</label>
		<input type="text" class="md-input" id="pilihan_b" name="pilihan_b" value="<?=$bs['b']?>">  
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan C</label>
		<input type="text" class="md-input" id="pilihan_c" name="pilihan_c" value="<?=$bs['c']?>">  
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan D</label>
		<input type="text" class="md-input" id="pilihan_d" name="pilihan_d" value="<?=$bs['d']?>">  
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pilihan E</label>
		<input type="text" class="md-input" id="pilihan_e" name="pilihan_e" value="<?=$bs['e']?>">  
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Jawaban</label>
	</div>	
	<div class="uk-margin-medium-bottom">
		A<input type="radio" class="md-input" id="jawaban" name="jawaban" value="a" <?php echo $bs['jawaban_pg']=='a'?'checked':''?>/>&nbsp;
		B<input type="radio" class="md-input" id="jawaban" name="jawaban" value="b" <?php echo $bs['jawaban_pg']=='b'?'checked':''?>/>&nbsp;
		C<input type="radio" class="md-input" id="jawaban" name="jawaban" value="c" <?php echo $bs['jawaban_pg']=='c'?'checked':''?>/>&nbsp;
		D<input type="radio" class="md-input" id="jawaban" name="jawaban" value="d" <?php echo $bs['jawaban_pg']=='d'?'checked':''?>/>&nbsp;
		E<input type="radio" class="md-input" id="jawaban" name="jawaban" value="e" <?php echo $bs['jawaban_pg']=='e'?'checked':''?>/>&nbsp;
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_bank_soal" value="<?=$bs['id_bank_soal']?>"/>
		<input type="hidden" name="id_pembelajaran" value="<?=$bs['id_pembelajaran']?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnupdate" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#edit_myform").show(); 
				$("#before-edit").hide();
			}, 100);
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#edit_myform').validate({
				rules:{
						id_pembelajaran:{
							required:true
						},
						pertanyaan:{
							required:true
						},
						pilihan_a:{
							required:true
						},
						pilihan_b:{
							required:true
						},
						pilihan_c:{
							required:true
						},
						pilihan_d:{
							required:true
						},
						pilihan_e:{
							required:true
						},
						jawaban:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$("#btnupdate").prop('value', 'Process'); 
					$('input[type="submit"]').prop('disabled', true);
					$.post('<?php echo base_url('soal/update_pg'); ?>', $('#edit_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#edit_message-pg").html(msg);
							
							$('#edit_message-pg').fadeTo(3000, 500).slideUp(500, function(){
								$('#edit_message-pg').hide();
								reload_pg();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#edit_message-pg").html(msg);
							$("#btnupdate").prop('value', 'Save'); 
							$('input[type="submit"]').prop('disabled', false);
						}
					});
				}
			})
		});
</script>
