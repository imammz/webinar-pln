<?php
class class_i extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->load->model(array(
            'class_i/class_model',
            'pegawai/pegawai_model',
            'periode/periode_model',
			'soal/soal_model',
            'document/document_model',
			'participant/participant_model'
        ));
        $this->load->helper('download');
        ini_set('upload_max_filesize', '800M');
        ini_set('post_max_size', '800M');
        ini_set('max_input_time', 204800000000000000000000);
        ini_set('max_execution_time', 30000000000);
        set_time_limit(0);
    }
    
    public function index()
    {
        $this->login_model->is_login();
        $data['title'] = 'Master Pembelajaran Digital';
        $data['link']  = base_url() . "class_i/list_data";
        $this->load->view('preview', $data);
    }
    
    public function list_data()
    {
        $this->login_model->is_login();
		$id_user          = $this->session->userdata("sesi_id_user");
        $data['class'] = $this->class_model->get_list_class_user($id_user);
        $data['title'] = 'Master Pembelajaran Digital';
        $data['link']  = base_url() . "class_i/list_data";
        $this->load->view('class', $data);
    }
    
    public function add()
    {
        $this->login_model->is_login();
        $data['instruktur'] = $this->pegawai_model->get_list_user_instruktur();
		$data['pembelajaran'] = $this->class_model->get_pembelajaran();
        $data['title']   = 'Add Kelas';
        $data['link']    = base_url() . "class_i/list_data";
        $this->load->view('add', $data);
    }
    
    public function edit()
    {
        $this->login_model->is_login();  
        $id              = $this->input->post('id');
        $data['class']   = $this->class_model->get_class_by_id($id);
        $data['pegawai'] = $this->pegawai_model->get_list_user_instruktur();
		$data['pembelajaran'] = $this->class_model->get_pembelajaran();
        $data['title']   = 'Edit Class';
        $data['link']    = base_url() . "class_i/list_data";
        $this->load->view('edit', $data);
    }
    
    public function detail()
    {
         $this->login_model->is_login();
        $id               = $this->input->post('id');
        $id_user          = $this->session->userdata("sesi_id_user");
        $data['id_class'] = $id;
        $data['class']    = $this->class_model->load_class_by_id($id);
        $data['pegawai']  = $this->pegawai_model->get_list_user_pegawai();
        $data['friend']   = $this->pegawai_model->get_list_user_pegawai_class($id, $id_user);
      
        $data['title']    = 'Detail Class ' . $data['class']['nama_pembelajaran'];
        $data['link']     = base_url() . "class_p/detail";
        
       
        $this->load->view('detail', $data);
    }
    
    public function join()
    {
         $this->login_model->is_login();
        $id               = $this->uri->segment(3);
        $id_periode               = $this->uri->segment(4);
        $data['id_class'] = $id;
        $data['class']    = $this->class_model->get_class_by_id($id);
        $data['periode'] = $this->class_model->get_class_periode_by_id($id,$id_periode);
        $data['pegawai']  = $this->pegawai_model->get_list_user_pegawai();
        $data['title']    = 'Detail Kelas ' . $data['periode']['nama_pembelajaran'];
        $data['link']     = base_url() . "class_p/detail";
        
       //print_r($data['periode']); exit;
        
        $this->load->view('join', $data);
    }
    
    public function update()
    {
        $this->login_model->is_login();
        $id          = $this->input->post('id');
        $id_class_name  = $this->input->post('id_class_name');
        $keterangan  = $this->input->post('keterangan');
        $start_date  = $this->input->post('start_date');
        $end_date    = $this->input->post('end_date');
        $lokasi      = $this->input->post('lokasi');
        $id_user     = $this->session->userdata("sesi_id_user");
        $this->class_model->update_class($id, $id_class_name, $keterangan, $start_date, $end_date, $lokasi, $id_user);
    }
    
    public function save()
    {
        $this->login_model->is_login();
        $id_class    = 'CLS' . date('ymdHis');
        $id_class_name  = $this->input->post('id_class_name');
        $keterangan  = $this->input->post('keterangan');
        $instruktur1 = $this->input->post('instruktur1');
        $instruktur2 = $this->input->post('instruktur2');
        $start_date  = $this->input->post('start_date');
        $end_date    = $this->input->post('end_date');
        $lokasi      = $this->input->post('lokasi');
        $id_user     = $this->session->userdata("sesi_id_user");
        
        $this->class_model->save_class($id_class, $id_class_name, $keterangan, $instruktur1, $instruktur2, $start_date, $end_date, $lokasi, $id_user);
    }
    
	public function save_class()
    {
        $this->login_model->is_login();
        $id_class    = 'CLS' . date('ymdHis');
        $id_class_name  = $this->input->post('id_class_name');
        $keterangan  = $this->input->post('keterangan');
        $instruktur1 = $this->input->post('instruktur1');
        $instruktur2 = $this->input->post('instruktur2');
        $start_date  = $this->input->post('start_date');
        $end_date    = $this->input->post('end_date');
        $lokasi      = $this->input->post('lokasi');
		
		$periode    		= $this->input->post('periode_name');
		$periode_start_date = $this->input->post('periode_start_date');
		$periode_end_date   = $this->input->post('periode_end_date');
			
        $id_user     = $this->session->userdata("sesi_id_user");
		
	
        $class = $this->class_model->save_class_new($id_class, $id_class_name, $keterangan,$start_date, $end_date, $lokasi, $id_user);
		
		if($class){
			$x =1;
			foreach ($periode as $key => $value){
				$id_periode = 'PRD' . date('ymdHi').$x;
				if($periode[$key]!==''){
					$this->periode_model->save_periode_new($id_periode, $id_class, $instruktur1[$key],$instruktur2[$key],$periode[$key], $periode_start_date[$key], $periode_end_date[$key], $id_user);
					$x++;
				}
			}
			echo json_encode(array('response' => 'success', 'msg' => 'Kelas berhasil disimpan'));
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Kelas gagal disimpan'));
		}	
	}
	
    //inactive aja jangan di delete
    public function delete()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id');
        $this->class_model->delete_class($id);
    }
    
    public function list_periode()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
        $data['periode']  = $this->periode_model->get_periode_class($id_class);
        $data['title']    = 'Master Periode';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_periode";
        $this->load->view('periode/periode', $data);
    }
    
    
    public function add_periode()
    {
        $this->login_model->is_login();
        $data['id_class'] = $this->input->post('id_class');
		$data['pegawai'] = $this->pegawai_model->get_list_user_instruktur();
        $data['title']    = 'Add Periode';
        $data['link']     = base_url() . "class_i/list_periode";
        $this->load->view('periode/add', $data);
    }
    
    public function edit_periode()
    {
        $this->login_model->is_login();
        $id_periode      = $this->input->post('id');
		$data['pegawai'] = $this->pegawai_model->get_list_user_instruktur();
        $data['periode'] = $this->periode_model->get_periode_by_id($id_periode);
        $data['title']   = 'Edit Periode';
        $data['link']    = base_url() . "class_i/list_periode";
        $this->load->view('periode/edit', $data);
    }
	
    public function save_periode()
    {
        $this->login_model->is_login();
        $id_periode = 'PRD' . date('ymdHis');
        $id_class   = $this->input->post('id_class');
        $periode    = $this->input->post('periode');
        $start_date = $this->input->post('start_date');
        $end_date   = $this->input->post('end_date');
		$instruktur1   = $this->input->post('instruktur1');
		$instruktur2   = $this->input->post('instruktur2');
        $id_user    = $this->session->userdata("sesi_id_user");
        $this->periode_model->save_periode($id_periode, $id_class, $periode, $start_date, $end_date,$instruktur1,$instruktur2, $id_user);
    }
    
    public function update_periode()
    {
        $this->login_model->is_login();
        $id_periode = $this->input->post('id_periode');
        $periode    = $this->input->post('periode');
        $start_date = $this->input->post('start_date');
        $end_date   = $this->input->post('end_date');
		$instruktur1   = $this->input->post('instruktur1');
		$instruktur2   = $this->input->post('instruktur2');
        $id_user    = $this->session->userdata("sesi_id_user");
        $this->periode_model->update_periode($id_periode, $periode, $start_date, $end_date,$instruktur1,$instruktur2, $id_user);
    }
    
    public function delete_periode()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_periode');
        $this->periode_model->delete_periode($id);
    }
    
    
    public function list_materi()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
        $data['materi']   = $this->document_model->get_document_class($id_class, 'materi');
        $data['title']    = 'Master Materi';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_materi";
        $this->load->view('materi/materi', $data);
    }
    
    public function add_materi()
    {
        $this->login_model->is_login();
        $data['id_class']        = $this->input->post('id_class');
		$data['document']= $this->document_model->get_all_list_document('materi');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']    = 'Add Materi';
        $data['link']     = base_url() . "class_i/list_materi";
        $this->load->view('materi/add', $data);
    }
    
    public function edit_materi()
    {
        $this->login_model->is_login();
        $data['id_document_class']       = $this->input->post('id_document_class');
        $data['id_class']        = $this->input->post('id_class');
        $data['materi']  = $this->document_model->get_class_periode_document($data['id_document_class']);
		$data['id_document']       =$data['materi']['id_document'];
		$data['document']= $this->document_model->get_all_list_document('materi');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']   = 'Edit materi';
        $data['link']    = base_url() . "class_i/list_materi";
        $this->load->view('materi/edit', $data);
    }
    
    public function save_materi()
    {
        $this->login_model->is_login();
		$id_class  = $this->input->post('id_class');
        $id_document = $this->input->post('id_document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->save_document_class_new($id_class,$id_document,$id_periode,$id_user);
    }
    
    public function update_materi()
    {
        $this->login_model->is_login();
		$id_document_class = $this->input->post('id_document_class');
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->update_document_class($id_document_class,$id_document,$id_periode,$id_user);
    }
    
    public function delete_materi()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_document_class');
        $this->document_model->delete_document_class($id);
    }
    
    public function list_video()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
        $data['video']    = $this->document_model->get_document_class($id_class, 'video');
        $data['title']    = 'Master video';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_video";
        $this->load->view('video/video', $data);
    }
    
    public function add_video()
    {
        $this->login_model->is_login();
        
        $data['id_class']        = $this->input->post('id_class');
		$data['document']= $this->document_model->get_all_list_document('video');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']    = 'Add Video';
        $data['link']     = base_url() . "class_i/list_video";
        $this->load->view('video/add', $data);
    }
    
    public function edit_video()
    {
        $this->login_model->is_login();
        $data['id_document_class']       = $this->input->post('id_document_class');
        $data['id_class']        = $this->input->post('id_class');
        $data['materi']  = $this->document_model->get_class_periode_document($data['id_document_class']);
		$data['id_document']       =$data['materi']['id_document'];
		$data['document']= $this->document_model->get_all_list_document('video');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']   = 'Edit Video';
        $data['link']    = base_url() . "class_i/list_materi";
        $this->load->view('video/edit', $data);
    }
    
    public function save_video()
    {
        $this->login_model->is_login();
		$id_class  = $this->input->post('id_class');
        $id_document = $this->input->post('id_document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->save_document_class($id_class,$id_document,$id_periode,$id_user);
    }
    
    public function update_video()
    {
        $this->login_model->is_login();
		$id_document_class = $this->input->post('id_document_class');
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->update_document_class($id_document_class,$id_document,$id_periode,$id_user);
    }
    
    public function delete_video()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_document_class');
        $this->document_model->delete_document_class($id);
    }
	
	
    public function list_file()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
        $data['file']    = $this->document_model->get_document_class($id_class, 'file');
        $data['title']    = 'Master file';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_file";
        $this->load->view('file/file', $data);
    }
    
    public function add_file()
    {
        $this->login_model->is_login();
        
        $data['id_class']        = $this->input->post('id_class');
		$data['document']= $this->document_model->get_all_list_document('file');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']    = 'Add file';
        $data['link']     = base_url() . "class_i/list_file";
        $this->load->view('file/add', $data);
    }
    
    public function edit_file()
    {
        $this->login_model->is_login();
        $data['id_document_class']       = $this->input->post('id_document_class');
        $data['id_class']        = $this->input->post('id_class');
        $data['materi']  = $this->document_model->get_class_periode_document($data['id_document_class']);
		$data['id_document']       =$data['materi']['id_document'];
		$data['document']= $this->document_model->get_all_list_document('file');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']   = 'Edit file';
        $data['link']    = base_url() . "class_i/list_materi";
        $this->load->view('file/edit', $data);
    }
    
    public function save_file()
    {
        $this->login_model->is_login();
		$id_class  = $this->input->post('id_class');
        $id_document = $this->input->post('id_document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->save_document_class($id_class,$id_document,$id_periode,$id_user);
    }
    
    public function update_file()
    {
        $this->login_model->is_login();
		$id_document_class = $this->input->post('id_document_class');
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->update_document_class($id_document_class,$id_document,$id_periode,$id_user);
    }
    
    public function delete_file()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_document_class');
        $this->document_model->delete_document_class($id);
    }
    
    public function download()
    {
		ini_set('memory_limit', '-1');
        $id_document = $this->uri->segment(3);
        $doc         = $this->document_model->download_document($id_document);
        $data        = file_get_contents($doc['path']);
        ini_set('memory_limit', '300M'); 
		set_time_limit(0);
		force_download($doc['file_name'], $data);
    }
	
	//pg
	public function list_pg()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
        $data['pg']       = $this->soal_model->get_soal_class($id_class,'pg');
        $data['title']    = 'Master PG';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_pg";
        $this->load->view('pg/pg', $data);
    }
    
    public function add_pg()
    {
        $this->login_model->is_login();
        
        $data['id_class']        = $this->input->post('id_class');
		//$data['soal']= $this->soal_model->get_all_list_document('pg');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']    = 'Add pg';
        $data['link']     = base_url() . "class_i/list_pg";
        $this->load->view('pg/add', $data);
    }
	
	function get_list_soal(){
		$data['id_class']     = $this->input->post('id_class');
		$data['id_periode']      = $this->input->post('id_periode');
		$jenis_soal   = $this->input->post('jenis_soal');
		if($data['id_periode']){
			$data['soal'] = $this->soal_model->get_all_soal($data['id_class'],$data['id_periode'],$jenis_soal);
		}else{
			return false;
		}
		if($jenis_soal=='pg'){
			$this->load->view('pg/soal_pg', $data);
		}else{
			$this->load->view('essay/soal_essay', $data);
		}
	}
	
	function get_list_materi(){
		$data['id_class']     = $this->input->post('id_class');
		$data['id_periode']      = $this->input->post('id_periode');
		$data['jenis_materi']   = $this->input->post('jenis_materi');
		if($data['id_periode']){
			$data['doc'] = $this->document_model->get_class_file($data['jenis_materi'],$data['id_class'],$data['id_periode']);
		}else{
			return false;
		}
		if($data['jenis_materi']=='materi'){
			$this->load->view('materi/list_materi', $data);
		}else{
			$this->load->view('essay/soal_essay', $data);
		}
	}
    
    public function edit_pg()
    {
        $this->login_model->is_login();
        $data['id_document_class']       = $this->input->post('id_document_class');
        $data['id_class']        = $this->input->post('id_class');
        $data['materi']  = $this->document_model->get_class_periode_document($data['id_document_class']);
		$data['id_document']       =$data['materi']['id_document'];
		$data['document']= $this->document_model->get_all_list_document('pg');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']   = 'Edit pg';
        $data['link']    = base_url() . "class_i/list_materi";
        $this->load->view('pg/edit', $data);
    }
    
    public function save_pg()
    {
        $this->login_model->is_login();
		$id_class  = $this->input->post('id_class');
        $id_bank_soal = $this->input->post('id_bank_soal');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->soal_model->save_soal_class($id_class,$id_bank_soal,$id_periode,$id_user);
    }
    
    public function update_pg()
    {
        $this->login_model->is_login();
		$id_document_class = $this->input->post('id_document_class');
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->update_document_class($id_document_class,$id_document,$id_periode,$id_user);
    }
    
    public function delete_pg()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_soal_class');
        $this->soal_model->delete_soal_class($id);
    }
	
	//essay
	public function list_essay()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
        $data['essay']       = $this->soal_model->get_soal_class($id_class,'essay');
        $data['title']    = 'Master essay';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_essay";
        $this->load->view('essay/essay', $data);
    }
    
    public function add_essay()
    {
        $this->login_model->is_login();
        
        $data['id_class']        = $this->input->post('id_class');
		//$data['soal']= $this->soal_model->get_all_list_document('essay');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']    = 'Add essay';
        $data['link']     = base_url() . "class_i/list_essay";
        $this->load->view('essay/add', $data);
    }
    
    public function edit_essay()
    {
        $this->login_model->is_login();
        $data['id_document_class']       = $this->input->post('id_document_class');
        $data['id_class']        = $this->input->post('id_class');
        $data['materi']  = $this->document_model->get_class_periode_document($data['id_document_class']);
		$data['id_document']       =$data['materi']['id_document'];
		$data['document']= $this->document_model->get_all_list_document('essay');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']   = 'Edit essay';
        $data['link']    = base_url() . "class_i/list_materi";
        $this->load->view('essay/edit', $data);
    }
    
    public function save_essay()
    {
        $this->login_model->is_login();
		$id_class  = $this->input->post('id_class');
        $id_bank_soal = $this->input->post('id_bank_soal');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->soal_model->save_soal_class($id_class,$id_bank_soal,$id_periode,$id_user);
    }
    
    public function update_essay()
    {
        $this->login_model->is_login();
		$id_document_class = $this->input->post('id_document_class');
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->update_document_class($id_document_class,$id_document,$id_periode,$id_user);
    }
    
    public function delete_essay()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_soal_class');
        $this->soal_model->delete_soal_class($id);
    }
	
	//peserta
	public function list_peserta()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
		$id_user    = $this->session->userdata("sesi_id_user");
        $data['peserta']       = $this->participant_model->get_class_peserta($id_class);
        $data['title']    = 'Master peserta';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_peserta";
        $this->load->view('peserta/peserta', $data);
    }
    
    public function add_peserta()
    {
        $this->login_model->is_login();
        
        $data['id_class']        = $this->input->post('id_class');
		$data['peserta']= $this->participant_model->get_class_peserta_not($data['id_class']);
        //$data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']    = 'Add peserta';
        $data['link']     = base_url() . "class_i/list_peserta";
        $this->load->view('peserta/add', $data);
    }
    
    public function edit_peserta()
    {
        $this->login_model->is_login();
        $data['id_document_class']       = $this->input->post('id_document_class');
        $data['id_class']        = $this->input->post('id_class');
        $data['materi']  = $this->document_model->get_class_periode_document($data['id_document_class']);
		$data['id_document']       =$data['materi']['id_document'];
		$data['document']= $this->document_model->get_all_list_document('peserta');
        $data['periode'] = $this->periode_model->get_periode_class($data['id_class']);
        $data['title']   = 'Edit peserta';
        $data['link']    = base_url() . "class_i/list_materi";
        $this->load->view('peserta/edit', $data);
    }
    
    public function save_peserta()
    {
        $this->login_model->is_login();
		$id_class  = $this->input->post('id_class');
        $id_user = $this->input->post('id_user');
        $id_periode  = $this->input->post('id_periode');
		$sesi_id_user    = $this->session->userdata("sesi_id_user");
        $this->participant_model->save_class_peserta($id_class,$sesi_id_user,$id_user);
    }
    
    public function update_peserta()
    {
        $this->login_model->is_login();
		$id_document_class = $this->input->post('id_document_class');
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_periode  = $this->input->post('id_periode');
		$id_user    = $this->session->userdata("sesi_id_user");
        $this->document_model->update_document_class($id_document_class,$id_document,$id_periode,$id_user);
    }
    
    public function delete_peserta()
    {
        $this->login_model->is_login();
        $id_peserta = $this->input->post('id_peserta');
        $this->participant_model->delete_class_peserta($id_peserta);
    }
    /*
    public function list_file()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
        $data['file']     = $this->document_model->get_document_class($id_class, 'file');
        $data['title']    = 'Master File';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_file";
        $this->load->view('file/file', $data);
    }
    
    public function add_file()
    {
        $this->login_model->is_login();
        $id_class         = $this->input->post('id_class');
        $data['periode']  = $this->periode_model->get_periode_class($id_class);
        $data['title']    = 'Add File';
        $data['id_class'] = $id_class;
        $data['link']     = base_url() . "class_i/list_file";
        $this->load->view('file/add', $data);
    }
    
    public function edit_file()
    {
        $this->login_model->is_login();
        $id_file         = $this->input->post('id_document');
        $id_class        = $this->input->post('id_class');
        $data['file']    = $this->document_model->get_class_periode_document($id_file);
        $data['periode'] = $this->periode_model->get_periode_class($id_class);
        $data['title']   = 'Edit File';
        $data['link']    = base_url() . "class_i/list_file";
        $this->load->view('file/edit', $data);
    }
    
    public function save_file()
    {
        $this->login_model->is_login();
        $id_document = 'FL' . date('ymdHis');
        $id_class    = $this->input->post('id_class');
        $document    = $this->input->post('document');
        $id_periode  = $this->input->post('id_periode');
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'png|jpg|doc|xls|pdf';
        $config['max_size']      = '10000000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true
                );
                $this->document_model->save_document($id_document, $document, $name, $id_class, $id_periode, 'file', $path . $upload_data['file_name'], $id_user);
            }
        } else {
            $this->document_model->save_document($id_document, $document, '', $id_class, $id_periode, 'file', $path, $id_user);
        }
    }
    
    
    
    public function update_file()
    {
        $this->login_model->is_login();
        $id_document = $this->input->post('id_document');
        $document    = $this->input->post('document');
        $id_periode  = $this->input->post('id_periode');
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'png|jpg|doc|xls|pdf';
        $config['max_size']      = '10000000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true
                );
                $this->document_model->update_document($id_document, $document, $name, $id_periode, 'file', $path . $upload_data['file_name'], $id_user);
            }
        } else {
            $this->document_model->update_document($id_document, $document, '', $id_periode, 'file', $path, $id_user);
        }
    }
    
    public function delete_file()
    {
        $this->login_model->is_login();
        $id = $this->input->post('id_file');
        $this->document_model->delete_document($id);
    }*/
}

?>