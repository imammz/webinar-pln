<div id="delete_message_peserta"></div>
<table id="dt_default_peserta" class="uk-table" cellspacing="0" width="100%">
	<thead>
		 <tr>
			<th rowspan="2" width="">Nip</th>
			<th colspan="3"><b>Master Peserta</b></th>
		</tr>
		<tr>
			<th width="">Nama</th>
			<th width="">Alamat</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($peserta){
	foreach($peserta->result() as $mat){?>	
			<tr>
				<td><?=$mat->nip?></td>
				<td><?=$mat->nama?></td>
				<td><?=$mat->alamat?></td>
			</tr>	
	<?php }
	}?>		
	</tbody>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_peserta" onclick="add_peserta('<?=$id_class?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal xl" id="new_peserta">
		
				<div id="modal-add-peserta"></div>
		</div>
		<div class="uk-modal" id="edit_peserta">
			<button class="uk-modal-close uk-close" type="button"></button>
			<div id="modal-edit-peserta"></div>
		</div>
	</div> 
	
	<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>
	
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default_peserta').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_peserta(id_peserta){
				uri = '<?=base_url()."class_m/edit_peserta"?>';
				$('#modal-edit-peserta').html(loading);
				$.post(uri,{ajax:true,id_peserta:id_peserta,id_class:'<?=$id_class?>'},function(data) {
					$('#modal-edit-peserta').html(data);
				});
			}
			
			function add_peserta(id_class){
				$('#modal-add-peserta').html(loading);
				uri = '<?=base_url()."class_m/add_peserta"?>';
				$.post(uri,{ajax:true,id_class:id_class},function(data) {
					$('#modal-add-peserta').html(data);
				});
			}
			
			function delete_peserta(id_peserta){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."class_m/delete_peserta"?>';
					$.post(uri,{ajax:true,id_peserta:id_peserta},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message_peserta").html(msg);
							
							$('#delete_message_peserta').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message_peserta').hide();
								
								reload_peserta();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message_peserta").html(msg);
						}
					});
				} 
			}
			
	</script>