<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/jquery.form.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>

<script type="application/javascript">
	var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
   jQuery(document).ready(function() {
	setTimeout(function() { 
		$("#form-edit-materi").show(); 
		$("#before-edit-materi").hide();
	}, 100);
	
	$('#dt_default_materi_list').dataTable({
		oLanguage: {
			sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
		}					
	});

	$("#id_periode_materi").change(function(){
		var id_periode = $("#id_periode_materi").val();
		$('#data_materi_materi').html(loading)
		$.post("<?php echo base_url().'class_m/get_list_materi'?>",{id_class:'<?=$id_class?>',id_periode:id_periode,jenis_materi:'materi'},function(data){
			$('#data_materi_materi').html(data);

		});
	});
	});
	
	function pilih(id_document,id_periode,id_class){
		$.post("<?php echo base_url().'class_m/save_materi'?>",{id_class:id_class,id_periode:id_periode,id_document:id_document},function(data){
			//$('#data_materi_materi').html(data);
			//alert(data);
			$("#materi_"+id_document).prop('disabled', true);
			if(data=='success'){
				$("#materi_"+id_document).removeClass("uk-badge uk-badge-primary"); //versions newer than 1.6
				$("#materi_"+id_document).addClass("uk-badge uk-badge-default");
				$("#materi_"+id_document).prop('disabled', true); //versions newer than 1.6
			}else{
				$("#materi_"+id_document).prop('disabled', false);
				$("#materi_"+id_document).addClass("uk-badge uk-badge-primary");
			}
		});
	}
	
	function save_pilih(){
		reload_materi();
	}
	
</script>

<div class="uk-modal-dialog">
	<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div id="before-edit-materi"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form id="form-edit-materi" style="display:none">
	<div id="edit_message_materi"></div>
	
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Periode</label>
			<select id="id_periode_materi" name="id_periode" data-md-selectize>
				<option value="">Choose..</option>
				<?php 
				foreach ($periode->result() as $row){?>
					<option value="<?=$row->id_periode?>"><?=$row->periode?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<table id="dt_default_materi_list" class="uk-table" cellspacing="0" width="100%">
				<thead>
					 <tr>
						<th rowspan="2" width="">Pilih</th>
						<th colspan="2"><b>Master Materi</b></th>
					</tr>
					<tr>
						<th width="">Nama Pembelajaran</th>
						<th width="">Nama FIle</th>
						<th width="">Keterangan</th>
					</tr>
				</thead>
				<tbody id="data_materi_materi">
					
				</tbody>
			</table>
		</div>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_class" value="<?=$id_class?>"/>
		<button type="button" onclick="save_pilih()" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<!--<button type="button" onclick="save_pilih()" class="md-btn md-btn-success">Save</button>-->
	</div>
</form>
</div>
