    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2">Nama Pembelajaran</th>
                                <th colspan="3">Master File</th>
                            </tr>
                            <tr>
								<th>Judul File</th>
								<th>Nama FIle</th>
								<th>Action</th>
							</tr>
                        </thead>

                        <tbody>
					<?php foreach ($document->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->nama_pembelajaran?></td>
							<td><?=$row->document?></td>
							<td><?=$row->file_name?></td>
                            <td>
								<a href="<?=base_url().'document/download/'.$row->id_document?>" class="uk-icon-button uk-icon-download"></a>
								<a href="#edit_file" data-uk-modal="{ center:true }" onclick="edit_file('<?=$row->id_document?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
								<a href="#" onclick="delete_file('<?=$row->id_document?>')"class="uk-icon-button uk-icon-trash-o"></a>
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_file" onclick="add_file()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_file">
		
				<div id="modal-add-fl"></div>
		</div>
		<div class="uk-modal" id="edit_file">
			<button class="uk-modal-close uk-close" type="button"></button>
			<div id="modal-edit-fl"></div>
		</div>
	
	
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_file(id_document){
				uri = '<?=base_url()."document/edit_file"?>';
				$('#modal-edit-fl').html(loading);
				$.post(uri,{ajax:true,id_document:id_document},function(data) {
					$('#modal-edit-fl').html(data);
				});
			}
			
			function add_file(){
				$('#modal-add-fl').html(loading);
				uri = '<?=base_url()."document/add_file"?>';
				$.post(uri,{ajax:true},function(data) {
					$('#modal-add-fl').html(data);
				});
			}
			
			function delete_file(id_document){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."document/delete_file"?>';
					$.post(uri,{ajax:true,id_document:id_document},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reload_file();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
			function reload_file(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",function(data){
					$('#list_data').html(data)
				});
			}
			
	</script>	
</html>