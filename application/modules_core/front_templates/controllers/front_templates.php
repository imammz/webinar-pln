<?php

class Front_templates extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Pragma: no-cache");
		$this->load->model('employee/employee_model');
    }
    
    public function index()
    {
        // $this->load->view('home_view',$data);
		exit();
    }
   

    public function header()
    {
		$data['pegawai'] = $this->employee_model->get_pegawai_user_by_id($this->session->userdata("sesi_id_user"));
		$data['parent']= $this->menu_model->get_list_menu_parent($this->session->userdata("sesi_id_level"));
        $this->load->view('header',$data);
    }
	
	 public function header2()
    {
		$data['parent']= $this->menu_model->get_list_menu_parent($this->session->userdata("sesi_id_level"));
        $this->load->view('header2',$data);
    }
	
	
    public function footer()
    {
        $this->load->view('footer');
    }
	public function footer2()
    {
        $this->load->view('footer2');
    }
}

?>