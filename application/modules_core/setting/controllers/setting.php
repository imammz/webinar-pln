<?php if (!defined('BASEPATH'))exit('No direct script access allowed');

class setting extends MX_Controller {

    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model('setting/setting_model');
		//$this->load->library('excel');
    }
    public function index() {
		$this->load->view('setting_view','');
	}
	
	function change(){
		//$this->login_model->checkPrivilege();
		$old	      = mysql_real_escape_string($this->input->post('old'));
		$new          = mysql_real_escape_string($this->input->post('new1'));
		$uid	= $this->session->userdata("sesi_user_id");
		$this->setting_model->update($uid,$this->fungsi->Md5AddSecret($new),$this->fungsi->Md5AddSecret($old),$uid);
		
	}
}
