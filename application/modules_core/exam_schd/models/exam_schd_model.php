<?php

class exam_schd_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	function get_list_examp_schd(){
		$sql = "SELECT a.*,b.`periode`,d.`nama_pembelajaran`,c.`id_class` ,
					CASE WHEN a.start_date = DATE_FORMAT(NOW(),'%Y-%m-%d')
						THEN 'ongoing'
					 WHEN a.start_date > DATE_FORMAT(NOW(),'%Y-%m-%d')
						THEN 'schedule'
					 WHEN  a.start_date < DATE_FORMAT(NOW(),'%Y-%m-%d')
						THEN 'finish'
					END status
				FROM wb_jadwal_ujian a, wb_periode b, wb_class c, wb_pembelajaran d
				WHERE a.`id_periode`=b.`id_periode`
				AND b.`id_class`=c.`id_class`
				AND c.`id_pembelajaran`=d.`id_pembelajaran`
				ORDER BY c.`id_class`,b.`start_date` ASC ";
		return $this->db->query($sql);
	}
	
	function get_list_examp_schd_peserta($id_user){
		$sql = "SELECT a.*,b.`periode`,d.`nama_pembelajaran`,c.`id_class` ,
					CASE WHEN a.start_date = DATE_FORMAT(NOW(),'%Y-%m-%d')
						THEN 'ongoing'
					 WHEN a.start_date > DATE_FORMAT(NOW(),'%Y-%m-%d')
						THEN 'schedule'
					 WHEN  a.start_date < DATE_FORMAT(NOW(),'%Y-%m-%d')
						THEN 'finish'
					END status
				FROM wb_jadwal_ujian a, wb_periode b, wb_class c, wb_pembelajaran d,`wb_peserta` e
				WHERE a.`id_periode`=b.`id_periode`
				AND b.`id_class`=c.`id_class`
				AND c.`id_pembelajaran`=d.`id_pembelajaran`
				AND c.`id_class`=e.`id_class`
				AND e.`id_user`='$id_user'
				ORDER BY c.`id_class`,b.`start_date` ASC";
		return $this->db->query($sql);
	}
	
	function get_list_exam_schd_by_id($id_jadwal_ujian){
		$sql = "SELECT a.*,b.`periode`,d.`nama_pembelajaran`,c.`id_class` 
				FROM wb_jadwal_ujian a, wb_periode b, wb_class c, wb_pembelajaran d
				WHERE a.`id_periode`=b.`id_periode`
				AND b.`id_class`=c.`id_class`
				AND c.`id_pembelajaran`=d.`id_pembelajaran`
				AND a.`id_jadwal_ujian`='$id_jadwal_ujian'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}	
	}
	function get_list_class(){
		$sql = "SELECT id_class,b.nama_pembelajaran,lokasi,keterangan,start_date, end_date,a.created_by,a.created_date,  
					a.updated_by,a.updated_date
				FROM wb_class a, wb_pembelajaran b
				WHERE a.id_pembelajaran = b.id_pembelajaran";
		return $this->db->query($sql);
	}
	
	function update_exam_schd($id_jadwal_ujian,$nama_ujian,$id_periode,$durasi,$start_date,$id_user){
		$sql="update wb_jadwal_ujian set nama_ujian='$nama_ujian',id_periode='$id_periode',
			  durasi='$durasi',start_date='$start_date',updated_by='$id_user', updated_date=now()
			  where id_jadwal_ujian='$id_jadwal_ujian'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function save_exam_schd($nama_ujian,$id_periode,$durasi,$start_date,$id_user){
		$sql="insert into wb_jadwal_ujian(nama_ujian,id_periode,durasi,start_date,created_by,created_date)
			  values('$nama_ujian','$id_periode','$durasi','$start_date','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function delete_exam_schd($id_jadwal_ujian){
		$sql="delete from wb_jadwal_ujian where id_jadwal_ujian='$id_jadwal_ujian'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	
	
	function get_soal_instruktur_by_id($id_soal){
		$sql = "select a.*,b.class,c.periode,b.created_by,b.instruktur1,b.instruktur2
				from wb_soal a, wb_class b, wb_periode c
				where a.id_class=b.id_class
				and a.id_periode=c.id_periode
				and a.id_soal='$id_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_list_bank_soal($id_soal,$categori){
		$sql = "select b.*,a.soal from wb_soal a, wb_bank_soal b
				where a.id_soal=b.id_soal
				and b.id_soal='$id_soal'
				and b.categori_quiz='$categori'";
		return $this->db->query($sql);
	}
	
	function get_bank_soal_by_id($id_bank_soal){
		$sql = "select b.* from wb_bank_soal b
				where b.id_bank_soal='$id_bank_soal'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}
	}
	
	
	function save_soal($id_soal,$soal,$id_class,$id_periode,$id_user){
		$sql="insert into wb_soal(id_soal,soal,id_class,id_periode,created_by,created_date)
			  values('$id_soal','$soal','$id_class','$id_periode','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_soal($id_soal,$soal,$id_class,$id_periode,$id_user){
		$sql="update wb_soal set soal='$soal',id_class='$id_class',id_periode='$id_periode',updated_by='$id_user',updated_date=now()
			  where id_soal='$id_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	
	function delete_soal($id_soal){
		$sql="delete from wb_soal where id_soal='$id_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	
	function save_bank_soal($id_soal,$categori_quiz,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="insert into wb_bank_soal(id_soal,categori_quiz,pertanyaan,a,b,c,d,e,jawaban_pg,jawaban_essay,created_by,created_date)
			  values('$id_soal','$categori_quiz','$pertanyaan','$a','$b','$c','$d','$e','$jawaban_pg','$jawaban_essay','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function update_bank_soal($id_bank_soal,$id_soal,$categori_quiz,$pertanyaan,$a,$b,$c,$d,$e,$jawaban_pg,$jawaban_essay,$id_user){
		$sql="update wb_bank_soal set categori_quiz='$categori_quiz',pertanyaan='$pertanyaan ',a='$a' ,b='$b', 				
			  c='$c',d='$d',e='$e', jawaban_pg='$jawaban_pg',jawaban_essay='$jawaban_essay',updated_by='$id_user',updated_date=now()
			  where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function delete_bank_soal($id_bank_soal){
		$sql="delete from wb_bank_soal where id_bank_soal='$id_bank_soal'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal sihapus'));		
		}
	}
	
}
