<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no"/>
        <link rel="icon" type="image/png" href="<?php echo base_url() ?>front_assets/assets/img/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo base_url() ?>front_assets/assets/img/favicon-32x32.png" sizes="32x32">
        <title>PLN Web Binar System</title>


        <link rel="stylesheet" href="<?php echo base_url() ?>front_assets/bower_components/metrics-graphics/dist/metricsgraphics.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>front_assets/bower_components/chartist/dist/chartist.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>front_assets/bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
        <link rel="stylesheet" href="<?php echo base_url() ?>front_assets/assets/icons/flags/flags.min.css" media="all">
        <link rel="stylesheet" href="<?php echo base_url() ?>front_assets/assets/css/main.min.css" media="all">
        <script src="<?php echo base_url('player/AC_RunActiveContent.js') ?>" language="javascript"></script>	
        <link href="http://vjs.zencdn.net/5.0.0/video-js.css" rel="stylesheet">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>front_assets/assets/slick/slick.min.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>front_assets/assets/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>front_assets/assets/slick/slick-theme.css"/>
        <style>

            .slider2{

                width:950px;
                margin:1px 1px;
                text-align:left;
            }
            .slider2 div{
                border : solid #ccc;
                border-top: solid #ccc;
                border-right: solid #ccc;
                border-bottom: solid #ccc;
                border-left: solid #ccc;
                margin-right:10px;
                float: left;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.slider').slick({
                    dots: true,
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 3
                });
            });
        </script>
        <!-- If you'd like to support IE8 -->
        <script src="http://vjs.zencdn.net/ie8/1.1.0/videojs-ie8.min.js"></script>


    </head>
    <body class="sidebar_main">
        <!---HEADER START-->
        <header id="header_main">
            <div class="header_main_content">
                <nav class="uk-navbar">
                    <!-- main sidebar switch -->
                    <a href="#" id="sidebar_main_toggle">
                        <span class="sSwitchIcon"></span>
                    </a>
                </nav>
            </div>
        </header>

        <div id="page_content">
            <div id="page_heading">
                <h1><?php echo $periode['nama_pembelajaran'] ?></h1>
                <h5><?php echo $periode['lokasi'] ?></h5>
                <span class="uk-text-muted uk-text-upper uk-text-small"><?php echo $periode['periode'] ?></span>
            </div>
            <div id="page_content_inner">

                <div class="uk-grid uk-grid-large" data-uk-grid-margin>
                    <div class="uk-width-xLarge-3-8  uk-width-large-3-10">

                        <?php
                        $user = $this->db->where('id_user', $this->session->userdata('sesi_id_user'))->get('wb_users');
                        $username = $user->row_array();
                        $all_user = $this->db->where('wb_users.id_user != "' . $this->session->userdata('sesi_id_user') . '" AND id_class = "' . $id_class . '" AND wb_users.id_user != ' . $this->session->userdata('sesi_id_user') . '')->join('wb_users', 'wb_users.id_user = wb_peserta.id_user')->get('wb_peserta');
                        $all_username = $all_user->result_array();
                        //print_r($all_username); exit;
                        ?>
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Instructure Page
                                </h3>
                            </div>
                            <?php
                            $instruktur1 = $this->db->query('select username from wb_users WHERE id_user = "' . $periode['instruktur1'] . '"')->row_array();
                            $instruktur1 = $instruktur1['username'];

                            if ($username['username'] == $instruktur1) {
                                ?>    
                                <div class="md-card-content">
                                    <div class="uk-margin-bottom uk-text-center">
                                        <object width="320" height="320" data="<?php echo base_url('player/admin.swf?user=' . $username['username']) ?>"></object> 
                                    </div>
                                </div>
                            <?php } else {
                                ?>

                                <div class="md-card-content">
                                    <div class="uk-margin-bottom uk-text-center">
                                        <object width="320" height="320" data="<?php echo base_url('player/user.swf?user=' . $instruktur1) ?>"></object> 
                                    </div>
                                </div>

                            <?php } ?>
                        </div>
                        <?php if ($username['username'] != $instruktur1) { ?>
                            <div class="md-card">
                                <div class="md-card-toolbar">
                                    <h3 class="md-card-toolbar-heading-text">
                                        My Page
                                    </h3>
                                </div>
                                <div class="md-card-content">
                                    <div class="uk-margin-bottom uk-text-center">
                                        <object width="320" height="320" data="<?php echo base_url('player/admin.swf?user=' . $username['username']) ?>"></object> 
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>



                        <?php } ?>

                    </div>



                    <div class="uk-width-xLarge-5-10  uk-width-large-5-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Slide Presentation
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">

                                <div class="uk-grid uk-grid-divider uk-grid-medium">
                                    <div class="uk-width-large-1-1">
                                        <div class="md-card-content">
                                            <div class="uk-margin-bottom uk-text-center">

                                                <!--<iframe src="https://docs.google.com/presentation/d/1Ny107zfzJXFS4-WzJhBn48KTtNbN0RcYUHpwKaJLjBs/embed?start=true&loop=false&delayms=3000" frameborder="0" width="480" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>-->
                                            </div>
                                        </div>	
                                    </div>
                                    <div class="uk-width-large-1-1">
                                        <p>
                                            <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">Information</span>
                                            <span class="uk-badge uk-badge-success">Slide 1 - Presentasi Tentang</span><br/>
                                        </p>
                                        <hr class="uk-grid-divider">
                                        <p>
                                            <span class="uk-text-muted uk-text-small uk-display-block uk-margin-small-bottom">Description</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-xLarge-2-8  uk-width-large-2-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Class Information
                                </h3>
                            </div>
                            <div class="md-card-content uk-margin-bottom">
                                <div>
                                    <p><?php echo $instruktur1 ?></p>
                                    <p><?php echo $periode['nama_pembelajaran'] ?></p>
                                    <p><?php echo $periode['periode'] ?></p>
                                    <p> <?php echo $periode['start_date'] . ' - ' . $periode['end_date'] ?> </p>
                                    <p>Lokasi Diklat <?php echo $periode['lokasi'] ?></p>
                                    <p>Peserta Lain
                                    <ul>
                                        <?php foreach ($all_username as $row) { ?>  
                                            <li style="margin-right: 1px;">
                                                <?php echo $row['username'] ?>
                                            </li>
                                        <?php } ?>

                                    </ul>

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    List Friend
                                </h3><br/>
                            </div>
                            <div class="md-card-content">
                                <div class="md-list-outside-wrapper">
                                    <ul class="md-list md-list-addon md-list-outside" id="chat_user_list">
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?php echo base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-contrtent">

                                                <span class="md-list-heading">Kusnadi</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-danger"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?php echo base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Taufan Arfianto</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Non Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?php echo base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Mamad Tohiri</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?php echo base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Daud Pulung</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?php echo base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Mila Rahma</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?php echo base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Fuadi Julian</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?php echo base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Dimas Galih</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="md-list-addon-element">
                                                <span class="element-status element-status-success"></span>
                                                <img class="md-user-image md-list-addon-avatar" src="<?php echo base_url() ?>front_assets/assets/img/avatars/user1.png" alt=""/>
                                            </div>
                                            <div class="md-list-content">

                                                <span class="md-list-heading">Sidiq Permana</span>
                                                <span class="uk-text-small uk-text-muted uk-text-truncate">Active</span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    <div class="clearfix" style="min-height: 20px; width: 100%;">  </div>


                </div>
            </div>


            <div class="md-fab-wrapper">
                <div id="my_camera"></div>
            </div>


            <!-- uikit functions -->
            <script src="<?php echo base_url() ?>front_assets/assets/js/uikit_custom.min.js"></script>
            <!-- altair common functions/helpers -->
            <script src="<?php echo base_url() ?>front_assets/assets/js/altair_admin_common.min.js"></script>

            <!-- page specific plugins -->

            <script type="text/javascript" src="<?php echo base_url() ?>front_assets/webcam/webcam.js"></script>

            <script language="JavaScript">
            Webcam.set({
                width: 320,
                height: 240,
                image_format: 'jpeg',
                jpeg_quality: 90
            });
            Webcam.attach('#my_camera');
            </script>

            <!-- Code to handle taking the snapshot and displaying it locally -->
            <script language="JavaScript">
                function take_snapshot() {
                    // take snapshot and get image data
                    Webcam.snap(function (data_uri) {
                        // display results in page
                        document.getElementById('results').innerHTML =
                                '<h2>Here is your image:</h2>' +
                                '<img src="' + data_uri + '"/>';
                    });
                }
            </script>