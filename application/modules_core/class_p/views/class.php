    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2">Nama Pembelajaran</th>
                                <th colspan="3">Informasi Pemebelajaran</th>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
								<th>Lokasi</th>
								<th>Start Date</th>
								<th>End Date</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
                        </thead>

                        <tbody>
					<?php foreach ($class->result() as $row){
						/*
						$peg1 = $this->pegawai_model->get_pegawai_by_uid($row->instruktur1);
						$peg2 = $this->pegawai_model->get_pegawai_by_uid($row->instruktur2);*/
						?>	
                        <tr>
                            <td><?=$row->nama_pembelajaran?></td>
							<td><?=$row->keterangan?></td>
							<!--
							<td><?php //$peg1['nama']?></td>
							<td><?php //$peg2['nama']?></td>-->
							<td><?=$row->lokasi?></td>
							<td><?=$row->start_date?></td>
							<td><?=$row->end_date?></td>
							<td>
								<?php if($row->status=='inprogress'){?>
										<span class="uk-badge uk-badge-warning"><?=$row->status?></span>
								<?php }else if($row->status=='schedule'){?>
										<span class="uk-badge uk-badge-primary"><?=$row->status?></span>
								<?php }else{?>
										<span class="uk-badge uk-badge-success"><?=$row->status?></span>
								<?php } ?>
							</td>	
							<td>
								<a onclick="detail('<?=$row->id_class?>')" class="uk-icon-button uk-icon-cogs" title="detail"></a>&nbsp;
					
								
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="<?=base_url()?>class_p/add">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>

	<!--
		<div class="uk-modal" id="new_class">
			<div id="modal-add"></div>
		</div>-->
		<div class="uk-modal" id="edit_class">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id){
				uri = '<?=base_url()."class_p/edit"?>';
				$('#new_class').hide();
				$('#edit_class').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(){
				/*
				uri = '<?=base_url()."class_p/add"?>';
				$('#edit_class').hide();
				$('#new_class').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-add').html(data);
				});*/
				uri = '<?=base_url()."class_p/add"?>';
				window.location.href = url; 
			}
			
			function deleted(id){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."class_p/delete"?>';
					$.post(uri,{ajax:true,id:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
			function detail(id){
				$('#list_data').html(loading);
				$.post('<?=base_url()."class_p/detail"?>',{id:id},function(data) 
				{
					$('#list_data').html(data);
				});
			}
	</script>	
</html>