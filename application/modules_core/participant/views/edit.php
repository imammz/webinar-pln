<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
	<div id="edit_message"></id>
</div>
<div id="before-edit-soal"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="edit_myform" style="display:none">
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Kelas</label>
		</div>
		<select id="id_class" name="id_class">

				<option value="<?=$class['id_class']?>"> <?=$class['class_name']?></option>
				
		</select>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Nama Peserta</label>
			
		</div>
		<select id="id_user" name="id_user">
				<option value="<?=$participant['id_user']?>"><?=$participant['nama'].' --> '.$participant['email']?></option>
				<?php 
				foreach ($user->result() as $row){?>
					<option value="<?=$row->id_user?>" <?php echo $participant['id_user']==$row->id_user ? 'selected':''?>><?=$row->nama.' --> '.$row->email?></option>
					
				<?php }?>
			</select>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_peserta" value="<?=$participant['id_peserta']?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnupdate" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#edit_myform").show(); 
				$("#before-edit-soal").hide();
			}, 200);
			
			function reloadlist(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",{id_class:'<?=$participant['id_class']?>'},function(data){
					$('#list_data').html(data);
				});
			}
			
			$("#id_class").change(function(){
				var id_class = $("#id_class").val();
				$.ajax({
					url: "soal/get_periode_class",
					data: "id_class="+id_class,
					cache: false,
					 dataType: 'json',
					success: function(msg){
						$('#id_user').html('');
						$.each(msg, function(index, row) {
							$('#id_user').append('<option value='+row.id_user+' >'+row.periode+'</option>');
					   });
					  
					}
				});
			  });
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#edit_myform').validate({
				rules:{
						id_user:{
							required:true
						},
						id_class:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$("#btnupdate").prop('value', 'Process...'); 
					$('input[type="submit"]').prop('disabled', true);
					$.post('<?php echo base_url('participant/update'); ?>', $('#edit_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						$('#btnupdate').attr('disabled');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#edit_message").html(msg);
							
							$('#edit_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#edit_message').hide();
								$('#edit_user').hide();
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#edit_message").html(msg);
							$("#btnupdate").prop('value', 'Save'); 
							$('input[type="submit"]').prop('disabled', false);
						}
					});
				}
			})
		});
</script>
