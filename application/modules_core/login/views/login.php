<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="<?=base_url()?>front_assets/assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?=base_url()?>front_assets/assets/img/favicon-32x32.png" sizes="32x32">
    <title>Web Binar Admin</title>
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/uikit/css/uikit.almost-flat.min.css"/>
    <link rel="stylesheet" href="<?=base_url()?>front_assets/assets/css/login_page.min.css" />
	<link rel="stylesheet" href="<?=base_url()?>front_assets/assets/css/bootstrapValidator.min.css" />
</head>
<body class="login_page">
    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <img src="<?=base_url()?>front_assets/assets/img/pln.png"></a>
                </div>
				<div id="err_message"></div>
                <form id="loginform" method="post">
                    <div class="uk-form-row">
                        <label for="login_username">Username</label>
                        <input class="md-input" type="text" id="username" name="username" />
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">Password</label>
                        <input class="md-input" type="password" id="password" name="password" />
                    </div>
                    <div class="uk-margin-medium-top">
                        <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="uk-margin-top uk-text-center">
        </div>
    </div>
    <script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
    <script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>
	<script>
	
			$(document).ready(function() {
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#loginform').validate({
				rules:{
						 username:{
								required:true
							  },
						 password:{
								required:true
							  }
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$.post('<?php echo base_url('login/process'); ?>', $('#loginform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						var opt  = $('#opt').val();
						if(data.status == "success") {
							window.location="<?php echo base_url('home'); ?>";
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#err_message").html(msg);
						}
					});
				}
			})
		});
	
	</script>

	
</body>
</html>