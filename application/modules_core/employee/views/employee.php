
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2">NIP</th>
                                <th colspan="4">Informasi Pegawai</th>
                            </tr>
                            <tr>
                                <th>Nama</th>
                                <th>Alamat</th>
								<th>Telp</th>
                                <th>Email</th>
								<th>Jabatan</th>
                            </tr>
                        </thead>

                        <tbody>
					<?php foreach ($employee->result() as $row){?>	
                        <tr>
                            <td><?=$row->nip?></td>
                            <td><?=$row->nama?></td>
							<td width="200"><?=$row->alamat?></td>
                            <td><?=$row->no_telp_kntr?></td>
							<td><?=$row->email?></td>
                            <td><?=$row->jabatan?></td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_user" onclick="add()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_user">
			<div id="modal-add"></div>
		</div>
		<div class="uk-modal" id="edit_user">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id){
				uri = '<?=base_url()."user/edit"?>';
				$('#new_user').hide();
				$('#edit_user').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(id){
				uri = '<?=base_url()."employee/add"?>';
				$('#edit_user').hide();
				$('#new_user').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id){
				var r = confirm("Yakin akan di nonaktifkan?");
				if (r == true) {
					uri = '<?=base_url()."user/delete"?>';
					$.post(uri,{ajax:true,id:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
	</script>	
</html>