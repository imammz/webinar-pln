<?php

class employee extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model(array('employee/employee_model','level/level_model'));
		$this->load->library('excel');
    }
    
    public function index()
	{ 	
		$this->login_model->is_login();
		$data['title'] 		='Master Peawai';
		$data['link'] = base_url()."employee/list_data"; 
		$this->load->view('preview',$data);
	}
	
	public function list_data() 
	{
		$this->login_model->is_login();
		$data['employee']	= $this->employee_model->get_list_pegawai();
		$data['title'] 	='Master Pegawai';
		$data['link'] 	= base_url()."employee/list_data"; 
		$this->load->view('employee', $data);        
    }
	
	public function add()
	{
		$this->login_model->is_login();
		$data['pegawai']= $this->employee_model->get_list_pegawai();
		$data['level']	= $this->level_model->get_list_level();
		$data['title'] 		= 'Add employee';
		$data['link'] = base_url()."employee/list_data"; 
		$this->load->view('add', $data); 
	}
	
	public function upload_file()
    {
        $this->login_model->is_login();
        $id_user     = $this->session->userdata("sesi_id_user");
        @$name = $_FILES["file"]["name"];
        $config['upload_path']   = './front_assets/file/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size']      = '10000000';
        $config['encrypt_name']  = TRUE;
        $path                    = "./front_assets/file/";
        //die($name);
        if ($name) {
            
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file')) {
                $upload_error = array(
                    $error = $this->upload->display_errors()
                );
                echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
            } else {
                
                $upload_data = $this->upload->data();
                $config      = array(
                    'source_image' => $upload_data['full_path'],
                    'new_image' => realpath(APPPATH . '../front_assets/file/thumbs/'),
                    'maintain_ration' => true);
				    $files = $path . $upload_data['file_name'];

					$objPHPExcel = PHPExcel_IOFactory::load($files);
					//get only the Cell Collection
					$sheet = $objPHPExcel->getSheet(0); 
					$highestRow = $sheet->getHighestRow(); 
					$highestColumn = $sheet->getHighestColumn();
				
					$row      = 2;
					$insert   = 0;
					$rejected = 0;
					for ($row; $row <= $highestRow; $row++) {
						$datarow  = $objPHPExcel->getActiveSheet()->rangeToArray('A' . $row . ':' . $objPHPExcel->getActiveSheet()->getHighestColumn() . $row);
						$i        = 0;
						$log_data = array();
						foreach ($datarow[0] as $values) {
							$log_data[$i] = $values;
							$i++;
						}
						
						$emp = $this->employee_model->get_employee_nip($log_data[0]);
							if($emp){
								$rejected++;
							}else{
							$this->employee_model->save_employee($log_data[0],$log_data[1],$log_data[2],$log_data[3],$log_data[4],$log_data[5],$log_data[6],$log_data[7],$log_data[8],$log_data[9],$log_data[10],
														         $log_data[11],$log_data[12],$log_data[13],$log_data[14],$log_data[15],$log_data[16],$log_data[17],$log_data[18],$log_data[19],$log_data[20],
																 $log_data[21],$log_data[22],$log_data[23]);
							$insert++;									 
						}
					}	

					echo json_encode(array(
                    'response' => 'succes',
                    'msg' => 'data insert '.$insert. ', data rejected '.$rejected
                ));
            }
        } else {
             echo json_encode(array(
                    'response' => 'failed',
                    'msg' => $error
                ));
        }
    }
	
	
	
	public function edit()
	{
		
		$this->login_model->is_login();
		
		$id				= $this->input->post('id');
		$data['employee']	= $this->employee_model->get_employee_by_id($id);
		$data['pegawai']= $this->employee_model->get_list_pegawai2();
		$data['level']	= $this->level_model->get_list_level();
		$data['title'] 	= 'Edit employee';
		$data['link'] 	= base_url()."employee/list_data"; 
		$this->load->view('edit', $data); 
	}
	
	public function update()
	{	
		$this->login_model->is_login();
		$id				= $this->input->post('id');
		$id_level		= $this->input->post('id_level');
		//$nip			= $this->input->post('nip');
		$passwords		= $this->input->post('passwords');
		$employeename		= $this->input->post('employeename');
		$isactive 		= $this->input->post('isactive');
		$id_employee		= $this->session->employeedata("sesi_id_employee");
		//save employee
		//$this->employee_model->update_employee($id,$employeename,$nip,$isactive,$passwords,$id_level,$id_employee);
		$this->employee_model->update_employee($id,$employeename,$isactive,$passwords,$id_level,$id_employee);
	}
	public function save()
	{	
		$this->login_model->is_login();
		$id_level		= $this->input->post('id_level');
		$nip			= $this->input->post('nip');
		$passwords		= $this->fungsi->Md5AddSecret($this->input->post('passwords'));
		$employeename		= $this->input->post('employeename');
		$isactive 		= $this->input->post('isactive');
		$id_employee		= $this->session->employeedata("sesi_id_employee");
		//save employee
		$this->employee_model->save_employee($employeename,$nip,$isactive,$passwords,$id_level,$id_employee);
	}
	
	//inactive aja jangan di delete
	public function delete()
	{	
		$this->login_model->is_login();
		$id	= $this->input->post('id');
		$this->employee_model->update_employee($id);
	
	}
	
}

?>