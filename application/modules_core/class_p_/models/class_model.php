<?php

class class_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	
	function get_list_class(){
		$sql = "SELECT *
				FROM wb_class";
		return $this->db->query($sql);
	}
	
	function get_list_class_peserta($id_user){
		$sql = "select a.*,b.nama_pembelajaran,
				case when start_date <= DATE_FORMAT(now(),'%Y-%m-%d') and end_date >= DATE_FORMAT(now(),'%Y-%m-%d')
						then 'inprogress'
					 when start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
					 when end_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
				end status from wb_class a,wb_pembelajaran b,wb_peserta c
				where a.id_class=c.id_class
				and a.id_pembelajaran=b.id_pembelajaran
				and c.id_user='$id_user'";
		return $this->db->query($sql);
	}
	
	function get_class_by_id($id_class){
		$sql = "select a.*,b.nama_pembelajaran,
				case when start_date <= DATE_FORMAT(now(),'%Y-%m-%d') and end_date >= DATE_FORMAT(now(),'%Y-%m-%d')
						then 'inprogress'
					 when start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
					 when end_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
				end status from wb_class a,wb_pembelajaran b,wb_peserta c
				where a.id_class=c.id_class
				and a.id_pembelajaran=b.id_pembelajaran
				and a.id_class='$id_class'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function save_class($class_name,$keterangan,$instruktur1,$instruktur2,$start_date,$end_date,$lokasi, $id_user){
		$sql="insert into wb_class(class,keterangan,instruktur1,instruktur2,start_date,end_date,lokasi,created_by,created_date)
			  values('$class_name','$keterangan','$instruktur1','$instruktur2','$start_date','$end_date','$lokasi','$id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Class berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Class gagal disimpan'));		
		}
		
	}
	
	
	
	function update_class($id,$class_name,$keterangan,$instruktur1,$instruktur2,$start_date,$end_date,$lokasi, $id_user){
		$sql="update wb_class set class='$class_name',
			  keterangan='$keterangan',instruktur1='$instruktur1',instruktur2='$instruktur2',
			  start_date='$start_date',end_date='$end_date',lokasi='$lokasi',
			  updated_by='$id_user',updated_date=now()
			  WHERE id_class='$id'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Class berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Class gagal di simpan'));		
		}		  
	
	}
	
	
	function delete_class($id_class){
		$sql="DELETE FROM wb_class 
			  WHERE id_class='$id_class'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Class berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Class gagal didelete'));		
		}		  
	}
	
	function inactive_class($id_class){
		$sql="update wb_class set keterangan='0'
			  WHERE id_class='$id_class'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Class berhasil di nonaktifkan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Class gagal di nonaktifkan'));		
		}		  
	}
	

}
