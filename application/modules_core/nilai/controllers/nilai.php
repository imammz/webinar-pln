<?php

class nilai extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");

		date_default_timezone_set('Asia/Jakarta');
    }
    
    public function index()
	{ 	
		$data['link'] = base_url()."nilai/list_data"; 
		$data['title']='nilai';
		
		$this->load->view('preview',$data);
		
	}
	
	public function list_data() 
	{
		//$this->login_model->checkPrivilege();
		$data['title'] 		='nilai';
		$data['link'] 		= base_url()."nilai/list_data"; 
		$this->load->view('nilai', $data);        
    }
	
	
	
	
}

?>