    <div id="page_content">
        <div id="page_content_inner">

            <div class="uk-width-medium-8-10 uk-container-center reset-print">
                <div class="uk-grid uk-grid-collapse" data-uk-grid-margin>
                    <div class="uk-width-large-7-10">
                        <div class="md-card md-card-single main-print" id="invoice">
                            <div id="invoice_preview"></div>
                            <div id="invoice_form"></div>
                        </div>
                    </div>
                    <div class="uk-width-large-3-10 hidden-print uk-visible-large">
                        <div class="md-list-outside-wrapper">
                            <ul class="md-list md-list-outside invoices_list" id="invoices_list">
                                <li class="heading_list">August 2015</li>
                                <li>
                                    <a href="#" class="md-list-content" data-invoice-id="1">
                                        <span class="md-list-heading uk-text-truncate">Kelas 1/2015 <span class="uk-text-small uk-text-muted">(17 Aug)</span></span>
                                        <span class="uk-text-small uk-text-muted">Greenholt Group</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="md-list-content" data-invoice-id="1">
                                        <span class="md-list-heading uk-text-truncate">Kelas 2/2015  <span class="uk-text-small uk-text-muted">(16 Aug)</span></span>
                                        <span class="uk-text-small uk-text-muted">Towne LLC</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="md-list-content" data-invoice-id="1">
                                        <span class="md-list-heading uk-text-truncate">Kelas 3/2015<span class="uk-text-small uk-text-muted">(15 Aug)</span></span>
                                        <span class="uk-text-small uk-text-muted">Boehm, Frami and Christiansen</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
						<br/>
						<a class="md-btn md-btn-success" href="ecommerce_product_details.html">ALL</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div id="sidebar_secondary">
        <div class="sidebar_secondary_wrapper uk-margin-remove"></div>
    </div>

    <script id="invoice_template" type="text/x-handlebars-template">
        <div class="md-card-toolbar">
            <div class="md-card-toolbar-actions hidden-print">
                <i class="md-icon material-icons" id="invoice_print">&#xE8ad;</i>
                <div class="md-card-dropdown" data-uk-dropdown>
                    <i class="md-icon material-icons">&#xE5D4;</i>
                </div>
            </div>
            <h3 class="md-card-toolbar-heading-text large" id="invoice_name">
               Kelas {{invoice_id.invoice_number}}
            </h3>
        </div>
        <div class="md-card-content">
            <div class="uk-margin-medium-bottom">
                <span class="uk-text-muted uk-text-small uk-text-italic">Start Data:</span> {{invoice_id.invoice_date}}
                <br/>
                <span class="uk-text-muted uk-text-small uk-text-italic">End Date:</span> {{invoice_id.invoice_due_date}}
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-small-3-5">
                    <div class="uk-margin-bottom">
                        <span class="uk-text-muted uk-text-small uk-text-italic">Fasilitator:</span>
                        <address>
                            <p><strong>Taufan Arfianto</strong></p>
                            <p>Pengajar 1</p>
                            <p>Staff Kementerian A</p>
                        </address>
                    </div>
                </div>
                <div class="uk-width-small-2-5">
                    <span class="uk-text-muted uk-text-small uk-text-italic">Result:</span>
                    <p class="heading_b uk-text-success">78</p>
                    <p class="uk-text-small uk-text-muted uk-margin-top-remove">LULUS</p>
                </div>
            </div>
            <div class="uk-grid uk-margin-large-bottom">
                <div class="uk-width-1-1">
                    <table class="uk-table">
                        <thead>
                        <tr class="uk-text-upper">
                            <th>Description Materi</th>
                        </tr>
                        </thead>
                        <tbody>
                        {{#each invoice_id.invoice_services}}
                            <tr class="uk-table-middle">
                                <td>
                                    <span class="uk-text-large">{{ service_name }}</span><br/>
                                    <span class="uk-text-muted uk-text-small">{{ service_description }}</span>
                                </td>
                            </tr>
                        {{/each}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </script>

    <!-- common functions -->
    <script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?=base_url()?>front_assets/assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- handlebars.js -->
    <script src="<?=base_url()?>front_assets/bower_components/handlebars/handlebars.min.js"></script>
    <script src="<?=base_url()?>front_assets/assets/js/custom/handlebars_helpers.min.js"></script>

    <!--  invoices functions -->
    <script src="<?=base_url()?>front_assets/assets/js/pages/page_invoices.min.js"></script>




</html>