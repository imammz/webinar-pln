<!--<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>-->
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div>
<div id="before-edit-essay"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="add_myform_essay" style="display:none">
	<div id="add_message-essay"></div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pertanyaan</label>
		<textarea class="md-input" id="pertanyaan" name="pertanyaan"></textarea> 
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Jawaban</label>
		<textarea class="md-input" id="jawaban" name="jawaban"></textarea> 
	</div>
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_soal" value="<?=$id_soal?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#add_myform_essay").show(); 
				$("#before-edit-essay").hide();
			}, 100);
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#add_myform_essay').validate({
				rules:{
						pertanyaan:{
							required:true
						},
						jawaban:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$("#btnsave").prop('value', 'Process'); 
					$('input[type="submit"]').prop('disabled', true);
					$.post('<?php echo base_url('soal/save_essay'); ?>', $('#add_myform_essay').serialize(), function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#add_message-essay").html(msg);
							
							$('#add_message-essay').fadeTo(3000, 500).slideUp(500, function(){
								$('#add_message-essay').hide();
								reload_essay();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#add_message-essay").html(msg);
							$("#btnsave").prop('value', 'Save'); 
							$('input[type="submit"]').prop('disabled', false);
						}
					});
				}
			})
		});
</script>
