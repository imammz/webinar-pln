<?php echo Modules::run( 'front_templates/front_templates/header'); ?>
<!-- BEGIN PAGE -->
<div id="main-content">
   <!-- BEGIN PAGE CONTAINER-->
   <div class="container-fluid">
      <!-- BEGIN PAGE HEADER-->
      <div class="row-fluid">
         <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <ul class="breadcrumb">
               <li>
                  <a href="#">Change Password</a>
                  <span class="divider">/</span>
               </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
         </div>
      </div>
      <!-- END PAGE HEADER-->
      <!-- BEGIN PAGE CONTENT-->
 
     
			<div id="message"></div>
            <div class="form-horizontal">
              
                  <div class="row-fluid">
                     <div class="span12 responsive" data-tablet="span12 fix-margin" data-desktop="span">
                        <div class="widget green">
                           <div class="widget-title">
                              <h4><i class="icon-calendar"></i>Change Password</h4>
                           </div>
						   <form id="form_change" method="post" action="setting/change" class="col-md-4 col-md-offset-6">
                            <div class="widget-body">
                              <div class="control-group">
                                 <label class="control-label">Old Password</label>
                                 <div class="controls">
									<input type="text" class="form-control" name="old" id="old">
                                 </div>
                              </div>
							  <div class="control-group">
                                 <label class="control-label">New Password</label>
                                 <div class="controls">
									<input type="text" class="form-control" name="new1" id="new1">
                                 </div>
                              </div>
							  <div class="control-group">
                                 <label class="control-label">Retype Password</label>
                                 <div class="controls">
									<input type="text" class="form-control" name="new2" id="new2">
                                 </div>
                              </div>
							   <div class="control-group">
                                 <div class="controls">
									<button class="btn btn-primary" id="btnsearch" >Save</button>
                                 </div>
                              </div>
                           </div>
						   </form>
					   </div>
					</div>
				 </div>
			</div>
		</div>
   <!-- END PAGE CONTENT-->
</div>
<!-- END PAGE CONTAINER-->
<?php echo Modules::run( 'front_templates/front_templates/footer'); ?>
<script>

	var Script = function () {
	$.validator.setDefaults({ ignore: ":hidden:not(select)" });
    $.validator.setDefaults({
        submitHandler: function() {	
				//$("#message").html("<img src='loading.gif'/>");
				var postData = $('#form_change').serializeArray();
				var formURL = $('#form_change').attr("action");
				var btn = $('#btnsearch');
				var css =  btn.attr("class");
				var text = btn.html();
				var loading ='<div align="center" class="loading"><img src="<?=base_url()?>front_assets/img/ajax-loader.gif"></div>';
				btn.html("Please wait..");
				btn.attr("class","btn btn-primary disabled");
				$.ajax({
					url : formURL,
					type: "POST",
					data : postData,
					dataType: "json",
					success:function(data) 
					{
						//alert(data.response);
						if(data.response == 'success'){
							var msg='<div class="alert alert-success alert-dismissable">Paswoord Updated</div>';
							$('#old').val('');
							$('#new1').val('');
							$('#new2').val('');
							$("#message").html(msg);
							$('#message').alert();
								btn.html("Save");
								btn.attr("class","btn btn-primary");
							$('#message').fadeTo(3000, 500).slideUp(500, function(){
								$('#message').hide();
							});
							
						}else if(data.response == 'failed'){
							var msg='<div class="alert alert-success alert-dismissable">Error : Invalid old password </div>';
							$("#message").html(msg);
							btn.html("Save");
							btn.attr("class","btn btn-primary");
							$('#message').fadeTo(2000, 500).slideUp(500, function(){
								$('#message').hide();
							});
						}
						
					},
					error: function(jqXHR, textStatus, errorThrown) 
					{
						var msg='<div class="alert alert-error alert-dismissable">'+errorThrown+'</div>';
						$("#message").html(msg);
						btn.html("Save");
						btn.attr("class","btn btn-primary");
						$('#message').alert();
							$('#message').fadeTo(2000, 500).slideUp(500, function(){
							$('#message').hide();
							
						});
					}
				});
		}
    });

    $().ready(function() {
        $("#form_change").validate({
            rules: {
                old: "required",
                new1: "required",
				new2: {
				  equalTo: "#new1"
				}
            },
			highlight: function(element) {
				$(element).closest('.control-group').addClass('error');
			},
			unhighlight: function(element) {
				$(element).closest('.control-group').removeClass('error');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			}
        });
    });
}();   
</script>
