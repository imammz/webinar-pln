
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2" width="400px">Keterangan</th>
                                <th colspan="4">Information</th>
                            </tr>
                            <tr>
                                <th>Class</th>
                                <th>Periode</th>
								<th width="100px">Action</th>
							</tr>
                        </thead>

                        <tbody>
					<?php foreach ($soal->result() as $row){
						?>	
                        <tr>
                            <td><?=$row->soal?></td>
							<td><?=$row->class?></td>
							<td><?=$row->periode?></td>
                            <td>
								<a href="#edit_soal" data-uk-modal="{ center:true }" onclick="edits('<?=$row->id_soal?>','<?=$row->id_class?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
								<a href="#" onclick="detail('<?=$row->id_soal?>')" class="uk-icon-button uk-icon-cogs" title="detail"></a>&nbsp;
								
								<a href="#" onclick="deleted('<?=$row->id_soal?>')"class="uk-icon-button uk-icon-trash-o"></a>
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
	
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_soal" onclick="add()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_soal">
			<div id="modal-add"></div>
		</div>
		<div class="uk-modal" id="edit_soal">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id_soal,id_class){
				uri = '<?=base_url()."soal/edit"?>';
				$('#new_soal').hide();
				$('#edit_soal').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id_soal:id_soal,id_class:id_class},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(){
				uri = '<?=base_url()."soal/add"?>';
				$('#edit_soal').hide();
				$('#new_soal').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id_soal){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."soal/delete"?>';
					$.post(uri,{ajax:true,id_soal:id_soal},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
			function detail(id_soal){
				$('#list_data').html(loading);
				$.post('<?=base_url()."soal/detail"?>',{id_soal:id_soal},function(data) 
				{
					$('#list_data').html(data);
				});
			}
	</script>	
</html>