<div id="delete_message"></div>
<table class="uk-table uk-table-hover">
	<thead>
		 <tr>
			<th rowspan="2" width="">Pertanyaan</th>
			<th colspan="2"><b><?=$title?></b></th>
		</tr>
		<tr>
			<th width="">Jawaban</th>
			<th width="100px">Action</th>
		</tr>
	</thead>
	<tfoot>
	<?php 
	if($soal){
	foreach($soal->result() as $sl){?>	
			<tr>
				<td><?=$sl->pertanyaan?></td>
				<td><?=$sl->jawaban_essay?></td>
				<td>
					<a href="#edit_essay" data-uk-modal="{ center:true }" onclick="edit_essay('<?=$sl->id_bank_soal?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
					<a href="#" onclick="delete_essay('<?=$sl->id_bank_soal?>')"class="uk-icon-button uk-icon-trash-o"></a>
				</td>
			</tr>	
	<?php }
	}?>		
	</tfoot>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_essay" onclick="add_essay()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_essay">
			<div id="modal-add-essay"></div>
		</div>
		<div class="uk-modal" id="edit_essay">
			<div id="modal-edit-essay"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				/*
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				*/
				
			});	
			
			function edit_essay(id_bank_soal){
				uri = '<?=base_url()."soal/edit_essay"?>';
				$('#new_essay').hide();
				$('#edit_essay').show();
				$('#modal-edit-essay').html(loading);
				$.post(uri,{ajax:true,id_bank_soal:id_bank_soal},function(data) {
					$('#modal-edit-essay').html(data);
				});
			}
			
			function add_essay(){
				uri = '<?=base_url()."soal/add_essay"?>';
				$('#edit_essay').hide();
				$('#new_essay').show();
				$('#modal-add-essay').html(loading);
				$.post(uri,{ajax:true},function(data) {
					$('#modal-add-essay').html(data);
				});
			}
			
			function delete_essay(id){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."soal/delete_essay"?>';
					$.post(uri,{ajax:true,id_bank_soal:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reload_essay();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
	</script>