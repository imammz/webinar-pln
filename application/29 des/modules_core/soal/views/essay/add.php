<script src="<?=base_url()?>front_assets/assets/js/tinymce.min.js"></script>
<style>
select {  
    padding: 8px; 
    border: 1px solid gainsboro; 
    width: 100%;
    border-radius: 1px;
}
</style>
  <script>
	tinymce.init({
	  selector: "textarea",
	  height: 100,
	  plugins: [
		"advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
		"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
		"table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
	  ],

	  toolbar1: " bold italic underline strikethrough | alignleft aligncenter alignright alignjustify|cut copy paste  | bullist numlist",
	  toolbar2: "outdent indent blockquote | undo redo | forecolor backcolor",
	  toolbar3: "table | hr removeformat | subscript superscript | charmap",

	  menubar: false,
	  toolbar_items_size: 'small'
	});
  </script>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
</div>
<div>
<div id="before-edit-essay"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="add_myform_essay" style="display:none">
	<div id="add_message-essay"></div>
	<div class="uk-grid">
		<div class="uk-width-medium-1-1 parsley-row">
			<label for="class_name">Nama Pembelajaran<span class="req">*</span></label>
			<select id="id_pembelajaran" name="id_pembelajaran" required>
				<option value="">Pilih ...</option>
				<?php 
				foreach ($pembelajaran->result() as $row){?>
					<option value="<?=$row->id_pembelajaran?>"><?=$row->nama_pembelajaran?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Pertanyaan</label>
		<textarea class="md-input" id="pertanyaan" name="pertanyaan"></textarea> 
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Jawaban</label>
		<textarea class="md-input" id="jawaban" name="jawaban"></textarea> 
	</div>
	<div class="uk-modal-footer uk-text-right">
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#add_myform_essay").show(); 
				$("#before-edit-essay").hide();
			}, 100);
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#add_myform_essay').validate({
				rules:{
						id_pembelajaran:{
							required:true
						},
						id_pembelajaran:{
							required:true
						},
						jawaban:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$("#btnsave").prop('value', 'Process'); 
					$('input[type="submit"]').prop('disabled', true);
					$.post('<?php echo base_url('soal/save_essay'); ?>', $('#add_myform_essay').serialize(), function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#add_message-essay").html(msg);
							
							$('#add_message-essay').fadeTo(3000, 500).slideUp(500, function(){
								$('#add_message-essay').hide();
								reload_essay();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#add_message-essay").html(msg);
							$("#btnsave").prop('value', 'Save'); 
							$('input[type="submit"]').prop('disabled', false);
						}
					});
				}
			})
		});
</script>
