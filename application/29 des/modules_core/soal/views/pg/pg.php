<div id="delete_message"></div>
<table id="dt_default_pg" class="uk-table" cellspacing="0" width="80%">
	<thead width="800px">
		 <tr>
			<th rowspan="2">Pertanyaan</th>
			<th colspan="7"><b><?=$title?></b></th>
		</tr>
		<tr>
			<th width="">Pilihan A</th>
			<th width="">Pilihan B</th>
			<th width="">Pilihan C</th>
			<th width="">Pilihan D</th>
			<th width="">Pilihan E</th>
			<th width="">Jawaban</th>
			<th width="100px">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($soal){
	foreach($soal->result() as $sl){?>	
			<tr>
				<td width="150px"><?=$sl->pertanyaan?></td>
				<td><?=$sl->a?></td>
				<td><?=$sl->b?></td>
				<td><?=$sl->c?></td>
				<td><?=$sl->d?></td>
				<td><?=$sl->e?></td>
				<td><?=$sl->jawaban_pg?></td>
				<td>
					<a href="#edit_pg" data-uk-modal="{ center:true }" onclick="edit_pg('<?=$sl->id_bank_soal?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;
					<a href="#" onclick="delete_pg('<?=$sl->id_bank_soal?>')"class="uk-icon-button uk-icon-trash-o"></a>
				</td>
			</tr>	
	<?php }
	}?>		
	</tbody>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_pg" onclick="add_pg()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_pg">
			<div id="modal-add-sl"></div>
		</div>
		<div class="uk-modal" id="edit_pg">
			<div id="modal-edit-sl"></div>
		</div>
	</div> 
	<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>

	
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default_pg').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_pg(id_bank_soal){
				uri = '<?=base_url()."soal/edit_pg"?>';
				$('#new_pg').hide();
				$('#edit_pg').show();
				$('#modal-edit-sl').html(loading);
				$.post(uri,{ajax:true,id_bank_soal:id_bank_soal},function(data) {
					$('#modal-edit-sl').html('');
					$('#modal-edit-sl').html(data);
				});
			}
			
			function add_pg(){
				uri = '<?=base_url()."soal/add_pg"?>';
				$('#edit_pg').hide();
				$('#new_pg').show();
				$('#modal-add-sl').html(loading);
				$.post(uri,{ajax:true},function(data) {
					$('#modal-add-sl').html(data);
				});
			}
			
			function delete_pg(id){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."soal/delete_pg"?>';
					$.post(uri,{ajax:true,id_bank_soal:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reload_pg();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
	</script>