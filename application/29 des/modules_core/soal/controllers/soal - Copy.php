<?php
class soal extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->load->model(array(
            'soal/soal_model',
            'pegawai/pegawai_model',
            'periode/periode_model',
            'document/document_model',
            'class_m/class_model'
        ));
    }
    public function index()
    {
        $this->login_model->is_login();
        $data['title'] = 'Master Soal';
        $data['link']  = base_url() . "soal/list_data";
        $this->load->view('preview', $data);
    }
    
    public function list_data()
    {
        $this->login_model->is_login();
        $id_user       = $this->session->userdata("sesi_id_user");
        // $data['soal'] = $this->soal_model->get_list_soal_instruktur($id_user);
        $data['soal']  = $this->soal_model->get_list_soal();
        $data['title'] = 'Master Soal';
        $data['link']  = base_url() . "soal/list_data";
        $this->load->view('soal', $data);
    }
  /*
    public function list_data()
    {	
        $this->login_model->is_login();
        $id_user       = $this->session->userdata("sesi_id_user");
        // $data['soal'] = $this->soal_model->get_list_soal_instruktur($id_user);
        $data['soal']  = $this->soal_model->get_list_soal();
        $data['title'] = 'Master Soal';
        $data['link']  = base_url() . "soal/list_data";
        $this->load->view('soal', $data);
    }
    
    public function add()
    {
        $this->login_model->is_login();
        $id_user       = $this->session->userdata("sesi_id_user");
        //$data['id_pembelajaran'] = $this->input->post('id_pembelajaran');
        //$data['class'] 	 = $this->class_model-> get_list_class_inst($id_user);
        $data['class'] = $this->class_model->get_list_class();
        $data['title'] = 'Add Soal';
        $data['link']  = base_url() . "soal/list_data";
        $this->load->view('add', $data);
    }
    
    public function edit()
    {
        $this->login_model->is_login();
        $id_user         = $this->session->userdata("sesi_id_user");
        $id_pembelajaran         = $this->input->post('id_pembelajaran');
        $id_class        = $this->input->post('id_class');
        $data['soal']    = $this->soal_model->get_soal_instruktur_by_id($id_pembelajaran);
        $data['periode'] = $this->periode_model->get_periode_class($id_class);
        $data['class']   = $this->class_model->get_list_class($id_user);
        $data['title']   = 'Edit Nama Soal';
        $data['link']    = base_url() . "soal/list_data";
        $this->load->view('edit', $data);
    }
    
    public function update()
    {
        $this->login_model->is_login();
        $id_pembelajaran    = $this->input->post('id_pembelajaran');
        $soal       = $this->input->post('soal');
        $id_class   = $this->input->post('id_class');
        $id_periode = $this->input->post('id_periode');
        $id_user    = $this->session->userdata("sesi_id_user");
        $this->soal_model->update_soal($id_pembelajaran, $soal, $id_class, $id_periode, $id_user);
    }
    
    public function save()
    {
        $this->login_model->is_login();
        $id_pembelajaran    = 'SL' . date('ymdHis');
        $id_class   = $this->input->post('id_class');
        $id_periode = $this->input->post('id_periode');
        $soal       = $this->input->post('soal');
        $id_user    = $this->session->userdata("sesi_id_user");
        
        $this->soal_model->save_soal($id_pembelajaran, $soal, $id_class, $id_periode, $id_user);
    }
    
    function get_periode_class()
    {
        $id_class = $this->input->get('id_class');
        $data     = $this->periode_model->get_periode_class($id_class);
        $arrkab   = array();
        foreach ($data->result() as $list) {
            array_push($arrkab, $list);
        }
        echo json_encode($arrkab);
        exit;
    }
    
    function get_soal()
    {
        $id_class   = $this->input->get('id_class');
        $id_periode = $this->input->get('id_periode');
        $data       = $this->soal_model->get_soal_class_periode($id_class, $id_periode);
        $arrkab     = array();
        foreach ($data->result() as $list) {
            array_push($arrkab, $list);
        }
        echo json_encode($arrkab);
        exit;
    }
    
    public function delete()
    {
        $this->login_model->is_login();
        $id_pembelajaran = $this->input->post('id_pembelajaran');
        $this->soal_model->delete_soal($id_pembelajaran);
    }
    */
    public function details()
    {
        $this->login_model->is_login();
        $data['id_pembelajaran']   = $this->input->post('id_pembelajaran');
		$data['nama_pembelajaran'] = $this->input->post('nama_pembelajaran');
        $id_user         = $this->session->userdata("sesi_id_user");
        $data['title']   = 'Soal '.$data['nama_pembelajaran']; 
        $data['link']    = base_url() . "soal/detail";
        $this->load->view('detail', $data);
    }
    
    
    public function list_pg()
    {
        $this->login_model->is_login();
        $id_pembelajaran         = $this->input->post('id_pembelajaran');
        $data['soal']    = $this->soal_model->get_soal_pembelajaran($id_pembelajaran, 'pg');
        $data['title']   = 'Soal PG';
        $data['id_pembelajaran'] = $id_pembelajaran;
        $data['link']    = base_url() . "soal/list_pg";
        $this->load->view('pg/pg', $data);
    }
    
    public function add_pg()
    {
        $this->login_model->is_login();
        $data['id_pembelajaran'] = $this->input->post('id_pembelajaran');
        $data['title']   = 'Add Soal';
        $data['link']    = base_url() . "soal/list_pg";
        $this->load->view('pg/add', $data);
    }
    
    public function save_pg()
    {
        $this->login_model->is_login();
        $id_pembelajaran    = $this->input->post('id_pembelajaran');
        $pertanyaan = $this->input->post('pertanyaan');
        $jawaban    = $this->input->post('jawaban');
        $a          = $this->input->post('pilihan_a');
        $b          = $this->input->post('pilihan_b');
        $c          = $this->input->post('pilihan_c');
        $d          = $this->input->post('pilihan_d');
        $e          = $this->input->post('pilihan_e');
        $id_user    = $this->session->userdata("sesi_id_user");
        $this->soal_model->save_bank_soal($id_pembelajaran, 'pg', $pertanyaan, $a, $b, $c, $d, $e, $jawaban, '', $id_user);
    }
    
    public function edit_pg()
    {
        $this->login_model->is_login();
        $id_bank_soal  = $this->input->post('id_bank_soal');
        $data['bs']    = $this->soal_model->get_bank_soal_by_id($id_bank_soal);
        $data['title'] = 'Edit Soal';
        $data['link']  = base_url() . "soal/list_periode";
        $this->load->view('pg/edit', $data);
    }
    
    public function update_pg()
    {
        $this->login_model->is_login();
        $id_bank_soal = $this->input->post('id_bank_soal');
        $id_pembelajaran      = $this->input->post('id_pembelajaran');
        $pertanyaan   = $this->input->post('pertanyaan');
        $jawaban      = $this->input->post('jawaban');
        $a            = $this->input->post('pilihan_a');
        $b            = $this->input->post('pilihan_b');
        $c            = $this->input->post('pilihan_c');
        $d            = $this->input->post('pilihan_d');
        $e            = $this->input->post('pilihan_e');
        $id_user      = $this->session->userdata("sesi_id_user");
        $this->soal_model->update_bank_soal($id_bank_soal, $id_pembelajaran, 'pg', $pertanyaan, $a, $b, $c, $d, $e, $jawaban, '', $id_user);
    }
    
    public function delete_pg()
    {
        $this->login_model->is_login();
        $id = $id_bank_soal = $this->input->post('id_bank_soal');
        $this->soal_model->delete_bank_soal($id);
    }
    //essay
    public function list_essay()
    {
        $this->login_model->is_login();
		$id_pembelajaran = $this->input->post('id_pembelajaran');
        $data['soal']    = $this->soal_model->get_soal_pembelajaran($id_pembelajaran, 'essay');
        $data['title']   = 'Soal Essay';
        $data['id_pembelajaran'] = $id_pembelajaran;
        $data['link']    = base_url() . "soal/list_essay";
        $this->load->view('essay/essay', $data);
    }
    
    public function add_essay()
    {
        $this->login_model->is_login();
        $data['id_pembelajaran'] = $this->input->post('id_pembelajaran');
        $data['title']   = 'Add Soal';
        $data['link']    = base_url() . "soal/list_essay";
        $this->load->view('essay/add', $data);
    }
    
    public function save_essay()
    {
        $this->login_model->is_login();
        $id_pembelajaran    = $this->input->post('id_pembelajaran');
        $pertanyaan = $this->input->post('pertanyaan');
        $jawaban    = $this->input->post('jawaban');
        $e          = $this->input->post('pilihan_e');
        $id_user    = $this->session->userdata("sesi_id_user");
        $this->soal_model->save_bank_soal($id_pembelajaran, 'essay', $pertanyaan, '', '', '', '', '', '', $jawaban, $id_user);
    }
    
    public function edit_essay()
    {
        $this->login_model->is_login();
        $id_bank_soal  = $this->input->post('id_bank_soal');
        $data['bs']    = $this->soal_model->get_bank_soal_by_id($id_bank_soal);
        $data['title'] = 'Edit Soal';
        $data['link']  = base_url() . "soal/list_periode";
        $this->load->view('essay/edit', $data);
    }
    
    public function update_essay()
    {
        $this->login_model->is_login();
        $id_bank_soal = $this->input->post('id_bank_soal');
        $id_pembelajaran      = $this->input->post('id_pembelajaran');
        $pertanyaan   = $this->input->post('pertanyaan');
        $jawaban      = $this->input->post('jawaban');
        $id_user      = $this->session->userdata("sesi_id_user");
        $this->soal_model->update_bank_soal($id_bank_soal, $id_pembelajaran, 'essay', $pertanyaan, '', '', '', '', '', '', $jawaban, $id_user);
    }
    
    public function delete_essay()
    {
        $this->login_model->is_login();
        $id = $id_bank_soal = $this->input->post('id_bank_soal');
        $this->soal_model->delete_bank_soal($id);
    }
}

?>