<?php

class event_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	function getListEvent(){
		$sql = "SELECT *
				FROM m_event_calendar";
		return $this->db->query($sql);
	}
	
	function getListEventId($id){
		$sql = "SELECT *
				FROM m_event_calendar 
				WHERE event_id='$id'";
		$hasil = $this->db->query($sql);
		if($hasil->num_rows() > 0){
			return $hasil->row_array(); 							 
		}
	}
	

	function saveEvent($title, $start, $end, $input_by)
	{
		$sql="insert into m_event_calendar(title,start_date,end_date,created_by,created_date)
			  values('$title','$start','$end','$input_by',now())";
		$return = $this->db->query($sql);	
		if($return)
		{
			echo json_encode(array('response' => 'success', 'msg' => 'Berhasil'));		
		}else
		{
			echo json_encode(array('response' => 'failed', 'msg' => 'Gagal'));		
		}	
	}
	
	function updateEvent($event_id, $title, $start, $end, $updated_by)
    {
		$sql="UPDATE m_event_calendar 
			  SET title='$title',start_date='$start',end_date='$end',updated_date=now(), updated_by='$updated_by'
			  WHERE event_id='$event_id'";
		$return = $this->db->query($sql);	
		if($return)
		{
			echo json_encode(array('response' => 'success', 'msg' => 'Berhasil'));		
		}else
		{
			echo json_encode(array('response' => 'failed', 'msg' => 'Gagal'));		
		}	
	}
	
	function deleteEvent($id)
    {
		$sql="DELETE FROM m_event_calendar
			  WHERE event_id='$id'";
		$return = $this->db->query($sql);	
		if($return)
		{
			echo json_encode(array('response' => 'success', 'msg' => 'Berhasil'));		
		}else
		{
			echo json_encode(array('response' => 'failed', 'msg' => 'Gagal'));		
		}	
	}
            
}
