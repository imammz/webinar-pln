<?php

class my404 extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model(array('user/user_model','level/level_model'));
    }
    
    public function index()
	{ 	
		$this->login_model->is_login();
		$data['link']  = base_url() . "my404/list_data";
		$this->load->view('preview',$data);
	}
	
	public function list_data() 
	{
		$this->login_model->is_login();
		$this->load->view('404');        
    }
	
}

?>