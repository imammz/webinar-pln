<?php

class class_p extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->load->model(array('class_p/class_model','pegawai/pegawai_model','periode/periode_model','document/document_model'));
    }
    
    public function index()
	{ 	
		$this->login_model->is_login();
		$data['title'] 		='Master class';
		$data['link'] = base_url()."class_p/list_data"; 
		$this->load->view('preview',$data);
	}
	
	public function list_data() 
	{
		$this->login_model->is_login();
		$id_user		= $this->session->userdata("sesi_id_user");
		$data['class']	= $this->class_model->get_list_class_peserta($id_user);
		$data['title'] 	='Master Pembelajaran';
		$data['link'] 	= base_url()."class_p/list_data"; 
		$this->load->view('class', $data);        
    }
	
	public function add()
	{
		$this->login_model->is_login();
		$data['pegawai']= $this->pegawai_model->get_list_user_pegawai();
		$data['title'] 	= 'Add Class';
		$data['link'] 	= base_url()."class_p/list_data"; 
		$this->load->view('add', $data); 
	}
	
	public function edit()
	{
		$this->login_model->is_login();
		
		$id				= $this->input->post('id');
		$data['class']	= $this->class_model->get_class_by_id($id);
		$data['pegawai']= $this->pegawai_model->get_list_user_pegawai();
		$data['title'] 	= 'Edit Class';
		$data['link'] 	= base_url()."class_p/list_data"; 
		$this->load->view('edit', $data); 
	}
	
	public function detail(){
		$this->login_model->is_login();
		$id					= $this->input->post('id');
		$id_user			= $this->session->userdata("sesi_id_user");
		$data['id_class'] 	= $id;
		$data['class']		= $this->class_model->get_class_by_id($id);
		//$data['pegawai']	= $this->pegawai_model->get_list_user_pegawai();
		$data['friend']		= $this->pegawai_model->get_list_user_pegawai_class($id,$id_user);
		$data['title']  	= 'Detail Class '.$data['class']['nama_pembelajaran'];
		$data['link'] 		= base_url()."class_p/detail"; 
		$this->load->view('detail', $data); 
	}
	
	public function join(){
		$this->login_model->is_login();
		$id					= $this->uri->segment(3);
		$data['id_class'] 	= $id;
		$data['class']		= $this->class_model->get_class_by_id($id);
		$data['pegawai']	= $this->pegawai_model->get_list_user_pegawai();
		$data['title']  	= 'Detail Class '.$data['class']['class'];
		$data['link'] 		= base_url()."class_p/detail"; 
		$this->load->view('join', $data); 
	}
	
	public function update()
	{	
		$this->login_model->is_login();
		$id				= $this->input->post('id');
		$class_name		= $this->input->post('class_name');
		$keterangan 	= $this->input->post('keterangan');
		$instruktur1 	= $this->input->post('instruktur1');
		$instruktur2 	= $this->input->post('instruktur2');
		$start_date 	= $this->input->post('start_date');
		$end_date 		= $this->input->post('end_date');
		$lokasi 		= $this->input->post('lokasi');
		$id_user		= $this->session->userdata("sesi_id_user");
		$this->class_model->update_class($id,$class_name,$keterangan,$instruktur1,$instruktur2,$start_date,$end_date,$lokasi, $id_user);
	}
	
	public function save()
	{	
		$this->login_model->is_login();
		$class_name		= $this->input->post('class_name');
		$keterangan 	= $this->input->post('keterangan');
		$instruktur1 	= $this->input->post('instruktur1');
		$instruktur2 	= $this->input->post('instruktur2');
		$start_date 	= $this->input->post('start_date');
		$end_date 		= $this->input->post('end_date');
		$lokasi 		= $this->input->post('lokasi');
		$id_user		= $this->session->userdata("sesi_id_user");
		
		$this->class_model->save_class($class_name,$keterangan,$instruktur1,$instruktur2,$start_date,$end_date,$lokasi, $id_user);
	}
	
	//inactive aja jangan di delete
	public function delete()
	{	
		$this->login_model->is_login();
		$id	= $this->input->post('id');
		$this->class_model->delete_class($id);
	}
	
	public function download()
    {
		ini_set('memory_limit', '-1');
        $id_document = $this->uri->segment(3);
        $doc         = $this->document_model->get_document_by_id($id_document);
        $data        = file_get_contents($doc['path']);
        ini_set('memory_limit', '300M'); 
		set_time_limit(0);
		force_download($doc['file_name'], $data);
    }
	
}

?>