<!---CONTENT START-->	
   <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-7-10">
                    <div class="md-card">
                        <div class="user_heading">
                            <div class="user_heading_avatar">
								
                                <img src="<?=@$peg1['photo']?>"/>
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom">
									<span class="uk-text-truncate">
										
									</span>
								<span class="sub-heading"></span>
								</h2>
								<span class="sub-heading"></span>
                            </div>
                           <div class="uk-width-medium-1-6">
                               <br>
								<a class="md-btn md-btn-danger" target="_blank" href="class_p/join/<?=$class['id_class']?>">Join</a>
							</div>
							<div id="waiting"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
                        </div>
						<?php
							//$materi  = $this->document_model->get_materi_periode_class($class['id_class']);
							
						?>
						<div id="loading"></div>
                        <div class="user_content" id="user_contents" style="display:none">
                             <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1'}">
                                <li><a href="#">Information Class</a></li>
                                <li><a href="#">Materi</a></li>
                                <li id="video"><a href="#">Video</a></li>
								<li><a href="#">File</a></li>
                            </ul>
                            <ul id="tabs_1" class="uk-switcher uk-margin">
							<!-- informasi kelas -->
                                <li>
									<?=$class['keterangan']?>
									<div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Informasi Kontak Fasilitator</h4>
                                            <ul class="md-list md-list-addon">
												  <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE55B;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><span class="md-list-heading">Lokasi Class</span>
                                                        <span class="uk-text-small uk-text-muted"><?=$class['lokasi']?></span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><span class="md-list-heading"></span>
                                                        <span class="uk-text-small uk-text-muted">Email</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"></span>
                                                        <span class="uk-text-small uk-text-muted">Phone</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-facebook-official"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">facebook.com</span>
                                                        <span class="uk-text-small uk-text-muted">Facebook</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-twitter"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">twitter.com</span>
                                                        <span class="uk-text-small uk-text-muted">Twitter</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Persiapan Kelas</h4>
                                             <ul class="md-list md-list-addon">
												<li>
													<div class="md-list-addon-element">
														<input type="checkbox" data-md-icheck checked disabled/>
													</div>
													<div class="md-list-content">
														<span class="md-list-heading">Koneksi Internet 100Mbps</span>
													</div>
												</li>
												<li>
													<div class="md-list-addon-element">
														<input type="checkbox" data-md-icheck checked disabled/>
													</div>
													<div class="md-list-content">
														<span class="md-list-heading">Adobe Flash Player</span>
													</div>
												</li>
												<li>
													<div class="md-list-addon-element">
													   <input type="checkbox" data-md-icheck checked disabled/>
													</div>
													<div class="md-list-content">
														<span class="md-list-heading">Akun Gmail</span>
													</div>
												</li>
												 <li>
													<div class="md-list-addon-element">
													   <input type="checkbox" data-md-icheck checked disabled/>
													</div>
													<div class="md-list-content">
														<span class="md-list-heading">Akun Speedshare</span>
													</div>
												</li>
											</ul>
                                        </div>
                                    </div>
								</li>
							<!-- end informasi kelas -->
							<?php
								$periode = $this->periode_model->get_periode_class($class['id_class']);
								//$video   = $this->document_model->get_document_periode_class('video',$periode['id_periode']);
								//$file    = $this->document_model->get_document_periode_class('file',$periode['id_periode']);
							?>
							<!-- materi -->
                                <li>
									<div class="uk-accordion" data-uk-accordion="{showfirst: false}">
									<?php foreach($periode->result() as $per){
										//$video = $this->document_model->get_document_periode_class('materi',$per->id_periode);
										$video = $this->document_model->get_document_class_periode($class['id_class'],'materi',$per->id_periode)
									?>	
										<h3 class="uk-accordion-title"><?=$per->periode?></h3>
										<div class="uk-accordion-content">
											<table class="uk-table uk-table-hover">
												<thead>
													<tr>
														<th width="500px">Title</th>
														<th>Action</th>
													</tr>
												</thead>
												<tfoot>
											<?php foreach ($video->result() as $vid){?>	
													<tr>
														<td><?=$vid->file_name?></td>
														<td>
															<a class="md-btn md-btn-success" href="<?=base_url().'class_p/download/'.$vid->id_document?>">Download</a>
														</td>
													</tr>
											<?php }?>			
												</tfoot>
											</table>
										</div>
									<?php }?>	
									</div>
								</li>
							<!-- end materi -->	
							
							<!--video-->
                                <li>
									<div class="uk-accordion" data-uk-accordion="{showfirst: false}">
									<?php foreach($periode->result() as $per){
										//$video = $this->document_model->get_document_periode_class('video',$per->id_periode);
										$video = $this->document_model->get_document_class_periode($class['id_class'],'video',$per->id_periode)
									?>	
										<h3 class="uk-accordion-title"><?=$per->periode?></h3>
										<div class="uk-accordion-content">
											<table class="uk-table uk-table-hover">
												<thead>
													<tr>
														<th width="500px">Title</th>
														<th>Action</th>
													</tr>
												</thead>
												<tfoot>
											<?php foreach ($video->result() as $vid){?>	
													<tr>
														<td><?=$vid->file_name?></td>
														<td>
															<a class="md-btn md-btn-success" href="<?=base_url().'class_p/download/'.$vid->id_document?>">Download</a>
														</td>
													</tr>
											<?php }?>			
												</tfoot>
											</table>
										</div>
									<?php }?>	
									</div>
								</li>
							<!--end video-->	
							
							<!-- file-->
                                <li>
									<div class="uk-accordion" data-uk-accordion="{showfirst: false}">
									<?php foreach($periode->result() as $per){
										//$video = $this->document_model->get_document_periode_class('file',$per->id_periode);
										$video = $this->document_model->get_document_class_periode($class['id_class'],'file',$per->id_periode)
									?>	
										<h3 class="uk-accordion-title"><?=$per->periode?></h3>
										<div class="uk-accordion-content">
											<table class="uk-table uk-table-hover">
												<thead>
													<tr>
														<th width="500px">Title</th>
														<th>Action</th>
													</tr>
												</thead>
												<tfoot>
											<?php foreach ($video->result() as $vid){?>	
													<tr>
														<td><?=$vid->file_name?></td>
														<td>
															<a class="md-btn md-btn-success btn-xs" href="<?=base_url().'class_p/download/'.$vid->id_document?>">Download</a>
														</td>
													</tr>
											<?php }?>			
												</tfoot>
											</table>
										</div>
									<?php }?>	
									</div>
								</li>
							<!--end file-->	
								
                            </ul>
                        </div>
						
                    </div>
                </div>
                <div class="uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-margin-medium-bottom">
                            <h3 class="heading_c uk-margin-bottom">Peserta</h3>
                            <ul class="md-list md-list-addon uk-margin-bottom">
							<?php foreach ($friend->result() as $fr){?>
                                <li>
                                    <div class="md-list-addon-element">
                                        <img class="md-user-image md-list-addon-avatar" src="<?=$fr->photo?>" alt=""/>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"><?=$fr->nama?></span>
                                        <span class="uk-text-small uk-text-muted"><?=$fr->email?></span>
                                    </div>
                                </li>
							<?php }?>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
	<!-- common functions -->
    <script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?=base_url()?>front_assets/assets/js/uikit_custom.min.js"></script>
	<script>
		var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/></div>';
		$(document).ready(function() {
			//$('user_contents').uk-();
			//$('#video').addClass("uk-active");
			$('#loading').html(loading);
			$('#loading').fadeTo(500, 500).slideUp(500, function(){
				$('#waiting').hide();
				$('#loading').hide();
				$('#user_contents').show();
			});
			
		});
		
		
	</script>
<!---CONTENT END-->