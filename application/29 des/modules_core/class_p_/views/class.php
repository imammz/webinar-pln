
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
                <div class="md-card-content large-padding">
					<div id="delete_message"></div>
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2">Nama Pembelajaran</th>
                                <th colspan="4">Informasi Pembelajaran</th>
                            </tr>
                            <tr>
								<th>Lokasi</th>
								<th>Mulai</th>
								<th>Selesai</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
                        </thead>

                        <tbody>
					<?php foreach ($class->result() as $row){?>
                        <tr>
                            <td><?=$row->nama_pembelajaran?></td>
							<td><?=$row->lokasi?></td>
							<td><?=$row->start_date?></td>
							<td><?=$row->end_date?></td>
								<td>
							<?php if($row->status=='active'){?>
								<span class="uk-badge uk-badge-success">
							<?php }else if($row->status=='close'){?>
								<span class="uk-badge uk-badge-danger">
							<?php }else{?>
								<span class="uk-badge uk-badge-success">
							<?php } ?>
								<?php echo $row->status?></span>
                            </td>
                                <td>
								<!--<a href="#edit_class" data-uk-modal="{ center:true }" onclick="edits('<?php//$row->id_class?>')" class="uk-icon-button uk-icon-edit"></a>&nbsp;-->
								<a href="#" onclick="detail('<?=$row->id_class?>')" class="uk-icon-button uk-icon-users" title="detail"></a>&nbsp;
								
								<!--<a href="#" onclick="deleted('<?php //$row->id_class?>')"class="uk-icon-button uk-icon-trash-o"></a>-->
							</td>
                        </tr>
					<?php
					} ?>	
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
		<!--
		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_class" onclick="add()" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>-->
		<div class="uk-modal" id="new_class">
			<div id="modal-add"></div>
		</div>
		<div class="uk-modal" id="edit_class">
			<div id="modal-edit"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edits(id){
				uri = '<?=base_url()."class_p/edit"?>';
				$('#new_class').hide();
				$('#edit_class').show();
				$('#modal-edit').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-edit').html(data);
				});
			}
			
			function add(id){
				uri = '<?=base_url()."class_p/add"?>';
				$('#edit_class').hide();
				$('#new_class').show();
				$('#modal-add').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-add').html(data);
				});
			}
			
			function deleted(id){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."class_p/delete"?>';
					$.post(uri,{ajax:true,id:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
			function detail(id){
				$('#list_data').html(loading);
				$.post('<?=base_url()."class_p/detail"?>',{id:id},function(data) 
				{
					$('#list_data').html(data);
				});
			}
	</script>	
</html>