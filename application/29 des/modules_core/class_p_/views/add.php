<div class="uk-modal-dialog">
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
	<div id="new_message"></id>
</div>

<form class="form-horizontal" id="new_myform">
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Class</label>
		<input type="text" class="md-input" id="class_name" name="class_name"/>
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Keterangan</label>
		<input type="text" class="md-input" id="keterangan" name="keterangan"/>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Instruktur 1</label>
			<select id="instruktur1" name="instruktur1" data-md-selectize>
				<option value="0">Choose..</option>
				<?php 
				foreach ($pegawai->result() as $row){?>
					<option value="<?=$row->id_user?>"><?=$row->nama?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Instruktur 2</label>
			<select id="instruktur2" name="instruktur2" data-md-selectize>
				<option value="0">Choose..</option>
				<?php 
				foreach ($pegawai->result() as $row){?>
					<option value="<?=$row->id_user?>"><?=$row->nama?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Lokasi</label>
		<input type="text" class="md-input" id="lokasi" name="lokasi"/>
	</div>
	<div class="uk-margin-medium-bottom">
		<label class="uk-form">Start Date</label>
		<input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'YYYY-MM-DD'}">
	</div>

	<div class="uk-margin-medium-bottom">
		<label for="task_title">End Date</label>
		<input type="text" class="md-input" id="end_date" name="end_date" data-uk-datepicker="{format:'YYYY-MM-DD'}">
	</div>
	
	<div class="uk-modal-footer uk-text-right">
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>
<script>

		$(document).ready(function() {
				
			function reloadlist(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",function(data){
					$('#list_data').html(data);
				});
			}	
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#new_myform').validate({
				rules:{
						class_name:{
							required:true
						},
						 isactive:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$.post('<?php echo base_url('class_m/save'); ?>', $('#new_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						$('#btnsave').attr('disabled');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#new_message").html(msg);
							
							$('#new_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#new_message').hide();
								$('#new_user').hide();
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#new_message").html(msg);
						}
					});
				}
			})
		});
</script>
