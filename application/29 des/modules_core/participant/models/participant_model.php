<?php

class participant_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	function get_class_peserta($id_class){
		$sql = "SELECT  a.id_peserta,d.*,c.id_user FROM wb_peserta a, wb_class b,wb_users c,wb_pegawai d
				WHERE a.`id_class`=b.`id_class`
				AND a.`id_user`=c.`id_user`
				AND c.`nip`=d.`nip`
				AND b.id_class='$id_class'";
		return $this->db->query($sql);
	}
	
	function get_class_peserta_not($id_class){
		$sql = "SELECT  d.*,c.id_user FROM wb_users c,wb_pegawai d
					WHERE c.`nip`=d.`nip`
					AND c.id_user not in(
						select id_user from wb_peserta
						where id_class='$id_class'
					)";
		return $this->db->query($sql);
	}
	
	function save_class_peserta($id_class,$sesi_id_user,$id_user){
		$sql = "DELETE from wb_peserta
				WHERE id_class='$id_class'
				AND id_user='$id_user'"; 
		$this->db->query($sql);
		
		$sql = "insert into wb_peserta (id_class,id_user,created_by,created_date)
				values('$id_class','$id_user','$sesi_id_user',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo 'success';		
		}else{
			echo 'failed';		
		}
	}
	
	function delete_class_peserta($id_peserta){
		$sql ="DELETE from wb_peserta
			   where id_peserta='$id_peserta'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	/*
	function get_list_user($id_class){
		$sql = "select a.id_user,a.nip,b.nama,b.email 
				from wb_users a, wb_pegawai b
				where a.nip=b.nip
				and id_user not in
				(
				  select id_user from wb_peserta where id_class='$id_class'

				)
				and a.id_level not in(1,6)";
		return $this->db->query($sql);
	}
	
	function get_list_class_participant(){
		$sql = "select a.*,b.class,b.status,b.start_date,b.end_date 
				from wb_peserta a, wb_class b
				where a.id_class=b.id_class
				order by status";
		return $this->db->query($sql);
	}
	
	function get_list_class_participant_detail($id_class){
		$sql = "select a.*,b.nip,c.nama,c.email,c.telp,c.alamat
				from wb_peserta a, wb_users b, wb_pegawai c
				where a.id_user=b.id_user
				and b.nip=c.nip
				and a.id_class='$id_class'
				order by nama";
		return $this->db->query($sql);
	}
	
	function get_participant_by_id($id_peserta){
		$sql = "select * from wb_peserta
				where id_peserta='$id_peserta'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_participant_by_id2($id_peserta){
		$sql = "select a.*,c.nama,c.email
				from wb_peserta a, wb_users b, wb_pegawai c
				where a.id_user=b.id_user
				and b.nip=c.nip
				and a.id_peserta='$id_peserta'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function update_participant($id_peserta,$id_user,$id_class,$updated_by){
		$sql = "update wb_peserta 
			    set id_user='$id_user',id_class='$id_class',updated_by='$id_user',updated_date=now()
				where id_peserta='$id_peserta'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	
	function save_participant($id_peserta,$id_user,$id_class,$created_by){
		$sql = "insert into wb_peserta(id_peserta,id_user,id_class,created_by,created_date)
				values('$id_peserta','$id_user','$id_class','$created_by',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal disimpan'));		
		}
	}
	function delete_participant($id_peserta){
		$sql="delete from wb_peserta where id_peserta='$id_peserta'";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Data berhasil dihapus'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Data gagal dihapus'));		
		}
	}
	*/
}
