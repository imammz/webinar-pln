<?php
class participant extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->load->model(array(
            'participant/participant_model',
            'pegawai/pegawai_model',
            'periode/periode_model',
            'document/document_model',
            'class_m/class_model'
        ));
    }
    
    public function index()
    {
        $this->login_model->is_login();
        $data['title'] = 'Master Peserta';
        $data['link']  = base_url() . "participant/list_data";
        $this->load->view('preview', $data);
    }
    
    public function list_data()
    {
        $this->login_model->is_login();
        $id_user             = $this->session->userdata("sesi_id_user");
        $data['participant'] = $this->class_model->get_list_class();
        $data['title']       = 'Master participant';
        $data['link']        = base_url() . "participant/list_data";
        $this->load->view('participant', $data);
    }
    
    public function detail()
    {
        $this->login_model->is_login();
        $id_class            = $this->input->post('id_class');
        $id_user             = $this->session->userdata("sesi_id_user");
        $data['class']       = $this->class_model->get_class_name_by_id($id_class);
        $data['participant'] = $this->participant_model->get_list_class_participant_detail($id_class);
        $data['title']       = 'Detail Peserta Kelas ' . $data['class']['class_name'];
        $data['link']        = base_url() . "participant/detail";
        $data['id_class']    = $id_class;
        $this->load->view('detail', $data);
    }
    
    
    
    public function add()
    {
        $this->login_model->is_login();
        $id_class      = $this->input->post('id_class');
        $data['user']  = $this->participant_model->get_list_user($id_class);
        $data['class'] = $this->class_model->get_class_name_by_id($id_class);
        $data['title'] = 'Edit Peserta';
        $data['link']  = base_url() . "participant/detail";
        $this->load->view('add', $data);
    }
    
    public function save()
    {
        $this->login_model->is_login();
        $id_peserta = 'PST' . date('ymdHis');
        $id_user    = $this->input->post('id_user');
        $id_class   = $this->input->post('id_class');
        $created_by = $this->session->userdata("sesi_id_user");
        $this->participant_model->save_participant($id_peserta, $id_user, $id_class, $created_by);
    }
    
    public function edit()
    {
        $this->login_model->is_login();
        $id_peserta          = $this->input->post('id_peserta');
        $id_class            = $this->input->post('id_class');
        $data['participant'] = $this->participant_model->get_participant_by_id2($id_peserta);
        $data['user']        = $this->participant_model->get_list_user($id_class);
        $data['class']       = $this->class_model->get_class_name_by_id($id_class);
        $data['title']       = 'Edit Peserta';
        $data['link']        = base_url() . "participant/detail";
        $this->load->view('edit', $data);
    }
    
    public function update()
    {
        $this->login_model->is_login();
        $id_peserta = $this->input->post('id_peserta');
        $id_user    = $this->input->post('id_user');
        $id_class   = $this->input->post('id_class');
        $updated_by = $this->session->userdata("sesi_id_user");
        $this->participant_model->update_participant($id_peserta, $id_user, $id_class, $updated_by);
    }
    
    public function delete()
    {
        $this->login_model->is_login();
        $id = $id_peserta = $this->input->post('id_peserta');
        $this->participant_model->delete_participant($id);
    }
    
}

?>