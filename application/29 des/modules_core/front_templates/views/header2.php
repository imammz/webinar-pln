<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="<?=base_url()?>front_assets/assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?=base_url()?>front_assets/assets/img/favicon-32x32.png" sizes="32x32">
    <title>PLN Web Binar System</title>
	   <!-- fullcalendar -->
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/weather-icons/css/weather-icons.css" media="all">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/metrics-graphics/dist/metricsgraphics.css">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/chartist/dist/chartist.min.css">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/assets/icons/flags/flags.min.css" media="all">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/assets/css/main.min.css" media="all">
</head>
<body class="sidebar_main">
    <!---HEADER START-->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle">
                    <span class="sSwitchIcon"></span>
                </a>
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li data-uk-dropdown="{mode:'click'}">
                            <a href="#" class="user_action_image">Taufan Arfianto &nbsp;
							<img class="md-user-image" src="<?=base_url()?>front_assets/assets/img/avatars/avatar_11_tn.png" alt=""/></a>
                            <div class="uk-dropdown uk-dropdown-small uk-dropdown-flip">
                                <ul class="uk-nav js-uk-prevent">
                                   <li><a href="#">My profile</a></li>
                                    <li><a href="#">Settings</a></li>
                                    <li><a href="login.html">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
