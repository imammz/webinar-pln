<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no"/>
    <title>PLN Web Binar System|<?php echo $title?></title>
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/weather-icons/css/weather-icons.css" media="all">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/metrics-graphics/dist/metricsgraphics.css">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/chartist/dist/chartist.min.css">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/assets/icons/flags/flags.min.css" media="all">
    <link rel="stylesheet" href="<?=base_url()?>front_assets/assets/css/main.min.css" media="all">
	
	
</head>
<body class="sidebar_main_open sidebar_main_swipe">
    <!---HEADER START-->
    <header id="header_main">
        <div class="header_main_content">
            <nav class="uk-navbar">
                <!-- main sidebar switch -->
                <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                    <span class="sSwitchIcon"></span>
                </a>
                <div class="uk-navbar-flip">
                    <ul class="uk-navbar-nav user_actions">
                        <li data-uk-dropdown="{mode:'click'}">
                            <a href="#" class="user_action_image"><?= $this->session->userdata("sesi_nama")?> &nbsp;
							<img class="md-user-image" src="<?= base_url().str_replace("./","",$pegawai['photo'])?> " style="width: 35px;height: 35px;"/></a>
                            <div class="uk-dropdown uk-dropdown-small uk-dropdown-flip">
                                <ul class="uk-nav js-uk-prevent">
                                   <li><a href="profile">My profile</a></li>
                                    <li><a href="#edit_pass" data-uk-modal="{ center:true }" onclick="change_pass()"> Settings</a></li>
                                    <li><a href="<?=base_url()?>login/logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- main sidebar -->
    <aside id="sidebar_main">
        <div class="sidebar_main_header">
            <div class="sidebar_logo">
                <a href="" class="sidebar_hide"><img src="<?=base_url()?>front_assets/assets/img/logocorpu.png" alt="" height="75" width="130"/></a>
            </div>
            <div class="sidebar_actions">
			<a style="color:#666666">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Web Binar Platform</a>
            </div>
        </div>
        <div class="menu_section">
            <ul>

			<?php 
				foreach ($parent->result() as $row){
			?>
                <li title="<?php echo $row->menu_name?>" class="<?php if($this->uri->segment(1)==$row->menu_url){echo 'current_section';}?>">
                    <a href="<?=base_url().$row->menu_url;?>">
                        <span class="menu_icon"><i class="material-icons">&#<?php echo $row->menu_icon?>;</i></span>
						<span class="menu_title"><strong><?=$row->menu_name?></strong></span>
                    </a>
					
				<?php
					$child =  $this->menu_model->get_list_menu_child($this->session->userdata("sesi_id_level"),$row->id_menu);
					if($child->num_rows()>0){
					echo "<ul>";
					foreach ($child->result() as $row2){
				?>
				
                    <li title="<?=$row2->menu_name?>" class="<?php if($this->uri->segment(1).'/'.$this->uri->segment(2)==$row2->menu_url){echo 'act_item';}?>"><a href="<?=base_url().$row2->menu_url;?>">
						<!--<span class="menu_icon"><i class="uk-icon-<?php //echo $row2->menu_icon?> uk-icon-small"></i></span>-->
						<span class="menu_title"><?=$row2->menu_name?></span>
						</a>
					</li>
                    
				<?php
					}
					echo "</ul>";
					}
				?>
					
                </li>
			<?php }?>	
				<!--
				<li class="current_section">
                    <a href="<?=base_url()?>home">
                        <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                        <span class="menu_title">Dashboard</span>
                    </a>
                </li>
				<li>
                    <a href="#">
                         <span class="menu_icon"><i class="material-icons">&#xE873;</i></span>
                        <span class="menu_title">Document</span>
                    </a>
                    <ul>
                        <li><a href="<?=base_url()?>document/video">Video</a></li>
                        <li><a href="<?=base_url()?>document/file">File</a></li>
                    </ul>
                </li>
                <li  title="Chat">
                    <a href="<?=base_url()?>nilai">
                        <span class="menu_icon"><i class="material-icons">&#xE3DA;</i></span>
                        <span class="menu_title">Nilai</span>
                    </a>
                </li>
                <li title="Scrum Board">
                    <a href="<?=base_url()?>report">
                        <span class="menu_icon"><i class="material-icons">&#xE85C;</i></span>
                        <span class="menu_title">Report</span>
                    </a>
                </li>-->
            </ul>
        </div>
		<div class="uk-modal" id="edit_pass">
		<div id="modal-edit-pass"></div>
	</div>
    </aside>
	
	<script>
		var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
		function change_pass(){
			uri = '<?=base_url()."profile/change_password"?>';
		$('#modal-edit-pass').show();	
		$('#modal-edit-pass').html(loading);
		$.post(uri,{ajax:true},function(data) {
			
			$('#modal-edit-pass').html(data);
		});
		}
	</script>
<!---HEADER END-->