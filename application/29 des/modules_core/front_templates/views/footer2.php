<!-- common functions -->
    <script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?=base_url()?>front_assets/assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
    <!-- fullcalendar -->
    <script src="<?=base_url()?>front_assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>front_assets/webcam/webcam.js"></script>
    <!--  calendar functions -->
    <script src="<?=base_url()?>front_assets/assets/js/pages/plugins_fullcalendar.min.js"></script>
    <!-- Configure a few settings and attach camera -->
	<script language="JavaScript">
		Webcam.set({
			width: 320,
			height: 240,
			image_format: 'jpeg',
			jpeg_quality: 90
		});
		Webcam.attach( '#my_camera' );
	</script>
	
		<!-- Code to handle taking the snapshot and displaying it locally -->
	<script language="JavaScript">
		function take_snapshot() {
			// take snapshot and get image data
			Webcam.snap( function(data_uri) {
				// display results in page
				document.getElementById('results').innerHTML = 
					'<h2>Here is your image:</h2>' + 
					'<img src="'+data_uri+'"/>';
			} );
		}
	</script>
	
</body>
