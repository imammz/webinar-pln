<?php

class periode_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	/*
	function get_class_periode_inst($id_class){
		$sql ="SELECT DISTINCT * FROM (
				SELECT c.`id_user`,b.`id_class`,e.`nama_pembelajaran`,a.`periode`,a.`start_date`,a.`end_date`,d.nama
				FROM wb_periode a, wb_class b, wb_users c,wb_pegawai d, wb_pembelajaran e
				WHERE a.`id_class`=b.`id_class`
				AND a.`instruktur1` =c.`id_user`
				AND b.`id_pembelajaran`=e.`id_pembelajaran`
				AND d.`nip`=c.`nip`
				UNION ALL
				SELECT c.`id_user`,b.`id_class`,e.`nama_pembelajaran`,a.`periode`,a.`start_date`,a.`end_date`,d.nama
				FROM wb_periode a, wb_class b, wb_users c,wb_pegawai d,wb_pembelajaran e
				WHERE a.`id_class`=b.`id_class`
				AND a.`instruktur2` =c.`id_user`
				AND b.`id_pembelajaran`=e.`id_pembelajaran`
				AND d.`nip`=c.`nip`)xx
				WHERE xx.id_user=5
				AND xx.id_class = '$id_class'";
		return $this->db->query($sql);
	}*/
	
	public function get_periode_class($id_class)
	{
		$sql=" select id_periode,periode,instruktur1,instruktur2,start_date,end_date,id_class,id_schedule,created_by,created_date,updated_by,updated_date,
			   case when start_date <= DATE_FORMAT(now(),'%Y-%m-%d') and end_date >= DATE_FORMAT(now(),'%Y-%m-%d')
						then 'inprogress'
					 when start_date > DATE_FORMAT(now(),'%Y-%m-%d')
						then 'schedule'
					 when end_date < DATE_FORMAT(now(),'%Y-%m-%d')
						then 'finish'
				end status,DATE_FORMAT(now(),'%Y-%m-%d')
			   from wb_periode
				where id_class='$id_class'
				order by created_date";
		return $this->db->query($sql);
	}

	
	function get_list_periode(){
		$sql = "SELECT * FROM wb_periode";
		return $this->db->query($sql);
	}
	
	
	
	function get_periode_by_id($id){
		$sql = "SELECT * FROM wb_periode
				WHERE id_periode='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function delete_periode($id_periode){
		$sql="DELETE FROM wb_periode
			  WHERE id_periode='$id_periode'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Periode berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Periode gagal didelete'));		
		}		  
	}
	
	function save_periode($id_periode,$id_class,$periode,$start_date,$end_date,$instruktur1,$instruktur2,$id_user){
		$sql="insert into wb_periode (id_periode,instruktur1,instruktur2,id_class,periode,start_date,end_date,created_by,created_date)
			  values('$id_periode','$instruktur1','$instruktur2','$id_class','$periode','$start_date','$end_date','$id_user',now())";
		$return =  $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Periode berhasil simpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Periode gagal disimpan'));		
		}	
	}
	
	function save_periode_new($id_periode,$id_class,$instruktur1,$instruktur2,$periode,$start_date,$end_date,$id_user){
		$sql="insert into wb_periode (id_periode,id_class,instruktur1,instruktur2,periode,start_date,end_date,created_by,created_date)
			  values('$id_periode','$id_class','$instruktur1','$instruktur2','$periode','$start_date','$end_date','$id_user',now())";
		return  $this->db->query($sql);	
	}
	
	function update_periode($id_periode,$periode,$start_date,$end_date,$instruktur1,$instruktur2,$id_user){
	   $sql="update wb_periode set periode='$periode',start_date='$start_date',
			  end_date='$end_date',instruktur1='$instruktur1',instruktur2='$instruktur2',
			  updated_by='$id_user',updated_date=now()
			  where id_periode = '$id_periode'";
		$return =  $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'Periode berhasil diupdate'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'Periode gagal diupdate'));		
		}	
	}
	
	
	
}
