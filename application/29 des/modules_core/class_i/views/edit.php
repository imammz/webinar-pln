<style>
	.uk-datepicker { width: 20em; padding: .2em .2em 0; display: none; z-index: 2000 !important;}
</style>
<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
	<div id="edit_message"></id>
</div>
<div id="before-edit-class"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="edit_myform" style="display:none">
	
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Instruktur 1</label>
			<select id="id_class_name" name="id_class_name" data-md-selectize>
				<option value="0">Pilih ...</option>
				<?php 
				foreach ($pembelajaran->result() as $row){?>
					<option value="<?=$row->id_pembelajaran?>" <?php echo $class['id_pembelajaran']==$row->id_pembelajaran ? 'selected':''?>><?=$row->nama_pembelajaran?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Keterangan</label>
		<input type="text" class="md-input" id="keterangan" name="keterangan" value="<?=$class['keterangan']?>"/>
	</div>
	<!--
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Instruktur 1</label>
			<select id="instruktur1" name="instruktur1" data-md-selectize>
				<option value="0">Choose..</option>
				<?php /*
				foreach ($pegawai->result() as $row){?>
					<option value="<?=$row->id_user?>" <?php echo $class['instruktur1']==$row->id_user ? 'selected':''?>><?=$row->nama?></option>
					
				<?php }*/?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Instruktur 2</label>
			<select id="instruktur2" name="instruktur2" data-md-selectize>
				<option value="0">Choose..</option>
				<?php 
				/*foreach ($pegawai->result() as $row){?>
					<option value="<?=$row->id_user?>" <?php echo $class['instruktur2']==$row->id_user ? 'selected':''?>><?=$row->nama?></option>
					
				<?php }*/?>
			</select>
		</div>
	</div>-->
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Lokasi</label>
		<input type="text" class="md-input" id="lokasi" name="lokasi" value="<?=$class['lokasi']?>"/>
	</div>
	<div class="uk-margin-medium-bottom">
		<label class="uk-form">Start Date</label>
		<input type="text" class="md-input" id="start_date" name="start_date" value="<?=$class['start_date']?>" 
		data-uk-datepicker="{format:'YYYY-MM-DD'}">
	</div>

	<div class="uk-margin-medium-bottom">
		<label for="task_title">End Date</label>
		<input type="text" class="md-input" id="end_date" name="end_date" value="<?=$class['end_date']?>" 
		data-uk-datepicker="{format:'YYYY-MM-DD'}">
	</div>
	
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id" value="<?=$class['id_class']?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnupdate" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#edit_myform").show(); 
				$("#before-edit-class").hide();
			}, 200);
			function reloadlist(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",function(data){
					$('#list_data').html(data);
				});
			}	
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#edit_myform').validate({
				rules:{
						id_class_name:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$.post('<?php echo base_url('class_m/update'); ?>', $('#edit_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						$('#btnupdate').attr('disabled');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#edit_message").html(msg);
							
							$('#edit_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#edit_message').hide();
								$('#edit_user').hide();
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#edit_message").html(msg);
						}
					});
				}
			})
		});
</script>
