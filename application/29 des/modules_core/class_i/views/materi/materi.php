<div id="delete_message_materi"></div>
<table id="dt_default_materi" class="uk-table" cellspacing="0" width="100%">
	<thead>
		 <tr>
			<th rowspan="2" width="">Periode</th>
			<th colspan="1"><b>Master Materi</b></th>
		</tr>
		<tr>
			<th width="">Nama File</th>
			<th width="">Keterangan	</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($materi){
	foreach($materi->result() as $mat){?>	
			<tr>
				<td><?=$mat->periode?></td>
				<td><a href="<?=base_url().'class_i/download/'.$mat->id_document?>" alt="Download"><?=$mat->file_name?></a></td>
				
				<td><?=$mat->document?></td>
			</tr>	
	<?php }
	}?>		
	</tbody>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_materi" onclick="add_materi('<?=$id_class?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_materi">
		
				<div id="modal-add-mtr"></div>
		</div>
		<div class="uk-modal" id="edit_materi">
			<button class="uk-modal-close uk-close" type="button"></button>
			<div id="modal-edit-mtr"></div>
		</div>
	</div> 
	
	<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>
	
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default_materi').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_materi(id_document_class){
				uri = '<?=base_url()."class_i/edit_materi"?>';
				$('#modal-edit-mtr').html(loading);
				$.post(uri,{ajax:true,id_document_class:id_document_class,id_class:'<?=$id_class?>'},function(data) {
					$('#modal-edit-mtr').html(data);
				});
			}
			
			function add_materi(id_class){
				$('#modal-add-mtr').html(loading);
				uri = '<?=base_url()."class_i/add_materi"?>';
				$.post(uri,{ajax:true,id_class:id_class},function(data) {
					$('#modal-add-mtr').html(data);
				});
			}
			
			function delete_materi(id_document_class){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."class_i/delete_materi"?>';
					$.post(uri,{ajax:true,id_document_class:id_document_class},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message_materi").html(msg);
							
							$('#delete_message_materi').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message_materi').hide();
								
								reload_materi();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message_materi").html(msg);
						}
					});
				} 
			}
			
	</script>