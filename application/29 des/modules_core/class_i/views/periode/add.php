<style>
	.uk-datepicker { width: 20em; padding: .2em .2em 0; display: none; z-index: 2000 !important;}
</style>

<div class="uk-modal-dialog">
<button class="uk-modal-close uk-close" type="button"></button>
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
	<div id="new_message"></id>
</div>
<div id="before-add-periode"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
<form class="form-horizontal" id="new_myform" style="display:none">
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Periode</label>
		<input type="text" class="md-input" id="periode" name="periode"/>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Instruktur 1</label>
			<select id="instruktur1" name="instruktur1" data-md-selectize>
				<option value="0">Choose..</option>
				<?php 
				foreach ($pegawai->result() as $row){?>
					<option value="<?=$row->id_user?>"><?=$row->nama?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Instruktur 2</label>
			<select id="instruktur2" name="instruktur2" data-md-selectize>
				<option value="0">Choose..</option>
				<?php 
				foreach ($pegawai->result() as $row){?>
					<option value="<?=$row->id_user?>"><?=$row->nama?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		<label class="uk-form">Start Date</label>
		<input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'YYYY-MM-DD'}">
	</div>

	<div class="uk-margin-medium-bottom">
		<label for="task_title">End Date</label>
		<input type="text" class="md-input" id="end_date" name="end_date" data-uk-datepicker="{format:'YYYY-MM-DD'}">
	</div>
	
	<div class="uk-modal-footer uk-text-right">
		<input type="hidden" name="id_class" value="<?=$id_class?>"/>
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave_periode" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<!--
<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>-->
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>

 
<script>

		$(document).ready(function() {
			setTimeout(function() { 
				$("#new_myform").show(); 
				$("#before-add-periode").hide();
			}, 100);
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#new_myform').validate({
				rules:{
						periode:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$("#btnsave_periode").prop('value', 'Process'); 
					$('input[type="submit"]').prop('disabled', true);
					$.post('<?php echo base_url('class_m/save_periode'); ?>', $('#new_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						$('#btnsave_periode').attr('disabled');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#new_message").html(msg);
							$('#new_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#new_message').hide();
								//$('#new_user').hide();
								reload_periode();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#new_message").html(msg);
							$("#btnsave_periode").prop('value', 'Save'); 
							$('input[type="submit"]').prop('disabled', false);
						}
					});
				}
			})
		});
</script>
