
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom"><?=$title?></h3>
            <div class="md-card">
				
                <div class="md-card-content large-padding">
					<div id="edit_message"></div>
					<form id="edit_myform">
					
					<button type="button" onclick="updated()" class="md-btn md-btn-warning">Save</button>
					<input type="hidden" name="id" value="<?=$id_level?>"/>
					
                    <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                        <thead>
							 <tr>
                                <th rowspan="2">Menu Name</th>
                                <th colspan="5">Menu Information</th>
                            </tr>
                            <tr>
								<th>Menu Parent</th>
                                <th>Url</th>
                                <th>Activated</th>
                            </tr>
                        </thead>

                        <tbody>
						
					<?php
					
					foreach ($parent->result() as $row){?>	
                        <tr>
                            <td><strong><?=$row->menu_name?></strong></td>
                            <td><strong></strong></td>
							<td><strong><?=$row->menu_url?></strong></td>
                            <td class="hidden-phone "><input type="checkbox" name="menu[]" value="<?=$row->id_menu.'|'.$row->id_level;?>" <?php 	 echo $row->isactive==1 ? 'checked':'';?>> Active
							</td>
                        </tr>
					<?php
						if($row->id_parent==0){
							$child = $this->menu_model->get_list_menu_child_role($id_level,$row->id_menu);
							foreach ($child->result() as $row2){
					?>
						<tr>
                            <td><?=$row2->menu_name?></td>
							<td><?=$row->menu_name?></td>
                            <td><?=$row2->menu_url?></td>
                            <td class="hidden-phone"><input type="checkbox" name="menu[]" value="<?=$row2->id_menu.'|'.$row2->id_level;?>" <?php 		echo $row2->isactive==1 ? 'checked':'';?>> Active
							</td>
                        </tr>
					<?php					
							}
						}
					} ?>	
                        </tbody>
                    </table>
					</form>
                </div>
            </div>

        </div>
    </div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				$('#dt_default').dataTable({
					"bPaginate": false					
				});
				
			});	
		
			function updated(){
				$.post('<?php echo base_url('level/update_mapping'); ?>', $('#edit_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#edit_message").html(msg);
							
							$('#edit_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#edit_message').hide();
								$('#list_data').html(loading);
								$.post("<?=$link?>",{id:<?=$id_level?>},function(data){
									$('#list_data').html(data);
								});
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#edit_message").html(msg);
						}
					});
			}
	</script>	
</html>