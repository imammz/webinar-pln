<?php

class Login extends MX_Controller
{
    function __construct()
    {
        parent:: __construct();
		$this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
		$this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
    }
    
    public function index()
	{		
		$this->login_model->is_login();
		$data = array();
		$data['title'] = 'Login';
		$this->load->view('login',$data);
	}
	
	public function process(){
	    $username = mysql_real_escape_string($this->input->post('username'));
		$password = mysql_real_escape_string($this->input->post('password'));
		
		$lock     = $this->login_model->check_locked($username);
		
		if($lock['username']==$username && $lock['isactive']==0){
			$result = json_encode(array('status' => 'error', 'msg' => 'User anda terkunci, hubungi administrator untuk mengaktifkan'));
			die($result);
		}
		
		if($lock['username']!=$username){
			$result = json_encode(array('status' => 'error', 'msg' => 'User anda tidak terdaftar'));
			die($result);
		}
		
		$data 	  = $this->login_model->get_user_login($username,$this->fungsi->Md5AddSecret($password));
		$sesion   = date('YmdHis');
		$sesion   = $this->fungsi->Md5AddSecret($sesion);
		if($data){
			$arsesi = array(
					'sesi_islogin' => TRUE,
					'sesi_id_sesion' => $sesion,
					'sesi_id_level' => $data['id_level'],
					'sesi_id_user' => $data['id_user'],
					'sesi_nip' => $data['nip'],
					'sesi_nama' => $data['nama']
				);
				
		    $this->login_model->update_sesion($sesion,$data['id_user']);	
		    $this->session->set_userdata($arsesi);	
		    echo json_encode(array('status' => 'success', 'msg' => 'success'));		
		}else{
			echo json_encode(array('status' => 'error', 'msg' => 'Username atau Password salah'));	
		}
	}
	
	function logout(){
		$this->session->sess_destroy();
		$this->login_model->update_sesion_logout($this->session->userdata('sesi_id_user'));
		redirect('/login', 'refresh');
	}
        

}

?>