<?php
class exam_schd extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->load->model(array(
            'exam_schd/exam_schd_model',
            'pegawai/pegawai_model',
            'periode/periode_model',
            'document/document_model',
			'soal/soal_model',
			'class_m/class_model'
        ));
    }
    
    public function index()
    {
        $this->login_model->is_login();
        $data['title'] = 'Master exam_schd';
        $data['link']  = base_url() . "exam_schd/list_data";
        $this->load->view('preview', $data);
    }
    
    public function list_data()
    {
        $this->login_model->is_login();
		$id_user       = $this->session->userdata("sesi_id_user");
        $data['exam_schd'] = $this->exam_schd_model->get_list_examp_schd();
		//jika perserta
		$id_level = $this->session->userdata('sesi_id_level');
		if($id_level==2){
			$data['exam_schd'] = $this->exam_schd_model->get_list_examp_schd_peserta($id_user);
		}
        $data['title'] = 'Master Jadwal Ujian';
        $data['link']  = base_url() . "exam_schd/list_data";
        $this->load->view('exam_schd', $data);
    }
    
    public function add()
    {	
		$this->login_model->is_login(); 
		$id_user     	 = $this->session->userdata("sesi_id_user");
		$data['class'] 	   = $this->exam_schd_model->get_list_class();
		$data['periode'] = $this->periode_model->get_list_periode();
        $data['title']   = 'Add Jadwal Ujian';
        $data['link']    = base_url() . "exam_schd/list_data";
        $this->load->view('add', $data);
    }
    
    public function edit()
    {
        $this->login_model->is_login(); 
		$id_user     	 = $this->session->userdata("sesi_id_user");
        $id_jadwal_ujian     = $this->input->post('id_jadwal_ujian');
		
        $data['exam_schd'] = $this->exam_schd_model->get_list_exam_schd_by_id($id_jadwal_ujian);
		$data['class'] 	   = $this->exam_schd_model->get_list_class();
		$data['periode'] = $this->periode_model->get_list_periode();
        $data['title']   = 'Edit Jadwal Ujian';
        $data['link']    = base_url() . "exam_schd/list_data";
        $this->load->view('edit', $data);
    }
	
    
    public function update()
    {
        $this->login_model->is_login();
        $id_jadwal_ujian     = $this->input->post('id_jadwal_ujian');
		$nama_ujian = $this->input->post('nama_ujian');
        $id_class  	 = $this->input->post('id_class');
        $id_periode  = $this->input->post('id_periode');
        $durasi  = $this->input->post('durasi');
		$start_date  = $this->input->post('start_date');
        $id_user     = $this->session->userdata("sesi_id_user");
        $this->exam_schd_model->update_exam_schd($id_jadwal_ujian,$nama_ujian,$id_periode,$durasi,$start_date,$id_user);
    }
    
    public function save()
    {
        $this->login_model->is_login();
		$nama_ujian = $this->input->post('nama_ujian');
        $id_class  	 = $this->input->post('id_class');
        $id_periode  = $this->input->post('id_periode');
        $durasi  = $this->input->post('durasi');
		$start_date  = $this->input->post('start_date');
        $id_user     = $this->session->userdata("sesi_id_user");
        $this->exam_schd_model->save_exam_schd($nama_ujian,$id_periode,$durasi,$start_date,$id_user);
    }
	
	function get_periode_class(){
		$id_class       = $this->input->post('id_class');
		$data           = $this->periode_model->get_periode_class($id_class);
        $arrkab         = array();
        foreach ($data->result() as $list) {
            array_push($arrkab, $list);
        }
        echo json_encode($arrkab);
        exit;
	}
	
	public function delete()
    {
        $this->login_model->is_login();
        $id_jadwal_ujian = $this->input->post('id_jadwal_ujian');
        $this->exam_schd_model->delete_exam_schd($id_jadwal_ujian);
    }
    
}

?>