<div class="uk-modal-dialog">
<div class="uk-modal-header">
	<h3 class="uk-modal-title"><?=$title?></h3>
	<div id="add_message"></id>
</div>

<form class="form-horizontal" id="add_myform">
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Nama</label>
			<select id="nip" name="nip" data-md-selectize>
				<option value="0">Choose..</option>
				<?php 
				foreach ($pegawai->result() as $row){?>
					<option value="<?=$row->nip?>"><?=$row->nama?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	
	<div class="uk-margin-medium-bottom">
		<label for="task_title">Username</label>
		<input type="text" class="md-input" id="username" name="username"/>
	</div>

	<div class="uk-margin-medium-bottom">
		<label for="task_title">password</label>
		<input type="password" class="md-input" id="passwords" name="passwords"/>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="" class="uk-form-label">Level</label>
			<select id="id_level" name="id_level" data-md-selectize>
				<option value="0">Choose..</option>
				<?php 
				foreach ($level->result() as $row){?>
					<option value="<?=$row->id_level?>"><?=$row->level_name?></option>
					
				<?php }?>
			</select>
		</div>
	</div>
	<div class="uk-margin-medium-bottom">
		 <div class="parsley-row">
			<label for="val_select" class="uk-form-label">Active</label>
			<select id="isactive" name="isactive" required data-md-selectize>
				<option value="">Choose..</option>
				<option value="1">Active</option>
				<option value="0">Inactiva</option>
			</select>
		</div>
	</div>
	<div class="uk-modal-footer uk-text-right">
		<button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
		<input type="submit" id="btnsave" class="md-btn md-btn-flat md-btn-flat-primary" value="Save">
	</div>
</form>
</div>
<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>
<script src="<?=base_url()?>front_assets/assets/js/altair_admin_common.min.js"></script>
<script>

		$(document).ready(function() {
				
			function reloadlist(){
				$('#list_data').html(loading);
				$.post("<?=$link?>",function(data){
					$('#list_data').html(data);
				});
			}	
			
			jQuery.validator.setDefaults({
			  debug: true,
			  success: "valid"
			});
			
			$('#add_myform').validate({
				rules:{
						username:{
							required:true
						},
						 nip:{
							required:true
						},
						 passwords:{
							required:true
						},
						 id_level:{
							required:true
						},
						 isactive:{
							required:true
						}
					  },
						highlight: function(element) {
							$(element).closest('.input-append lock-input').addClass('uk-alert');
					  },
						unhighlight: function(element) {
							$(element).closest('.metro double-size green').removeClass('uk-alert');
					 },
						errorElement: 'span',
						errorClass: 'uk-text-danger',
						errorPlacement: function(error, element) {
							if(element.parent('.input-group').length) {
								error.insertAfter(element.parent());
							} else {
								error.insertAfter(element);
							}
					},
					
				submitHandler: function (form) {
					$.post('<?php echo base_url('user/save'); ?>', $('#add_myform').serialize(), function(data) {
						var data = eval('('+ data + ')');
						$('#btnsave').attr('disabled');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#add_message").html(msg);
							
							$('#add_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#add_message').hide();
								$('#add_user').hide();
								reloadlist();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#add_message").html(msg);
						}
					});
				}
			})
		});
</script>
