<?php

class pegawai_model extends  CI_Model {
	/**
	 * Constructor
	 */
	function __construct()
         {
        parent::__construct();
        
	}
	
	function get_pegawai_by_id($id){
		$sql = "SELECT * FROM wb_pegawai
				WHERE id_pegawai='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_pegawai_by_uid($id){
		$sql = "SELECT * FROM wb_pegawai a,wb_users b
				WHERE a.nip=b.nip
				and b.id_user='$id'";
		$hasil = $this->db->query($sql);	
		if($hasil->num_rows() > 0){
			return $hasil->row_array();							 
		}		
	}
	
	function get_list_user_pegawai(){
		$sql = "SELECT * FROM wb_pegawai a,wb_users b
				WHERE a.nip=b.nip";
		return $this->db->query($sql);			
	}
	
	function get_list_user_instruktur(){
		$sql = "SELECT * FROM wb_pegawai a,wb_users b
				WHERE a.nip=b.nip
				and b.id_level=6";
		return $this->db->query($sql);			
	}
	
	function get_list_user_pegawai_class($class,$id_pegawai){
		$sql = "SELECT * FROM wb_pegawai a,wb_users b,wb_peserta c
				WHERE a.nip=b.nip
				and c.id_user=b.id_user
				and c.id_class='$class'
				and b.id_user <>$id_pegawai";
		return $this->db->query($sql);			
	}
	
	/*
	
	function get_list_pegawai(){
		$sql = "SELECT a.*,b.nama,c.level_name 
				FROM wb_pegawais a, wb_pegawai b, wb_level c
				where a.nip=b.nip
				and a.id_level=c.id_level";
		return $this->db->query($sql);
	}
	

	
	function get_list_pegawai2(){
		$sql = "SELECT * 
				FROM wb_pegawai";
		return $this->db->query($sql);
	}
	
	
	function save_pegawai($username,$nip,$isactive,$passwords,$id_level,$id_pegawai){
		$sql="insert into wb_pegawais(username,nip,passwords,id_level,isactive,created_by,created_date)
			  values('$username','$nip','$passwords','$id_level','$isactive','$id_pegawai',now())";
		$return = $this->db->query($sql);	
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal disimpan'));		
		}
		
	}
	*/

	
	/*
	function update_pegawai($id,$username,$isactive,$passwords,$id_level,$id_pegawai){
		$sql="update wb_pegawais set username='$username',passwords='$passwords',
			  id_level='$id_level',isactive='$isactive',updated_by='$id_pegawai',updated_date=now()
			  WHERE id_pegawai='$id'";
		$return = $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil disimpan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal di simpan'));		
		}		  
	
	}
	
	
	function delete_pegawai($id_pegawai){
		$sql="DELETE FROM wb_pegawais 
			  WHERE id_pegawai='$id_pegawai'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil didelete'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal didelete'));		
		}		  
	}
	
	function inactive_pegawai($id_pegawai){
		$sql="update wb_pegawais set isactive='0'
			  WHERE id_pegawai='$id_pegawai'";
		$return =  $this->db->query($sql);	
		
		if($return){
			echo json_encode(array('response' => 'success', 'msg' => 'User berhasil di nonaktifkan'));		
		}else{
			echo json_encode(array('response' => 'failed', 'msg' => 'User gagal di nonaktifkan'));		
		}		  
	}
	
	*/
}
