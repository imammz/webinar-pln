<!---CONTENT START-->	
   <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large">
                    <div class="md-card">
                        <div class="user_heading">
                            <div class="user_heading_avatar">
                                <img src="<?=@$peg1['photo']?>"/>
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b uk-margin-bottom">
									<span class="uk-text-truncate">
										<?php
											//$peg1 = $this->pegawai_model->get_pegawai_by_uid($class['instruktur1']);
											//echo $peg1['nama'];
										?>
									</span>
								<span class="sub-heading"><?php //=$peg1['skill']?></span>
								</h2>
								<!--<span class="sub-heading">Kepala Pengembangan Sub PLN Direktorat dsa</span>-->
                            </div>
                           <div class="uk-width-medium-1-3">
								<a class="md-btn md-btn-danger" target="_blank" href="class_p/join/<?=$class['id_class']?>">Join</a>
							</div>
							<div id="waiting"><div align="center"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif"> </div></div>
                        </div>
						<?php
							//$materi  = $this->document_model->get_materi_periode_class($class['id_class']);
							
						?>
						<div id="loading"></div>
                        <div class="user_content" id="user_contents" style="display:none">
                             <ul class="uk-tab" data-uk-tab="{connect:'#tabs_1'}">
                                <li><a href="#">Information Class</a></li>
								<li><a href="#" onclick="reload_page('periode')">Periode</a></li>
                                <li><a href="#" onclick="reload_page('materi')">Materi</a></li>
                                <li><a href="#" onclick="reload_page('video')">Video</a></li>
								<li><a href="#" onclick="reload_page('file')">File</a></li>
								<li><a href="#" onclick="reload_page('peserta')">Peserta</a></li>
                            </ul>
                            <ul id="tabs_1" class="uk-switcher uk-margin">
							<!-- informasi kelas -->
                                <li>
									<?=$class['keterangan']?>
									<div class="uk-grid uk-margin-medium-top uk-margin-large-bottom" data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Contact Info</h4>
                                            <ul class="md-list md-list-addon">
												  <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><span class="md-list-heading">Lokasi Class</span>
                                                        <span class="uk-text-small uk-text-muted"><?=$class['lokasi']?></span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><span class="md-list-heading"><?php //$peg1['email']?></span>
                                                        <span class="uk-text-small uk-text-muted">Email</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading"><?php //$peg1['telp']?></span>
                                                        <span class="uk-text-small uk-text-muted">Phone</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-facebook-official"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">facebook.com</span>
                                                        <span class="uk-text-small uk-text-muted">Facebook</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon uk-icon-twitter"></i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">twitter.com</span>
                                                        <span class="uk-text-small uk-text-muted">Twitter</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Preparation Class</h4>
                                             <ul class="md-list md-list-addon">
												<li>
													<div class="md-list-addon-element">
														<input type="checkbox" data-md-icheck checked />
													</div>
													<div class="md-list-content">
														<span class="md-list-heading">Koneksi Internet 100Mbps</span>
														<span class="uk-text-small uk-text-muted">Cum fugiat qui consequatur optio sit sequi amet non.</span>
													</div>
												</li>
												<li>
													<div class="md-list-addon-element">
														<input type="checkbox" data-md-icheck checked />
													</div>
													<div class="md-list-content">
														<span class="md-list-heading">Adobe Flash Player</span>
														<span class="uk-text-small uk-text-muted">Quia magni adipisci odio dolorum explicabo dolorem ut.</span>
													</div>
												</li>
												<li>
													<div class="md-list-addon-element">
													   <input type="checkbox" data-md-icheck checked />
													</div>
													<div class="md-list-content">
														<span class="md-list-heading">Akun Gmail</span>
														<span class="uk-text-small uk-text-muted">Eaque est quis soluta suscipit et sed atque laborum.</span>
													</div>
												</li>
												 <li>
													<div class="md-list-addon-element">
													   <input type="checkbox" data-md-icheck checked />
													</div>
													<div class="md-list-content">
														<span class="md-list-heading">Akun Speedshare</span>
														<span class="uk-text-small uk-text-muted">Eaque est quis soluta suscipit et sed atque laborum.</span>
													</div>
												</li>
											</ul>
                                        </div>
                                    </div>
								</li>
							<!-- end informasi kelas -->
							
							<!--periode-->
								<li>
									<div id="data_periode"></div>	
								</li>

							<!--end periode-->
							
							<!-- materi -->
                                <li>
									<div id="data_materi"></div>	
								</li>
							<!-- end materi -->	
			
							<!--video-->
                                <li>
									<div id="data_video"></div>								
								</li>
							<!--end video-->	
							
							<!-- file-->
                                <li>
									<div id="data_file"></div>
								</li>
							<!--end file-->	
			
							<!-- pg-->
                                <li>
									<div id="data_peserta"></div>
								</li>
							<!--end pg-->							
								
                            </ul>
                        </div>
						
                    </div>
                </div>
				<!--
                <div class="uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-margin-medium-bottom">
                            <h3 class="heading_c uk-margin-bottom">Peserta</h3>
                            <ul class="md-list md-list-addon uk-margin-bottom">
							<?php //foreach ($friend->result() as $fr){ ?>
                                <li>
                                    <div class="md-list-addon-element">
                                        <img class="md-user-image md-list-addon-avatar" src="<?php //$fr->photo?>" alt=""/>
                                    </div>
                                    <div class="md-list-content">
                                        <span class="md-list-heading"><?php //$fr->nama?></span>
                                        <span class="uk-text-small uk-text-muted"><?php //$fr->email?></span>
                                    </div>
                                </li>
							<?php //}?>	
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
    </div>  
	<!-- common functions -->
    <script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?=base_url()?>front_assets/assets/js/uikit_custom.min.js"></script>
	<script>
		var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="40" height="40"/></div>';
		$(document).ready(function() {
			//$('user_contents').uk-();
			//$('#video').addClass("uk-active");
			$('#loading').html(loading);
			$('#loading').fadeTo(50, 50).slideUp(50, function(){
				
				//reload_periode();
				//reload_materi();
				//reload_video();
				//reload_file();
				//reload_pg();
				//reload_essay();
				
				$('#waiting').hide();
				$('#loading').hide();
				$('#user_contents').show();
			});
		});
		
		function reload_page(tab){
			if(tab=='periode'){
				reload_periode()
			}else if(tab=='materi'){
				reload_materi();
			}else if(tab=='video'){
				reload_video();
			}else if(tab=='file'){
				reload_file();
			}else if(tab=='peserta'){
				reload_peserta();
			}
		}
		
		function reload_periode(){
			$('#data_periode').html(loading);
			$.post("<?php echo base_url().'class_p/list_periode'?>",{id_class:'<?=$class['id_class']?>'},function(data){
				$('#data_periode').html(data)
			});
		}

		function reload_materi(){
			$('#data_materi').html(loading);
			$.post("<?php echo base_url().'class_p/list_materi'?>",{id_class:'<?=$class['id_class']?>'},function(data){
				$('#data_materi').html(data)
			});
		}	
		
		function reload_video(){
			$('#data_video').html(loading);
			$.post("<?php echo base_url().'class_p/list_video'?>",{id_class:'<?=$class['id_class']?>'},function(data){
				$('#data_video').html(data)
			});
		}
		
		function reload_file(){
			$('#data_file').html(loading);
			$.post("<?php echo base_url().'class_p/list_file'?>",{id_class:'<?=$class['id_class']?>'},function(data){
				$('#data_file').html(data)
			});
		}
		
		function reload_peserta(){
			$('#data_peserta').html(loading);
			$.post("<?php echo base_url().'class_p/list_peserta'?>",{id_class:'<?=$class['id_class']?>'},function(data){
				$('#data_peserta').html(data)
			});
		}
		
	</script>
<!---CONTENT END-->