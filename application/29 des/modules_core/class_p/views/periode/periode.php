<div id="delete_message"></div>
<table class="uk-table uk-table-hover">
	<thead>
		 <tr>
			<th rowspan="2" width="150px">Periode</th>
			<th colspan="6"><b>Master Periode</b></th>
		</tr>
		<tr>
			<th>Instruktur 1</th>
			<th>Instruktur 1</th>
			<th width="">Start Date</th>
			<th width="">End date</th>
			<th width="">Status</th>
		</tr>
	</thead>
	<tfoot>
	<?php 
	if($periode){
	foreach($periode->result() as $per){
		$peg1 = $this->pegawai_model->get_pegawai_by_uid($per->instruktur1);
		$peg2 = $this->pegawai_model->get_pegawai_by_uid($per->instruktur2);
		?>	
			<tr>
				<td><?=$per->periode?></td>
				<td><?=$peg1['nama']?></td>
				<td><?=$peg2['nama']?></td>
				<td><?=$per->start_date?></td>
				<td><?=$per->end_date?></td>
					
				<td>
					<?php if($per->status=='inprogress'){?>
							<span class="uk-badge uk-badge-warning"><?=$per->status?></span>
					<?php }else if($per->status=='schedule'){?>
							<span class="uk-badge uk-badge-primary"><?=$per->status?></span>
					<?php }else{?>
							<span class="uk-badge uk-badge-success"><?=$per->status?></span>
					<?php } ?>
				</td>	
			</tr>	
	<?php }
	}?>		
	</tfoot>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_periode" onclick="add_peiode('<?=$id_class?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_periode">
			<div id="modal-add-prd"></div>
		</div>
		<div class="uk-modal" id="edit_periode">
			<div id="modal-edit-prd"></div>
		</div>
	</div> 
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				/*
				$('#dt_default').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				*/
				
			});	
			
			function edit_peiode(id){
				uri = '<?=base_url()."class_p/edit_periode"?>';
				$('#new_periode').hide();
				$('#edit_periode').show();
				$('#modal-edit-prd').html(loading);
				$.post(uri,{ajax:true,id:id},function(data) {
					$('#modal-edit-prd').html(data);
				});
			}
			
			function add_peiode(id_class){
				uri = '<?=base_url()."class_p/add_periode"?>';
				$('#edit_periode').hide();
				$('#new_periode').show();
				$('#modal-add-prd').html(loading);
				$.post(uri,{ajax:true,id_class:id_class},function(data) {
					$('#modal-add-prd').html(data);
				});
			}
			
			function delete_peiode(id){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."class_p/delete_periode"?>';
					$.post(uri,{ajax:true,id_periode:id},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message").html(msg);
							
							$('#delete_message').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message').hide();
								
								reload_periode();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message").html(msg);
						}
					});
				} 
			}
			
	</script>