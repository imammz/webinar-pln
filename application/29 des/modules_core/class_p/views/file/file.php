<div id="delete_message_file"></div>
<table id="dt_default_file" class="uk-table" cellspacing="0" width="100%">
	<thead>
		 <tr>
			<th rowspan="2" width="">file</th>
			<th colspan="2"><b>Master file</b></th>
		</tr>
		<tr>
			<th width="">Nama File</th>
			<th width="">Periode</th>
			<th width="100px">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($file){
	foreach($file->result() as $mat){?>	
			<tr>
				<td><?=$mat->document?></td>
				<td><?=$mat->file_name?></td>
				<td><?=$mat->periode?></td>
				<td>
					
					<a href="<?=base_url().'class_p/download/'.$mat->id_document?>" class="uk-icon-button uk-icon-download" alt="Download"></a>
				</td>
			</tr>	
	<?php }
	}?>		
	</tbody>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_file" onclick="add_file('<?=$id_class?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_file">
		
				<div id="modal-add-fl"></div>
		</div>
		<div class="uk-modal" id="edit_file">
			<button class="uk-modal-close uk-close" type="button"></button>
			<div id="modal-edit-fl"></div>
		</div>
	</div> 
	
	<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>
	
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default_file').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_file(id_document_class){
				uri = '<?=base_url()."class_p/edit_file"?>';
				$('#modal-edit-fl').html(loading);
				$.post(uri,{ajax:true,id_document_class:id_document_class,id_class:'<?=$id_class?>'},function(data) {
					$('#modal-edit-fl').html(data);
				});
			}
			
			function add_file(id_class){
				$('#modal-add-fl').html(loading);
				uri = '<?=base_url()."class_p/add_file"?>';
				$.post(uri,{ajax:true,id_class:id_class},function(data) {
					$('#modal-add-fl').html(data);
				});
			}
			
			function delete_file(id_document_class){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."class_p/delete_file"?>';
					$.post(uri,{ajax:true,id_document_class:id_document_class},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message_file").html(msg);
							
							$('#delete_message_file').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message_file').hide();
								
								reload_file();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message_file").html(msg);
						}
					});
				} 
			}
			
	</script>