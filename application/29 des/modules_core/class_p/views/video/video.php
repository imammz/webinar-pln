<div id="delete_message_video"></div>
<table id="dt_default_video" class="uk-table" cellspacing="0" width="100%">
	<thead>
		 <tr>
			<th rowspan="2" width="">Video</th>
			<th colspan="2"><b>Master Video</b></th>
		</tr>
		<tr>
			<th width="">Nama File</th>
			<th width="">Periode</th>
			<th width="100px">Action</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	if($video){
	foreach($video->result() as $mat){?>	
			<tr>
				<td><?=$mat->document?></td>
				<td><?=$mat->file_name?></td>
				<td><?=$mat->periode?></td>
				<td>
					
					<a href="<?=base_url().'class_p/download/'.$mat->id_document?>" class="uk-icon-button uk-icon-download" alt="Download"></a>
				</td>
			</tr>	
	<?php }
	}?>		
	</tbody>
</table>

		<div class="md-fab-wrapper">
			<a class="md-fab md-fab-accent" href="#new_video" onclick="add_video('<?=$id_class?>')" data-uk-modal="{ center:true }">
				<i class="material-icons">&#xE145;</i>
			</a>
		</div>
		<div class="uk-modal" id="new_video">
		
				<div id="modal-add-vdo"></div>
		</div>
		<div class="uk-modal" id="edit_video">
			<button class="uk-modal-close uk-close" type="button"></button>
			<div id="modal-edit-vdo"></div>
		</div>
	</div> 
	
	<script src="<?=base_url()?>front_assets/assets/js/common.min.js"></script>
	<script src="<?=base_url()?>front_assets/assets/js/jquery.validate.min.js"></script>	
	<script src="<?=base_url()?>front_assets/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
		<!-- datatables custom integration -->
	<script src="<?=base_url()?>front_assets/assets/js/custom/datatables_uikit.min.js"></script>
	
		<script>
			var loading = '<div align="center" class="loading"><img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif" width="64"</div>'; 
			$( document ).ready(function() {
				
				$('#dt_default_video').dataTable({
					oLanguage: {
						sLoadingRecords: '<img src="<?=base_url()?>front_assets/assets/img/spinners/spinner_large.gif">'
					}					
				});
				
				
			});	
			
			function edit_video(id_document_class){
				uri = '<?=base_url()."class_p/edit_video"?>';
				$('#modal-edit-vdo').html(loading);
				$.post(uri,{ajax:true,id_document_class:id_document_class,id_class:'<?=$id_class?>'},function(data) {
					$('#modal-edit-vdo').html(data);
				});
			}
			
			function add_video(id_class){
				$('#modal-add-vdo').html(loading);
				uri = '<?=base_url()."class_p/add_video"?>';
				$.post(uri,{ajax:true,id_class:id_class},function(data) {
					$('#modal-add-vdo').html(data);
				});
			}
			
			function delete_video(id_document_class){
				var r = confirm("Yakin akan di hapus?");
				if (r == true) {
					uri = '<?=base_url()."class_p/delete_video"?>';
					$.post(uri,{ajax:true,id_document_class:id_document_class},function(data) {
						var data = eval('('+ data + ')');
						if(data.response == "success") {
							var msg='<div class="uk-alert uk-alert-success">'+data.msg+'</div>';
							$("#delete_message_video").html(msg);
							
							$('#delete_message_video').fadeTo(3000, 500).slideUp(500, function(){
								$('#delete_message_video').hide();
								
								reload_video();
							});
							 
						}else{
							var msg='<div class="uk-alert uk-alert-danger">'+data.msg+'</div>';
							$("#delete_message_video").html(msg);
						}
					});
				} 
			}
			
	</script>